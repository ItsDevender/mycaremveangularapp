
import { Injectable } from '@angular/core';
import { ApiService } from '@services/api.service';
import { Subject } from 'rxjs';
import { AppService } from '@services/app.service';


@Injectable({
  providedIn: 'root'
})
export class OrderService {

  Accesstoken = '';

  /**
   * Disabled modify of order service
   */
  disabledModify = 'disabled'
  // disabledDiscount = 'disabled'

  inputpart = '';
  Mainurl = '';
  APIstring = '';
  AddframesUrl = '';
  mainpart = '';
  Getframesfilter: string;
  orderScreenMultipleSavePath = '';
  ordersearchvalueService: any[];
  orderScreenSavePath = 'Frames';
  otherItemRows = [];    /*Collection of Item Rows from Other Details Page*/
  public savedata = new Subject<any>();
  savedataObj$ = this.savedata.asObservable();

  public orderFrame = new Subject<any>();
  orderFrameSave$ = this.orderFrame.asObservable();

  public orderLocation = new Subject<any>();
  orderLocationId$ = this.orderLocation.asObservable();

  public orderselection = new Subject<any>();
  orderselectionId$ = this.orderselection.asObservable();

  public orderFrameSave = new Subject<any>();
  orderFrameSaveEnable$ = this.orderFrameSave.asObservable();

  public orderFormUpdate = new Subject<any>();
  orderFormUpdate$ = this.orderFormUpdate.asObservable();

  public orderLensSelectionSave = new Subject<any>();
  orderLensSelectionSave$ = this.orderLensSelectionSave.asObservable();

  public orderSaveNew = new Subject<any>();
  orderSaveNew$ = this.orderSaveNew.asObservable();

  public lensPhysicianId = new Subject<any>();
  lensPhysicianId$ = this.lensPhysicianId.asObservable();

  public orderDateobj = new Subject<any>();
  orderDateobj$ = this.orderDateobj.asObservable();

  public orderSelectTypeobj = new Subject<any>();
  orderSelectTypeobj$ = this.orderSelectTypeobj.asObservable();

  public orderEncounterId = new Subject<any>();
  orderEncounterId$ = this.orderEncounterId.asObservable();

  public browsOrderFrameobj = new Subject<any>();
  browsOrderFrameobj$ = this.browsOrderFrameobj.asObservable();

  public orderLensTypeUPC = new Subject<any>();
  orderLensTypeUPC$ = this.orderLensTypeUPC.asObservable();

  public orderRowSeletedUPCOD = new Subject<any>();
  orderRowSeletedUPCOD$ = this.orderRowSeletedUPCOD.asObservable();

  public orderRowSeletedUPCOS = new Subject<any>();
  orderRowSeletedUPCOS$ = this.orderRowSeletedUPCOS.asObservable();

  public ShippingDetalesEnable = new Subject<any>();
  ShippingDetalesEnable$ = this.ShippingDetalesEnable.asObservable();

  public ShippingTo = new Subject<any>();
  ShippingTo$ = this.ShippingTo.asObservable();

  public orderspectacleLensSave = new Subject<any>();
  orderspectacleLensSave$ = this.orderspectacleLensSave.asObservable();

  /**
   * Only other items saving of order service other order saving
   */
  public onlyOtherItemsSaving = new Subject<any>();
  onlyOtherItemsSaving$ = this.onlyOtherItemsSaving.asObservable();

  public ODNAME = new Subject<any>();
  ODNAME$ = this.ODNAME.asObservable();

  public ODITEID = new Subject<any>();
  ODITEID$ = this.ODITEID.asObservable();

  public EqualNAME = new Subject<any>();
  EqualNAME$ = this.EqualNAME.asObservable();

  public EqualITEID = new Subject<any>();
  EqualITEID$ = this.EqualITEID.asObservable();

  public OSNAME = new Subject<any>();
  OSNAME$ = this.OSNAME.asObservable();

  public OSITEID = new Subject<any>();
  OSITEID$ = this.OSITEID.asObservable();

  public FrameNAME = new Subject<any>();
  FrameNAME$ = this.FrameNAME.asObservable();

  public FrameNAMEID = new Subject<any>();
  FrameNAMEID$ = this.FrameNAMEID.asObservable();

  public FramesOnly = new Subject<any>();
  FramesOnly$ = this.FramesOnly.asObservable();

  public lensSelectionPopUP = new Subject<any>();
  lensSelectionPopUP$ = this.lensSelectionPopUP.asObservable();

  public OrderAlertPopup = new Subject<any>();
  OrderAlertPopup$ = this.OrderAlertPopup.asObservable();

  public OrderSearchValue = new Subject<any>();
  OrderSearchValue$ = this.OrderSearchValue.asObservable();

  public OrderSearchFilterValue = new Subject<any>();
  OrderSearchFilterValue$ = this.OrderSearchFilterValue.asObservable();


  public OrderCancleValue = new Subject<any>();
  OrderCancleValue$ = this.OrderCancleValue.asObservable();

  // order Details
  public orderDetails = new Subject<any>();
  orderDetails$ = this.orderDetails.asObservable();
  // frames Price
  public framesPrice = new Subject<any>();
  framesPrice$ = this.framesPrice.asObservable();

  public frameNAME = new Subject<any>();
  frameNAME$ = this.FrameNAME.asObservable();

  public frameNAMEID = new Subject<any>();
  frameNAMEID$ = this.FrameNAMEID.asObservable();
  // lens selection OD
  public lensSelectionODPrice = new Subject<any>();
  lensSelectionODPrice$ = this.lensSelectionODPrice.asObservable();
  // lens selection OS
  public lensSelectionOSPrice = new Subject<any>();
  lensSelectionOSPrice$ = this.lensSelectionOSPrice.asObservable();

  public showorderdiv = new Subject<any>();
  showorderdiv$ = this.showorderdiv.asObservable();

  public framesOnly = new Subject<any>();
  framesOnly$ = this.FramesOnly.asObservable();


  // ordersearchvalueService:any[];
  lensSelectionPopCondition = false;
  lensPopupOrderCondi = true;
  paymentOrderNavigation = false;
  orderSaveButton = true;
  orderSavePaymentSavingStatus = true;
  orderSaveUpdateStatus = true;
  orderFormChangeStatus = true;
  // orderSpectacleLensODisvalid = false;
  // orderSpectacleLensOSisvalid = false;

  /**
   * Order spectacle lens valid of order service
   */
  order_SpectacleLens_Valid = false;
  /**
   * Lens selection upcstatus of order service
   */
  lensSelectionUPCStatus = '';


  // contact lens declarations
  public ordercontactlensselection = new Subject<any>();
  ordercontactlensselection$ = this.ordercontactlensselection.asObservable();

  public orderContactLensById = new Subject<any>();
  orderContactLensById$ = this.orderContactLensById.asObservable();

  public orderSoftContactLens = new Subject<any>();
  orderSoftContactLens$ = this.orderSoftContactLens.asObservable();

  public orderContactlenssoft = new Subject<any>();
  orderContactlenssoft$ = this.orderContactlenssoft.asObservable();


  public ContactLensPopupAlertPopup = new Subject<any>();
  ContactLensPopupAlertPopup$ = this.ContactLensPopupAlertPopup.asObservable();

  public orderPopupAlertPopup = new Subject<any>();
  orderPopupAlertPopup$ = this.orderPopupAlertPopup.asObservable();

  ofterSalesView: boolean;
  othersView: boolean;
  orderId: string = '';
  allowNegativeInventory: any;
  orderOtherDetails: any;
  orderTypeId: any;
  orderLabValid: boolean = false;

  /**
   * Order status global of order service
   */
  orderStatusGlobal = '9';

  disableSavePayBtn: boolean;
  // Specifies the typr of spectacle lens selected
  spectaclesLensType: string;
  /**
   * Ordersource  of order service for binding Source data
   */
  ordersource: any = [];

  /**
   * Total amount discount of order service total discount
   */
  totalAmountDiscount: any;

  /**
   * Discount object of order service is keeping the discount values in save frame object
   */
  discountObject: any = '';

  /**
   * Saveorderform data of order service for creating the discount object with order save object 
   */
  saveorderformData: any = [];

  /**
   * Discount charge list of order service chargelines data
   */
  discountChargeList: any = [];

  /**
   * Disable payment button of order service payments buttons
   */
  disablePaymentButton = true;

  /**
   * Golobal physician Selection will be stored
   */
  physicianID: any;

  /**
   * Frame item id of order service
   */
  frameItemId: any = "";

  /**
   * Spectaclelens od of order service
   */
  spectaclelensOD: any = "";

  /**
   * Spectaclelens os of order service
   */
  spectaclelensOS: any = "";

  /**
   * Soft contactlens od of order service
   */
  softContactlensOd: any = "";

  /**
   * Soft contactlens os of order service
   */
  softContactlensOS: any = "";

  /**
   * Hard contact lens odvalue of order service
   */
  hardContactLensODvalue: any = "";

  /**
   * Hard contact lens osvalue of order service
   */
  hardContactLensOSvalue: any = "";

  /**
   * Pdsaving validation of order service
   */
  PDsavingValidation: boolean = false;

  /**
   * Sphere validation of order service
   */
  SphereValidation: boolean = false;

  mandatoryFields_Validation: any;

  /**
   * Eye status of order service
   */
  eye_Status: any = '';

  /**
   * Order other selected of order service
   */
  orderOtherSelected: boolean = false;

  /**
   * Show category of order service here this variable is for loading the category dropdown in others screen 
   */
  showCategory: boolean;

  /**
   * Discount reason of order service for using as cache storage
   */
  discountReason: Array<object>;
  constructor(private api: ApiService, private app: AppService
  ) {
    this.discountReason = [];
  }


  /**
   * Updates order frames
   * @param OrderFrameData order frame Payload Data
   * @param orderId  for order id
   * @param ItemId  for item id
   * @returns  {object} updating frame
   */
  updateOrderFrames(OrderFrameData: any[], orderId, ItemId) {
    this.mainpart = '/order/' + orderId + '/item/' + ItemId;
    // const bodyString = JSON.stringify(OrderFrameData);
    return this.api.putApiMicro(this.mainpart, OrderFrameData);

  }
  saveOrderLensSelection(OrderOtherlens: any) {
    // alert("lensorder")
    this.saveorderformData = [];
    const orderId = this.orderId;
    this.mainpart = '/order/' + orderId + '/item';

    if (this.discountObject !== '') {
      this.saveorderformData.push(OrderOtherlens);
      for (let j = 0; j <= this.saveorderformData.length - 1; j++) {
        // this.saveorderformData[j]['discount_type'] = this.discountObject.discountType === 'Amount' ? '1' : '2';
        this.saveorderformData[j]['discount_rate'] = this.discountObject.discountNumber;
        this.saveorderformData[j]['discount_reasonid'] = this.discountObject.reason;
        this.saveorderformData[j]['total_overall_discount'] = this.totalAmountDiscount;
        this.saveorderformData[j]['overall_discount_code'] = this.discountObject.discountType === 'Amount' ? 1 : 2;
        this.saveorderformData[j]['overall_discount'] = this.discountObject.discountNumber;
        this.saveorderformData[j]['discount_reason_id'] = this.discountObject.reason;
        const LensItemId = this.saveorderformData[j]['item_id']
        for (let k = 0; k <= this.discountChargeList.length - 1; k++) {
          if (LensItemId === this.discountChargeList[k]['item_id']) {
            const discAmount = this.discountChargeList[k]['discountAmount']
            this.saveorderformData[j]['discount'] = discAmount.toFixed(2);
          }

        }
      }
      return this.api.postApiMicro(this.mainpart, this.saveorderformData[0]);
    } else {
      return this.api.postApiMicro(this.mainpart, OrderOtherlens);
    }
    // const bodyString = JSON.stringify(OrderOtherlens);


  }
  updateOrderLensSelection(OrderOtherlens: any, ItemId) {
    // alert("lens")
    OrderOtherlens['order_status_id'] = this.orderStatusGlobal;
    const orderId = this.orderId;
    this.mainpart = '/order/' + orderId + '/item/' + ItemId;

    // const bodyString = JSON.stringify(OrderOtherlens);
    return this.api.putApiMicro(this.mainpart, OrderOtherlens);

  }
  saveOrderOtherDetails(OrderOtherDetailsData: any) {
    this.saveorderformData = [];
    // alert("otherOrderDetails")
    const orderId = this.orderId;
    if (this.orderScreenMultipleSavePath == 'others' && this.orderId == '') {
      this.mainpart = '/order/item';
    } else {
      this.mainpart = '/order/' + orderId + '/item';
    }

    if (this.discountObject !== '') {
      this.saveorderformData.push(OrderOtherDetailsData);
      for (let j = 0; j <= this.saveorderformData.length - 1; j++) {
        // this.saveorderformData[j]['discount_type'] = this.discountObject.discountType === 'Amount' ? 'a' : 'p';
        this.saveorderformData[j]['discount_rate'] = this.discountObject.discountNumber;
        this.saveorderformData[j]['discount_reasonid'] = this.discountObject.reason;
        this.saveorderformData[j]['total_overall_discount'] = this.totalAmountDiscount;
        this.saveorderformData[j]['overall_discount_code'] = this.discountObject.discountType === 'Amount' ? 1 : 2;
        this.saveorderformData[j]['overall_discount'] = this.discountObject.discountNumber;
        this.saveorderformData[j]['discount_reason_id'] = this.discountObject.reason;
        const otherItemId = this.saveorderformData[j]['item_id']
        for (let k = 0; k <= this.discountChargeList.length - 1; k++) {
          if (otherItemId === this.discountChargeList[k]['item_id']) {
            const discAmount = this.discountChargeList[k]['discountAmount']
            this.saveorderformData[j]['discount'] = discAmount.toFixed(2);
          }

        }
      }
      return this.api.postApiMicro(this.mainpart, this.saveorderformData[0]);
    } else {
      return this.api.postApiMicro(this.mainpart, OrderOtherDetailsData);
    }  // const bodyString = JSON.stringify(OrderOtherDetailsData);


  }
  updateOrderOtherDetails(OrderUpdateOtherDetailsData: any, orderId, id) {
    this.mainpart = '/order/' + orderId + '/item/' + id;
    // const bodyString = JSON.stringify(OrderUpdateOtherDetailsData);
    return this.api.putApiMicro(this.mainpart, OrderUpdateOtherDetailsData);

  }
  updateLaborderotherdetails(OrderLabDetailsData: any) {
    const orderId = this.orderId;
    this.mainpart = '/order/' + orderId + '/otherDetails';
    // const bodyString = JSON.stringify(OrderLabDetailsData);
    return this.api.putApiMicro(this.mainpart, OrderLabDetailsData);

  }


  /**
   * Saves order frames
   * @param {object} OrderFrameData 
   * @returns data for the subscription  
   */
  saveOrderFrames(OrderFrameData) {
    // alert("Frames")
    OrderFrameData['order_status_id'] = this.orderStatusGlobal;
    // console.log(OrderFrameData);
    this.saveorderformData = []
    if (this.discountObject !== '') {
      this.saveorderformData.push(OrderFrameData);
      for (let j = 0; j <= this.saveorderformData.length - 1; j++) {
        // this.saveorderformData[j]['discount_type'] = this.discountObject.discountType === 'Amount' ? 'a' : 'p';
        this.saveorderformData[j]['discount_rate'] = this.discountObject.discountNumber;
        this.saveorderformData[j]['discount_reasonid'] = this.discountObject.reason;
        this.saveorderformData[j]['total_overall_discount'] = this.totalAmountDiscount;
        this.saveorderformData[j]['overall_discount_code'] = this.discountObject.discountType === 'Amount' ? 1 : 2;
        this.saveorderformData[j]['overall_discount'] = this.discountObject.discountNumber;
        this.saveorderformData[j]['discount_reason_id'] = this.discountObject.reason;
        const FrameItemId = this.saveorderformData[j]['item_id']
        for (let k = 0; k <= this.discountChargeList.length - 1; k++) {
          if (FrameItemId === this.discountChargeList[k]['item_id']) {
            const discAmount = this.discountChargeList[k]['discountAmount']
            this.saveorderformData[j]['discount'] = discAmount.toFixed(2);
          }

        }
      }
      // this.discountObject = '';
      this.mainpart = '/order/item';
      // const bodyString = JSON.stringify(OrderFrameData);
      return this.api.postApiMicro(this.mainpart, this.saveorderformData[0]);

    } else {
      this.mainpart = '/order/item';
      // const bodyString = JSON.stringify(OrderFrameData);
      return this.api.postApiMicro(this.mainpart, OrderFrameData);
    }
  }

  saveSpecticalRX(orderLensForm: any[]) {

    const orderIdValue = 'order_id';
    orderLensForm[orderIdValue] = this.orderId;
    const patientId = this.app.patientId;
    // localStorage.getItem('PatientId');
    this.mainpart = '/patient/' + patientId + '/rx';
    // alert("LensSelection")
    // if (this.discountObject !== '') {
    //   this.saveorderformData.push(orderLensForm);
    //   for (let j = 0; j <= this.saveorderformData.length - 1; j++) {
    //     this.saveorderformData[j]['total_overall_discount'] = this.totalAmountDiscount;
    //     this.saveorderformData[j]['overall_discount_code'] = this.discountObject.discountType === 'Amount' ? 1 : 2;
    //     this.saveorderformData[j]['overall_discount'] = this.discountObject.discountNumber;
    //     this.saveorderformData[j]['discount_reason_id'] = this.discountObject.reason;
    //     const LensItemId = this.saveorderformData[j]['item_id']
    //     for (let k = 0; k <= this.discountChargeList.length - 1; k++) {
    //       if (LensItemId === this.discountChargeList[k]['item_id']) {
    //         const discAmount = this.discountChargeList[k]['discountAmount']
    //         this.saveorderformData[j]['discount'] = discAmount.toFixed(2);
    //       }

    //     }
    //   }
    //   return this.api.postApiMicro(this.mainpart, this.saveorderformData[0]);
    // } else {
    return this.api.postApiMicro(this.mainpart, orderLensForm);
    // }
    // this.AddframesUrl = this.Mainurl + 'IMWAPI/optical/addModifySpectacleRx';
    // const httpOptions = {
    //   headers: new HttpHeaders({
    //     'Content-Type': 'application/json'
    //   })
    // };
    // const bodyString = JSON.stringify(orderLensForm);


  }
  updateSpecticalRX(orderLensForm: any[], rxId) {
    const patientId = this.app.patientId;
    // localStorage.getItem('PatientId');
    this.mainpart = '/patient/' + patientId + '/rx/' + rxId;
    // const bodyString = JSON.stringify(orderLensForm);
    return this.api.putApiMicro(this.mainpart, orderLensForm);

  }
  GetotherlabDetails(orderId) {

    this.mainpart = '/order/' + orderId + '/otherDetails';
    return this.api.getApiMicro(this.mainpart);

  }

  getHeardAbout() {
    this.mainpart = '/heardAbout';
    return this.api.getApiMicro(this.mainpart);
  }
  getAccountStatus() {
    this.mainpart = '/PatientAccountStatuses';
    return this.api.getApiMicro(this.mainpart);
  }
  updatePatientStatus(body) {
    const patient = this.app.patientId;
    // localStorage.getItem('PatientId');
    this.mainpart = '/patient/' + patient;
    const bodyString = JSON.stringify(body);
    return this.api.putApiMicro(this.mainpart, bodyString);

  }

  getAccountStatusAll(patient) {
    this.mainpart = '/patient/' + patient;
    return this.api.getApiMicro(this.mainpart);
  }

  getSourceMaster(id: '') {
    this.mainpart = '/source';

    return this.api.getApiMicro(this.mainpart);
  }
  getOrderStatusData(id) {

    this.mainpart = '/order/' + id;
    return this.api.getApiMicro(this.mainpart);


  }



  getLensSelectionBasedOnSelectedRx(id, Rxid) {
    this.mainpart = '/patient/' + id + '/rx/' + Rxid;
    return this.api.getApiMicro(this.mainpart);
  }
  getPhysician(id: '') {
    this.mainpart = '/physician';
    return this.api.getApiMicro(this.mainpart);
  }

  getPhysicianFilter(id: '', value) {
    // const bodyString = JSON.stringify(value);
    this.mainpart = '/users/filter';
    return this.api.postApiMicro(this.mainpart, value);
  }



  getPatientImage(id) {
    this.mainpart = '/patient/' + id + '/image';
    return this.api.getApiMicro(this.mainpart);
  }

  getPatientDue(id) {
    this.mainpart = '/charge/patient/' + id + '/sum/patientDue';
    return this.api.getApiMicro(this.mainpart);
  }



  // getOrderSearchMaster
  getOrderSearchMaster(
    Id = '',
    PatientId = '',
    OperatorId = '',
    LocationId = '',
    FromDate = '',
    ToDate = '', includeInactive = '', maxRecord = 1000,
    offset = '', sortBy = 'Id',
    sortOrder = 'desc', upccode = '',
    itemname = '', itemtype = '',
    orderStatus = '', familydetails = '') {
    this.mainpart = 'IMWAPI/optical/getOrderSearch';
    this.inputpart = '&Id=' + Id + '&PatientId=' + PatientId + '&OperatorId=' + OperatorId + '&LocationId=' +
      LocationId + '&FromDate=' + FromDate + '&ToDate=' + ToDate;
    this.inputpart += '&ToDate=' + ToDate + '&includeInactive=' + includeInactive + '&maxRecord=' + maxRecord + '&offset=' +
      offset + '&sortBy=' + sortBy + '&sortOrder=' + sortOrder;
    this.inputpart += '&UpcCode=' + upccode + '&ItemName=' + itemname + '&' + familydetails + '&ItemType=' + itemtype +
      '&OrderStatus=' + orderStatus + '';
    this.APIstring = this.Mainurl + 'IMWAPI/optical/getOrderSearch';
    return this.api.GetApi(this.mainpart, this.inputpart);

  }

  // gets the orders with the particular order Id.
  getOrderItemDetails(orderId = '') {
    this.mainpart = '/order/' + this.orderId + '/item';
    return this.api.getApiMicro(this.mainpart);
  }

  /**
   * Gets order item detailsof item
   * @param [orderId] 
   * @returns  
   */
  getOrderItemDetailsofItem(itemId) {
    this.mainpart = '/order/' + this.orderId + '/item/' + itemId;
    return this.api.getApiMicro(this.mainpart);
  }



  // gets the orders with order Id
  getthechargevaluesfororder(orderdetails: any) {
    this.mainpart = '/charge/filter';
    // const bodyString = JSON.stringify(orderdetails);
    return this.api.postApiMicro(this.mainpart, orderdetails);

  }


  // gets the orders with the particular order Id.
  getLensTreatmentPrice(itemid: any) {
    this.mainpart = '/item/' + itemid + '/charge/';
    return this.api.getApiMicro(this.mainpart);
  }
  DeactivateInventory(inventory: any) {

    this.mainpart = '/item/deactivate';
    const bodyString = JSON.stringify(inventory);
    return this.api.putApiMicro(this.mainpart, bodyString);

  }

  ActivateInventory(inventory: any) {

    this.mainpart = '/item/activate';
    const bodyString = JSON.stringify(inventory);
    return this.api.putApiMicro(this.mainpart, bodyString);

  }

  sliceNum(num: number, size: number): string {
    let s = num + '';
    while (s.length < size) { s = '0' + s; }
    return s;
  }


  public genDropDowns(configs: any = []): void {

    const s = configs.start; // Start Point
    const e = configs.end; // End Point
    const nt = configs.neutral; // Neutral Point
    const d = configs.step; // Difference
    const datagen: any = [];
    let i = 0;
    for (i = s; i <= e; i += d) {
      // Fractions Add
      const f = (configs.roundOff > 0) ? i.toLocaleString('en', { useGrouping: false, minimumFractionDigits: configs.roundOff }) : i;

      // Set Slice
      const j = (configs.slice > 0) ? this.sliceNum(Number(f), configs.slice) : f;

      const va = (i > 0 && configs.sign) ? '+' + j : ((i === 0) ? nt : j);
      datagen.push({ id: va, name: va });
    }

    return datagen;
  }


  // gets the Contact Lens with the patient Id.
  getLensContactLens(patientId) {
    this.mainpart = '/patient/' + patientId + '/cl_rx/';
    return this.api.getApiMicro(this.mainpart);
  }

  // gets the Contact Lens with the patient Id.
  getPatientrxContactLens(value, patientId) {
    // const bodyString = JSON.stringify(value);
    this.mainpart = '/patient/' + patientId + '/cl_rx/filter';
    return this.api.postApiMicro(this.mainpart, value);
  }



  // save the Contact Lens with the patient Id.
  SaveContactLens(patientId, value: any) {

    // const bodyString = JSON.stringify(value);
    this.mainpart = '/patient/' + patientId + '/cl_rx';
    return this.api.postApiMicro(this.mainpart, value);
  }
  // update the Contact Lens with the patient Id.
  UpdateContactLens(patientId, value: any, id: any) {
    // const bodyString = JSON.stringify(value);
    this.mainpart = '/patient/' + patientId + '/cl_rx/' + id;
    return this.api.putApiMicro(this.mainpart, value);
  }


  // new calls
  saveorderdetailswithorderId(values: any, orderId) {

    this.mainpart = '/order/' + orderId + '/item';
    // const bodyString = JSON.stringify(values);
    return this.api.postApiMicro(this.mainpart, values);
  }


  savepatientlensdetails(values, patientid) {
    this.mainpart = '/patient/' + patientid + '/cl_rx';
    // const bodyString = JSON.stringify(values);
    return this.api.postApiMicro(this.mainpart, values);
  }
  updatepatientlensdetails(value, patientid, itemid) {
    this.mainpart = '/patient/' + patientid + '/cl_rx/' + itemid;
    // const bodyString = JSON.stringify(value);
    return this.api.putApiMicro(this.mainpart, value);
  }
  returnDisplayQuantity(values) {
    this.mainpart = '/order/' + this.orderId + '/return';
    // const bodyString = JSON.stringify(values);
    return this.api.putApiMicro(this.mainpart, values);
  }

  Getlaborderstatusdata() {
    const mainpart = '/orderStatus';
    return this.api.getApiMicro(mainpart);
  }

  getLensTreatmentswithcategory(Id: any, Name: any, upc: any, categoryId: any, maxRecord: any, offset: any, delstatus: any) {
    // this.Mainurl = this._AppSettings.ImedicUrl;
    // // this.Accesstoken = this._StateInstanceService.accessTokenGlobal;
    // this.APIstring = this.Mainurl + 'IMWAPI/optical/getLensTreatments?accessToken=' + this.Accesstoken + '&Id=' + Id + '&Name=' +
    //   Name + '&upc=' + upc + '&categoryId=' + categoryId;
    // this.APIstring += '&maxRecord=' + maxRecord + '&offset=' + offset + '&del_status=' + del_status + '   ';
    // return this.http.get<any[]>(this.APIstring)
    //   .do(data => data);

    const mainpart = 'IMWAPI/optical/getLensTreatments';
    const inputPart = '&Id=' + Id + '&Name=' + Name + '&upc=' + upc + '&categoryId=' + categoryId + '&maxRecord=' + maxRecord +
      '&offset=' + offset + '&del_status=' + delstatus + '   ';
    return this.api.GetApi(mainpart, inputPart);
  }
  getUsage() {
    this.mainpart = '/frame/usage';
    return this.api.getApiMicro(this.mainpart);
  }
  /**
   * Saves other details
   * @returns  modify the tax details in order
   */
  saveOtherDetails(orderid, orderdetailsid, values) {
    // orderdetailsid['order_status_id']= this.orderStatusGlobal
    // values['order_status_id'] = this.orderStatusGlobal;
    this.mainpart = '/order/' + orderid + '/item/' + orderdetailsid;
    return this.api.putApiMicro(this.mainpart, values);
  }

  /**
   * Gets discount reasons
   * @returns  data for subscription
   */
  getDiscountReasons() {
    this.mainpart = '/discountReason';
    return this.api.getApiMicro(this.mainpart);
  }

  /**
   * Posts discount reason
   * @param {object} discountReason 
   * @returns  data for subscription
   */
  postDiscountReason(discountReason) {
    this.mainpart = '/discountReason';
    return this.api.postApiMicro(this.mainpart, discountReason);
  }


  /**
   * Deletediscountreasons order service
   * @param {string} reasonID 
   * @returns  data for subscription
   */
  deletediscountreason(reasonID) {
    this.mainpart = '/discountReason/'.concat(reasonID).concat('/delete');
    return this.api.deleteApiMicro(this.mainpart);
  }

  /**
   * Gets list discounttype
   * @returns  data for subscription
   */
  getListDiscounttype() {
    this.mainpart = '/discountCode';
    return this.api.getApiMicro(this.mainpart);
  }

  /**
   * Items adding tothe paymentpayments
   * @param {object} paymentsData 
   * @returns   data with the encounter id
   */
  ItemsAddingTothePaymentpayments(paymentsData) {
    this.mainpart = '/order/processPayment';
    return this.api.putApiMicro(this.mainpart, paymentsData);
  }

  /**
   * Labs integration upload
   * @returns  {string} for getting referenceId of lab inegrations
   */
  labIntegrationUpload(Payload) {
    return this.api.postApiMicro('/Integrations', Payload, 2);
  }
  GetlabFTpconnection(supplierid) {
    this.Mainurl = '/vendor/' + supplierid;
    return this.api.getApiMicro(this.Mainurl);
    // .map((res: Response) => res.json());
  }

  /**
   * Gets upc items grid data
   * @param {object} payLoad 
   * @returns data for the subscription  
   */
  getUpcItemsGridData(payLoad: object) {
    this.mainpart = '/item/filter';
    return this.api.postApiMicro(this.mainpart, payLoad);
  }


  /**
   * Gets frame data by pagination
   * Pagination api
   */
  getDataByPagination(payload, page: number) {
    this.mainpart =  '/item/advanceFilter';
    if (page !== 0) {
      this.mainpart += '?page=' + page;
    }
    return this.api.postApiMicro(this.mainpart, payload);
}
}
