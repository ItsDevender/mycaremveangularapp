import { environment as env } from '@env/environment';
import { HttpClient} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { JwtService } from './jwt.service';


@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(
    private http: HttpClient,
    private jwtService: JwtService
  ) { }

  // Get API Old Style
  GetApi(mainPart: string, inputPart: string = '') {
    const token = this.jwtService.getPHPToken();
    const mainUrl = `${env.imedicUrl}`;
    const url = mainUrl + mainPart + '?accessToken=' + token + inputPart;
    return this.http.get(url);
  }


  postApi(mainPart: string, data: object = {}) {
    const mainUrl = `${env.imedicUrl}`;
    const token = this.jwtService.getPHPToken();
    const url = mainUrl + mainPart;
    const tokenkey = 'accessToken';
    data[tokenkey] = token;
    return this.http.post(url, data);
  }

  /**
   * Posts api micro
   * @param path suffix path to attach with endpoint
   * @param {object} body Payload to be sent in API request
   * @param {number} gway check which endpoint to be used
   * @returns  subscription object
   */
  postApiMicro(path: string, body: object = {}, gway: number = 1) {
    const endPoint = (gway === 1) ? env.apiEndPoint : env.apiEndPoint2;
    return this.http.post(
      `${endPoint}${path}`,
      JSON.stringify(body)
    );
  }

  // Uploading File /items
  postApiMicroUpload(path: any, params: any) {
    return this.http.post(
      `${env.apiEndPoint}${path}`,
      params);
  }

  // Put API Micro
  putApiMicro(path: any, params: any, gway: number = 1) {
    const endPoint = (gway === 1) ? env.apiEndPoint : env.apiEndPoint2;
    return this.http.put(
      `${endPoint}${path}`
      , params);
  }

  // Get Micro API
  getApiMicro(path: any) {
    return this.http.get(
      `${env.apiEndPoint}${path}`);
  }

// Images Post api Micro
  postImageApi(mainPart: string, data: any) {
    const mainUrl = `${env.imedicUrl}`;
    const token = this.jwtService.getPHPToken();
    const url = mainUrl + mainPart;
    const tokenkey = 'accessToken';
    data.append('accessToken', token);
    return this.http.post(url, data);
  }

  /**
   * Deletes api micro
   */
  deleteApiMicro(path: any, par: object = {}) {
    const options = { headers: {}, body: par,
    };
    return this.http.delete(`${env.apiEndPoint}${path}`, options);
    }
}
