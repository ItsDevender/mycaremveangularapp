import { Injectable } from '@angular/core';
import { ApiService } from '@services/api.service';

@Injectable({
  providedIn: 'root'
})
export class OrderHistoryService {

  Mainurl = '';
  mainpart = '';
  inputpart = '';
  APIstring = '';
  orderType: object;
  /**
   * Creates an instance of order history service.
   * @param api for run the get post put delete
   */
  constructor(private api: ApiService) { }

  /**
   * Gets order search master
   * @returns  Getting the order search master data
   */
  getOrderSearchMaster(
    Id = '',
    PatientId = '',
    OperatorId = '',
    LocationId = '',
    FromDate = '',
    ToDate = '', includeInactive = '', maxRecord = 1000,
    offset = '', sortBy = '',
    sortOrder = '', upccode = '',
    itemname = '', itemtype = '',
    orderStatus = '', familydetails = '') {
    this.mainpart = 'IMWAPI/optical/getOrderSearch';
    this.inputpart = '&Id=' + Id + '&PatientId=' + PatientId + '&OperatorId=' + OperatorId + '&LocationId=' +
      LocationId + '&FromDate=' + FromDate + '&ToDate=' + ToDate;
    this.inputpart += '&ToDate=' + ToDate + '&includeInactive=' + includeInactive + '&maxRecord=' + maxRecord + '&offset=' +
      offset + '&sortBy=' + sortBy + '&sortOrder=' + sortOrder;
    this.inputpart += '&UpcCode=' + upccode + '&ItemName=' + itemname + '&' + familydetails + '&ItemType=' + itemtype +
      '&OrderStatus=' + orderStatus + '';
    this.APIstring = this.Mainurl + 'IMWAPI/optical/getOrderSearch';
    return this.api.GetApi(this.mainpart, this.inputpart);

  }

  /**
   * Params order history service
   * @param orderDataList for getting the grid data list
   */
  getOrderHistoryData(orderDataList) {
    this.mainpart = '/order/searchFilter';
    return this.api.postApiMicro(this.mainpart, orderDataList)
  }

  /**
   * Gets order history bypagination
   * @param rowData for data list 
   * @param page for navigation list 
   * @returns  for pagination data list
   */
  getOrderHistoryBypagination(rowData, page: number) {
    this.mainpart = '/order/searchFilter';
    if (page !== 0) {
      this.mainpart += '?page=' + page;
    }
    return this.api.postApiMicro(this.mainpart, rowData);
  }

  /**
   * Getthechargevaluesfororders order history service
   * @returns getting charge filter items
   */
  getthechargevaluesfororder(orderdetails: any) {
    this.mainpart = '/charge/filter';
    return this.api.postApiMicro(this.mainpart, orderdetails);
  }

  /**
   * Gets order item details
   * @returns get order item details
   */
  getOrderItemDetails(orderId = '') {
    this.mainpart = '/order/' + orderId + '/item';
    return this.api.getApiMicro(this.mainpart);
  }







}
