import { Injectable } from '@angular/core';
import { ApiService } from '@services/api.service';
import { ErrorService } from '@services/error.service';

@Injectable({
  providedIn: 'root'
})
export class TaxService {

  /**
   * Mainurl  will store the actual API call syntax
   */
  Mainurl = '';

  /**
   * Edittaxid  of tax service will store the selected taxid from component
   */
  edittaxid;

  /**
   * Creates an instance of tax service.
   *  apiservice  - service to interact with tax component
   *  errorHandle - Error handler to interact with tax component
   */
  constructor(private apiservice: ApiService,
              public errorHandle: ErrorService) { }

  /**
   * Savetaxdata does operation related add/edit tax
   *  taxdata  - inputs the body object of request
   *  taxid - inputs the selected taxID
   */
  Savetaxdata(taxdata, taxid) {
    if (taxid > 0) {
      this.Mainurl = '/tax/' + taxid;
      return this.apiservice.putApiMicro(this.Mainurl, taxdata);
    } else {
      this.Mainurl = '/tax';
      return this.apiservice.postApiMicro(this.Mainurl, taxdata);
    }
  }

  /**
   * Gettaxdata  return the list of taxes added
   */
  gettaxdata() {
    this.Mainurl = '/tax';
    return this.apiservice.getApiMicro(this.Mainurl);
  }

  /**
   * Gettaxbyid returns the data for selected TaxID
   * taxid - inputs the selected taxID
   */
  gettaxbyID(taxid: string) {
    this.Mainurl = '/tax/' + taxid;
    return this.apiservice.getApiMicro(this.Mainurl);
  }

  /**
   * Updatetaxstatus will allow to update tax status to active/inactive
   * data - inputs the body object of request
   * taxid - inputs the selected taxID
   * status - inputs the status of selected tax
   */
  updatetaxstatus(data: any, taxid, status) {

    if (status === 'Active') {
      this.Mainurl = '/tax/' + taxid + '/activate';
      return this.apiservice.putApiMicro(this.Mainurl, data);
    } else if (status === 'Inactive') {
      this.Mainurl = '/tax/' + taxid + '/deactivate';
      return this.apiservice.putApiMicro(this.Mainurl, data);
    }

  }

}
