import { ApiService, ErrorService } from '..';
import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class CommissionStructureService {
  /**
   * Mainpart  of commission structure service
   * stores path of given api
   */
  mainPart: string;
/**
 * Edit commistion id of commission structure service
 * it stores the seleted row data details
 */
editCommistionId: number;
/**
 * Double click id of commission structure service
 * it stores the seleted row data details
 */
doubleClickId: number;
/**
 * Multiselectio id of commission structure service
 * it stores the seleted row data id
 */
multiSelectionId: number;
  constructor(private apiService: ApiService,
              public errorHandle: ErrorService) {
                this.editCommistionId = null;
               }
/**
 * Saves commition structures
 * @returns save commisstion API
 */
saveCommissionStructures(commission: object) {
  this.mainPart = '/commissionStructure';
  return this.apiService.postApiMicro(this.mainPart, commission);

}
/**
 * Getting commition structures
 * @returns  getting commission structure details
 */
gettingCommissionStructures() {
  this.mainPart = '/commissionStructure';
  return this.apiService.getApiMicro(this.mainPart);

}
/**
 * Gets commition id
 * @returns  getting selected item details
 */
getCommissionId(id: number) {
  this.mainPart = '/commissionStructure/' + id;
  return this.apiService.getApiMicro(this.mainPart, );
}
/**
 * Updates commission details
 * @returns  updated item responce
 */
updateCommissionDetails(id: number, data: object) {
  this.mainPart = '/commissionStructure/' + id;
  return this.apiService.putApiMicro(this.mainPart, data);
}
/**
 * Deletes commission data
 * @return delete item responce
 */
deleteCommissionData(reqBody: object) {
  this.mainPart = '/commissionStructure/delete';
  return this.apiService.deleteApiMicro(this.mainPart, reqBody);

}
}
