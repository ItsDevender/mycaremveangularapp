import { Injectable } from '@angular/core';

import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
// import 'rxjs/add/operator/map';
// import 'rxjs/add/operator/do';
import { ApiService } from '../api.service';
import { ErrorService } from '../error.service';
import { Observable } from 'rxjs/internal/Observable';
import { Suppliers } from '@app/core/models/inventory/addsuppliers.model';
// import { Suppliers } from '@app/core/models/addsuppliers.model';
// import { Observable } from 'rxjs';
@Injectable()
export class SupplierService {
    Accesstoken = '';
    Mainurl = '';
    APIstring = '';
    AddSuppliersUrl = '';
    GetSupplierfilter: any = '';
    editsupplierid: any;
    FortrasactionItemID: string;
    constructor(private http: HttpClient, private apiservice: ApiService, public errorHandle: ErrorService) { }

    SaveSupplier(supplier: Suppliers) {
        this.AddSuppliersUrl = this.Mainurl + 'IMWAPI/optical/addUpdateVendor';
        const httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json'
            })
        };
        // const bodyString = JSON.stringify(supplier);
        return this.apiservice.postApi(this.AddSuppliersUrl, supplier);


    }

    getsupplierstransmissiondata() {
        this.Mainurl = '/jobTransmission';
        return this.apiservice.getApiMicro(this.Mainurl);
    }

    GetSuppliersDetails(includeInactive, maxRecord, offset, id) {
        this.Accesstoken = ''
        // this._StateInstanceService.accessTokenGlobal;
        this.APIstring = 'IMWAPI/optical/getVendors';
        const inputpart = this.Accesstoken + '&Id=' + id + '&includeInactive=' +
            includeInactive + '&offset=' + offset + '&maxRecord=' + maxRecord + '';
        return this.apiservice.GetApi(this.APIstring,inputpart);
        // .do(data => data);
    }

    getLocations() {

        this.APIstring = 'IMWAPI/optical/getLocations';
        // this._AppSettings.getToken();
        return this.apiservice.GetApi(this.APIstring);
        // .do(data => data);
    }

    GetSuppliersDetailsdata() {
        this.Mainurl = '/vendor';
        return this.apiservice.getApiMicro(this.Mainurl);
    }

    GetsupplierFTpconnection(supplierid) {
        this.Mainurl = '/vendor/' + supplierid + '/testFtp';
        return this.apiservice.getApiMicro(this.Mainurl);
            // .map((res: Response) => res.json());
    }



    GetSuppliersDetailsdatabyID(supplierid) {
        this.Mainurl = '/vendor/' + supplierid;
        return this.apiservice.getApiMicro(this.Mainurl);
    }

    SaveSupplierdata(supplier: Suppliers, supplierid) {
        // const reqbody = JSON.stringify(supplier);
        if (supplierid > 0) {
            this.Mainurl = '/vendor/' + supplierid;
            return this.apiservice.putApiMicro(this.Mainurl, supplier);
        } else {
            this.Mainurl = '/vendor';
            return this.apiservice.postApiMicro(this.Mainurl, supplier);
        }



    }
}
