import { Injectable } from '@angular/core';
import { ApiService } from '../api.service';

@Injectable()
export class ReceivingService {
  mainpart: string;

  constructor(private apiservice: ApiService) { }


  // receiving.service
  postingOftheInventoryReceiveDataBulk(data) {
    // const reqbody = JSON.stringify(data);
    this.mainpart = '/inventoryRecieve';
    return this.apiservice.postApiMicro(this.mainpart, data);
  }
  // receiving.service
  updatreOftheInventoryReceiveDataBulk(data) {
    const reqbody = JSON.stringify(data);
    this.mainpart = '/inventoryRecieve';
    return this.apiservice.putApiMicro(this.mainpart, reqbody);
  }

  // receiving.service
  uploadInvoice(uploadedFiles, receiveId) {
    this.mainpart = '/inventoryRecieve/' + receiveId + '/uploadInvoice';
    return this.apiservice.postApiMicroUpload(this.mainpart, uploadedFiles);

  }

  // receiving.service
  DeleteInvoice(id) {
    this.mainpart = '/inventoryRecieve/' + id + '/deleteInvoice';
    return this.apiservice.deleteApiMicro(this.mainpart);

  }

  // receiving.service
  ViewInvoice(id) {
    this.mainpart = '/inventoryRecieve/' + id + '/viewInvoice';
    return this.apiservice.getApiMicro(this.mainpart);

  }

  // receiving.service
  filterReceivingInventoryData(data) {
    // const reqbody = JSON.stringify(data);
    this.mainpart = '/inventoryRecieve/filter';
    return this.apiservice.postApiMicro(this.mainpart, data);
  }
  /**
   * Saving stock details
   * @param data for payload
   * @returns  for saving result
   */
  savingStockDetails(data){
    this.mainpart = '/stockDetail';
    return this.apiservice.postApiMicro(this.mainpart, data);
    
  }
  /**
   * Updating stock details
   * @param data for payload
   * @returns  for update result
   */
  UpdatingStockDetails(data){
    this.mainpart = '/stockDetail';
    return this.apiservice.putApiMicro(this.mainpart, data);
    
  }
  
  /**
   * Getthes inventory receive data
   * @param id for PO Id
   * @returns  for getting receive items
   */
  gettheInventoryReceiveData(id) {
    // const reqbody = JSON.stringify(data);
    this.mainpart = '/inventoryRecieve/'+id;
    return this.apiservice.getApiMicro(this.mainpart);
  }
}
