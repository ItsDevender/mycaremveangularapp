import { Injectable } from '@angular/core';
import { ApiService } from '@app/core';
// import 'rxjs/add/operator/map';
// import 'rxjs/add/operator/do';
import { Observable } from 'rxjs/internal/Observable';
import { HttpClient } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';
import { ErrorService } from '../error.service';
@Injectable()
export class PhysicalcountService {
  data: any = [];
  error: any = [];
  mainpart: string;
  constructor(private http: HttpClient, private apiservice: ApiService,public errorHandle: ErrorService) {
    // this._AppSettings.locid
    this.data['batchform'] = {
      location: '',
      batchID: '',
      batchDate: '',
      batchStatus: '0',
      batchNotes: 'NA',
      item: ''
    };
  }

  deleteBatch(batchID) {
    this.mainpart =  '/physicalCounts/' + batchID;
    return this.apiservice.deleteApiMicro(this.mainpart);
  }

  // Get Items based on UPC or null
  getPhysicalCounts(reqBody) {
    this.mainpart =  '/item/filter';
    // const postdata = JSON.stringify(value);

    return this.apiservice.postApiMicro(this.mainpart, reqBody);
  }

  // Add/Update Batch
  addUpdateBatch(reqbody, batchId: number = 0) {
    this.mainpart =  '/physicalCounts';
    if (batchId === 0) {
      return this.apiservice.postApiMicro(this.mainpart, reqbody);
    } else {
      return this.apiservice.putApiMicro(this.mainpart + '/' + batchId, reqbody);
    }
  }

  // Get Search Batch
  getBatches(reqBody) {
    this.mainpart =  '/physicalCounts/filter';
    return this.apiservice.postApiMicro(this.mainpart, reqBody);
  }

  // Get Batch Quantities
  getBatchItems(batchId) {
    this.mainpart =  '/physicalCounts/batch/' + batchId + '/items';
    return this.apiservice.getApiMicro(this.mainpart);
  }

  // Upload User File to Batch
  uploadUserImportFile(reqBody: any, batchId: number) {
    this.mainpart =  '/physicalCounts/batch/' + batchId + '/import';
    return this.apiservice.postApiMicroUpload(this.mainpart, reqBody);
  }

  // Get Uploaded File Data
  getFileItems(fileId: number, page: number) {
    this.mainpart =  '/physicalCounts/fileData/' + fileId;
    if (page !== 0) {
      this.mainpart += '?page=' + page;
    }
    return this.apiservice.getApiMicro(this.mainpart);
  }

  // physicalcount.service
  getNewManufactures(reqbody) {
    this.mainpart = '/manufacturer/filter';
    return this.apiservice.postApiMicro(this.mainpart, reqbody);
  }
// physicalcount.service
  getFilteredBrands(mid, reqbody) {
    this.mainpart = '/manufacturer/' + mid + '/brands/filter';
    return this.apiservice.postApiMicro(this.mainpart, reqbody);
  }
// physicalcount.service
  getItemTypes() {
    this.mainpart = '/item/types';
    return this.apiservice.getApiMicro(this.mainpart);
  }
}
