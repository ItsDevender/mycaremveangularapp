import { Injectable } from '@angular/core';
import { ApiService } from '@app/core';
import { ErrorService } from '../error.service';
@Injectable()
export class LenstreatmentService {
  /**
   * barCodelist of lenstreatment service
   * storing barCodelist type data
   */
  barCodelist: any[] = [];
  /**
   * Commission type of lenstreatment service
   * storing Accesstoken type data
   */
  Accesstoken = '';
  /**
   * Commission type of lenstreatment service
   * storing Mainurl type data
   */
  Mainurl = '';
  /**
   * Commission type of lenstreatment service
   * storing APIstring type data
   */
  APIstring = '';
  /**
   * Commission type of lenstreatment service
   * storing AddframesUrl type data
   */
  AddframesUrl = '';
  /**
   * Commission type of lenstreatment service
   * storing Getframesfilter type data
   */
  Getframesfilter: any = '';
  /**
   * Commission type of lenstreatment service
   * storing AddLocations type data
   */
  AddLocations = '';
  /**
   * Commission type of lenstreatment service
   * storing editframeid type data
   */
  editframeid: any = '';
  /**
   * Commission type of lenstreatment service
   * storing FortrasactionItemIDFrame type data
   */
  FortrasactionItemIDFrame: any = '';
  /**
   * Commission type of lenstreatment service
   * storing editDuplicateId type data
   */
  editDuplicateId: any = '';
  /**
   * Commission type of lenstreatment service
   * storing mainpart type data
   */
  mainpart: string;
  /**
   * Commission type of lenstreatment service
   * storing inputpart type data
   */
  inputpart: string;
  /**
   * Commission type of lenstreatment service
   * storing editLensTreatmentid type data
   */
  editLensTreatmentid: any = '';
  /**
   * Commission type of lenstreatment service
   * storing editLensTreatmentidDoubleClick type data
   */
  editLensTreatmentidDoubleClick: any = '';
  /**
   * Commission type of lenstreatment service
   * storing editduplicateLensTreatmentid type data
   */
  editduplicateLensTreatmentid: any = '';
  /**
   * Commission type of lenstreatment service
   * storing lensTreatmentslist type data
   */
  lensTreatmentslist: any = [];
  /**
   * Commission type of lenstreatment service
   * storing duplicate type data
   */
  duplicate: string='';

  
  constructor(private apiservice: ApiService, public errorHandle: ErrorService) { }
  getLensTreatmentList(Id = '', Name = '', upc = '', categoryId = '', maxRecord = '', offset = '', unitmeasure = '') {
    this.mainpart = 'IMWAPI/optical/getLensTreatments';
    this.inputpart = '&Id=' + Id + '&Name=' + Name + '&&upc=' + upc + '&categoryId=' + categoryId + '&unitMeasure=' +
      unitmeasure + '&maxRecord=' + maxRecord + '&offset=' + offset + '';
    return this.apiservice.GetApi(this.mainpart, this.inputpart);
    //  .do(data => data);
  }

  /**
   * getLensTreatmentReactivateList lenstreatment service
   * to display the lenstreat
   */
  getLensTreatmentReactivateList(Id = '', Name = '', upc = '', categoryId = '', maxRecord = '', offset = '') {
    this.mainpart = 'IMWAPI/optical/getLensTreatments';
    this.inputpart = '&Id=' + Id + '&Name=' + Name + '&categoryId=' + categoryId + '&maxRecord=' + maxRecord +
      '&offset=' + offset + '' + '&del_status=1';
    return this.apiservice.GetApi(this.mainpart, this.inputpart);
    //  .do(data => data);
  }
  /**
   * Commission type of lenstreatment service
   * storing duplicate type data
   */
  SaveLensTreatment(lenstreament: any) {
    this.mainpart = 'IMWAPI/optical/addUpdateLensTreatments';
    // const postdata = JSON.stringify(lenstreament);
    return this.apiservice.postApi(this.mainpart, lenstreament);
  }
  /**
   * Commission type of lenstreatment service
   * storing duplicate type data
   */
  getVCPdropDownData() {
    this.Mainurl = '/vcp';
    return this.apiservice.getApiMicro(this.Mainurl);
  }
  /**
   * Commission type of lenstreatment service
   * storing duplicate type data
   */
  getStructuredropData() {
    this.Mainurl = '/commissionStructure';
    return this.apiservice.getApiMicro(this.Mainurl);
  }
  /**
   * Commission type of lenstreatment service
   * storing duplicate type data
   */
  getCommissionTypedropData() {
    this.Mainurl = '/commissionType';
    return this.apiservice.getApiMicro(this.Mainurl);
  }
  /**
   * Commission type of lenstreatment service
   * storing duplicate type data
   */ 
  getLensTreatmentProcedures() {
    this.Mainurl = '/cpt';
    return this.apiservice.getApiMicro(this.Mainurl);
  }
  /**
   * Commission type of lenstreatment service
   * storing duplicate type data
   */
  getSavedProceduresById(itemId) {
    this.Mainurl = '/item/' + itemId + '/procedure';
    return this.apiservice.getApiMicro(this.Mainurl);
  }
  /**
   * Commission type of lenstreatment service
   * storing duplicate type data
   */
  saveLenstreatmentProcedures(data, itemId) {
    const reqBody = {
      'procedure': data
    };
    this.Mainurl = '/item/' + itemId + '/procedure';
    return this.apiservice.postApiMicro(this.Mainurl , reqBody);
  }
  /**
   * Commission type of lenstreatment service
   * storing duplicate type data
   */
  deleteSavedProceduresById(itemId, id) {
    this.Mainurl = '/item/' + itemId + '/procedure/' + id;
    return this.apiservice.deleteApiMicro(this.Mainurl);
  }
}
