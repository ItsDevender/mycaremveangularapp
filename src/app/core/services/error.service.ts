import { MessageService } from 'primeng/api';
import { Injectable, OnDestroy } from '@angular/core';
import { ErrorCodes, ApiError, ExcludeErrors } from '@models/errors.model';

@Injectable({
  providedIn: 'root'
}) 
export class ErrorService implements OnDestroy {
  public msgs = [];
  previousApiError: number;
  previousApiTime: number;
  errorCount: number;
  noDuplicateRequest: boolean;

  constructor(private mess: MessageService) {
    this.errorCount = 0; 
    this.noDuplicateRequest = false;
  }
  ngOnDestroy() { console.log('Error instance destroyed.');}

  displayError(obj) {
    this.mess.add(obj);
  }

  /**
   * Shows errors
   * @param error Actual API Error Response returns object
   * @param showErrors Environment Check for hide/show errors returns boolean value
   */
  showErrors(error: ApiError, showErrors: boolean) {

    // First API Error Execution Start Time
    const start = new Date().getTime();
    let checkErrorVisibility: number;
    switch (showErrors) {
      case true: {
        checkErrorVisibility = 0;
        break;
      }
      case false: {
        checkErrorVisibility = (ExcludeErrors.indexOf(error.status) === -1) ? 0 : 1;
        break;
      }
    }

    // Check Duplicate Requests
    if (this.previousApiError === error.status) {
      this.noDuplicateRequest = true;
      this.errorCount++;
      let timeDiff = start - this.previousApiTime;
      timeDiff = parseInt(((timeDiff % 60000) / 1000).toFixed(0), 10);
      if (timeDiff > 2) {
        this.noDuplicateRequest = false;
        this.errorCount = 0;
      }
    }

    // Show Error if it is not Duplicate
    if (checkErrorVisibility === 0 && !this.noDuplicateRequest) {
      this.mess.add({
        severity: 'error',
        summary: ErrorCodes[error.status].statusText,
        detail: ErrorCodes[error.status].desc
      });
      this.previousApiError = error.status;
      if (this.errorCount === 0) {
        this.previousApiTime = start;
      }
    }
  }

  // Show Syntax Errors
  syntaxErrors(error: any) {
    this.mess.add({
      severity: 'error',
      summary: 'Syntax Error',
      detail: error
    });
  }

  private findInvalidControls(formControls: any) {
    const invalid = [];
    const controls = formControls;
    for (const name in controls) {
        if (controls[name].invalid) {
            invalid.push(name);
        }
    }
    return invalid;
  } 

  displayFormErrors(formControls: any) {
    const invalidFields = this.findInvalidControls(formControls);
    invalidFields.forEach((item, index) => {
      this.mess.add({
        severity: 'error', 
        summary: 'Required', 
        detail: item.charAt(0).toUpperCase() + item.slice(1).toLowerCase()
      })
    });
  }
}
