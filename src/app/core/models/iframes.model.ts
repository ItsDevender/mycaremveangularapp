export const IframesSource = {
     // Company
     FramesURL: 'https://preweb.whoisyourinsurance.com/Portal/FramesList.aspx',
     LocationUrl: 'interface/admin/facility/index.php',
     CompanyUrl: 'interface/admin/groups/index.php',

     // Patient
     DemographicsURL: 'interface/patient_info/demographics/sep.php',
     PatientInsuranceUrl: 'interface/patient_info/insurance/index.php',
     CalendarUrl: 'interface/scheduler/base_day_scheduler.php',
     PtDocsUrl: 'interface/patient_info/consent_forms/index.php',
     PtMergeUrl: 'interface/patient_info/common/merge_patient.php',
     MonitorUrl: 'interface/core/multi_login.php?app_name=imedicmonitor',
     AppointmentsURL: 'interface/scheduler/appointments/index.php',

     // Settings
     InsuranceSetupUrl: 'interface/admin/billing/add_insurance/index.php',

     // Patient History Parent URL
     PatientHistoryUrl: 'interface/Medical_history/index.php',

     // Tests Url
     TestsUrl: 'interface/tests/index.php',

     // Exam Screen URL
     ExamUrl: 'interface/chart_notes/work_view.php',
     ContactLensExamUrl: 'interface/chart_notes/contact_lens_worksheet_popup.php?mode=newContactLensSheet',
     PatientInstructionsUrl: 'interface/chart_notes/pt_instructions/index.php',
     PatientProceduresUrl: 'interface/chart_notes/onload_wv.php?elem_action=Procedures',

     // Employees
     EmployeesUrl: 'interface/admin/providers/index.php',

     // Batch
     BatchPaymentUrl: 'interface/billing/batch_process_list.php',

     // Era
     UploadEra: 'interface/billing/era_file_lists.php',
     PostPaymentsEra: 'interface/billing/era_post_payments.php',
     ClaimRejectionEra: 'interface/billing/denied_payment_correction.php',


     // Reports
     SchedulerUrl: 'interface/reports/scheduler_report.php',
     SchedulerPatientMonitorUrl: 'interface/reports/patient_monitor_index.php',
     FaceSheetUrl: 'interface/reports/day_facesheet_index.php',
     ApptInfoUrl: 'interface/reports/app_info_index.php',
     SPtDocsUrl: 'interface/reports/pt_docs_index.php',
     SxPlanningSheetUrl: 'interface/reports/sx_planning_sheet.php',
     RecallUrl: 'interface/reports/recall_fulfillment_index.php',
     ConsultLetterUrl: 'interface/reports/consult_letter_report_index.php',
     SchedulerReportUrl: 'interface/reports/scheduler_report_default.php',
     PatientCSVReportUrl: 'interface/reports/detail_patient_report.php',
     PracticeAnalyticUrl: 'interface/reports/prac_analytics_report.php',
     ComplianceUrl: 'interface/reports/compliance_index.php',
     ComplianceQRDAUrl: 'interface/reports/qrda/index.php',
     ComplianceCQMUrl: 'interface/reports/cqm_import/index.php',
     ApiAccessLogUrl: 'interface/reports/api/index.php',
     ApiCallLogUrl: 'interface/reports/api/callLog.php',
     CCDUrl: 'interface/reports/ccd/index.php',
     KYStateReportUrl: 'interface/reports/index_state_ky.php',
     TNStateReportUrl: 'interface/reports/index_state_tn.php',
     OpticalUrl: 'interface/reports/optical/index.php',
     ReminderDayApptUrl: 'interface/reports/day_appts_index.php',
     ReminderRecallsUrl: 'interface/reports/reminder_recall_index.php',
     ReminderListUrl: 'interface/reports/reminder_lists_index.php',
     ClinicalReport: 'interface/reports/clinical/clinical_index.php',

     // Order Payments
     OrderPaymentUrl: 'interface/accounting/quickPay.php',
     FinancialUrl: 'interface/reports/financial_index.php',
     PreviousHCFAUrl: 'interface/reports/previous_hcfa.php',
     EIDStatusUrl: 'interface/reports/eid_status.php',
     EIDPaymentUrl: 'interface/reports/eid_payments.php',
     NewStatementUrl: 'interface/reports/new_statement.php',
     PreviousStatementUrl: 'interface/reports/previous_statement.php',
     //Order Cliams navigation URL

     ClaimWithEncounterId : '/interface/accounting/accounting_view.php',

     // Scheduler Pages Calendar
     AvailableUrl: 'interface/admin/scheduler_admin/available/index.php',
     chainEvent: 'interface/admin/scheduler_admin/chainevent/index.php',
     ProcedureTemplateUrl: 'interface/admin/scheduler_admin/procedure_template/index.php',
     ProviderScheduleUrl: 'interface/admin/scheduler_admin/provider_schedule/provider_sch.php',
     ScheduleReasonUrl: 'interface/admin/scheduler_admin/schedule_reason/index.php',
     ScheduleStatusUrl: 'interface/admin/scheduler_admin/schedule_status/index.php',
     ScheduleTemplateUrl: 'interface/admin/scheduler_admin/schedule_template/index.php',


     addPatientUrl: 'interface/scheduler/common/new_patient_info_popup_new.php?source=demographics&search=true&frm_status=show_check_in',
     paymentLedgerUrl: 'interface/accounting/review_payments.php',
     DocumentsUrl: 'interface/admin/documents/index.php',
     ScanUploadsUrl: 'interface/admin/console/manage_folder/index.php',
     SetMarginUrl: 'interface/admin/set_margin/index.php',
     EmployeeCenterUrl: 'interface/physician_console/index.php',

     CPTUrl: 'interface/admin/billing/cpt_fee_tbl/index.php',
     DXCodesUrl: 'interface/admin/billing/diagnosis_code/index.php',
     ICD10Url: 'interface/admin/billing/icd10/index.php',
     WriteOffCodesUrl: 'interface/admin/billing/revenue_code/write_code_list.php',
     ModifiersUrl: 'interface/admin/billing/modifiers/index.php',
     FeeTableUrl: 'interface/admin/billing/cpt_fee/index.php',
     InsuranceGroupUrl: 'interface/admin/billing/ins_group/index.php',
     ManagePOSUrl: 'interface/admin/pos/pos_merchant_index.php',
     PoliciesUrl: 'interface/admin/billing/policies/index.php',
     ElectronicUrl: 'interface/billing/electronic_billing.php',
     patientAlertsURl: 'interface/patient_info/demographics/patient_alert.php'

};
