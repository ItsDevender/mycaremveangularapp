/**
 * Master drop down this model for binding the data to the all the master dropdowns with id and Name
 */
export class MasterDropDown {
    Id: string;
    Name: string;
}

/**
 * Manufacturer drop data for binding the data to manufacturer master dropdown 
 */
export interface ManufacturerDropData {
    data: Array<Manufacturer>;
}
/**
 * Manufacturer dropdown data single object values
 */
export interface Manufacturer {
    accessories_chk: number;
    city: string;
    cont_lenses_chk: number;
    contact_name: string;
    del_status: number;
    email: string;
    fax: string;
    frames_chk: number;
    id: string;
    lenses_chk: number;
    manufacturer_address: string;
    manufacturer_name: string;
    medicine_chk: number;
    mobile: string;
    state: string;
    supplies_chk: number;
    tel_num: string;
    zip: string;
    zip_ext: string;
}
/**
 * Lens color master  this model for loading lens color master dropdown
 */
export interface LensColorMaster {
    data: Array<LensColor>;
}
/**
 * LensColor dropdown data single object values
 */
export interface LensColor {
    id: string;
    colorname: string;
    colorcode: string;
    vsp_code: string;
    vsp_description: string;
    del_status: string;
}
/**
 * CommissionStructure master  this model for loading CommissionStructure master dropdown
 */
export interface CommissionStructure {
    data: Array<Structure>;

}
/**
 * Structure dropdown data single object values
 */
export interface Structure {
    amount: string;
    commissiontype_id: number;
    gross_percentage: string;
    id: string;
    margin_percentage_after: string;
    margin_percentage_amount: string;
    margin_percentage_before: string;
    mve_id: string;
    spiff: string;
    structure_name: string;
}
/**
 * CommissionType master  this model for loading CommissionType master dropdown
 */
export interface CommissionType {
    data: Array<Type>;

}
/**
 * Type dropdown data single object values
 */
export interface Type {
    commissiontype: string;
    id: string;
}
/**
 * FramesMaterial master  this model for loading FramesMaterial master dropdown
 */
export interface FramesMaterial {
    data: Array<Material>;
}
/**
 * Material dropdown data single object values
 */
export interface Material {
    id: string;
    materialname: string;
    colorcode: string;
    del_status: string;
}
/**
 * FrameSupplier master  this model for loading FrameSupplier master dropdown
 */
export interface FrameSupplier {
    data: Array<Supplier>;
}
/**
 * Supplier dropdown data single object values
 */
export interface Supplier {
    id: string;
    vendor_name: string;
    vendor_id: string;
    second_vendor_id: string;
    vendor_address: string;
    tel_num: string;
    mobile: string;
    fax: string;
    email: string;
    city: string;
    zip: string;
    zip_ext: string;
    state: string;
    del_status: string;
    contact_name: string;
    sales_rep_fname: string;
    sales_rep_mname: string;
    sales_rep_lname: string;
    sales_rep_work_no: string;
    sales_rep_cell_no: string;
    sales_rep_email: string;
}

/**
 * FrameTypeMaster master  this model for loading FrameTypeMaster master dropdown
 */
export interface FrameTypeMaster {
    data: Array<FrameType>;
}
/**
 * FrameType dropdown data single object values
 */
export interface FrameType {
    id: string;
    type_name: string;
    CollectionFramesMasterID: string;
    StyleFramesMasterID: string;
}
/**
 * FrameShapeMaster master  this model for loading FrameShapeMaster master dropdown
 */
export interface FrameShapeMaster {
    data: Array<FrameShape>;
}
/**
 * FrameShape dropdown data single object values
 */
export interface FrameShape {
    id: string;
    shape_name: string;
    del_status: string;
    CollectionFramesMasterID: string;
    StyleFramesMasterID: string;
}

/**
 * FrameGender master  this model for loading FrameGender master dropdown
 */
export interface FrameGender {
    data: Array<Gender>;
}
/**
 * Gender dropdown data single object values
 */
export interface Gender {
    gender_id: string;
    gender_name: string;
    gender_code: string;
}
/**
 * FrameStyleMaster master  this model for loading FrameStyleMaster master dropdown
 */
export interface FrameStyleMaster {
    data: Array<FramesStyle>;
}
/**
 * FramesStyle dropdown data single object values
 */
export interface FramesStyle {
    id: string;
    style_name: string;
    prac_code: string;
    del_status: string;
    StyleFramesMasterID: string;
    CollectionFramesMasterID: string;
}
/**
 * LensStyleMaster master  this model for loading LensStyleMaster master dropdown
 */
export interface LensStyleMaster {
    data: Array<LensStyle>;
}
/**
 * LensStyle dropdown data single object values
 */
export interface LensStyle {
    id: string;
    type_name: string;
    prac_code: string;
    vw_code: string;
    del_status: string;
    domainid: string;
}

/**
 * VcpDropDown master  this model for loading VcpDropDown master dropdown
 */
export interface VcpDropDown {
    data: Array<Vcp>;
}
/**
 * Vcp dropdown data single object values
 */
export interface Vcp {
    id: string;
    code: string;
    description: string;
    type: string;
    retail_price: string;
    modifier: string;
    sort_order: string;
    vsp_option_code: string;
    lens_type: string;
    diagnosis_mapping: string;
    diagnosis_plan: string;
    category: string;
    icd10_flag: string;
    eye: string;
    del_status: string;
}
/**
 * ContactReplacement master  this model for loading ContactReplacement master dropdown
 */
export interface ContactReplacement {
    data: Array<Replacement>;
}
/**
 * Replacement dropdown data single object values
 */
export interface Replacement {
    id: string;
    opt_val: string;
    del_status: string;
}
/**
 * ContactPackage master  this model for loading ContactPackage master dropdown
 */
export interface ContactPackage {
    data: Array<Package>;
}
/**
 * Package dropdown data single object values
 */
export interface Package {
    id: string;
    opt_val: string;
    del_status: string;
}

/**
 * SpectacleLensMaterial master  this model for loading SpectacleLensMaterial master dropdown
 */
export interface SpectacleLensMaterial {
    data: Array<LensMaterial>;
}
/**
 * LensMaterial dropdown data single object values
 */
export interface LensMaterial {
    id: string;
    material_name: string;
    vw_code: string;
    prac_code_sv: string;
    prac_code_pr: string;
    prac_code_bf: string;
    prac_code_tf: string;
    design_id: string;
    wholesale_price: string;
    purchase_price: string;
    retail_price: string;
}
/**
 * OtherInvCategory master  this model for loading OtherInvCategory master dropdown
 */
export interface OtherInvCategory {
    data: Array<InvCategory>;
}
/**
 * InvCategory dropdown data single object values
 */
export interface InvCategory {
    id: string;
    categoryname: string;
    color_code: string;
    del_status: string;
}
/**
 * InvUpcType master  this model for loading InvUpcType master dropdown
 */
export interface InvUpcType {
    data: Array<UpcType>;
}
/**
 * UpcType dropdown data single object values
 */
export interface UpcType {
    id: string;
    upctype: string;
    del_status: string;
}
/**
 * FrameSource master  this model for loading FrameSource master dropdown
 */
export interface FrameSource {
    data: Array<Source>;
}
/**
 * Source dropdown data single object values
 */
export interface Source {
    id: string;
    sourcename: string;
    del_status: string;
}
/**
 * LensLab master  this model for loading LensLab master dropdown
 */
export interface LensLab {
    data: Array<Lab>;
}
/**
 * Lab dropdown data single object values
 */
export interface Lab {
    id: string;
    lab_name: string;
}


/**
 * FrameColor master  this model for loading FrameColor master dropdown
 */
export interface FrameColor {
    Id: string;
    Name: string;
    colorCode: string;
    Active: string;
}
/**
 * Optical dropdown data single object values
 */
export interface Optical {
    Total: string;
    FrameColors: FrameColor[];
}
/**
 * Result master  this model for loading Result master dropdown
 */
export interface Result {
    Optical: Optical;
}
/**
 * ColorObject  this model for loading ColorObject dropdown
 */
export interface ColorObject {
    Success: number;
    result: Result;
}
/**
 * Frame color data
 * frame color list payload
 */
export interface FrameColorData {
    current_page: number;
    data: [Data];
}
/**
 * Data
 * getting fields in frame color list payload
 */
export interface Data {
    id: string;
    color_name: string;
    color_code: string;
    del_status: number;
    import_id: string;
}
/**
 * Len color data
 * spectacel lens color list payload
 */
export interface LenColorData {
    current_page: number;
    data: [LensData];
}
/**
 * Lens data
 * getting fields in  spectacel lens color list payload
 */
export interface LensData {
    colorcode: number;
    colorname: string;
    del_status: number;
    id: string;
    vsp_code: number;
    vsp_description: string;
}

/**
 * LocationsMaster  this model for loading LocationsMaster dropdown
 */
export interface LocationsMaster {
    id: string;
    name: string;
}

/**
 * TransactionTypeMaster master  this model for loading TransactionTypeMaster master dropdown
 */
export interface TransactionTypeMaster {
    data: [TransactionType];
}
/**
 * TransactionType dropdown data single object values
 */
export interface TransactionType {
    id: string;
    transactiontype: string;
    isdefault: string;
    systemdefined: string;
    sort_order: string;
    del_status: string;
    vsp_code: string;
    vsp_description: string;
}
/**
 * PaymentTermsMaster master  this model for loading PaymentTermsMaster master dropdown
 */
export interface PaymentTermsMaster {
    data: [PaymentTerms];
}
/**
 * PaymentTerms dropdown data single object values
 */
export interface PaymentTerms {
    id: string;
    paymentterm: string;
    isdefault: string;
    systemdefined: string;
    sort_order: string;
    del_status: string;
}
export interface RimType {
    colorcode: string;
    del_status: number;
    id: string;
    rimtype: string;
    vsp_code: string;
    vsp_description: string;
}


export interface FrameUsage {
    data: [Usage];
}

export interface Usage {
    del_status: number;
    domainid: string;
    id: string;
    usagetype: string;
}

export interface FrameBrands {
    data: [Brands];
}

export interface Brands {
    id: string;
    frame_source: string;
    ManufacturerFramesMasterID: string;
    BrandFramesMasterID: string;
    Market: string;
}
/**
 * SpectacleMaterial master  this model for loading SpectacleMaterial master dropdown
 */
export interface SpectacleMaterial {
    data: [Material];
}
/**
 * Material dropdown data single object values
 */
export interface Material {
    del_status: string;
    design_id: string;
    domainid: string;
    id: string;
    material_name: string;
    prac_code_bf: string;
    prac_code_pr: string;
    prac_code_sv: string;
    prac_code_tf: string;
    purchase_price: string;
    retail_price: string;
    vw_code: string;
    wholesale_price: string;
}
/**
 * DiscountReason master  this model for loading DiscountReason master dropdown
 */
export interface DiscountReason {
    data: [Reason];
}
/**
 * Reason dropdown data single object values
 */
export interface Reason {
    cas_code: string;
    cas_desc: string;
    cas_id: string;
}

/**
 * TreatmentCategory master  this model for loading TreatmentCategory master dropdown
 */
export interface TreatmentCategory {
    data: [Category];
}
/**
 * Category dropdown data single object values
 */
export interface Category {
    id: string;
    categoryname: string;
    categorytypeid: string;
    del_status: string;
}



