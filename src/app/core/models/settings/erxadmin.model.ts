export interface ErxAdmin {
    portal: number;
    erx_mode: number;
    system_name: string;
    secret_key: string;
    erx_user_name: string;
    erx_status: number;
    modified_by: ModifiedBy;
    modified_date_time: string;
}

export interface ModifiedBy {
    id: number;
    fname: string;
    mname: string;
    lname: string;
}
