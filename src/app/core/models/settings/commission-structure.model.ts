/**
 * Commission structure
 * stores multiple selectionitems
 */
export interface CommissionStructure {
    deleteIds: Array<number>;
}
/**
 * Add commission structure
 * stores formcontroller details.
 */
export interface AddCommissionStructure {
    data: object;
}
/**
 * Save commission structure
 * result of requested payload data
 */
export interface SaveCommissionStructure {
    status: string;
    record: number;
}
/**
 * Add update commission structure
 * updated commission structure details getting result pay load
 */
export interface AddUpdateCommissionStructure {
    structure_name: string;
    commissiontype_id: number;
    gross_percentage: number;
    margin_percentage_before: number;
    margin_percentage_after: number;
    margin_percentage_amount: number;
    amount: number;
    spiff: number;
}
/**
 * Getting commission structure
 * getting commission structure details getting result pay load
 */
export interface GettingCommissionStructure {
    current_page: number;
    data: [Data];

}
/**
 * Data
 * getting result payload details
 */
export interface Data {
    id: number;
    structure_name: string;
    commissiontype_id: number;
    gross_percentage: number;
    margin_percentage_before: number;
    margin_percentage_after: number;
    margin_percentage_amount: number;
    amount: number;
    spiff: number;
    mve_id: number;
}
/**
 * Emitter
 * emmiting the sidebar
 */
export interface Emitter {
    id: number;
    cancel: string;
    value: object;
}
