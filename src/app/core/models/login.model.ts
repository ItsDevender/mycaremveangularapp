export class MicroServiceLogin {
    username: string;
    password: string;
    facility: number;
}

export class GatewayServiceLogin {
    key: string;
    user: string;
}