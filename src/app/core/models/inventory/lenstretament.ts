
    export interface LensTreatment {
        itemId: string;
        UPC: string;
        Name: string;
        vspCode: string;
        CategoryId: string;
        Category: string;
        QtyOnHand: string;
        unitmeasure: string;
        retail_price: string;
        Cost: string;
        purchase_price: string;
        module_type_id: string;
        Inventory: string;
        sendToLab: string;
        printOnOrder: string;
        VcpModifierId: string;
        VcpModifiercode: string;
        VcpModifierdescription: string;
        dviCodeId: string;
        StructureId: string;
        StructureName: string;
        CommissionTypeId: string;
        CommissionTypeName: string;
        Amount: string;
        GrossPercentage: string;
        Spiff: string;
    }

    export interface Optical {
        Total: string;
        LensTreatment: LensTreatment[];
    }

    export interface Result {
        Optical: Optical;
    }

    export interface RootObject {
        Success: number;
        result: Result;
    }

