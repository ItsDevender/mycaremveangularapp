export interface Frame {
    itemId: string;
    UPC: string;
    Name: string;
    ManufacturerId: string;
    Manufacturer: string;
    BrandId: string;
    Brand: string;
    FrameTypeId: string;
    FrameType: string;
    ColorCode: string;
    ColorId: string;
    Color: string;
    ShapeId: string;
    Shape: string;
    StyleId: string;
    Style: string;
    QtyOnHand: string;
    a: string;
    b: string;
    ed: string;
    dbl: string;
    temple: string;
    bridge: string;
    fpd: string;
    module_type_id: string;
    VendorId: string;
    Vendor: string;
    RimTypeId: string;
    RimType: string;
    SourceId: string;
    SourceName: string;
    UsageId: string;
    UsageType: string;
    MaterialId: string;
    MaterialName: string;
    Upctypeid: string;
    UpcType: string;
    ProcedureCode: string;
    Gender: string;
    Inventory: string;
    Eye: string;
    Groupcost: string;
    FrameLensRangeId: string;
    FrameLensRangeName: string;
    UserSKU: string;
    MSRP: string;
    Notes: string;
    min_qty: string;
    max_qty: string;
    lenscolor: string;
    StructureId: string;
    StructureName: string;
    RetailPrice: string;
    Cost: string;
    Amount: string;
    TraceFile: string;
    Status: string;
    Spiff: string;
    CommissionTypeId: string;
    CommissionTypeName: string;
    GrossPercentage: string;
    IDA_Id: string;
    del_status: number | string;
}

export interface Optical {
    Total: string;
    Frames: Frame[];
}

export interface Result {
    Optical: Optical;
}

export interface RootObject {
    Success: number;
    result: Result;
}

/**
 * Getting frames details
 * getting result pay load data
 */
export interface GettingFramesDetails {
    total: string;
    current_page: number;
    data: [Frame];
}

