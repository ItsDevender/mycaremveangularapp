

/**
 * Frames lens materials is bind the frame lens material data of the particular frame item.
 */
export class FramesLensMaterials {
    data: Array<MaterialsList> = []
}

/**
 * Materials list response from the API of frame item materials
 */
export class MaterialsList {
    del_status: number;
    frameid: number;
    id: number;
    lens_material_id: number;
}

export class LensMaterials {
    items: Array<MaterialsList>;
    status: string;
}

/**
 * Styles list response from the API of frame item styles
 */
export class StylesList {
    del_status: number;
    frameid: number;
    id: number;
    lens_style_id: number;
}

export class StyleMaterialDataList {
    name: string;
    value: number;
}



