

/**
 * Inventory model is for the API responce of the loading of the items of all the inventory items like 
 * SpectacleLens,Frames,LensTreatment,Others,COntactLens
 */
export class InventoryModel {
    data:Array<dataModel> =[]
}
export class dataModel {
    CollectionFramesMasterID: string
    ConfigurationFPC: string
    ConfigurationFramesMasterID: string
    StyleFramesMasterID: string
    a: string
    a_r_id: string
    addd: string
    agegroup: string
    amount: string
    axis: string
    axis_max: string
    b: string
    bc: string
    bc2: string
    brand: string
    brand_id: number
    bridge: string
    bridgesize: string
    calendarduration: string
    category: string
    categoryid: number
    char_size: number
    circumference: string
    cl_packaging: number
    cl_replacement: string
    cl_type: string
    cl_wear_schedule: string
    class_id: number
    clod_guid: string
    collectionname: string
    color: string
    color_code: string
    colorid: number
    commission_type_id: number
    country: string
    cylindep_negative: string
    cylindep_negative_max: string
    cylindep_positive: string
    cylindep_positive_max: string
    cylinder: string
    dbl: string
    del_status: number
    design_id: number
    detailed_domain_id: number
    di2: string
    diameter: string
    discount: string
    discount_till: string
    dosage: string
    dot: string
    dvi_code_id: number
    dx_code: string
    ed: string
    edangle: string
    edge_id: number
    eppointments_appt_type: string
    expiry_date: string
    externalid: string
    eyesize: string
    fd_price_change_alert: number
    fd_price_modified_on: string
    fd_price_temp: number
    fee: string
    finish_type: string
    finish_type_other: string
    formula: string
    fpd: string
    frame_lens_range_id: number
    frame_mve_id: string
    frame_shape: string
    frame_style: string
    frame_type: FrameType
    framenumber: string
    frameusageid: number
    frontprice: string
    gender: string
    gross_percentage: string
    groupcost: number
    halftemplesprice: string
    harcardous: string
    hard_contact: string
    id: number
    ida_id: number
    inventory: number
    is_calendarappointment: string
    is_favorite: string
    is_online: string
    item_prac_code: number
    l_check: number
    lab_id: number
    lenscolor: string
    lenscolorcode: string
    locations: [Locations];
    lodseries: string
    lt_type_id: string
    manufacturer: manufacturer
    manufacturer_id: number
    material_id: number
    max_add_size: number
    max_qty: number
    measurment: number
    med_typ: string
    min_ht: number
    min_qty: number
    minimum_segment: string
    minimum_segment_id: number
    modifierMultifocal: string
    modifier_id: number
    module_type_id: number
    msrp: number
    mve_contact_id: string
    mve_otherid: string
    mve_spec_id: string
    name: string
    ndc: string
    not_on_claim: string
    note_on_claim: string
    notes: string
    num_size: number
    other: string
    oversize_diameter_charge: number
    pay_by: string
    pgx_check: number
    polarized_id: number
    portal_appt_type: string
    pricing_type: string
    print_on_order: number
    procedure_code: string
    progressive_id: number
    purchase_price: number
    qty_on_hand: string
    r_check: number
    recallreasonid: number
    retail_price: number
    retail_price_flag: number
    returnable_flag: number
    rimtypeid: number
    sadd: string
    send_to_lab_flag: number
    six_months_price: number
    six_months_qty: number
    sku: string
    sourceid: string
    spectacle_lens_range_id: number
    spectacle_material: Material
    spectaclelens_pricing_id: number
    sphere: number
    sphere_negative: string
    sphere_negative_max: string
    sphere_positive: string
    sphere_positive_max: string
    spherepower: string
    spiff: number
    ssize: string
    status: string
    stock_image: string
    structure_id: number
    style: string
    style_other: string
    supply_id: string
    temple: string
    templesprice: string
    th: string
    third_party_transferred: string
    threshold: string
    tint_id: number
    trace_file: string
    transition_id: number
    trial_chk: number
    trial_lens_id: string
    twelve_months_price: number
    twelve_months_qty: number
    type_desc: string
    type_id: string
    unitmeasure: string
    units: string
    upc_code: string
    upcsystem: string
    upctypeid: number
    user_defined: string
    uv_check: number
    vcp_modifier_id: number
    vendor_id: number
    vsp_code: string
    vsp_code_description: string
    vsp_option_code_conflict: string
    wearing_schedule_id: string
    wholesale_cost: number
    yearintroduced: string
}


class manufacturer {
    id: number
    manufacturer_name: string
}

class Material {
    id: number
    materialname: string
}
class Locations {
    id: number
    item_id: number
    loc_id: number
    stock: number
    taxid: string
    taxid2: string
}

class FrameType {
    id: number
    type_name: string
}
/**
 * Get procedure code
 * @returns getting procedure code payload
 */
export interface GetProcedureCode {
    current_page: number;
    data: [Data];
}
/**
 * Data
 *  @returns getting procedure code fields payload
 */
export interface Data {
        cpt_fee_id: number;
        cpt_cat_id: number;
        cpt4_code: string;
        cpt_prac_code: string;
        cpt_desc: string;
        cpt_comments: string;
        status: string;
        delete_status: number;
}

/**
 * Inventory result details
 * Inventory saved, updated result request payload
 */
export interface InventoryResultDetails {
    item_Id: number;
    status: string;
}
