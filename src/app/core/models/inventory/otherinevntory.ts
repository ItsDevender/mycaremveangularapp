    export interface OtherInventory {
        ItemId: string;
        UPC: string;
        Name: string;
        CategoryId: string;
        Category: string;
        VendorId: string;
        Vendor: string;
        module_type_id: string;
        StructureId: string;
        StructureName: string;
        Inventory: string;
        SendToLab: string;
        PrintOnOrder: string;
        RetailPrice: string;
        Cost: string;
        ProcedureCode: string;
        ModifierId: string;
        Modifier: string;
        NoteOnClaim: string;
        Notes: string;
        MinQty: string;
        MaxQty: string;
        CommissionTypeId: string;
        CommissionTypeName: string;
        Spiff: string;
        Amount: string;
        GrossPercentage: string;
    }

    export interface Optical {
        Total: string;
        OtherInventory: OtherInventory[];
    }

    export interface Result {
        Optical: Optical;
    }

    export interface RootObject {
        Success: number;
        result: Result;
    }



