export interface RootObject {
  Success: number;
  result: Result;
}

export interface Result {
  SearchPatient: SearchPatient[];
}

export interface SearchPatient {
  ID: string;
  ExternalID: string;
  FirstName: string;
  LastName: string;
  MiddleName: string;
  Suffix: string;
  DateOfBirth: string;
  SSN: string;
  Sex: string;
  HomePhone: string;
  BusinessPhone: string;
  CellPhone: string;
  Email: string;
  Address: Address[];
}

export interface Address {
  Street1: string;
  Street2: string;
  City: string;
  State: string;
  Zip: string;
}
