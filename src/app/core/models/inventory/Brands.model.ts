
export interface FrameBrand {
    Id: string;
    Name: string;
    ManufacturerId: string;
    Active: string;
    ManufacturerActive: string;
}

export interface Optical {
    Total: string;
    FrameBrands: FrameBrand[];
}

export interface Result {
    Optical: Optical;
}

export interface BrandObject {
    Success: number;
    result: Result;
}
