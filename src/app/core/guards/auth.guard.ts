// Core Modules
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

// Services
import { AppService } from '@services/app.service';


@Injectable()
export class AuthGuard implements CanActivate {
   constructor(private router: Router, private app: AppService) { }

   canActivate(next: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
      if (this.app.isUserLoggedIn()) {
         return true;
      } else {
         this.router.navigate(['login']);
         return false;
      }
   }

}
