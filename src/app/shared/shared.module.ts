import { MessageService } from 'primeng/api';
import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';


// Prime NG Modules
import { ToastModule } from 'primeng/components/toast/toast';
import { SidebarModule } from 'primeng/sidebar';
import {
  CheckboxModule, DropdownModule, AutoCompleteModule,
  TreeTableModule, PickListModule, MultiSelectModule,
  ListboxModule, TreeModule, ConfirmDialogModule, FileUploadModule,
  GrowlModule, MessagesModule, ProgressBarModule, AccordionModule, InputSwitchModule
} from 'primeng/primeng';
import { DialogModule } from 'primeng/dialog';

import { TableModule } from 'primeng/table';
import { TabViewModule } from 'primeng/tabview';
import { RadioButtonModule } from 'primeng/radiobutton';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { CalendarModule } from 'primeng/calendar';
import { SpinnerModule } from 'primeng/spinner';

// Core Modules
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

// Pipes
import { SafeUrlPipe } from './pipes/safe-url.pipe';

// Components
import { DialogBoxComponent } from './components/dialog-box/dialog-box.component';
import { NgxBarcode6Module } from 'ngx-barcode6';
import { NgxBarcodeComponent } from './barcode/barcode.component';
import { patientAlertComponent } from './components/patient-alert/patient-alert.component';
import { SpeechRecognitionService } from '@app/core/services/speech-recognition/speech-recognition.service';


@NgModule({
  imports: [
    CommonModule,
    ToastModule,
    SidebarModule,
    CheckboxModule,
    TableModule,
    DropdownModule,
    TabViewModule,
    ReactiveFormsModule,
    DialogModule,
    RadioButtonModule,
    MatCheckboxModule,
    FormsModule,
    AutoCompleteModule,
    TreeTableModule,
    PickListModule,
    MultiSelectModule,
    SpinnerModule,
    ListboxModule,
    TreeModule,
    ConfirmDialogModule,
    FileUploadModule,
    CalendarModule,
    GrowlModule,
    MessagesModule,
    FormsModule,
    ProgressBarModule,
    NgxBarcode6Module,
    AccordionModule,
    InputSwitchModule
  ],
  providers: [
    SafeUrlPipe,
    MessageService,
    DatePipe,
    SpeechRecognitionService
  ],
  declarations: [
    SafeUrlPipe,
    DialogBoxComponent,
    NgxBarcodeComponent,
    patientAlertComponent
  ],
  exports: [
    SafeUrlPipe,
    ToastModule,
    SidebarModule,
    CheckboxModule,
    TableModule,
    DropdownModule,
    TabViewModule,
    ReactiveFormsModule,
    DialogModule,
    RadioButtonModule,
    MatCheckboxModule,
    DialogBoxComponent,
    AutoCompleteModule,
    TreeTableModule,
    PickListModule,
    MultiSelectModule,
    SpinnerModule,
    ListboxModule,
    TreeModule,
    ConfirmDialogModule,
    FileUploadModule,
    CalendarModule,
    GrowlModule,
    MessagesModule,
    ProgressBarModule,
    FormsModule,
    NgxBarcode6Module,
    patientAlertComponent,
    AccordionModule,
    InputSwitchModule
  ]
})
export class SharedModule { }
