
/**
 * Common validator Class
 * This class for implementing custom validators on form fields
 * How to use it ?
 * FormGroup.group{
 *  field: ['', CommonValidator.urlValidator]
 * }
 * Currently it supports
 * Url Validation
 * Zip Code Validation
 * Phone Number Validation
 * Numeric Validation
 * Match Password
 * SSN Verification
 * Invoice Number Verification
 */
export class CommonValidator {
    /**
     * Urls validator
     * @param url it will be string of input field.
     * @returns false or true based on string it is valid or invalid.
     */
    static urlValidator(url): any {
        if (url.pristine) {
            return null;
        }
        const URL_REGEXP = /^(http?|ftp):\/\/([a-zA-Z0-9.-]+(:[a-zA-Z0-9.&%$-]+)*@)*((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9][0-9]?)(\.(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[1-9]?[0-9])){3}|([a-zA-Z0-9-]+\.)*[a-zA-Z0-9-]+\.(com|edu|gov|int|mil|net|org|biz|arpa|info|name|pro|aero|coop|museum|[a-zA-Z]{2}))(:[0-9]+)*(\/($|[a-zA-Z0-9.,?'\\+&%$#=~_-]+))*$/;
        url.markAsTouched();
        if (URL_REGEXP.test(url.value)) {
            return null;
        }
        return {
            invalidUrl: true
        };
    }

    /**
     * Match password
     * @param group Form group object in which password and confirm field is mandatory
     * @returns boolean false/true based on regex condition
     */
    static matchPassword(group): any {
        const password = group.controls.password;
        const confirm = group.controls.confirm;
        if (password.pristine || confirm.pristine) {
            return null;
        }
        group.markAsTouched();
        if (password.value === confirm.value) {
            return null;
        }
        return {
            invalidPassword: true
        };
    }

    /**
     * Numbers validator
     * @param numericValue contains number field value
     * @returns boolean value based on regex condition
     */
    static numberValidator(numericValue): any {
        if (!numericValue.pristine && numericValue.value === '') {
            return null;
        }

        if (numericValue.pristine) {
            return null;
        }
        const NUMBER_REGEXP = /^-?[\d.]+(?:e-?\d+)?$/;
        numericValue.markAsTouched();
        if (NUMBER_REGEXP.test(numericValue.value)) {
            return null;
        }
        return {
            invalidNumber: true
        };
    }

    /**
     * Ssns validator
     * @param ssn contains ssn field value
     * @returns  boolean value based on regex condition
     */
    static ssnValidator(ssn: any): any {
        if (ssn.pristine) {
            return null;
        }
        const SSN_REGEXP = /^(?!219-09-9999|078-05-1120)(?!666|000|9\d{2})\d{3}-(?!00)\d{2}-(?!0{4})\d{4}$/;
        ssn.markAsTouched();
        if (SSN_REGEXP.test(ssn.value)) {
            return null;
        }
        return {
            invalidSsn: true
        };
    }


    /**
     * Phones validator
     * @param phNumber contains phone number field value
     * @returns boolean value based on regex condition
     */
    static phoneValidator(phNumber: any): any {
        if (phNumber.pristine) {
            return null;
        }
        const PHONE_REGEXP = /^(\([0-9]{3}\) |[0-9]{3}-)[0-9]{3}-[0-9]{4}$/;
        phNumber.markAsTouched();
        if (PHONE_REGEXP.test(phNumber.value)) {
            return null;
        }
        return {
            invalidNumber: true
        };
    }


    /**
     * Zips code validator
     * @param zip contains zip code field value
     * @returns boolean value based on regex condition
     */
    static zipCodeValidator(zip: any): any {
        if (zip.pristine) {
            return null;
        }
        const ZIP_REGEXP = /^[0-9]{5}(?:-[0-9]{4})?$/;
        zip.markAsTouched();
        if (ZIP_REGEXP.test(zip.value)) {
            return null;
        }
        return {
            invalidZip: true
        };
    }

    /**
     * Invoices number validator
     * @param invoice contains invoice number field value
     * @returns boolean value based on regex condition
     */
    static invoiceNumberValidator(invoice: any): any {
        if (invoice.pristine) {
            return null;
        }
        const INVOICE_REGEXP = /^[\w\.\/\-(\):]+$/;
        invoice.markAsTouched();
        if (INVOICE_REGEXP.test(invoice.value)) {
            return null;
        }
        return {
            invalidInvoice: true
        };
    }

/**
 * Numbers validator
 * @param numericValue contains number field value
 * @returns boolean value based on regex condition
 */
    static numberDecimalValidator(numericValue): any {
        if (!numericValue.pristine && numericValue.value === '') {
            return null;
        }
        if (numericValue.pristine) {
            return null;
        }
        const NUMBER_REGEXP = /^(?=.?\d{0,2})\d*(\.\d{0,2})?$/;
        numericValue.markAsTouched();
        if (NUMBER_REGEXP.test(numericValue.value)) {
            return null;
        }
        return {
            invalidDecimal: true
        };
    }
}
