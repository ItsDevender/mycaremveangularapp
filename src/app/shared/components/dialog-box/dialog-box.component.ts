import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';


@Component({
    selector: 'dialog-box',
    templateUrl: 'dialog-box.component.html'
})
export class DialogBoxComponent implements OnInit {
    @Output() btnAct: EventEmitter<any> = new EventEmitter();
    display = false;
    header: any = '';
    footer: any = '';
    buttonone: any = '';
    buttontwo: any = '';
    Id: number = 0;
    ngOnInit() {

    }

    dialogBox(objectset, id: number = 0) {
        this.header = objectset.header;
        this.footer = objectset.footer;
        this.buttonone = objectset.buttonone;
        this.buttontwo = objectset.buttontwo;
        this.Id = id;
        this.display = true;
    }
}

