import { Injectable } from '@angular/core';
import { DatePipe } from '@angular/common';

@Injectable({
  providedIn: 'root'
})
export class UtilityService {
  lensPopupOrderCondi = true;
  constructor(private datePipe: DatePipe) { }



  public genDropDowns(configs: any = []): void {
    const s = configs.start; // Start Point
    const e = configs.end; // End Point
    const nt = configs.neutral; // Neutral Point
    const d = configs.step; // Difference
    const data_gen: any = [];
    let i = 0;
    for (i = s; i <= e; i += d) {
      // Fractions Add
      const f = (configs.roundOff > 0) ? i.toLocaleString('en', { useGrouping: false, minimumFractionDigits: configs.roundOff }) : i;
      // Set Slice
      const j = (configs.slice > 0) ? this.sliceNum(Number(f), configs.slice) : f;
      const va = (i > 0 && configs.sign) ? '+' + j : ((i === 0) ? nt : j);
      data_gen.push({ id: va, name: va });
    }
    return data_gen;
  }

  sliceNum(num: number, size: number): string {
    let s = num + '';
    while (s.length < size) { s = '0' + s; }
    return s;
  }

  getObjectLength(data: Object) {
    return Object.keys(data).length
  }


  format(inputJson: any) {
    for (const key in inputJson.value) {
      if (inputJson.value[key] === null || inputJson.value[key] === '') {
        delete inputJson.value[key];
      }
      }
    return inputJson.value;
  }

  /**
   * Formats with out controls for the formating the json object without null and empty
   * @param {object} inputJson 
   * @returns  this will remove all the null and empty keys in the object
   */
  formatWithOutControls(inputJson: any) {
    for (const key in inputJson) {
      if (inputJson[key] == null || inputJson[key] == '') {
        delete inputJson[key];
      }
      }
    return inputJson;
  }

  /**
   * Serializes data
   * @param data Any Json Object from API
   * @returns Json object with replaced null values with empty string.
   */
  serializeData(data: object) {
     return JSON.parse(JSON.stringify(data).replace(/\:null/gi, '\:""'));
  }

  /**
   * Gets array item
   * @param [config] for getting item data
   * @returns  return the data from array
   */
  getArrayItem(config: any = []) {
    let Value;
    if(config.matchValue === null){
      return '';
    }
    switch (config.dataType) {
      case 'parseInt': {
        Value = parseInt(config.matchValue);
        break;
      }
      case 'string': {
        Value = config.matchValue.toString();
        break;
      }
      case 'int': {
        Value = config.matchValue;
        break;
      }
    }

    const val = config.data.filter(p => p[config.matchWith] === Value);
    if (val.length > 0) {
      return val[0][Object.keys(val[0])[config.position]];
    } else {
      return '';
    }
  }

 // Transform Date
 transformDate(date) {
    return this.datePipe.transform(date, 'yyyy-MM-dd 00:00:00');
 }

  /**
   * Numbers only
   * @param event for getting keypress value
   * @returns true if only
   */
  numberOnly(event): boolean {
    const charCode = (event.which) ? event.which : event.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }
}
