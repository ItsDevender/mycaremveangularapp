
import { JwtService } from '@services/jwt.service';
import { ErrorService } from '@services/error.service';
import { AppService } from '@services/app.service';

import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

/**
 * LayOut Component
 * Global component for rendering router-outlet after login.
 */
@Component({
  selector: 'app-layout',
  templateUrl: './layout.component.html',
  styleUrls: ['./layout.component.css']
})
export class LayoutComponent {

  loading: boolean;
  /**
  * Hide html of header component
  */
  hideHtml = environment.prodHideLinks

  /**
  * DescriptionMessage will store valid messages related to Portal SSO
  */
  descriptionMessage: string;

  /**
    * displayMessage will store the value to enable the popup dialog for the End-user
    */
  displayMessage: boolean;

  /**
   * Creates an instance of layout component.
   * @param router Angular Router Library for handling routes.
   * @param service App Service for loading global variables.
   * @param error Error Service for handling errors.
   * @param jwt JWT service for destroying tokens.
   */
  constructor(private router: Router,
    public service: AppService,
    private error: ErrorService,
    private jwt: JwtService) {

  }

  /**
   * Logs out
   * Log Out from application 
   * Destroy Token
   * Show Logout Message
   * Redirects to Login Page
   */
  logOut() {
    this.service.userLoggedIn = false;
    this.jwt.destroyToken();
    this.router.navigate(['login']);
    this.error.displayError({
      severity: 'success',
      summary: 'Authentication',
      detail: 'LogOut Successfully.'
    });
  }

  /**
   * Infos msg show
   * Used for showing messages for which there are no screens.
   */
  infoMsgShow() {
    this.error.displayError({
      severity: 'error',
      summary: 'Error 404',
      detail: 'No Path Found'
    });
  }

  /**
   * pdialog show the messages in popup window format 
   * Used for showing dynamic messages build on the screens.
   */
  pdialogShow(description: string) {
    this.descriptionMessage = description;
    this.displayMessage = true;
  }

  /**
   * Goto demographics
   * @param {string} grid returns string value for showing patient family or demographics.
   */
  gotoDemographics(grid) {
    const patient = this.service.patientId;

    if (patient !== null) {
      this.router.navigate(['/patient/demographics'], { queryParams: { 'grid': grid } });
    }

  }

  /**
   * Gets username
   * @returns  {string} Active Login user username
   */
  getUsername() {
    return this.service.userName;
  }

  /**
   * Go to dash board
   * Redirect to Dashboard page.
   */
  goToDashBoard(event) {
    if (event == 'noPatient') {
      this.error.displayError({
        severity: 'warn',
        summary: 'Warning Message', detail: 'Please Select Patient'
      });
      return;
    }
    this.router.navigate(['/dashboard']);
  }
  /**
   * Reloads pages event
   * @param event for getting navigation endtags
   */
  reloadPagesEvent(event) {
    this.router.navigateByUrl('/').then(() =>
      this.router.navigate([event]));
  }
}
