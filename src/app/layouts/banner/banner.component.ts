import { Router } from '@angular/router';
import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';
import { DropdownService } from '@shared/services/dropdown.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { OrderHistory, OrderFilter } from '@app/core/models/order/order.model';
import { UtilityService } from '@app/shared/services/utility.service';
import { ErrorService } from '@app/core';
@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.css']
})
export class BannerComponent {


  /**
   * @ignore
   */
  @Input() app: any;


  /**
   * Output  of banner component.
   * This will output to layout component to redirect to demographics page.
   */
  @Output() gotoDemographics: EventEmitter<any> = new EventEmitter();


  /**
   * Patients form of banner component
   * Form Group for patient for saving Heard About Us, Status and other things.
   */
  patientsForm: FormGroup;
  // showdivorder: Subscription;


  /**
   * Patientdue  of banner component
   * Used for binding Patient amount balance in HTML.
   */
  patientdue: any;

  /**
   * Patientimage  of banner component
   * Used for binding Patient image in HTML.
   */
  patientimage: any;


  /**
   * Patient name of banner component
   * Used for binding Patient Name in HTML.
   */
  patientName: any;

  /**
   * Patient id of banner component
   * Used for binding Patient Id in HTML.
   */
  PatientId: any;

  /**
   * Last exam of banner component
   * Used for binding Last exam in HTML.
   */
  LastExam: any;

  /**
   * Phone no. of banner component
   * Used for binding Patient Default phone number in HTML.
   */
  PhoneNo: any;

  /**
   * Email of banner component
   * Used for binding Patient email in HTML.
   */
  Email: any;

  /**
   * Patient insurance of banner component
   * Used for binding Patient insurances in HTML.
   */
  Insurance: any[];

  /**
   * Patient Age DOB of banner component
   * Used for binding Patient date of birth and age in HTML.
   */
  patientAgeDob: any;

  /**
   * Patient sex of banner component
   * Used for binding Patient sex in HTML.
   */
  patientSex: any;


  /**
   * Heard about us of banner component
   * Used for binding heard about us drop down in HTML
   */
  heardAboutUs: any;

  /**
   * Account status of banner component
   * Used for binding account status drop down in HTML
   */
  accountStatus: any;

  /**
   * Save patient id of banner component
   * Used for handling multiple API requests.
   */
  // savedPatientData: any;

  // insurance

  /**
   * Insurancemodel  of banner component
   * Used for storing the patientinsurancedata to use for plan selection.
   */
  public insurancemodel: any = [];


  /**
   * Insurance drop down of banner component
   * Used for loading insurancedata to insuranceDropDown .
   */
  public insuranceDropDown = [];

  /**
   * Selectedinsurance id of banner component
   * store the selected insuranceID .
   */
  public selectedinsuranceID: any;

  /**
   * Insuranceplan  of banner component
   * used to fill the plan input for selected insurance  .
   */
  public insuranceplan: string;

  /**
   * Noof alerts of banner component no of alerts
   */
  noofAlerts: any = 0;

  /**
   * Previous banner id of banner component for storing previously selected order id
   */
  previousBannerId: number;
  /**
  /**
   * Creates an instance of banner component.
   * @param _fb Form Builder for grouping forms
   * @param dropDown Drop down service for loading default functions.
   * @param router used for navigation 
   * @param utility for calling numbers only method
   */
  constructor(private _fb: FormBuilder,
    private dropDown: DropdownService,
    private router: Router,
    public utility: UtilityService,
    private errorService: ErrorService) {
  }

  /**
   * Checks for orders dropdowns
   * @returns  {boolean} value showing orders screen dropdown. True for showing and false for hiding drop down.
   */
  checkForOrdersDropdowns() {
    return this.app.forOrders;
  }

  /**
   * Orders enter value
   * @param event  for getting change event value 
   */
  orderEnterValue(event: any) {
    this.app.bannerOrderId = event.target.value;
  }

  /**
   * Patients bar activator event
   * @returns  {boolean} value for showing Patient Banner. True for showing complete banner and false for hiding complete banner.
   */
  patientBarActivatorEvent() {
    if (this.app.bannerOrderId !== this.previousBannerId) {
          this.patientsForm.controls.orderId.patchValue(this.app.bannerOrderId);
          this.previousBannerId = this.app.bannerOrderId;
    }
    if (this.app.validateBanner !== true) {
      return false;
    }
    if (this.app.checkPatientSigned() === true) {
      this.grantAccessPatientBar();
      return true;
    }
  }

  /**
   * Grants access patient bar
   * For Loading data and binding in banner html.
   */
  grantAccessPatientBar() {
    if (this.app.patientId !== this.app.savePatientId) {
      this.app.savePatientId = this.app.patientId;
      if (this.app.patientId !== undefined) {
        this.getpatientHeader();
      }
    }
  }


  /**
   * Binds patientdata
   * @param {any} patientdata for getting banner data
   * @returns {object} banner binding data
   */
  bindPatientdata(patientdata: any) {
    const dt = new Date();
    const pc = patientdata.result.Demographics.Demographic.PreferredContact;
    let Dateob: any;
    let sex: any;
    let dob: any;
    try {
      const agesplit = patientdata.result.Demographics.Demographic.DateOfBirth.split('-');
      this.patientAgeDob = dt.getFullYear() - agesplit[0];
      Dateob = (patientdata.result.Demographics.Demographic.DateOfBirth !== '') ?
        patientdata.result.Demographics.Demographic.DateOfBirth : '';
      sex = (patientdata.result.Demographics.Demographic.Sex !== '') ?
        patientdata.result.Demographics.Demographic.Sex + '|' : '';
      dob = (this.patientAgeDob !== '') ? this.patientAgeDob + '|' : '';
    } catch (error) {
      this.patientAgeDob = '';
      Dateob = '';
      sex = '';
      dob = '';
    }
    this.patientSex = '(' + sex + dob + Dateob + ')';
    this.patientName = patientdata.result.Demographics.Demographic.LastName + ' ' +
      patientdata.result.Demographics.Demographic.FirstName;
    this.PatientId = patientdata.result.Demographics.Demographic.PatientID;
    this.PhoneNo = patientdata.result.Demographics.Demographic[pc];
    this.Email = patientdata.result.Demographics.Demographic.Email;
    if (patientdata.result.Demographics.Insurance !== 'No record.') {
      this.Insurance = patientdata.result.Demographics.Insurance;
    }
  }

  /**
   * Patients image
   * @returns {object} for getting patient image
   */
  patientImage() {
    this.app.getPatientImage(this.app.patientId).subscribe(
      details => {
        this.patientimage = 'data:image/JPEG;base64,' + details.data;
        if (this.patientimage === undefined) {
          // default empty image byte 64
          this.patientimage = 'assets/img/userimg.png';
        }
      },
      error => {
        // default empty image byte 64
        this.patientimage = 'assets/img/userimg.png';
      }
    );
  }

  /**
   * Patients due
   * @returns {object} for getting patient due value
   */
  patientDue() {
    this.app.getPatientDue(this.app.patientId).subscribe(due => {
      this.patientdue = parseFloat(due.total).toFixed(2);
    });
  }

  /**
   * Patients account status
   * @returns {object} for getting patient account status
   */
  patientAccountStatus() {
    this.app.getAccountStatusAll(this.app.patientId).subscribe((data: any) => {
      this.patientsForm.controls.patientAccountStatus.patchValue(data.pat_account_status);
      this.patientsForm.controls.heardAbout.patchValue(data.heard_abt_us);
      this.patientsForm.controls.patientStatus.patchValue(data.patientStatus);
    });
  }

  /**
   * Hears about
   * @returns {object} getting hear about data
   */
  hearAbout() {
    if (this.heardAboutUs !== null || this.heardAboutUs !== '') {
      this.app.getHeardAbout().subscribe((data: any) => {
        this.heardAboutUs = data.data;
      });
    }
  }
  /**
   * Accounts status
   * @returns {object} getting account status data
   */
  AccountStatus() {
    this.app.getAccountStatus().subscribe((data: any) => {
      this.accountStatus = data.data;
    });
  }
  /**
   * Getpatients header
   *  Calling API's for getting patient Data.
   */
  getpatientHeader() {
    if (this.app.patientId !== 0) {
      // Get Demographics
      let patientdata: any = [];

      this.dropDown.getDemographics(this.app.patientId).subscribe(
        data => {
          patientdata = data;
          this.bindPatientdata(patientdata);
          this.patientImage();
          this.patientDue();
          this.patientAccountStatus();
          this.hearAbout();
          this.AccountStatus();
          this.getPatientInsurance();
          this.getPatientAlerts()
        }
      );
    }
  }

  /**
   * on init
   * Intializing Form and Loading Default functions required for loading banner.
   */
  ngOnInit() {
    this.initializePatientForm();
    this.app.insuranceValue = '';
  }

  /**
   * Intializes patient form
   * Intialize Forms Values
   */
  initializePatientForm() {
    this.patientsForm = this._fb.group({
      patientStatus: null,
      patientAccountStatus: null,
      heardAbout: null,
      report: false,
      vip: false,
      hs: false,
      insuranceplan: '',
      orderId: ''
    });

  }


  /**
   * Updates patient status
   * For Updating patient Status like heard about us etc.
   */
  updatePatientStatus() {

    const patientAccountStatus = this.patientsForm.controls.patientAccountStatus.value;
    const heardAbout = this.patientsForm.controls.heardAbout.value;
    const patientStatus = this.patientsForm.controls.patientStatus.value;

    const dataref = {
      patientStatus,
      pat_account_status: patientAccountStatus,
      heard_abt_us: heardAbout
    };
    this.app.updatePatientStatus(dataref).subscribe((data: any) => { });
  }

  /**
   * Loads patient insurance will load all the insurances for selected patient
   */
  getPatientInsurance() {

    if (this.app.patientId !== 0) {
      const id = this.app.patientId;
      this.insuranceDropDown = [];
      this.dropDown.getPatientInsurance(id).subscribe(data => {
        this.insurancemodel = data;
        this.insurancemodel.forEach(element => {
          this.insuranceDropDown.push({ Id: element.id, Name: element.type + '-' + element.get_provider_name.name });
        });
      });
    }
  }


  /**
   * Insuraneselect will automatically select the respective plan related to the insurance
   * Input Parameter
   * event-stores the selected event to get the selected value
   */
  Insuraneselect(event) {
    this.app.insuranceValue = event.target.value;
    this.patientsForm.controls.insuranceplan.patchValue('');
    this.insurancemodel.forEach(element => {
      if (element.id.toString() === event.target.value) {
        this.patientsForm.controls.insuranceplan.patchValue(element.get_provider_name.name);
      }
    });


  }
  alertsClick() {
    this.app.displayalertsPopUp = true
  }
  getPatientAlerts() {
    if (this.app.patientId !== 0) {
      this.dropDown.patientAlerts(this.app.patientId).subscribe(resp => {
        this.noofAlerts = resp['total']
      })
    }
  }


  /**
   * Orders search for navigating to specified order location by order id
   */
  orderSearch() {
    const payLoad: OrderFilter = {
      filter: [{ field: 'id', operator: '=', value: this.patientsForm.controls.orderId.value }],
      sort: [{ field: 'id', order: 'DESC' }]
    };
    this.app.getOrderBySearchFilter(payLoad).subscribe(
      (res: OrderHistory) => {
        let mdata = [];
        mdata = res.data;
        const PatientId = mdata[0].patient_id;
        this.app.patientId = 0;
        this.app.patientId = PatientId;
        this.app.isPatientSigned = true;
        this.navigateToOrders(mdata[0].id, mdata[0].ordertypeid);
      },
      error => {
        this.errorService.displayError({
          severity: 'warn',
          summary: 'Warning Message', detail: 'Order Not found'
      });
      }
  );
  }


  /**
   * Navigates to orders
   * @param {number} rowData for getting selected row data
   * @param {number} ordertype for getting order type data
   */
  navigateToOrders(rowData: number, orderType: number) {
    this.router.navigateByUrl('/').then(success => {
      switch (orderType) {
        case 1: {
          this.router.navigate(['/order/frame']);
          break;
        }
        case 7: {
          this.router.navigate(['/order/spectacle-lens']);
          break;
        }
        case 4: {
          this.router.navigate(['/order/contact-lens']);
          break;
        }
        case 5: {
          this.router.navigate(['/order/contact-lens']);
          break;
        }
        case 6: {
          this.router.navigate(['/order/other-details']);
          break;
        }
      }

      this.app.ordersearchvalueService = [];
      const data = {
        Id: rowData,
        OrderType: orderType
      };

      this.app.ordersearchvalueService.push(data);
    });
  }

  /**
   * Orders enter event
   * @param event for getting keypress event value
   */
  orderEnterEvent(event: any) {
    if (event.keyCode === 13) {
      this.orderSearch();
    }
  }
}
