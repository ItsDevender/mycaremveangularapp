import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

// Modules
import { CoreModule } from './core';
import { SharedModule } from '@app/shared';

// Routing Module
import { AppRoutingModule } from './app-routing.module'; 

// Components
import { AppComponent } from './app.component';
import { HeaderComponent, LeftnavComponent} from './layouts';
import { LayoutComponent } from './layouts/layout.component';

import { MatProgressBarModule } from '@angular/material/progress-bar';
import { BannerComponent } from './layouts/banner/banner.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    LeftnavComponent,
    LayoutComponent,
    BannerComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    BrowserAnimationsModule,
    CoreModule,
    SharedModule,
    MatProgressBarModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
