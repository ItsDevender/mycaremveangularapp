import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientMergeComponent } from './merge.component';

describe('PatientMergeComponent', () => {
  let component: PatientMergeComponent;
  let fixture: ComponentFixture<PatientMergeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatientMergeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientMergeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
