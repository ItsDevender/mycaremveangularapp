// App Configuration Module
import { IframesService } from '@shared/services/iframes.service';
// Core Modules
import { Component} from '@angular/core';


// Setting Up Component
@Component({
  selector: 'app-patient-merge',
  templateUrl: './merge.component.html'
})
export class PatientMergeComponent {

  PtMergeUrl: Object;

  constructor(private service: IframesService) {
        this.PtMergeUrl = this.service.routeUrl('PtMergeUrl', true);
  }

  onLoad() {
    this.service.resizeIframe();
    $('.loader-iframe').fadeOut();
  }

}
