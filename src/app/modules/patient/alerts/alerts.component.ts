import { Component, OnInit } from '@angular/core';



@Component({
    selector: 'app-patient-alerts',
    templateUrl: 'alerts.component.html'
})
export class PatientAlertsComponent implements OnInit {

    addPatientAlert = false;


    // @ViewChild('deleteRow') DeleteRows: DialogBoxComponent;
    cols: any[];
    gridlists: any[];
    ngOnInit() {

        this.cols = [
            { field: 'date', header: 'Date' },
            { field: 'employee', header: 'Employee' },
            { field: 'alert', header: 'Alert' },
            { field: 'alertlevel', header: 'Alert Level' },
            { field: 'alertexpiry', header: 'Alert Expiry' },
            { field: 'category', header: 'Category' },
            { field: 'type', header: 'Type' },
            { field: 'notes', header: 'Notes' }
        ];
        this.gridlists = [
            {
                'date': '10-04-2018 05:12 pm',
                'employee': 'Admin',
                'alert': ' ',
                'alertlevel': 'Global',
                'alertexpiry': '10-05-2018',
                'category': 'Appointment Rem',
                'type': 'Patient',
                'notes': 'Viewable outsie of My Vission Express'
            },
            {
                'date': '10-04-2018 05:12 pm',
                'employee': 'Admin',
                'alert': ' ',
                'alertlevel': 'Global',
                'alertexpiry': '10-05-2018',
                'category': 'Appointment Rem',
                'type': 'Patient',
                'notes': 'Viewable outsie of My Vission Express'
            },
            {
                'date': '10-04-2018 05:12 pm',
                'employee': 'Admin',
                'alert': ' ',
                'alertlevel': 'Global',
                'alertexpiry': '10-05-2018',
                'category': 'Appointment Rem',
                'type': 'Patient',
                'notes': 'Viewable outsie of My Vission Express'
            },
            {
                'date': '10-04-2018 05:12 pm',
                'employee': 'Admin',
                'alert': ' ',
                'alertlevel': 'Global',
                'alertexpiry': '10-05-2018',
                'category': 'Appointment Rem',
                'type': 'Patient',
                'notes': 'Viewable outsie of My Vission Express'
            },
            {
                'date': '10-04-2018 05:12 pm',
                'employee': 'Admin',
                'alert': ' ',
                'alertlevel': 'Global',
                'alertexpiry': '10-05-2018',
                'category': 'Appointment Rem',
                'type': 'Patient',
                'notes': 'Viewable outsie of My Vission Express'
            },
            {
                'date': '10-04-2018 05:12 pm',
                'employee': 'Admin',
                'alert': ' ',
                'alertlevel': 'Global',
                'alertexpiry': '10-05-2018',
                'category': 'Appointment Rem',
                'type': 'Patient',
                'notes': 'Viewable outsie of My Vission Express'
            },
            {
                'date': '10-04-2018 05:12 pm',
                'employee': 'Admin',
                'alert': ' ',
                'alertlevel': 'Global',
                'alertexpiry': '10-05-2018',
                'category': 'Appointment Rem',
                'type': 'Patient',
                'notes': 'Viewable outsie of My Vission Express'
            },
            {
                'date': '10-04-2018 05:12 pm',
                'employee': 'Admin',
                'alert': ' ',
                'alertlevel': 'Global',
                'alertexpiry': '10-05-2018',
                'category': 'Appointment Rem',
                'type': 'Patient',
                'notes': 'Viewable outsie of My Vission Express'
            },
            {
                'date': '10-04-2018 05:12 pm',
                'employee': 'Admin',
                'alert': ' ',
                'alertlevel': 'Global',
                'alertexpiry': '10-05-2018',
                'category': 'Appointment Rem',
                'type': 'Patient',
                'notes': 'Viewable outsie of My Vission Express'
            },
            {
                'date': '10-04-2018 05:12 pm',
                'employee': 'Admin',
                'alert': ' ',
                'alertlevel': 'Global',
                'alertexpiry': '10-05-2018',
                'category': 'Appointment Rem',
                'type': 'Patient',
                'notes': 'Viewable outsie of My Vission Express'
            },
            {
                'date': '10-04-2018 05:12 pm',
                'employee': 'Admin',
                'alert': ' ',
                'alertlevel': 'Global',
                'alertexpiry': '10-05-2018',
                'category': 'Appointment Rem',
                'type': 'Patient',
                'notes': 'Viewable outsie of My Vission Express'
            },
        ];
    }
    constructor() {

    }

    // deleteRows() {
    //     this.DeleteRows.dialogBox();
    // }


}

