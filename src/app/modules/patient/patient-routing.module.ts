import { AddPatientComponent,
    AppointmentsComponent,
    DemographicsComponent,
    PatientAlertsComponent,
    PatientDocsComponent,
    PatientInsuranceComponent,
    PatientMergeComponent,
    PatientOrderHistoryComponent,
    PatientSearchComponent,
    PatientTrackComponent
    } from './';


import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
const routes: Routes = [
  {
    path: '',
    children: [
        {
            path: 'demographics',
            component: DemographicsComponent,
            data: { 'title': 'Demographics - Patient', 'banner' : true }
        },
        {
            path: 'family',
            component: DemographicsComponent,
            data: { 'title': 'Family - Patient', 'banner' : true }
        },

        {
            path: 'insurance',
            component: PatientInsuranceComponent,
            data: { 'title': 'Insurance - Patient', 'banner' : true }
        },
        {
            path: 'search',
            component: PatientSearchComponent,
            data: { 'title': 'Search - Patient', 'banner' : true }
        },
        {
            path: 'documents',
            component: PatientDocsComponent,
            data: { 'title': 'Patient Documents', 'banner' : true }
        },
        {
            path: 'patientorderhistory',
            component: PatientOrderHistoryComponent,
            data: { 'title': 'Patient Order History', 'banner' : true }
        },
        {
            path: 'merge',
            component: PatientMergeComponent,
            data: { 'title': 'merge - Patient', 'banner' : false }
        },
        {
            path: 'add-patient',
            component: AddPatientComponent,
            data: { 'title': 'add-patient - Patient', 'banner' : false }
        },
        {
            path: 'alerts',
            component: PatientAlertsComponent,
            data: { 'title': 'alerts - Patient', 'banner' : false }
        },
        {
            path: 'appointments',
            component: AppointmentsComponent,
            data: { 'title': 'Appointments - Patient', 'banner' : true }
        },
        {
            path: 'track',
            component: PatientTrackComponent,
            data: { 'title': 'Tracker', 'banner' : false }
        }

    ]

  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PatientRoutingModule { }
