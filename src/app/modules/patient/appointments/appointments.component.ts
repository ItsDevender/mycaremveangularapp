// Core Modules
import { Component } from '@angular/core';

// Frame Service
import { IframesService } from '@shared/services/iframes.service';

@Component({
  selector: 'app-appointments',
  templateUrl: './appointments.component.html'
})
export class AppointmentsComponent {
  AppointmentsURL: Object;
  constructor(private service: IframesService) {
       this.AppointmentsURL = this.service.routeUrl('AppointmentsURL', true);
  }

  onLoad(e) {
    this.service.resizeIframe();
    $('.loader-iframe').fadeOut();
  }

}
