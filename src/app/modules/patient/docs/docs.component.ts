// Service
import { IframesService } from '@shared/services/iframes.service';
// Core Modules
import { Component } from '@angular/core';


// Setting Up Component
@Component({
  selector: 'app-patient-docs',
  templateUrl: './docs.component.html'
})


export class PatientDocsComponent {


  PtDocsUrl: Object;

  constructor(private service: IframesService) {
      this.PtDocsUrl = this.service.routeUrl('PtDocsUrl', true);
  }

  onLoad() {
    this.service.resizeIframe();
    $('.loader-iframe').fadeOut();
  }

}
