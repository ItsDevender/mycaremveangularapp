import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PatientDocsComponent } from './docs.component';

describe('PatientDocsComponent', () => {
  let component: PatientDocsComponent;
  let fixture: ComponentFixture<PatientDocsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PatientDocsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PatientDocsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
