import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OcularComponent } from './ocular.component';

describe('OcularComponent', () => {
  let component: OcularComponent;
  let fixture: ComponentFixture<OcularComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OcularComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OcularComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
