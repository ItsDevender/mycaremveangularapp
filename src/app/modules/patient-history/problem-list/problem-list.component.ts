// Core Modules
import { Component, OnInit } from '@angular/core';
import { IframesService } from '@app/shared/services/iframes.service';


// Setting Up Component
@Component({
  selector: 'app-problem-list',
  templateUrl: './problem-list.component.html'
})
export class ProblemListComponent implements OnInit {
  PatientHistoryUrl: Object;
  constructor(private service: IframesService) {
     this.PatientHistoryUrl = this.service.routeUrl('PatientHistoryUrl', true, '?showpage=problem_list');
  }

  ngOnInit() {
  }


  onLoad() {
    this.service.resizeIframe();
    $('.loader-iframe').fadeOut();
  }

}
