import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SxProceduresComponent } from './sx-procedures.component';

describe('SxProceduresComponent', () => {
  let component: SxProceduresComponent;
  let fixture: ComponentFixture<SxProceduresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SxProceduresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SxProceduresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
