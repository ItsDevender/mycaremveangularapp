// Modules
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AllergiesComponent } from './allergies/allergies.component';
import { CcHistoryComponent } from './cc-history/cc-history.component';
import { GeneralHealthComponent } from './general-health/general-health.component';
import { ImmunizationsComponent } from './immunizations/immunizations.component';
import { LabComponent } from './lab/lab.component';
import { MedicationComponent } from './medication/medication.component';

import { ProblemListComponent } from './problem-list/problem-list.component';
import { OrderSetsComponent } from './order-sets/order-sets.component';
import { OcularComponent } from './ocular/ocular.component';
import { RadComponent } from './rad/rad.component';



// Components
import { SxProceduresComponent } from './sx-procedures/sx-procedures.component';
import { TestsComponent } from './tests/tests.component';
import { VitalSignsComponent } from './vital-signs/vital-signs.component';

const routes: Routes = [
  {
      path: '',
      children: [
          {
              path: 'ocular',
              component: OcularComponent,
              data: { 'title': 'Ocular  Patient History', 'banner' : true }
          },
          {
              path: 'general-health',
              component: GeneralHealthComponent,
              data: { 'title': 'General Health  Patient History', 'banner' : true }
          },
          {
              path: 'medication',
              component: MedicationComponent,
              data: { 'title': 'Medication  Patient History', 'banner' : true }
          },
          {
              path: 'allergies',
              component: AllergiesComponent,
              data: { 'title': 'Allergies  Patient History', 'banner' : true }
          },
          {
              path: 'sx_procedures',
              component: SxProceduresComponent,
              data: { 'title': 'SX Procedures  Patient History', 'banner' : true }
          },
          {
              path: 'immunizations',
              component: ImmunizationsComponent,
              data: { 'title': 'Immunizations  Patient History', 'banner' : true }
          },
          {
              path: 'cc-history',
              component: CcHistoryComponent,
              data: { 'title': 'CC History  Patient History', 'banner' : true }
          },
          {
              path: 'vital-signs',
              component: VitalSignsComponent,
              data: { 'title': 'Vital Signs  Patient History', 'banner' : true }
          },
          {
              path: 'order-sets',
              component: OrderSetsComponent,
              data: { 'title': 'Order Sets  Patient History', 'banner' : true }
          },
          {
              path: 'problem-list',
              component: ProblemListComponent,
              data: { 'title': 'Problem List  Patient History', 'banner' : true }
          },
          {
              path: 'lab',
              component: LabComponent,
              data: { 'title': 'LAB  Patient History', 'banner' : true }
          },
          {
              path: 'rad',
              component: RadComponent,
              data: { 'title': 'RAD  Patient History', 'banner' : true }
          },
          {
              path: 'tests',
              component: TestsComponent,
              data: { 'title': 'Tests  Patient History', 'banner' : true }
          }
      ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class PatientHistoryRoutingModule { }
