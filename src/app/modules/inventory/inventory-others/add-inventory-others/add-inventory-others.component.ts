import { Component, OnInit, Input, OnDestroy, EventEmitter, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { Subscription } from 'rxjs';
import { OthersService } from '@services/inventory/others.service';
import { FramesService } from '@services/inventory/frames.service';
import { ErrorService } from '@app/core/services/error.service';
import { InventoryService } from '@app/core/services/inventory/inventory.service';
import { UtilityService } from '@app/shared/services/utility.service';
import { DropdownService } from '@app/shared/services/dropdown.service';
import { GetProcedureCode, Data, InventoryResultDetails } from '@app/core/models/inventory/inventoryItems.model';
import { OtherInventoryFormData } from '@app/core/models/inventory/addotherinventory.model';

import {
    CommissionStructure,
    CommissionType,
    FrameSupplier,
    OtherInvCategory,
    LocationsMaster,
} from '@app/core/models/masterDropDown.model';


@Component({
    selector: 'app-add-inventory-others',
    templateUrl: 'add-inventory-others.component.html'
})
export class AddInventoryOthersComponent implements OnInit, OnDestroy {

    public othersForm: FormGroup;
    upcdisable = false;
    addOtherInvTransaction = false;
    transactionsBtnHide = false;
    public supplierslist: any[];
    private locationlist: any[];
    private categorylist: any;
    categories: any[];
    /**
     * Suppliers  of add inventory others component
     */
    suppliers: Array<object>;
    orderTypes: any[];
    locations: Array<LocationsMaster>;
    vendorId: string;
    postotherinventory: any;
    transacctionStockDetails: any = [];
    StockDetails: any = [];
    UpdateStock: any;
    locationid = '';
    subscriptionStockDetailsUpdateOther: Subscription;
    FilteredTrasaction: any = [];
    TypeDropData: any[] = [];
    StructureDropData: any[] = [];
    listImages = [];
    getlistImage = [];
    uploadedFiles = [];
    @Input() imageItem: any;
    @Output() otherAddItem = new EventEmitter<any>();
    locationsList: any;
    /**
     * Inventory save data updated data
     */
    inventorySavedData: any;
    /**
     * Selectedotherid  stores selected item id
     */
    selectedotherid: any;
    frameTrasactionItemId = '';
    /**
     * Procedurecoderesults  of add inventory others component
     * Holding procedure code data
     */
    procedurecoderesults: Array<Data>;
    /**
     * Filtered procedures of add inventory others component
     * Holding procedure code name
     */
    filteredProcedures: Array<object>;
    /**
     * Saving pay load of add inventory others component
     * Holding form controller values
     */
    savingPayLoad: OtherInventoryFormData;
    /**
     * Creates an instance of add inventory others component.
     * @param _fb formbuilder
     * @param otherinventoryService  provide other service 
     * @param utility  provide utility service
     * @param frameService provide frameservice
     * @param inventoryCommonService  provide inventorycommon service
     * @param error provide error service
     */
    constructor(
        private _fb: FormBuilder,
        private otherinventoryService: OthersService,
        private utility: UtilityService,
        private frameService: FramesService,
        private inventoryCommonService: InventoryService,
        private error: ErrorService,
        private dropdownService: DropdownService
    ) {


    }


    ngOnInit() {

        this.GetSupplierDetails();
        this.GetcategoryDetails();
        this.Loadformdata();
        this.loadStructures();
        this.loadCommisions();
        this.LoadLocations();

    }
    ngOnDestroy() {

    }
    /**
     * Loads locations
     * @returns location details
     */
    LoadLocations() {

        this.locations = this.dropdownService.locationsData;
        if (this.utility.getObjectLength(this.locations) === 0) {
            this.dropdownService.getLocationsDropData().subscribe(
                (data: LocationsMaster[]) => {
                    try {
                        this.locations.push({ id: '', name: 'Select Location' });
                        for (let i = 0; i <= data.length - 1; i++) {
                            this.locations.push(data[i]);
                        }
                        this.dropdownService.locationsData = this.locations;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }
                }
            );
        }
    }
    /**
     * Loads commisions
     * @returns commision
     */
    loadCommisions() {
        this.TypeDropData = this.inventoryCommonService.commissionType;
        if (this.utility.getObjectLength(this.TypeDropData) === 0) {
            this.inventoryCommonService.getCommissionTypedropData().subscribe((values: CommissionType) => {
                try {
                    this.TypeDropData.push({ Id: '', Name: 'Select Type' });
                    const dropData  = values.data;
                    for (let i = 0; i <= dropData.length - 1; i++) {
                        this.TypeDropData.push({ Id: dropData[i].id.toString(), Name: dropData[i].commissiontype });
                    }
                    this.inventoryCommonService.commissionType = this.TypeDropData;
                } catch (error) {
                    this.error.syntaxErrors(error);
                }
            });
        }
    }
    /**
     * Loads structures
     * @returns structure data
     */
    loadStructures() {
        this.StructureDropData = this.inventoryCommonService.structureData;
        if (this.utility.getObjectLength(this.StructureDropData) === 0) {

            this.inventoryCommonService.getStructuredropData().subscribe((values: CommissionStructure) => {
                try {
                    this.StructureDropData.push({ Id: '', Name: 'Select Structure' });
                    const dropData  = values.data;
                    for (let i = 0; i <= dropData.length - 1; i++) {
                        this.StructureDropData.push({ Id: dropData[i].id, Name: dropData[i].structure_name });
                    }
                    this.inventoryCommonService.structureData = this.StructureDropData;
                } catch (error) {
                    this.error.syntaxErrors(error);
                }
            });
        }
    }

    /**
     * Loadformdatas add inventory others component
     * @returns getting data
     */
    Loadformdata() {
        this.upcdisable = false;
        this.frameTrasactionItemId = this.inventoryCommonService.FortrasactionItemID;
        this.othersForm = this._fb.group({

            upc_code: ['', [Validators.required]],
            name: ['', [Validators.required]],
            categoryid: ['', [Validators.required]],
            vendor_id: '',
            retail_price: null,
            wholesale_cost: null,
            inventory: false,
            send_to_lab_flag: false,
            print_on_order: false,
            procedure_code: '',
            modifier_id: '',
            structure_id: '0',
            notes: '',
            profit: '',
            min_qty: '',
            max_qty: '',
            locationid: '',
            commission_type_id: '',
            spiff: '',
            gross_percentage: '',
            amount: '',
            module_type_id: "6",
            imagesList: this._fb.array([
            ])


        });
        this.selectedotherid = this.otherinventoryService.editotherinventoryid ||
            this.otherinventoryService.editotherinventoryidDoubleClick;

        if (this.selectedotherid) {
            this.upcdisable = true;
            this.inventoryFormDetails();

        }
        let duplicateselectedotherid = this.otherinventoryService.editDuplicateotherinventoryid;
        if (duplicateselectedotherid) {
            this.selectedotherid = '';
            this.upcdisable = false;
            this.inventoryFormDetails();
            //    / this.othersForm.controls.upc.patchValue('');

        }

    }



    /**
     * Getcategorys details
     * @returns categoty data
     */
    GetcategoryDetails() {
        this.categories = this.otherinventoryService.categoriesData;
        if (this.utility.getObjectLength(this.categories) === 0) {
            this.otherinventoryService.getcategorydata().subscribe((values: OtherInvCategory) => {
                    try {
                        const dropData = values.data;
                        this.categories.push({ Id: '', Name: 'Select Category' });
                        for (let i = 0; i <= dropData.length - 1; i++) {
                            this.categories.push({ Id: dropData[i].id.toString(), Name: dropData[i].categoryname });
                        }
                        this.otherinventoryService.categoriesData = this.categories;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }
                });

        }

    }


    /**
     * Profitcalculates add inventory others component
     * @returns calculate the profit
     */
    profitcalculate() {
        if (this.othersForm.controls['wholesale_cost'].value !== '' ||
            this.othersForm.controls['wholesale_cost'].value != null ||
            this.othersForm.controls['retail_price'].value !== '' ||
            this.othersForm.controls['retail_price'].value != null) {
            const diffvalue = this.othersForm.controls['retail_price'].value - this.othersForm.controls['wholesale_cost'].value;
            this.othersForm.controls['profit'].patchValue(diffvalue.toFixed(2));
        }
        if (this.othersForm.controls['wholesale_cost'].value === '' && this.othersForm.controls['retail_price'].value === '') {
            this.othersForm.controls['profit'].patchValue('');
        }
    }

    /**
     * Gets supplier details
     * @returns supplier data
     */
    GetSupplierDetails() {
        this.suppliers = this.dropdownService.frameSupplier;
        if (this.utility.getObjectLength(this.suppliers) === 0) {
            this.dropdownService.getSupplierDropData().subscribe(
                (values: FrameSupplier) => {
                    try {
                        const dropData  = values.data;
                        this.suppliers.push({ Id: '', Name: 'Select Supplier' });
                        for (let i = 0; i <= dropData.length - 1; i++) {
                                this.suppliers.push({ Id: dropData[i].id, Name: dropData[i].vendor_name });
                        }
                        this.dropdownService.frameSupplier = this.suppliers;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }
                });
        }
    }

    /**
     * Checkminmaxvalues add inventory others component
     * @returns true if checkminmaxvalue else false
     */
    checkminmaxvalue(): boolean {
        if (this.othersForm.controls['min_qty'].value !== undefined) {
            const minvalue = parseInt(this.othersForm.controls['min_qty'].value, 10);
            const maxvalue = parseInt(this.othersForm.controls['max_qty'].value, 10);
            if (maxvalue != null && maxvalue !== 0 && minvalue != null && minvalue !== 0 && minvalue > maxvalue) {
                this.error.displayError({
                    severity: 'warn',
                    summary: 'Warning Message', detail: 'Min qty should be less then Max qty/Max qty should be Grater then Min qty.'
                });




                return false;
            }
        }
    }


    /**
     * Numbers only
     * @returns true 
     */
    numberOnly(event): boolean {
        const charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
    }

    /**
     * Cancelotherinventorys add inventory others component
     * @returns close the other inventory page
     */
    cancelotherinventory() {
        const item = {
            "status": 'othercancle',
            "data": ''
        }
        this.otherAddItem.emit(item);

    }

    /**
     * Saveotherinventorys add inventory others component
     * @returns {object} for saving item 
     */
    saveotherinventory() {
        this.structureformcontrols();
        const savePayload = this.othersForm.value;
        delete savePayload.imagesList;
        this.saveRequestPayLoad();
        // removeControl('imagesList');
        if (this.selectedotherid > 0) {
            this.otherinventoryService.updateInventoryOthers(this.utility.formatWithOutControls(this.savingPayLoad), this.selectedotherid)
                .subscribe((data: InventoryResultDetails) => {
                    this.inventorySavedData = data;
                    this.savingImage(this.inventorySavedData);
                    const item = {
                        status: 'Updated',
                        data: this.inventorySavedData.item_Id
                    };
                    this.otherAddItem.emit(item);
                    this.inventorySucessMsgs('Updated');
                },
                    error => {
                        // this.ErrorShow();
                        this.error.displayError({
                            severity: 'error',
                            summary: 'Error Message', detail: 'Save Failed'
                        });


                    }
                );
        } else {
            this.otherinventoryService.SaveOtherdetailsData(this.utility.formatWithOutControls(this.savingPayLoad))
                .subscribe((data: InventoryResultDetails) => {
                    this.inventorySavedData = data;
                    this.savingImage(this.inventorySavedData);
                    const item = {
                        status: 'save',
                        data: this.inventorySavedData.item_Id
                    };
                    this.otherAddItem.emit(item);
                    this.inventorySucessMsgs('Saved');
                },
                    error => {
                        // this.ErrorShow();
                        this.error.displayError({
                            severity: 'error',
                            summary: 'Error Message', detail: 'Save Failed'
                        });


                    }

                );
        }
    }
    /**
     * Inventorys sucess msgs
     * @returns success or failure messages 
     */
    inventorySucessMsgs(message) {
        this.error.displayError({
            severity: 'success',
            summary: 'Success Message', detail: 'Other Inventory ' + message + ' Successfully'
        });

    }

    /**
     * Saving image
     * @returns image data 
     */
    savingImage(data) {
        if (this.imageItem === undefined) {
            if (data) {
                for (let i = 0; i <= this.uploadedFiles.length - 1; i++) {
                    const formData: FormData = new FormData();
                    formData.append('itemId', data['item_Id']);

                    formData.append('item_pic', this.uploadedFiles[i]);

                    this.frameService.framesImageAdding(formData).subscribe(value => {

                    });
                }

            }
        } else {
            for (let i = 0; i <= this.uploadedFiles.length - 1; i++) {
                const formData: FormData = new FormData();

                formData.append('itemId', this.imageItem);

                formData.append('item_pic', this.uploadedFiles[i]);
                this.frameService.framesImageAdding(formData).subscribe(value => {

                });
            }
        }

        this.postotherinventory = data;
        if (this.postotherinventory.status !== undefined && this.postotherinventory.status === "New record added successfully") {
            // this.otherAddItem.emit('othersave');

        }
    }


    /**
     * Adds other transaction
     * @returns other transaction 
     */
    addOtherTransaction() {
        if (this.frameTrasactionItemId !== '') {
            this.addOtherInvTransaction = true;
        } else {
            this.error.displayError({
                severity: 'error',
                summary: 'Error Message', detail: ' Select any item or save new item for saving transactions'
            });

            this.addOtherInvTransaction = false;
        }
    }

    /**
     * Determines whether other tabs click on
     * @returns display transcation tab 
     */
    onOtherTabsClick(event) {
        if (event.index === 0) {
            this.transactionsBtnHide = false;
        }
        if (event.index === 1) {
            this.transactionsBtnHide = false;
        }
        if (event.index === 2) {
            this.transactionsBtnHide = true;
            this.filterTransactionOnLocation();
        }
    }
    /**
     * Gets other lenstock details
     * @returns 
     */
    getOtherLenstockDetails() {
        let stockData: any = [];
        this.frameService.getstockDetail().subscribe(data => {
            stockData = data['data'];
            for (let i = 0; i <= stockData.length - 1; i++) {
                const loca = this.locationlist.filter(x => x.Id === stockData[i]['loc_id']);
                stockData[i]['loc_id'] = loca[0]['Name'];
                stockData[i]['trans_type'] = stockData[i]['transaction_type']['transactiontype'];

            }
            this.StockDetails = stockData;
            this.transacctionStockDetails = this.StockDetails;

        });

    }
    /**
     * Sales api
     * @returns stock details
     */
    salesApi() {
        this.transacctionStockDetails = [];
        for (let i = 0; i <= this.StockDetails.length - 1; i++) {
            if (this.StockDetails[i]['order_id'] > 0) {
                this.transacctionStockDetails.push(this.StockDetails[i]);
            }
        }

    }
    /**
     * Trasactions api
     * @returns transaction details
     */
    trasactionApi() {
        this.transacctionStockDetails = [];
        for (let i = 0; i <= this.StockDetails.length - 1; i++) {
            if (this.StockDetails[i]['order_id'] >= 0) {
                this.transacctionStockDetails.push(this.StockDetails[i]);

            }
        }

    }
    /**
     * Filters transaction on location
     * @returns filter transaction id details
     */
    filterTransactionOnLocation() {
        let transactionLocation: any = [];
        this.transacctionStockDetails = [];
        this.FilteredTrasaction = [];
        this.StockDetails = [];
        if (this.inventoryCommonService.FortrasactionItemID === '') {
            this.transacctionStockDetails = [];
        } else {
            const dataFilter = {
                filter: [
                    {
                        field: 'item_id',
                        operator: '=',
                        value: this.inventoryCommonService.FortrasactionItemID,
                    }
                ]
            };
            this.frameService.filterTrasactionData(dataFilter).subscribe(data => {
                transactionLocation = data['data']
                for (let i = 0; i <= transactionLocation.length - 1; i++) {

                    transactionLocation[i]['trans_type'] = transactionLocation[i]['transaction_type']['transactiontype'];

                }
                this.StockDetails = transactionLocation;
                this.transacctionStockDetails = this.StockDetails;

            });
        }

    }
    /**
     * Filters transaction on location based
     * @returns filter transcations based on location
     */
    filterTransactionOnLocationBased() {
        this.transacctionStockDetails = [];
        this.FilteredTrasaction = [];
        this.StockDetails = [];
        if (this.inventoryCommonService.FortrasactionItemID !== '') {
            if (this.othersForm.controls['locationid'].value === '') {
                this.filterTransactionOnLocation();
            } else {
                const LocationBased = {
                    filter: [
                        {
                            field: 'item_id',
                            operator: '=',
                            value: this.inventoryCommonService.FortrasactionItemID,
                        },
                        {
                            field: 'loc_id',
                            operator: '=',
                            value: this.othersForm.controls['locationid'].value,
                        }
                    ]
                };
                this.frameService.filterTrasactionData(LocationBased).subscribe(data => {
                    this.FilteredTrasaction = data['data'];
                    this.StockDetails = this.FilteredTrasaction;
                    for (let i = 0; i <= this.StockDetails.length - 1; i++) {

                        this.StockDetails[i]['trans_type'] = this.StockDetails[i]['transaction_type']['transactiontype'];

                    }
                    this.transacctionStockDetails = this.StockDetails;

                });
            }
        } else {
            this.transacctionStockDetails = [];
        }
    }
    /**
     * Keys press
     * @returns alloow only numbers
     */
    keyPress(event: any) {
        const pattern = /[0-9]/;

        const inputChar = String.fromCharCode(event.charCode);
        if (event.keyCode !== 8 && !pattern.test(inputChar)) {
            event.preventDefault();
        }
    }
    /**
     * Others transevent
     * @returns display transactions
     */
    OtherTransevent(data) {
        if (data === 'cancle') {
            this.addOtherInvTransaction = false;
        } else {
            this.UpdateStock = data;
            this.addOtherInvTransaction = false;
            this.filterTransactionOnLocation();
        }
    }

    /**
     * Adds add inventory others component
     * @returns add the image details
     */
    add(event) {

        for (const file of event.target.files) {
            this.uploadedFiles.push(file);
            const reader = new FileReader();
            reader.readAsDataURL(file);
            const control = <FormArray>this.othersForm.controls.imagesList;
            reader.onload = (value) => { // called once readAsDataURL is completed
                control.push(
                    this._fb.group({
                        Image: value.target['result']
                    })
                );
            };
        }

    }
    /**
     * Deletes image
     * @returns delete the image details
     */
    deleteImage(i) {
        if (this.imageItem === undefined) {
            const control = <FormArray>this.othersForm.controls['imagesList'];
            control.removeAt(i);
            this.uploadedFiles.splice(i, 1);
        } else {
            if (this.othersForm.controls['imagesList']['controls'][i].value.Id) {
                const formData: FormData = new FormData();

                formData.append('Id', this.othersForm.controls['imagesList']['controls'][i].value.Id);
                formData.append('itemId', this.imageItem);
                this.frameService.framesImageDeleting(formData).subscribe(data => {
                    const control = <FormArray>this.othersForm.controls['imagesList'];
                    control.removeAt(i);
                });
            } else {
                const control = <FormArray>this.othersForm.controls['imagesList'];
                control.removeAt(i);
                this.uploadedFiles.splice(i, 1);
            }
        }
    }
    /**
     * Initlanguages add inventory others component
     * @returns  image details
     */
    initlanguage() {
        return this._fb.group({
            Id: [],
            Image: []

        });
    }
    /**
     * Inventorys form details
     * @returns inventory foprm details
     */
    inventoryFormDetails() {
        let filterdata = {
            filter: [{
                field: "id",
                operator: "=",
                value: this.selectedotherid
            },
            {
                field: "module_type_id",
                operator: "=",
                value: "6"
            },
            ]
        }
        let response: any = [];
        this.otherinventoryService.getOtherInventoryDetails(filterdata).subscribe(
            data => {
                response = data;
                const otherdata = response.data[0];
                this.othersForm = this._fb.group({

                    upc_code: [otherdata.upc_code, [Validators.required]],
                    name: [otherdata.name, [Validators.required]],
                    categoryid: [otherdata.categoryid === '' ? null :
                        otherdata.categoryid.toString(), [Validators.required]],
                    vendor_id: otherdata.vendor_id === 0 ? '' : otherdata.vendor_id,
                    retail_price: otherdata.retail_price,
                    wholesale_cost: otherdata.wholesale_cost,
                    inventory: otherdata.inventory === 1 ? true : false,
                    send_to_lab_flag: otherdata.send_to_lab_flag === 1 ? true : false,
                    print_on_order: otherdata.print_on_order === 1 ? true : false,
                    procedure_code: otherdata.procedure_code === '' ? null : otherdata.procedure_code,
                    modifier_id: otherdata.modifier_id,
                    structure_id: otherdata.structure_id === null ? '0' : otherdata.structure_id.toString(),
                    notes: otherdata.notes,
                    profit: '',
                    min_qty: otherdata.min_qty,
                    max_qty: otherdata.max_qty,
                    gross_percentage: otherdata.gross_percentage,
                    spiff: otherdata.spiff,
                    amount: otherdata.amount,
                    module_type_id: "6",
                    locationid: '',
                    commission_type_id: otherdata.commission_type_id === 0 ? '' : otherdata.commission_type_id.toString(),
                    imagesList: this._fb.array([]),
                    duplicateselectedotherid: '',
                });
                this.filterProcedureCodeDetails(otherdata.procedure_code);
                if (this.upcdisable === false) {
                    this.othersForm.controls.upc_code.patchValue('');
                }

                this.listImages = [];
                this.getlistImage = [];
                let imageData: any = [];
                let imageValue: any = [];
                this.frameService.framesListImages(this.imageItem)
                    .subscribe(framesListImagesData => {
                        imageData = framesListImagesData['result']['Optical'];
                        // if (imageData!== 'No record found') {
                        this.listImages = imageData.GetImages;
                        for (let i = 0; i <= this.listImages.length - 1; i++) {
                            this.frameService.framesGetImages(this.listImages[i].Id,
                                this.listImages[i].ItemId).subscribe(Value => {
                                    imageValue = Value;
                                    this.getlistImage.push(imageValue.result.Optical.GetImage[0]);
                                    const control = this.othersForm.controls.imagesList as FormArray;

                                    control.push(
                                        this._fb.group({
                                            Id: imageValue.result.Optical.GetImage[0].Id,
                                            Image: 'data:image/JPEG;base64,' + imageValue.result.Optical.GetImage[0].Image
                                        })
                                    );

                                });
                        }

                        // } else {
                        //     // const control = <FormArray>this.FarmesForm.controls['imagesList'];
                        //     // control.push(this.initlanguage());
                        // }
                    });
                this.profitcalculate();

            });

    }
    /**
     * Structureformcontrols add inventory others component
     * @returns remove the unnecessary request playload data
     */
    structureformcontrols() {
        if (!this.othersForm.valid) {
            return;
        }
        this.othersForm.removeControl('status');
        this.othersForm.removeControl('profit');
        const maxqtyrcheck = this.checkminmaxvalue();
        if (maxqtyrcheck === false) {
            return;
        }
        if (this.othersForm.value['commission_type_id'] === '0') {
            this.othersForm.value['commission_type_id'] = null;
        }
        if (this.othersForm.value['structure_id'] === '0') {
            this.othersForm.value['structure_id'] = null;
        }
        if (this.othersForm.value['vendor_id'] === '0') {
            this.othersForm.value['vendor_id'] = null;
        }



        if (this.othersForm.value['inventory'] === true) {
            this.othersForm.value['inventory'] = '1';
        } else {
            this.othersForm.value['inventory'] = '0';
        }

        if (this.othersForm.value['send_to_lab_flag'] === true) {
            this.othersForm.value['send_to_lab_flag'] = '1';
        } else {
            this.othersForm.value['send_to_lab_flag'] = '0';
        }

        if (this.othersForm.value['print_on_order'] === true) {
            this.othersForm.value['print_on_order'] = '1';
        } else {
            this.othersForm.value['print_on_order'] = '0';
        }
    }

    /**
     * Filters procedures
     * @returns filtering procedure codes details based on hiting alphabets
     */
    filterProcedures(event) {
        this.procedurecoderesults = [];
        //    const string = this.othersForm.controls.procedure_code.value;
        // let query = string.query;
        const values = {

            filter: [{
                field: 'cpt_prac_code',
                operator: 'LIKE',
                value: '%' + event.query.toLowerCase() + '%'
            }]
        };


        this.frameService.getProcedureCode(values).subscribe((data: GetProcedureCode) => {
            this.procedurecoderesults = data.data;
            this.filteredProcedures = [];
            // tslint:disable-next-line:prefer-for-of
            for (let i = 0; i < this.procedurecoderesults.length; i++) {
                const procedure = this.procedurecoderesults[i].cpt_prac_code;
                const procedureId = this.procedurecoderesults[i].cpt_fee_id;
                if (procedure.toLowerCase().indexOf(event.query.toLowerCase()) === 0) {
                    this.filteredProcedures.push({ Id: procedureId, Name: procedure });
                }
            }
            if (this.filteredProcedures.length === 0) {
                this.error.displayError({ severity: 'error', summary: 'Error Message', detail: 'No Data Found.' });


            }
        });
    }
    /**
     * Selects pocedure data
     * @param event getting click event values
     */
    selectPocedureData(event: any) {
        if (event.target.value !== undefined) {
            event.target.select();
        }
    }
    /**
     * Saves request pay load
     * Getting form controller values
     */
    saveRequestPayLoad() {
        if (this.othersForm.controls.inventory.value === true) {
            this.othersForm.controls.inventory.patchValue('1');
        } else if (this.othersForm.controls.send_to_lab_flag.value === true) {
            this.othersForm.controls.send_to_lab_flag.patchValue('1');
        } else if (this.othersForm.controls.print_on_order.value === true) {
            this.othersForm.controls.print_on_order.patchValue('1');
        } else {
            this.othersForm.controls.inventory.patchValue('0');
            this.othersForm.controls.send_to_lab_flag.patchValue('0');
            this.othersForm.controls.print_on_order.patchValue('0');
        }
        this.savingPayLoad = {
            module_type_id: '6',
            upc_code: this.othersForm.controls.upc_code.value,
            name: this.othersForm.controls.name.value,
            categoryid: this.othersForm.controls.categoryid.value,
            vendor_id: this.othersForm.controls.vendor_id.value === 0 ? '' : this.othersForm.controls.vendor_id.value,
            retail_price: this.othersForm.controls.retail_price.value,
            wholesale_cost: this.othersForm.controls.wholesale_cost.value,
            procedure_code: this.othersForm.controls.procedure_code.value === '' ? null :
                this.othersForm.controls.procedure_code.value.Id.toString(),
            modifier_id: this.othersForm.controls.modifier_id.value,
            structure_id: this.othersForm.controls.structure_id.value,
            notes: this.othersForm.controls.notes.value,
            min_qty: this.othersForm.controls.min_qty.value,
            max_qty: this.othersForm.controls.max_qty.value,
            locationid: this.othersForm.controls.locationid.value,
            commission_type_id: this.othersForm.controls.commission_type_id.value,
            spiff: this.othersForm.controls.spiff.value,
            gross_percentage: this.othersForm.controls.gross_percentage.value,
            amount: this.othersForm.controls.amount.value,
        };

    }
    /**
     * Filters procedure code details
     * @param procedureValue getting selected id procedure code details
     */
    filterProcedureCodeDetails(procedureValue) {
        this.procedurecoderesults = [];
        if (procedureValue !== null) {
            const values = {

                filter: [{
                    field: 'cpt_fee_id',
                    operator: '=',
                    value: procedureValue
                }]
            };
            this.frameService.getProcedureCode(values).subscribe((data: GetProcedureCode) => {
                this.procedurecoderesults = data.data;
                this.othersForm.controls.procedure_code.patchValue({
                    Id: this.procedurecoderesults[0].cpt_fee_id, Name: this.procedurecoderesults[0].cpt_prac_code
                });
            });
        }
    }
}
