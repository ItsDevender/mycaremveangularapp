import { Component, OnInit, ViewChild, OnDestroy, Output, EventEmitter } from '@angular/core';


import { Subscription } from 'rxjs';
import { InventoryService } from '@services/inventory/inventory.service';
import { OthersService } from '@services/inventory/others.service';
import { ErrorService } from '@app/core/services/error.service';
import { UtilityService } from '@app/shared/services/utility.service';
import { FrameSupplier, OtherInvCategory } from '@app/core/models/masterDropDown.model';
import { DropdownService } from '@app/shared/services/dropdown.service';


@Component({
    selector: 'app-others-reactivate-inventory-items',
    templateUrl: 'others-reactivate-inventory-items.component.html'
})
export class OthersReactivateInventoryItemsComponent implements OnInit, OnDestroy {
    cols: any[];
    gridlists: any[];
    reActivateItems: boolean;
    reactiverefreshdetails: Subscription;
    otheractivateMultipleSelection = [];
    activatedReqbody = [];
    @ViewChild('dtreactive') public dtreactive: any;
    @Output() othersReactive = new EventEmitter<any>();
    /**
     * Reqbodysearch  of others reactivate inventory items component variables
     */
    reqbodysearch: { filter: { field: string; operator: string; value: string; }[]; sort: { field: string; order: string; }[]; };
    /**
     * Supliers  stores the supplier  on inventory common service variable
     */
    suppliers: Array<object>;
    /**
     * Supplierslist   stores supplier details
     */
    supplierslist: any;
    /**
     * Categories  stores the others  inventory service variable
     */
    categories: any[];
    /**
     * Categorylist stores category details
     */
    categorylist: any[];
    /**
     * Searchvendor  stores the others  inventory service variable
     */
    searchvendor: string;
    /**
     * Searchcategory  filter the selected category
     */
    searchcategory: string;
    /**
     * Creates an instance of others reactivate inventory items component.
     * @param otherinventoryService provide other inventory service
     * @param _InventoryService provide inventory service
     * @param error provide error service
     * @param utility provide utility service
     * @param _InventoryCommonService provide inventory common service
     */
    constructor(
        private otherinventoryService: OthersService,
        private _InventoryService: InventoryService,
        private error: ErrorService,
        private utility: UtilityService,
        private dropdownService: DropdownService) {

    }
    ngOnInit() {
        this.cols = [
            

            { field: 'id', header: 'ID' },
            { field: 'category', header: 'Category' },
            { field: 'name', header: 'Name' },
            { field: 'upc_code', header: 'UPC' },
            { field: 'vendor', header: 'Supplier' }
        ];
        this.dtreactive.selection = false;
        this.GetinventoryotherList();
        this.GetSupplierDetails() ;
        this.GetcategoryDetails();
        this.searchcategory = '';
    }
    ngOnDestroy() {
        
    }
    /**
     * Getinventoryothers list
     * @returns getting other inventory deactive details
     */
    GetinventoryotherList() {
        this.gridlists = [];
        this.dtreactive.selection = false;
        const otherDetails = {
            filter: [

                {
                    field: "module_type_id",
                    operator: "=",
                    value: "6"
                },
                 {
                    field: "vendor_id",
                    operator: "LIKE",
                    value: '%' + this.searchvendor + '%'
                },
                {
                    field: "categoryid",
                    operator: "LIKE",
                    value: '%' + this.searchcategory + '%'
                },
            ],

            
        };
        const resultData = otherDetails.filter.filter(p => p.value !== "" && p.value !== null && p.value !== undefined && p.value !== '%undefined%'
            && p.value !== "%  %" && p.value !== "%%");
        this.reqbodysearch = {
            filter: resultData,
            sort: [
                {
                    field: "id",
                    order: "DESC"
                }

            ]

        }
        let inventoryReactive: any = []
        this.otherinventoryService.getotherinventoriesReactive(this.reqbodysearch).subscribe(
            data => {
                inventoryReactive = data['data']
                for (let i = 0; i <= inventoryReactive.length - 1; i++) {
                  
                    if (inventoryReactive[i]['del_status'] == 1) {
                        this.gridlists.push(inventoryReactive[i]);
                    }

                }
                

            });

    }
    /**
     * Cancels others reactivate inventory items component
     * @returns close the reactive page
     */
    Cancel() {
        this.otheractivateMultipleSelection = [];
        this.othersReactive.emit('reactiveCancle');
      
    }
    /**
     * Activates others reactivate inventory items component
     * @returns  displaying other inventory reactive detail
     */
    Activate() {
        this.otherinventoryService.errorHandle.msgs = [];
        if (this.otheractivateMultipleSelection.length === 0) {
            this.error.displayError({
                severity: 'warn',
                summary: 'Warning Message', detail: 'Please Select Item to Re-Activate'
            });
            return;
        }

        this.activatedReqbody = [];
        this.otheractivateMultipleSelection.forEach(element => {
            this.activatedReqbody.push(element.id);
        });
        const activatedata = {
            'itemId': this.activatedReqbody
        };

        this._InventoryService.ActivateInventory(activatedata).subscribe(data => {
            this.gridlists = [];
            this.otheractivateMultipleSelection = [];
            this.error.displayError({
                severity: 'success',
                summary: 'Success Message', detail: 'Re-Activated Successfully'
            });
            this.GetinventoryotherList();
            this.othersReactive.emit('reactive');
            
        },
            error => {
                this.error.displayError({
                    severity: 'error',
                    summary: 'Error Message', detail: 'Unable to Re-Activate the selected values'
                });

            }
        );

    }
    
    /**
     * Gets supplier details
     * @returns supplier data
     */
    GetSupplierDetails() {
        this.suppliers = this.dropdownService.frameSupplier;
        if (this.utility.getObjectLength(this.suppliers) === 0) {
            this.dropdownService.getSupplierDropData().subscribe(
                (values: FrameSupplier) => {
                    try {
                        const dropData  = values.data;
                        this.suppliers.push({ Id: '', Name: 'Select Supplier' });
                        for (let i = 0; i <= dropData.length - 1; i++) {
                                this.suppliers.push({ Id: dropData[i].id, Name: dropData[i].vendor_name });
                        }
                        this.dropdownService.frameSupplier = this.suppliers;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }

                });
        }
    }



    /**
    * Getcategorys details
    * @returns category data
    */
    GetcategoryDetails() {
        this.categories = [];
        this.categorylist = [];
        this.categories = this.otherinventoryService.categoriesData;
        if (this.utility.getObjectLength(this.categories) === 0) {
            this.otherinventoryService.getcategorydata().subscribe((values: OtherInvCategory) => {
                try {
                    const dropData = values.data;
                    this.categories.push({ Id: '', Name: 'Select Category' });
                    for (let i = 0; i <= dropData.length - 1; i++) {
                        this.categories.push({ Id: dropData[i].id.toString(), Name: dropData[i].categoryname });
                    }
                    this.otherinventoryService.categoriesData = this.categories;
                } catch (error) {
                    this.error.syntaxErrors(error);
                }
            });
        }
    }



}

