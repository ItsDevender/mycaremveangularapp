import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { SortEvent } from 'primeng/components/common/sortevent';
import { Subscription } from 'rxjs';
import { OthersService } from '@services/inventory/others.service';
// import { FramesService } from '@services/inventory/frames.service';
import { ErrorService } from '@app/core/services/error.service';
import { InventoryService } from '@app/core/services/inventory/inventory.service';
import { DialogBoxComponent } from '@app/shared/components/dialog-box/dialog-box.component';
import { Optical } from '@app/core/models/inventory/otherinevntory';
import { DeactivateComponent } from '../deactivate/deactivate.component';
import { ImportCsvComponent } from '../import-csv/Import-csv.component';
import { UtilityService } from '@app/shared/services/utility.service';
import { LazyLoadEvent } from 'primeng/api';
import { ReceivingService } from '@app/core/services/inventory/receiving.service';
import { FrameSupplier, OtherInvCategory } from '@app/core/models/masterDropDown.model';
import { DropdownService } from '@app/shared/services/dropdown.service';


@Component({
    selector: 'app-inventory-others',
    templateUrl: 'inventory-others.component.html'
})
export class InventoryOthersComponent implements OnInit, OnDestroy {
    Accesstoken: string;
    addLoctionOtherType = false;
    addInventoryOthers = false;
    pricecalculation = false;
    sortallinventory = false;
    othersReActivateItems = false;
    othersdata: any[];
    otherdatacolumns: any[];
    samplevisible = true;
    visible = true;
    display = false;
    public tableHeaderFilters = [];
    public otherinventorylist: Optical[];
    @ViewChild('dt') public dt: any;
    @ViewChild('importcsvBox') importCsvdialog: ImportCsvComponent;
    rowData: any;
    advancedFilter;
    othersMultipleSelection = [];
    othersinventorysunscription: Subscription;
    /**
     * Suppliers  of inventory others component
     */
    suppliers: Array<object>;
    supplierslist = [];
    categories: any[];
    searchValueID = '';
    searchValueVendor = '';
    searchValueCategory = '';
    searchValueName = '';
    searchValueRetailPrice = '';
    searchValueUPC = '';
    private categorylist: any;
    subscriptionCSV: Subscription;
    addbarcodelabels = false;
    subscriptionlocationclose: Subscription;

    imageItemId: any;
    paginationValue: any = 0;
    totalRecords: string;
    paginationPage: any = 0;
    @ViewChild('deleteRow') DeleteRows: DialogBoxComponent;
    @ViewChild('deactivateRow') deactivateRows: DeactivateComponent;
    /**
     * Patient stores data
     */
    patient: any;
    /**
     * Reqbodysearch  stores filter data
     */
    reqbodysearch: any;
    /**
     * Row id  is a boolean variable
     */
    rowId = true;
    /**
     * Row category is a boolean variable
     */
    rowCategory = true;
    /**
     * Row supplier  is a boolean variable
     */
    rowSupplier = true;
    /**
     * Row name  is a boolean variable
     */
    rowName = true;
    /**
     * Row retail is a boolean variable
     */
    rowRetail = true;
    /**
     * Row upc  is a boolean variable
     */
    rowUpc = true;
    /**
     * Search value procedure code of inventory others component for procedure code data
     */
    searchValueProcedureCode: string;
    /**
     * Search value on hand of inventory others component for qty on hand data
     */
    searchValueOnHand: string;
    ngOnInit() {
        this.searchValueProcedureCode = '',
        this.searchValueOnHand = '';
        this.loadotherinventorycolumns();
        this.GetinventoryotherList();
        this.GetcategoryDetails();
        this.GetSupplierDetails();
        this._InventoryCommonService.InventoryType = 'Others';

    }
    ngOnDestroy() {
    }
    /**
     * Creates an instance of inventory others component.
     * @param otherinventoryService provide otherinventory service
     * @param _InventoryCommonService  provid inventory common service
     * @param error provide error service
     * @param utility  provide utility service
     */
    constructor(
        private otherinventoryService: OthersService,
        private _InventoryCommonService: InventoryService,
        private error: ErrorService,
        private utility: UtilityService,
        private dropdownService: DropdownService) {

    }

    deleteRows() {
        // this.DeleteRows.dialogBox();
    }
    /**
     * Reactives inventory others component
     * @returns display reactivate page 
     */
    reactive() {
        this.othersReActivateItems = true;
    }
    /**
     * active
     *  @returns display deactivate page 
     */
    deActive() {
        if (this.othersMultipleSelection.length !== 0) {
            const otherdata = {
                'OtherSelection': this.othersMultipleSelection
            };
            this.deactivateRows.deactivate(otherdata);
        }
    }

    /**
     * Dialogs box
     * @returns display dialog box
     */
    dialogBox() {
        this.display = true;

    }
    /**
     * Importcsvs inventory others component
     * @returns import csv
     */
    importcsv() {
        this.importCsvdialog.importCsvbox('others');
    }
    /**
     * Csvs importvent
     * @param event getting the import data
     */
    csvImportEvent(event) {
        if (event === 'updateCsvData') {
            this.GetinventoryotherList();
        }
    }
    /**
     * Loadotherinventorycolumns inventory others component
     * @returns fields details
     */
    loadotherinventorycolumns() {
        this.otherdatacolumns = [
            { field: 'id', header: 'ID', visible: this.visible },
            { field: 'category', header: 'Category', visible: this.visible },
            { field: 'Vendor', header: 'Supplier', visible: this.visible },
            { field: 'name', header: 'Name', visible: this.visible },
            { field: 'retail_price', header: 'Retail Price', visible: this.visible },
            { field: 'upc_code', header: 'UPC', visible: this.visible },
        ];

    }




    /**
     * Applyed filter details
     * @returns  filter list
     */
    ApplyedFilterDetails() {
        const otherDetails = {
            filter: [

                {
                    field: "module_type_id",
                    operator: "=",
                    value: "6"
                },
                {
                    field: "id",
                    operator: "LIKE",
                    value: '%' + this.searchValueID + '%'
                },
                {
                    field: "vendor_id",
                    operator: '=',
                    value: this.searchValueVendor
                },
                {
                    field: "categoryid",
                    operator: "=",
                    value:  this.searchValueCategory
                },
                {
                    field: "name",
                    operator: "LIKE",
                    value: '%' + this.searchValueName + '%'
                },
                {
                    field: "retail_price",
                    operator: "LIKE",
                    value: '%' + this.searchValueRetailPrice + '%'
                },
                {
                    field: "upc_code",
                    operator: "LIKE",
                    value: '%' + this.searchValueUPC + '%'
                },
                {
                    field: "procedure_code",
                    operator: "LIKE",
                    value: '%' + this.searchValueProcedureCode + '%'
                },
                {
                    field: "qty_on_hand",
                    operator: "LIKE",
                    value: '%' + this.searchValueOnHand + '%'
                }
            ],

            sort: [
                {
                    field: "module_type_id",
                    order: "ASC"
                }
            ]
        };
        const resultData = otherDetails.filter.filter(p => p.value !== "" && p.value !== null && p.value !== undefined && p.value !== '%undefined%'
            && p.value !== "%  %" && p.value !== "%%");
        this.reqbodysearch = {
            filter: resultData,
            sort: [
                {
                    field: "id",
                    order: "DESC"
                }

            ]

        };
    }

    /**
     * Getinventoryothers list
     * @returns other inventory filter details
     */
    GetinventoryotherList() {
        this.ApplyedFilterDetails();
        this.otherinventorylist = [];
        let inventoryDetails: any = [];
        this.otherinventoryService.getOtherInventoryDetails(this.reqbodysearch).subscribe(
            data => {
                this.totalRecords = data['totalRecords'];
                inventoryDetails = data['data']
                for (let i = 0; i <= inventoryDetails.length - 1; i++) {
                    if (inventoryDetails[i]['del_status'] == 0) {
                    }
                }
                this.otherinventorylist = inventoryDetails;
                //  this.otherinventorylist = data['data'];
            });

    }
    /**
     * Gets supplier details
     * @returns supplier data
     */
    GetSupplierDetails() {
        this.suppliers = this.dropdownService.frameSupplier;
        if (this.utility.getObjectLength(this.suppliers) === 0) {
            this.dropdownService.getSupplierDropData().subscribe(
                (values: FrameSupplier) => {
                    try {
                        const dropData  = values.data;
                        this.suppliers.push({ Id: '', Name: 'Select Supplier' });
                        for (let i = 0; i <= dropData.length - 1; i++) {
                                this.suppliers.push({ Id: dropData[i].id, Name: dropData[i].vendor_name });
                        }
                        this.dropdownService.frameSupplier = this.suppliers;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }

                });
        }
    }

    /**
     * Getcategorys details
     * @returns category data
     */
    GetcategoryDetails() {
        this.categories = this.otherinventoryService.categoriesData;
        if (this.utility.getObjectLength(this.categories) === 0) {
            this.otherinventoryService.getcategorydata().subscribe((values: OtherInvCategory) => {
                try {
                    const dropData = values.data;
                    this.categories.push({ Id: '', Name: 'Select Category' });
                    for (let i = 0; i <= dropData.length - 1; i++) {
                        this.categories.push({ Id: dropData[i].id.toString(), Name: dropData[i].categoryname });
                    }
                    this.otherinventoryService.categoriesData = this.categories;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }
                });
        }
    }

    /**
     * Checks box click
     * @returns data view deatails
     */
    checkBoxClick(item, e) {

        if (item === 'selectAll') {
            this.visible = e.checked;
            this.rowId = e.checked;
            this.rowCategory = e.checked;
            this.rowSupplier = e.checked;
            this.rowName = e.checked;
            this.rowRetail = e.checked;
            this.rowUpc = e.checked;
            this.loadotherinventorycolumns();
            this.applyDataViewChanges(null, null);
        } else {
            this.applyDataViewChanges(item, e.checked);
        }
    }

    /**
     * Applys data view changes
     * @returns selected or unselected data view changes
     */
    applyDataViewChanges(item, selected) {
        this.otherdatacolumns.forEach(element => {
            if (element.header === item) {
                element.visible = selected;
            }

        });
        this.displayColsData(item, selected);
        const colSpanVar = this.otherdatacolumns.filter(x => x.visible).length;
        if (colSpanVar < this.otherdatacolumns.length) {
            this.samplevisible = false;
        } else {
            this.samplevisible = true;
        }



    }
    /**
     * Displays cols data
     * @returns columns data
     */
    displayColsData(item, selected) {
        if (item === 'ID') {
            this.rowId = selected;
        } if (item === 'Category') {
            this.rowCategory = selected;
        } if (item === 'Supplier') {
            this.rowSupplier = selected;
        } if (item === 'Name') {
            this.rowName = selected;
        } if (item === 'Retail Price') {
            this.rowRetail = selected;
        } if (item === 'UPC') {
            this.rowUpc = selected;
        }
    }

    /**
     * Headers filters
     * @returns filter the header fields
     */
    headerFilters(value, field, type = 'in') {
        this.dt.filter(value, field, type);
        setTimeout(() => {
            this.tableHeaderFilters = Object.keys(this.dt.filters);
        }, 500);

    }

    /**
     * Filtervalues inventory others component
     * @returns filter values
     */
    Filtervalues() {
        this.otherinventorylist = [];
        this.dt.reset();
        this.GetinventoryotherList();
    }





    /**
     * Determines whether row select on
     * @returns selected item id
     */
    onRowSelect(event) {

        this.otherinventoryService.editotherinventoryid = event.data.id;
        // this._InventoryCommonService.FortrasactionItemID = event.data.id;

    }
    /**
     * Determines whether row unselect on
     * @returns clear the unselected row data
     */
    onRowUnselect(event) {
        this.otherinventoryService.editotherinventoryid = '';
        this.otherinventoryService.editDuplicateotherinventoryid = '';

    }
    /**
     * Addotherinventorys inventory others component
     * @returns add other inventory details
     */
    addotherinventory() {
        this._InventoryCommonService.FortrasactionItemID = '';
        this.dt.selection = true;
        this.dt.selection = false;
        this.addInventoryOthers = true;
        this.imageItemId = undefined;
        this.otherinventoryService.editotherinventoryid = '';
        this.otherinventoryService.editotherinventoryidDoubleClick = '';
        this._InventoryCommonService.FortrasactionItemID = '';

    }

    /**
     * Resets inventory others component
     * @returns clear the data
     */
    reset(table) {

        this.dt.selection = false;
        table.reset();
        this.cleardetails();


        this.GetinventoryotherList();
        this.paginationValue = 0;
    }

    /**
     * Cleardetails inventory others component
     * @returns cler the data
     */
    cleardetails() {
        this.searchValueID = '';
        this.searchValueVendor = '';
        this.searchValueCategory = '';
        this.searchValueName = '';
        this.searchValueRetailPrice = '';
        this.searchValueUPC = '';
        this.searchValueProcedureCode = '';
        this.searchValueOnHand = '';
    }

    /**
     * Updateotherinventorys inventory others component
     * @returns modified data
     */
    updateotherinventory() {
        // this.frameService.errorHandle.msgs = [];

        this._InventoryCommonService.FortrasactionItemID = this.othersMultipleSelection[0].id;

        if (this.othersMultipleSelection.length === 1) {
            this.otherinventoryService.editotherinventoryid = this.othersMultipleSelection[0].id;

            this.imageItemId = this.othersMultipleSelection[0].id;
            const selectedFrameid = this.otherinventoryService.editotherinventoryid; //  localStorage.getItem('editotherinventoryid');
            if (!selectedFrameid) {

                this.error.displayError({
                    severity: 'error',
                    summary: 'Error Message', detail: 'Please Select Other Inventory.'
                });


            } else {
                this.addInventoryOthers = true;

            }
        } else {
            this.error.displayError({ severity: 'error', summary: 'Error Message', detail: 'Please Select One Item.' });


            this.imageItemId = undefined;
        }
    }
    /**
     * Doubles clickframes
     * @returns selectedv row detils 
     */
    DoubleClickframes(rowData) {

        this.imageItemId = rowData.id;
        this._InventoryCommonService.FortrasactionItemID = rowData.id;
        this.othersMultipleSelection = [];


        this.otherinventoryService.editotherinventoryidDoubleClick = rowData.id;
        this.othersMultipleSelection.push(rowData);

        this.addInventoryOthers = true;

    }
    /**
     * Duplicates other
     * @returns selecte row details
     */
    duplicateOther() {
        // this.frameService.errorHandle.msgs = [];


        if (this.othersMultipleSelection.length === 1) {

            this.otherinventoryService.editDuplicateotherinventoryid = this.othersMultipleSelection[0].upc_code;
            this.imageItemId = this.othersMultipleSelection[0].id;
            this.addInventoryOthers = true;
        } else {
            this.error.displayError({
                severity: 'error',
                summary: 'Error Message', detail: 'Please Select One Item.'
            });



        }
        this.othersMultipleSelection = [];
    }
    objChanged(event: any) {







    }

    /**
     * Adds location other
     * @returns add to alldetails
     */
    addLocationOther() {

        // this.frameService.errorHandle.msgs = [];
        this._InventoryCommonService.multiLocationForOtherInventory = [];
        if (this.othersMultipleSelection.length === 1) {

            this._InventoryCommonService.multiLocationForOtherInventory = this.othersMultipleSelection[0].id;
            this._InventoryCommonService.FortrasactionItemID = this.othersMultipleSelection[0].id;
             

            this.addLoctionOtherType = true;
        } else {
            this.error.displayError({
                severity: 'error',
                summary: 'Error Message', detail: 'Please Select  Only One'
            });
        }
    }
    /**
     * Addbarcodelabels set
     * @returns 
     */
    addbarcodelabelsSet() {
        if (this.othersMultipleSelection.length !== 0) {
            this._InventoryCommonService.barCodelist = this.othersMultipleSelection;
            this._InventoryCommonService.barCodeLable = 'OtherBarcode';
        } else {
            this._InventoryCommonService.barCodelist = [];
            this._InventoryCommonService.barCodeLable = 'OtherBarcode';
        }
        this.addbarcodelabels = true;
    }
    /**
     * Sortallevents inventory others component
     * @returns sortall details 
     */
    sortallevent(event) {
        if (event === 'ok') {
            this.sortallinventory = false;
        }
    }

    /**
     * Customs sort
     * @returns custom sort details 
     */
    customSort(event: SortEvent) {
        this.dt.first = this.paginationValue;
        const tmp = this.otherinventorylist.sort((a: any, b: any): number => {
            if (event.field) {
                return a[event.field] > b[event.field] ? 1 : -1;
            }
        });
        if (event.order < 0) {
            tmp.reverse();
        }
        const thisRef = this;
        this.otherinventorylist = [];
        tmp.forEach(function (row: any) {
            thisRef.otherinventorylist.push(row);
        });
    }
    /**
     * Others deactiveevent
     * @returns deactivate the selected item
     */
    otherDeactiveevent(event) {
        if (event === 'otherdeactivate') {
            this.GetinventoryotherList();
            this.othersMultipleSelection = [];
        }
    }
    /**
     * Others locations event
     * @returns location details
     */
    otherLocationsEvent(event) {
        if (event === 'other') {
            this.addLoctionOtherType = false;
        } else {
            this.addLoctionOtherType = false;
            this.GetinventoryotherList();
            this.othersMultipleSelection = [];
        }

    }

    /**
     * Others reactive event
     * @returns other reactive details
     */
    othersReactiveEvent(event) {
        if (event === 'reactiveCancle') {
            this.othersReActivateItems = false;
        } else {
            this.GetinventoryotherList();
            this.othersReActivateItems = false;
        }
    }
    /**
     * Others add item event
     * @returns add the deactive data to reactive page
     */
    otherAddItemEvent(event) {
        if (event.status === 'save') {
            this.addInventoryOthers = false;
            //  this.cleardetails();
            this.GetinventoryotherList();
        } if (event.status === 'othercancle') {
            this.addInventoryOthers = false;
        }
        if (event.status === 'Updated') {
            let payload = {
                "filter": [

                    {
                        "field": "module_type_id",
                        "operator": "=",
                        "value": "6"
                    },
                    {
                        "field": "id",
                        "operator": "=",
                        "value": event.data
                    }
                ],

                "sort": [
                    {
                        "field": "module_type_id",
                        "order": "ASC"
                    }
                ]
            }
            this.otherinventoryService.getOtherInventoryDetails(payload).subscribe(data => {
                this.addInventoryOthers = false;
                let req = data['data'][0];
                for (let i = 0; i <= this.otherinventorylist.length - 1; i++) {
                    if (event.data == this.otherinventorylist[i]['id']) {
                        this.otherinventorylist[i]['upc_code'] = req.upc_code;
                        this.otherinventorylist[i]['name'] = req.name;
                        this.otherinventorylist[i]['categoryid'] = req.categoryid;
                        this.otherinventorylist[i]['vendor_id'] = req.vendor_id;
                        this.otherinventorylist[i]['retail_price'] = req.retail_price;
                        this.otherinventorylist[i]['wholesale_cost'] = req.wholesale_cost;
                        this.otherinventorylist[i]['inventory'] = req.inventory;
                        this.otherinventorylist[i]['send_to_lab_flag'] = req.send_to_lab_flag;
                        this.otherinventorylist[i]['print_on_order'] = req.print_on_order;
                        this.otherinventorylist[i]['procedure_code'] = req.procedure_code;
                        this.otherinventorylist[i]['modifier_id'] = req.modifier_id;
                        this.otherinventorylist[i]['structure_id'] = req.structure_id;
                        this.otherinventorylist[i]['notes'] = req.notes;
                        this.otherinventorylist[i]['profit'] = req.profit;
                        this.otherinventorylist[i]['min_qty'] = req.min_qty;
                        this.otherinventorylist[i]['max_qty'] = req.max_qty;
                        this.otherinventorylist[i]['locationid'] = req.locationid;
                        this.otherinventorylist[i]['commission_type_id'] = req.commission_type_id;
                        this.otherinventorylist[i]['spiff'] = req.spiff;
                        this.otherinventorylist[i]['gross_percentage'] = req.gross_percentage;
                        this.otherinventorylist[i]['amount'] = req.amount;
                        this.otherinventorylist[i]['module_type_id'] = req.module_type_id;

                    }
                }
            })
        }
    }
    /**
   * Loads file data
   * @returns load Other Inventory Data
   */
    loadOtherInventoryData(page: number = 0) {

        this.ApplyedFilterDetails();
        let otherInventoryData: any = [];

        this.patient.getInventoryDetails(this.reqbodysearch, page).subscribe(
            data => {
                otherInventoryData = data;
                this.otherinventorylist = otherInventoryData.data;
            },
        );

    }
    /**
     * Lazys load file items
     * @returns Load lazyLoad Other data
     */
    loadLazyOtherItems(event: LazyLoadEvent) {
        // Generating Page
        let page = 0;
        page = ('first' in event && 'rows' in event)
            ? (event.first / event.rows) + 1
            : 0;
        // Calling Page Data
        this.loadOtherInventoryData(page);
    }
    /**
     * Advanced filter click for opening advance filter page
     */
    advancedFilterClick() {
        this.advancedFilter = true;
    }
    /**
     * Ohters adv filter event
     * @param event for getting output data from child page
     */
    othersAdvFilterEvent(event) {
        this.advanceFilterList(event);
    }

    /**
     * Advances filter list
     * @param data filter the data
     */
    advanceFilterList(data) {
        this.searchValueID = data.id;
        this.searchValueUPC = data.upc_code;
        this.searchValueName = data.name;
        this.searchValueCategory = data.categoryid;
        this.searchValueRetailPrice = data.retail_price;
        this.searchValueProcedureCode = data.procedure_code;
        this.searchValueOnHand = data.qty_on_hand;
        this.GetinventoryotherList();
        this.advancedFilter = false;
    }
}

