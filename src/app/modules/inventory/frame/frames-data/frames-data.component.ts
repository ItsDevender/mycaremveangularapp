// import { AppSettings } from '../../../shared/config/AppSettings';
import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { FramesService } from '@services/inventory/frames.service';
import { InventoryService } from '@services/inventory/inventory.service';
import { DropdownService } from '@app/shared/services/dropdown.service';
import { UtilityService } from '@app/shared/services/utility.service';
import { ErrorService } from '@app/core';
import { FrameColorData, Data } from '@app/core/models/inventory/Colors.model';
import { FrameBrands } from '@app/core/models/masterDropDown.model';
// import { FrameService } from '../../../../app/ConnectorEngine/services/frame.service';




@Component({
    selector: 'app-frames-data',
    templateUrl: 'frames-data.component.html'
})
export class FramesDataComponent implements OnInit {

    visible = true;
    framedatalist: any[];
    loadFramesSidebar = false;
    selectFrameData: any[];
    public Manufacturerslist: any;
    Manufacturers: any[];
    public collectionlist: any;
    collections: any[];
    public stylelist: any;
    styles: any[];
    colors: any[];
    public Manufacturerssearchlist: any[];
    FarmesdataForm: FormGroup;
    Framedatacolumns: any[];
    selectedFrameData: any;
    FramesmanufacturerURL: any = '';
    @Output() FramesdatasubOutput = new EventEmitter<any>();
    /**
     * Brandlist  of frames data component
     * storing bandlist
     */
    Brandlist: any;
    ngOnInit() {
        // this.loadFramescolumns();
        // this.FramesmanufacturerURL = this._AppSettings.routeUrl('FramesURL', true);
        this.loadFramesdatacolumns();
        this.Loadformdata();
        this.LoadManufacturers();
        this.LoadBrands();
        this.LoadColor();
        this.LoadStyles();

    }
    /**
     * Creates an instance of frames data component.
     * @param _fb  frombuilder
     * @param frameService  provide framrservice
     * @param dropdownService provide dropdownservice
     * @param utility provide utility service
     * @param error provide error service
     */
    constructor(
        private _fb: FormBuilder,
        private frameService: FramesService,
        // private _AppSettings: AppSettings,
        private dropdownService: DropdownService,
        private utility: UtilityService,
        private error: ErrorService,

    ) {

    }

    /**
     *  Loadformdatas add modify component for formgroup object
     */
    Loadformdata() {
        this.FarmesdataForm = this._fb.group({
            manufacturerID: null,
            BrandID: '',
            ColorID: '',
            StyleID: null,
        });
    }

    LoadManufacturers() {
        this.Manufacturers = [];
        this.frameService.getFramesManufacturers().subscribe(
            data => {
                this.Manufacturerslist = data;

                for (let i = 0; i <= this.Manufacturerslist.length - 1; i++) {

                    if (!this.Manufacturers.find(a => a.Name === this.Manufacturerslist[i]['manufacturername'])) {
                        this.Manufacturers.push({ Id: 0, Name: this.Manufacturerslist[i]['manufacturername'] });
                    }
                }
            });
    }

    /**
     * Loads brands
     * @returns brand data
     */
    LoadBrands() {
        // this.collections = [];
        // this.frameService.getFramesbrands().subscribe(
        //     data => {
        //         this.collectionlist = data;

        //         for (let i = 0; i <= this.collectionlist.length - 1; i++) {

        //             if (!this.collections.find(a => a.Name === this.collectionlist[i]['collectionname'])) {
        //                 this.collections.push({ Id: 0, Name: this.collectionlist[i]['collectionname'] });
        //             }
        //         }
        //     });
        this.collections = this.dropdownService.brands;
        if (this.utility.getObjectLength(this.collections) === 0) {
            this.dropdownService.getBrands().subscribe(
                (values: FrameBrands) => {
                    try {
                        const dropData = values.data;
                        this.collections.push({ Id: '', Name: 'Select Brand' });
                        for (let i = 0; i <= dropData.length - 1; i++) {
                            this.collections.push({ Id: dropData[i].id, Name: dropData[i].frame_source });
                        }
                        this.dropdownService.brands = this.collections;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }
                });
        }
    }
    LoadStyles() {
        this.styles = [];
        this.frameService.getFramesstyles().subscribe(
            data => {
                this.stylelist = data;

                for (let i = 0; i <= this.stylelist.length - 1; i++) {

                    if (!this.styles.find(a => a.Name === this.stylelist[i]['stylename'])) {
                        this.styles.push({ Id: 0, Name: this.stylelist[i]['stylename'] });
                    }
                }
            });
    }

    /**
     * Loads color
     * @returns color list data
     */
    LoadColor() {
        this.colors = this.dropdownService.colors;
        if (this.utility.getObjectLength(this.colors) === 0) {
            this.dropdownService.getColors().subscribe(
                (values: FrameColorData) => {
                    try {
                        const dropData = values.data;
                        this.colors.push({ Id: '', Name: 'Select Color' });
                        for (let i = 0; i <= dropData.length - 1; i++) {

                                this.colors.push({ Id: dropData[i].id, Name: dropData[i].color_name });
                        }
                        this.dropdownService.colors = this.colors;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }
                });
        }
    }

    GetFramesdata() {

        const data = {
            filter: []
        };

        if (this.FarmesdataForm.controls.manufacturerID.value != null) {
            data.filter.push({
                field: 'manufacturername',
                operator: '=',
                value: this.FarmesdataForm.controls.manufacturerID.value
            });
        }

        if (this.FarmesdataForm.controls.BrandID.value != null) {
            data.filter.push({
                field: 'collectionname',
                operator: '=',
                value: this.FarmesdataForm.controls.BrandID.value
            });
        }

        if (this.FarmesdataForm.controls.StyleID.value != null) {
            data.filter.push({
                field: 'stylename',
                operator: '=',
                value: this.FarmesdataForm.controls.StyleID.value
            });
        }

        if (this.FarmesdataForm.controls.ColorID.value !== '') {
            data.filter.push({
                field: 'colordescription',
                operator: '=',
                value: this.FarmesdataForm.controls.ColorID.value
            });
        }

        this.framedatalist = [];
        this.frameService.filterFramesdata(data).subscribe(
            dataframes => {
                this.framedatalist = dataframes['data'];
            });
    }

    loadFramesdatacolumns() {
        this.Framedatacolumns = [
            { field: 'manufacturername', header: 'Manufacturer', visible: this.visible },
            { field: 'collectionname', header: 'Collection', visible: this.visible },
            { field: 'stylename', header: 'Frame Style', visible: this.visible },
            { field: 'colordescription', header: 'Color', visible: this.visible },
            { field: 'colorcode', header: 'Color Code', visible: this.visible },
            { field: 'material', header: 'Material', visible: this.visible },
            { field: 'material', header: 'Frame Type', visible: this.visible },
            { field: 'rimtype', header: 'Rim Type', visible: this.visible },
            { field: 'frameshape', header: 'Frame Shape', visible: this.visible },
            { field: 'upc', header: 'UPC', visible: this.visible },
            { field: 'fpc', header: 'IDA ID', visible: this.visible },
            { field: 'eye', header: 'Eye', visible: this.visible },
            { field: 'a', header: 'A', visible: this.visible },
            { field: 'b', header: 'B', visible: this.visible },
            { field: 'ed', header: 'ED', visible: this.visible },
            { field: 'bridge', header: 'Bridge', visible: this.visible },
            { field: 'temple', header: 'Temple', visible: this.visible },
            { field: 'gendertype', header: 'Gender', visible: this.visible },
            { field: 'lenscolor', header: 'Lens Color', visible: this.visible },
            { field: 'activestatus', header: 'Status', visible: this.visible },
            { field: 'changedprice', header: 'Price Change', visible: this.visible },
            { field: 'changedprice', header: 'Cost', visible: this.visible },
            { field: 'changedprice', header: 'Group Cost', visible: this.visible },
            { field: 'changedprice', header: 'Retail Price', visible: this.visible }
        ];


    }

    DoubleClickframes(rowData) {
        this.selectedFrameData = rowData;
        this.FramesdatasubOutput.emit(this.selectedFrameData);
        // this.frameService.Framesdatasub.next(this.selectedFrameData);
    }


    GetManufacturersData() {
        this.Manufacturerssearchlist = [];
        this.frameService.getFramesManufacturers().subscribe(
            data => {
                if (data['result']['Optical']['Manufacturers'] !== 'No record found') {
                    this.Manufacturerssearchlist = data['result']['Optical']['Manufacturers'];
                }

            });
    }

    objChanged(event: any) {
        this.GetManufacturersData();
    }


    onLoad() {

        // this._AppSettings.resizeIframe();
        // $('.loader-iframe').fadeOut();
    }


}

