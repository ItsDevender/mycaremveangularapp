import { Component, OnInit, ViewChild, OnDestroy, EventEmitter, Output } from '@angular/core';
// import { FrameService } from '../../../ConnectorEngine/services/frame.service';
// import { OrderSearchService } from '../../../ConnectorEngine/services/order.service';
import { Router } from '@angular/router';
import { InventoryService } from '@services/inventory/inventory.service';
import { FramesService } from '@services/inventory/frames.service';
import { ErrorService } from '@app/core/services/error.service';


@Component({
    selector: 'app-reactivate-inventory-items',
    templateUrl: 'reactivate-inventory-items.component.html'
})
export class ReactivateInventoryItemsComponent implements OnInit, OnDestroy {
    // reactiverefreshdetails: Subscription;
    activatedReqbody: any;
    cols: any[];
    gridlists: any[];
    reActivateItems: boolean;
    framesactivateMultipleSelection = [];
    @ViewChild('dtreactive') public dtreactive: any;
    @Output() frameReactiveOutput = new EventEmitter<any>();
    /**
     * Creates an instance of reactivate inventory items component.
     * @param frameService provide frameservice data
     * @param _InventoryService provide inventory data
     * @param error provide error service data
     */
    constructor(private frameService: FramesService, private _InventoryService: InventoryService, private error: ErrorService) {

        // this.reactiverefreshdetails = frameService.reactivescreenRefresh$.subscribe(data => {
        //     this.dtreactive.selection = false;
        //     this.GetFramesList();
        // });

    }


    /**
     * on init for initilize methods
     */
    ngOnInit() {
        this.cols = [
            { field: 'id', header: 'ID' },
            { field: 'location', header: 'Location' },
            { field: 'Manufacturer', header: 'Manufacturer' },
            { field: 'Name', header: 'Collection' },
            { field: 'name', header: 'Name' },
            { field: 'color', header: 'Color' },
            { field: 'color_code', header: 'Color Code' },
            { field: 'qty_on_hand', header: 'On Hand' },
            { field: 'retail_price', header: 'Default Retail' },
        ];
        this.dtreactive.selection = false;
        this.GetFramesList();

    }

    Cancel() {
        this.framesactivateMultipleSelection = [];
        this.frameReactiveOutput.emit('reactiveClose');
        //  this.frameService.deactivateObj.next('value');
    }
    /**
     * Activates reactivate inventory items component
     * @returns  Activate item
     */
    Activate() {
        this.frameService.errorHandle.msgs = [];
        if (this.framesactivateMultipleSelection.length === 0) {
            this.error.displayError({
                severity: 'warn',
                summary: 'Warning Message', detail: 'Please select item to Re-Activate'
            });

            // this.ConflictShow('Please select item to Re-Activate');
            return;
        }

        this.activatedReqbody = [];
        this.framesactivateMultipleSelection.forEach(element => {
            this.activatedReqbody.push(element.id);
        });
        const activatedata = {
            'itemId': this.activatedReqbody
        };

        this._InventoryService.ActivateInventory(activatedata).subscribe(data => {
            this.gridlists = [];
            this.framesactivateMultipleSelection = [];
            this.error.displayError({
                severity: 'success',
                summary: 'Success Message', detail: 'Re-Activated Successfully'
            });

            // this.SuccessShow('Re-Activated Successfully');
            this.GetFramesList();
            this.frameReactiveOutput.emit('reactivate');
            // this.frameService.deactivateObj.next('value');

        },
            error => {
                this.error.displayError({
                    severity: 'error',
                    summary: 'Error Message', detail: 'Unable to Re-Activate the selected values'
                });

                // this.ErrorShow('Unable to Re-Activate the selected values');

            }
        );

    }

    /**
     * Gets frames list for reactive items
     */
    GetFramesList() {
        this.gridlists = [];
        this.dtreactive.selection = false;
        this.frameService.getFramesGridData().subscribe(
            data => {
                for (let i = 0; i <= data['data'].length - 1; i++) {
                    if (data['data'][i]['del_status'] == 1) {
                        this.gridlists.push(data['data'][i]);
                    }
                }
                // Check whether the result is empty
                // if (data['result']['Optical'] !== 'No record found') {
                //     for (let i = 0; i <= data['result']['Optical']['Frames'].length - 1; i++) {
                //         data['result']['Optical']['Frames'][i]['itemId'] = parseInt(data['result']['Optical']['Frames'][i]['itemId'], 10);

                //     }
                //     this.gridlists = data['result']['Optical']['Frames'];
                // }
            });

    }

    ngOnDestroy(): void {
        // this.reactiverefreshdetails.unsubscribe();
    }


    // SuccessShow(messagesucess) {
    //     this.msgs.push({ severity: 'success', summary: 'Success Message', detail: messagesucess });
    // }
    // ErrorShow(message = '') {
    //     this.msgs.push({ severity: 'error', summary: 'Error Message', detail: message });
    // }

    // ConflictShow(message) {
    //     this.msgs.push({ severity: 'warn', summary: 'Warning Message', detail: message });
    // }
}

