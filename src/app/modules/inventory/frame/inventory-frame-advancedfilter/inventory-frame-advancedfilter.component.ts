import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
// import { FrameBrand } from 'src/app/ConnectorEngine/models/Brands.model';
// import { FrameColor } from 'src/app/ConnectorEngine/models/Colors.model';
// import { FrameService } from 'src/app/ConnectorEngine/services/frame.service';
// import { Manufacturer } from 'src/app/ConnectorEngine/models/Manufacturer.model';
// import { DropdownsPipe } from 'src/app/shared/pipes/dropdowns.pipe';
import { FramesService } from '@services/inventory/frames.service';
import { InventoryService } from '@services/inventory/inventory.service';
import { ErrorService } from '@app/core/services/error.service';
// import { Manufacturer } from '@app/core/models/Manufacturer.model';
// import { FrameColor } from '@app/core/models/Colors.model';
// import { FrameBrand } from '@app/core/models/Brands.model';
import { DropdownService } from '@app/shared/services/dropdown.service';
import { UtilityService } from '@app/shared/services/utility.service';
import { Manufacturer } from '@app/core/models/inventory/Manufacturer.model';
import { FrameColorData, Data } from '@app/core/models/inventory/Colors.model';
import { FrameBrand } from '@app/core/models/inventory/Brands.model';
import { FrameTypeMaster, ManufacturerDropData, FrameBrands, SpectacleMaterial } from '@app/core/models/masterDropDown.model';


@Component({
    selector: 'app-inventory-frame-advancedfilter',
    templateUrl: 'inventory-frame-advancedfilter.component.html'
})
export class InventoryFrameAdvancedFilterComponent implements OnInit {
    FarmesAdvanceFilterForm: FormGroup;
    orderTypes: any[];
    public Manufacturers: any[];
    public manufacturerlist: Manufacturer[];
    /**
     * Colors  of inventory frame advanced filter component
     * holding list of frame colorId, colorName
     */
    public colors: Array<object>;
    public Brands: any[];
    public Brandlist: FrameBrand[];
    locations: any[];
    public locationslist: any[];
    framesshapes: any[];
    public framesshapeslist: any[];
    selectedlocation = 'null';
    /**
     * Materials  of inventory frame advanced filter component for storing material dropdown data
     */
    materials: Array<object>;
    materiallist: any[];
    framestypes: any[];
    public framestypeslist: any[];
    delstatusID: any = '';
    activestatus: any[];
    @Output() frameAdvFilterOutPut = new EventEmitter<any[]>();
    constructor(private _fb: FormBuilder, private frameService: FramesService,
        private dropdownService: DropdownService,
        private inventoryService: InventoryService, private error: ErrorService, private utility: UtilityService) {

    }

    ngOnInit() {
        this.LoadMasterData();
        // this.Manufacturers = [];
        // this.Manufacturers = this._DropdownsPipe.manufacturersDropData();
        // this.Brands = [];
        // this.Brands = this._DropdownsPipe.brandsDropData();
        // this.Colors = [];
        // this.Colors = this._DropdownsPipe.coloursDropData();
        // this.locations = [];
        // this.locations = this._DropdownsPipe.locationsDropData();
        // this.materials = [];
        // this.materials = this._DropdownsPipe.specticalMaterialDropDown();
    }

    LoadMasterData() {
        this.formGroupdata();
        this.LoadFrameShapes();
        this.LoadFrameTypes();
        this.LoadMaterialType();
        this.LoadManufacturers();
        this.LoadBrands();
        this.LoadColors();
        this.Loadstatus();
    }
    /**
     * Loads colors
     * @returns frame colors
     */
    LoadColors() {
        this.colors = this.dropdownService.colors;
        if (this.utility.getObjectLength(this.colors) === 0) {
            this.dropdownService.getColors().subscribe(
                (values: FrameColorData) => {
                    try {
                        const dropData = values.data;
                        this.colors.push({ Id: '', Name: 'Select Color' });
                        for (let i = 0; i <= dropData.length - 1; i++) {

                                this.colors.push({ Id: dropData[i].id, Name: dropData[i].color_name });
                        }
                        this.dropdownService.colors = this.colors;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }
                });
        }

    }
    LoadBrands() {
        this.Brands = this.dropdownService.brands;
        if (this.utility.getObjectLength(this.Brands) === 0) {
            this.dropdownService.getBrands().subscribe(
                (values: FrameBrands) => {
                    try {
                        const dropData = values.data;
                        this.Brands.push({ Id: '', Name: 'Select Brand' });
                        for (let i = 0; i <= dropData.length - 1; i++) {
                            this.Brands.push({ Id: dropData[i].id, Name: dropData[i].frame_source });
                        }
                        this.dropdownService.brands = this.Brands;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }
                });
        }
    }
    /**
     *  formGroupdata add modify component for formgroup object
     */
    formGroupdata() {
        this.FarmesAdvanceFilterForm = this._fb.group({
            ID: '',
            ManufacturerId: null,
            Name: '',
            color: '',
            UPC: '',
            ColorCode: '',
            Brand: '',
            location: null,
            framesshape: null,
            framestype: null,
            materialid: '',
            del_status: null
        });
    }
    LoadManufacturers() {
        this.Manufacturers = this.dropdownService.Manufactures;
        if (this.utility.getObjectLength(this.Manufacturers) === 0) {
            const reqbody = {
                filter: [
                    {
                        field: 'manufacturer_name',
                        operator: 'LIKE',
                        value: '%%'
                    }
                ]
            };
            this.dropdownService.manufacturesDropDown(reqbody).subscribe(
                (values: ManufacturerDropData) => {
                    try {
                        const dropData = values.data;
                        this.Manufacturers.push({ Id: '', Name: 'Select Manufacturer' });
                        for (let i = 0; i <= dropData.length - 1; i++) {
                            this.Manufacturers.push({ Id: dropData[i].id, Name: dropData[i].manufacturer_name });
                        }
                        this.dropdownService.Manufactures = this.Manufacturers;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }
                });
        }
    }
    /**
     * Loadstatus inventory frame advanced filter component
     * @returns status details
     */
    Loadstatus() {
        this.activestatus = [];
        this.activestatus.push({ Id: '0', Name: 'Active' });
        this.activestatus.push({ Id: '1', Name: 'Discontinue' });

    }
    // LoadLocations() {
    //     this.locations = [];
    //     this.frameService.getLocations().subscribe(
    //         data => {
    //             this.locationslist = data['result']['Optical']['Locations'];

    //             this.locations.push({ Id: null, Name: ' Select Location' });
    //             for (let i = 0; i <= this.locationslist.length - 1; i++) {

    //                 if (!this.locations.find(a => a.Name === this.locationslist[i]['Name'])) {
    //                     this.locations.push({ Id: this.locationslist[i]['Id'], Name: this.locationslist[i]['Name'] });
    //                 }
    //             }
    //         });
    // }

    LoadFrameShapes() {
        this.framesshapes = [];
        this.framesshapes = this.dropdownService.framesshapes;
        if (this.utility.getObjectLength(this.framesshapes) === 0) {
            this.dropdownService.getFrameShapes().subscribe(
                data => {
                    try {
                        this.framesshapeslist = data['result']['Optical']['FrameShapes'];
                        this.framesshapes.push({ Id: null, Name: ' Select Frame Shapes' });
                        for (let i = 0; i <= this.framesshapeslist.length - 1; i++) {
                            if (!this.framesshapes.find(a => a.Name === this.framesshapeslist[i]['Name'])) {
                                this.framesshapes.push({ Id: this.framesshapeslist[i]['Id'], Name: this.framesshapeslist[i]['Name'] });
                            }
                        }
                        this.dropdownService.framesshapes = this.framesshapes;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }
                });
        }
    }

    LoadMaterialType() {
        this.materials = this.dropdownService.materials;
        if (this.utility.getObjectLength(this.materials) === 0) {
            this.dropdownService.getLensMaterial().subscribe((values: SpectacleMaterial) => {
                try {
                    const dropData = values.data;
                    this.materials.push({ value: '', name: 'Select Material' });
                    for (let i = 0; i <= dropData.length - 1; i++) {
                        this.materials.push({ name: dropData[i].material_name, value: dropData[i].id });
                    }
                    this.dropdownService.materials = this.materials;
                } catch (error) {
                    this.error.syntaxErrors(error);
                }
            });
        }
    }


    LoadFrameTypes() {
        this.framestypes = this.frameService.framesTypeDropDownData;
        if (this.utility.getObjectLength(this.framestypes) === 0) {
            this.frameService.getFramesType().subscribe((values: FrameTypeMaster) => {
                try {
                    const dropData = values.data;
                    this.framestypes.push({ Id: '', Name: 'Select Frametype' });
                    for (let i = 0; i <= dropData.length - 1; i++) {
                        this.framestypes.push({ Id: dropData[i].id, Name: dropData[i].type_name });
                    }
                    this.frameService.framesTypeDropDownData = this.framestypes;
                } catch (error) {
                    this.error.syntaxErrors(error);
                }
            });
        }
    }




    /**
     * Advanced filter get for filter the data
     */
    advancedFilterGet() {
        const formDataList = this.FarmesAdvanceFilterForm.value;
        this.frameAdvFilterOutPut.emit(formDataList);
    }
    Clearall() {
        this.formGroupdata();
    }
}


