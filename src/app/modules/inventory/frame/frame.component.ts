
import { Component, OnInit, ViewChild, OnDestroy } from '@angular/core';
import { SelectItem } from 'primeng/primeng';
import { SortEvent } from 'primeng/primeng';
import { Subscription } from 'rxjs';
import { FramesService } from '@services/inventory/frames.service';
import { ErrorService } from '@app/core/services/error.service';
import { DialogBoxComponent } from '@app/shared/components/dialog-box/dialog-box.component';
import { InventoryService } from '@app/core/services/inventory/inventory.service';
import { DropdownService } from '@app/shared/services/dropdown.service';
import { UtilityService } from '@app/shared/services/utility.service';
import { Manufacturer } from '@app/core/models/inventory/Manufacturer.model';
import { FrameBrand } from '@app/core/models/inventory/Brands.model';
import { Optical, FrameColorData, Data } from '@app/core/models/inventory/Colors.model';
import { DeactivateComponent } from '../deactivate/deactivate.component';
import { ImportCsvComponent } from '../import-csv/Import-csv.component';
import { Frame, GettingFramesDetails } from '@app/core/models/inventory/frames.model';
import { ManufacturerDropData, FrameBrands } from '@app/core/models/masterDropDown.model';

@Component({
    selector: 'app-frame',
    templateUrl: './frame.component.html'
})
export class FrameComponent implements OnInit, OnDestroy {
    advancedFilterSidebarFrame = false;
    rowData: any[];
    activestatus: any[];
    searchcolour: any = '';
    searchbrand: any = null;
    searchColor = '';
    manufratureValue: any;
    searchIsDot = '';
    framecolourValue: any;
    searchIsTrial = '';
    searchUPC = '';
    searchQtyOnHand = '';
    searchName = '';
    searchValue = '';
    samplevisible = true;
    visible = true;
    display: boolean;
    addFramesSidebar = false;
    frameDetailsSidebar = false;
    addbarcodelabels = false;
    reActivateItems = false;
    sortallinventory = false;
    selectFrameSidebar: boolean;
    searchManufa: any = null;
    addLoction = false;
    locationFrameId: any;
    imageItemId: any;
    paginationPage: any = 0;
    paginationValue: any = 0;
    pageRowsCount: any = 15;
    pageRowsvalue: any = 15;
    /**
     * Pagination rows of frame component for getting pagination rows value
     */
    paginationRows: number;
    /**
     * Searchstatus  of frame component for search status value
     */
    searchstatus: string;
    @ViewChild('dt') public dt: any;
    @ViewChild('deleteRow') DeleteRows: DialogBoxComponent;
    /**
     * View child of frame component for deactivate pop up calling
     */
    @ViewChild('deactivateRow') deactivateRows: DeactivateComponent;
    /**
     * View child of frame component for import popup calling
     */
    @ViewChild('importcsvBox') importCsvdialog: ImportCsvComponent;
    public manufacturerlist: Manufacturer[];
    public Brandlist: FrameBrand[];
    /**
     * Framelist  of frame component for storing frame grid data
     */
    public framelist: Frame[];
    public Manufacturers: any[];
    public Brands: any[];
    /**
     * Colors  of frame component
     * stores list of frame colors
     */
    public colors: Array<object>;
    public Activestatus: any[];
    public ColorCodes: SelectItem[];
    public QtyOnHands: SelectItem[];
    Framecolumns: any[];
    Accesstoken = '';
    maufacturerid = '';
    upc = '';
    colorcode = '';
    barndid = '';
    colorid = '';
    delstatusID: any = '';
    Framename = '';
    advancedFilterSidebar = false;
    framesMultipleSelection = [];
    // subscriptionframesChange: Subscription;
    subscriptionframesclose: Subscription;
    subscriptionlocationclose: Subscription;
    // subscriptionAdvanceFilter: Subscription;
    // subscriptionDeactive: Subscription;
    subscriptionCSV: Subscription;
    totalRecords: string;
    /**
     * Pay load of frame component
     */
    payLoad: any;
    /**
     * Row id of frame component
     */
    rowID = true;
    /**
     * Row lens name of frame component
     */
    rowLensName = true;
    /**
     * Row color of frame component
     */
    rowColor = true;
    /**
     * Row packaging of frame component
     */
    rowPackaging = true;
    /**
     * Row trial of frame component
     */
    rowTrial = true;
    /**
     * Row dot of frame component
     */
    rowDot = true;
    /**
     * Row on hand of frame component
     */
    rowOnHand = true;
    /**
     * Row upc of frame component
     */
    rowUPC = true;
    /**
     * Row manufacturer of frame component
     */
    rowManufacturer = true;
    /**
     * Sort by name of the frame component of assigning names
     */
    sortByName: any;
    /**
     * Sortby value of the frame component of sort by id
     */
    sortbyValue: any = "id";
    /**
     * thenbyValue  of frame component of sort items
     */
    thenbyValue: any = [];

    /**
     * Sort enable mode of frame component
     */
    sortEnableMode: boolean;

    /**
     * Sort order of frame component for getting sort order status
     */
    sortOrder: string;
    deleteRows() {
        // this.DeleteRows.dialogBox();
    }
    deActive() {
        if (this.framesMultipleSelection.length !== 0) {
            const Framedata = {
                'FrameSelection': this.framesMultipleSelection
            };
            this.deactivateRows.deactivate(Framedata);
        }
    }
    importcsv() {
        this.importCsvdialog.importCsvbox('frames');
    }
    constructor(private frameService: FramesService,
        private _InventoryCommonService: InventoryService,
        // private _DropdownsPipe: DropdownsPipe,
        private error: ErrorService, public utility: UtilityService,
        private dropdownService: DropdownService,
    ) {


    }

    ngOnInit() {
        this.paginationValue = 0;
        this.paginationRows = 100;
        this.sortOrder = 'DESC';
        this.searchstatus = '';
        this.searchbrand = '';
        this.searchManufa = '';
        this.sortEnableMode = false;
        // localStorage.removeItem('InventoryType');
        // localStorage.setItem('InventoryType', 'Frames');
        this._InventoryCommonService.InventoryType = 'Frames';
        this.loadFramescolumns();
        this.GetFramesList();
        this.Loadmastersdata();
        // localStorage.removeItem('editframeid');
        // localStorage.removeItem('editDuplicateId');
        this._InventoryCommonService.barCodeLable = '';
        // this.Manufacturers = [];
        // this.Manufacturers = this._DropdownsPipe.manufacturersDropData();
        // this.Brands = [];
        // this.Brands = this._DropdownsPipe.brandsDropData();
        // this.Colors = [];
        // this.Colors = this._DropdownsPipe.coloursDropData();
        this.LoadManufacturers();
        this.LoadBrands();
        this.LoadColors();
    }
    /**
     * Loads colors for loading colors dropdown
     * @returns list of colors data
     */
    LoadColors() {
        this.colors = this.dropdownService.colors;
        if (this.utility.getObjectLength(this.colors) === 0) {
            this.dropdownService.getColors().subscribe(
                (values: FrameColorData) => {
                    try {
                        const dropData = values.data;
                        this.colors.push({ Id: '', Name: 'Select Color' });
                        for (let i = 0; i <= dropData.length - 1; i++) {

                            this.colors.push({ Id: dropData[i].id, Name: dropData[i].color_name });
                        }
                        this.dropdownService.colors = this.colors;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }
                });
        }

    }
    LoadBrands() {
        this.Brands = this.dropdownService.brands;
        if (this.utility.getObjectLength(this.Brands) === 0) {
            this.dropdownService.getBrands().subscribe(
                (values: FrameBrands) => {
                    try {
                        const dropData = values.data;
                        this.Brands.push({ Id: '', Name: 'Select Brand' });
                        for (let i = 0; i <= dropData.length - 1; i++) {
                            this.Brands.push({ Id: dropData[i].id, Name: dropData[i].frame_source });
                        }
                        this.dropdownService.brands = this.Brands;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }
                });
        }
    }
    /**
     * Loads manufacturers for loading dropdown
     */
    LoadManufacturers() {
        this.Manufacturers = this.dropdownService.Manufactures;
        if (this.utility.getObjectLength(this.Manufacturers) === 0) {
            const reqbody = {
                filter: [
                    {
                        field: 'manufacturer_name',
                        operator: 'LIKE',
                        value: '%%'
                    }
                ]
            };
            this.dropdownService.manufacturesDropDown(reqbody).subscribe(
                (values: ManufacturerDropData) => {
                    try {
                        const dropData = values.data;
                        this.Manufacturers.push({ Id: '', Name: 'Select Manufacturer' });
                        for (let i = 0; i <= dropData.length - 1; i++) {
                            this.Manufacturers.push({ Id: dropData[i].id, Name: dropData[i].manufacturer_name });
                        }
                        this.dropdownService.Manufactures = this.Manufacturers;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }
                });
        }
    }
    frameAdvFilterOutPutEvent(event) {
        this.advencedFilterList(event);
    }
    /**
     * Advenced filter list
     * @param data for getting the filter data
     */
    advencedFilterList(data) {
        const filterListData = data;
        this.upc = filterListData.UPC;
        this.Framename = filterListData.Name;
        this.searchName = filterListData.Name;
        this.searchValue = filterListData.ID;
        this.maufacturerid = filterListData.ManufacturerId;
        this.searchManufa = filterListData.ManufacturerId;
        this.searchbrand = filterListData.Brand;
        this.colorid = filterListData.color;
        this.searchcolour = filterListData.color;
        this.delstatusID = filterListData.del_status;
        this.searchstatus = filterListData.del_status;
        this.searchUPC = filterListData.UPC;
        this.GetFramesListBySorting();
        this.advancedFilterSidebar = false;

    }
    GetreActivateItems() {
        this.reActivateItems = true;
        // this.frameService.reactivescreenRefresh.next('value');
    }
    Loadmastersdata() {
        this.Loadstatus();
    }
    frameReactiveOutputEvent(event) {
        if (event === 'reactiveClose') {
            this.reActivateItems = false;
        }
        if (event === 'reactivate') {
            this.GetFramesList();
            this.framesMultipleSelection = [];
            this.reActivateItems = false;
        }
    }

    Loadstatus() {
        this.activestatus = [];
        this.activestatus.push({ Id: '0', Name: 'Active' });
        this.activestatus.push({ Id: '1', Name: 'Discontinue' });
    }



    // LoadBrands() {
    //     this.Brands = [];
    //     this.frameService.getBrands().subscribe(
    //         data => {
    //             this.Brandlist = data['result']['Optical']['FrameBrands'];

    //             for (let i = 0; i <= this.Brandlist.length - 1; i++) {

    //                 if (!this.Brands.find(a => a.Name === this.Brandlist[i]['Name'])) {
    //                     this.Brands.push({ Id: this.Brandlist[i]['Id'], Name: this.Brandlist[i]['Name'] });
    //                 }
    //             }
    //         });
    // }

    // LoadSuppliers() {
    //     this.Brands = [];
    //     this.frameService.getBrands().subscribe(
    //         data => {
    //             this.Brandlist = data['result']['Optical']['FrameBrands'];

    //             for (let i = 0; i <= this.Brandlist.length - 1; i++) {

    //                 if (!this.Brands.find(a => a.Name === this.Brandlist[i]['Name'])) {
    //                     this.Brands.push({ Id: this.Brandlist[i]['Id'], Name: this.Brandlist[i]['Name'] });
    //                 }
    //             }
    //         });
    // }

    // LoadColors() {
    //     this.Colors = [];
    //     this.frameService.getColors().subscribe(
    //         data => {
    //             this.Colorlist = data['result']['Optical']['FrameColors'];

    //             for (let i = 0; i <= this.Colorlist.length - 1; i++) {

    //                 if (!this.Colors.find(a => a.Name === this.Colorlist[i]['Name'])) {
    //                     this.Colors.push({ Id: this.Colorlist[i]['Id'], Name: this.Colorlist[i]['Name'] });
    //                 }
    //             }
    //         });
    // }
    // dialogBox() {
    //     this.display = true;
    // }
    DataViewdialogBox() {
        this.display = true;
    }
    /**
     * Checks box click
     * @param item for selected item
     * @param e status of checkbox
     */
    checkBoxClick(item, e) {

        if (item === 'selectAll') {
            this.visible = e.checked;
            this.loadFramescolumns();

            this.rowID = e.checked;
            this.rowLensName = e.checked;
            this.rowColor = e.checked;
            this.rowPackaging = e.checked;
            this.rowTrial = e.checked;
            this.rowDot = e.checked;
            this.rowOnHand = e.checked;
            this.rowUPC = e.checked;
            this.rowManufacturer = e.checked;

            this.applyDataViewChanges(null, null);
        } else {
            this.applyDataViewChanges(item, e.checked);
        }
    }

    /**
     * Applys data view changes
     * @param item for selected item
     * @param selected for select status
     */
    applyDataViewChanges(item, selected) {
        this.Framecolumns.forEach(element => {
            if (element.header === item) {
                element.visible = selected;

            }
        });
        this.displayCols(item, selected);
        const colSpanVar = this.Framecolumns.filter(x => x.visible).length;
        if (colSpanVar < this.Framecolumns.length) {
            this.samplevisible = false;
        } else {
            this.samplevisible = true;
        }



    }
    /**
     * Displays cols
     * @param item for selected item
     * @param selected for select status
     */
    displayCols(item, selected) {
        if (item == 'ID') {
            this.rowID = selected
        }
        if (item == 'Name') {
            this.rowLensName = selected
        }
        if (item == 'Color') {
            this.rowColor = selected
        }
        if (item == 'Color Code') {
            this.rowPackaging = selected
        }
        if (item == 'Brand') {
            this.rowTrial = selected
        }
        if (item == 'Status') {
            this.rowDot = selected
        }
        if (item == 'OnHand') {
            this.rowOnHand = selected
        }
        if (item == 'UPC') {
            this.rowUPC = selected
        }
        if (item == 'Manufacturer') {
            this.rowManufacturer = selected
        }
    }


    // COLUMNS
    /**
     * Loads frames columns 
     */
    loadFramescolumns() {
        this.Framecolumns = [
            { field: 'id', header: 'ID', visible: this.visible },
            { field: 'manufacturer', header: 'Manufacturer', visible: this.visible },
            { field: 'brand', header: 'Brand', visible: this.visible },
            { field: 'name', header: 'Name', visible: this.visible },
            { field: 'color', header: 'Color', visible: this.visible },
            { field: 'color_code', header: 'Color Code', visible: this.visible },
            { field: 'qty_on_hand', header: 'On Hand', visible: this.visible },
            { field: 'upc_code', header: 'UPC', visible: this.visible },
            { field: 'del_status', header: 'Status', visible: this.visible }
        ];


    }


    /**
     * Gets frames list
     * Pull the frames inventories data added and filters data with below parameters
     * @param [upc]
     * @param [manufacturerid]
     * @param [brandid]
     * @param [colorid]
     * @param [Framename]
     * @param [colorcode]
     * @param [delete_status]
     */
    GetFramesList(upc = '', manufacturerid = '', brandid = '', colorid = '', Framename = '', colorcode = '', delete_status = '') {
        // Get History Params


        this.frameService.getFramesGridData().subscribe(
            data => {
                this.framelist = [];
                this.totalRecords = data['total'];
                for (let i = 0; i <= data['data'].length - 1; i++) {
                    if (data['data'][i]['del_status'] == 0) {
                        data['data'][i]['del_status'] = data['data'][i]['del_status'] == 0 ? 'Active' : 'Discontinue';
                        this.framelist.push(data['data'][i]);
                    }
                }
            })
        // upc = this.searchUPC;
        // Framename = this.searchName;
        // colorid = (this.searchcolour == null) ? '' : this.searchcolour;
        // brandid = (this.searchbrand == null) ? '' : this.searchbrand;
        // manufacturerid = (this.searchManufa == null) ? '' : this.searchManufa;
        // colorcode = this.searchColor;
        // delete_status = this.delstatusID;

        // this.framelist = [];
        // this.frameService.getframes(upc, manufacturerid, brandid, colorid, '15', Framename, colorcode, delete_status).subscribe(
        //     data => {
        //         if (data['result']['Optical'] !== 'No record found') {
        //             this.totalRecords = data['result']['Optical']['Total'];
        //             for (let i = 0; i <= data['result']['Optical']['Frames'].length - 1; i++) {
        //                 data['result']['Optical']['Frames'][i]['itemId'] = parseInt(data['result']['Optical']['Frames'][i]['itemId'], 10);
        //                 data['result']['Optical']['Frames'][i]['del_status'] =
        //                     data['result']['Optical']['Frames'][i]['del_status'] === '0' ? 'Active' : 'Discontinue';
        //             }
        //             this.framelist = data['result']['Optical']['Frames'];
        //         }

        //     });
        // this.framesMultipleSelection = [];

    }
    /**
     * Loads frame data
     * @param [page] for getting page number
     */
    loadFrameData(page: number = 0) {

        if (this.sortEnableMode === false) {

            this.sortbyValue = 'id';
            this.thenbyValue = [];
            this.payloadFilter();
            this.frameService.getFrameDataByPagination(this.payLoad, page).subscribe(
                (data: Frame) => {
                    this.totalRecords = data['total'];
                    this.framelist = [];
                    for (let i = 0; i <= data['data'].length - 1; i++) {
                        data['data'][i]['del_status'] = data['data'][i]['del_status'] === 0 ? 'Active' : 'Discontinue';
                        this.framelist.push(data['data'][i]);
                    }
                },
            );
        } else {
            this.payloadFilter();
            this.sortByAllPaginator(this.payLoad, page);
        }

    }


    /**
     * Gets frames list by paginator
     * @param event for getting page rows and counts
     */
    GetFramesListByPaginator(event) {
        this.framesMultipleSelection = [];
        this.paginationValue = event.first;
        this.paginationRows = event.rows;
        let page = 0;
        page = ('first' in event && 'rows' in event)
            ? (event.first / event.rows) + 1
            : 0;

        this.loadFrameData(page);

    }



    customSort(event: SortEvent) {
        this.thenbyValue = [];
        this.sortEnableMode = true;
        this.dt.first = this.paginationValue;
        switch (event.field) {
            case 'id': {
                this.sortbyValue = 'id';
                break;
            }
            case 'name': {
                this.sortbyValue = 'name';
                break;
            }
            case 'color_code': {
                this.sortbyValue = 'color_code';
                break;
            }
            case 'qty_on_hand': {
                this.sortbyValue = 'qty_on_hand';
                break;
            }
            case 'upc_code': {
                this.sortbyValue = 'upc_code';
                break;
            }
            case 'manufacturer': {
                this.sortbyValue = 'manufacturer_name';
                break;
            }
            case 'brand': {
                this.sortbyValue = 'brand';
                break;
            }
            case 'color': {
                this.sortbyValue = 'color';
                break;
            }
        }
        this.sortOrder = 'ASC';
        if (event.order !== 1) {
            this.sortOrder = 'DESC';
        }
        let page = 0;
        page =  (this.paginationValue / this.paginationRows) + 1;
        this.payloadFilter();
        this.sortByAllPaginator(this.payLoad, page);
    }

    /**
     * Determines whether row select on
     * @param event for getting selected item id
     */
    onRowSelect(event) {
        // localStorage.removeItem('editframeid');
        // localStorage.removeItem('editDuplicateId');
        // localStorage.setItem('editframeid', event.data.UPC);
        this.frameService.editframeid = event.data.id;

        this._InventoryCommonService.FortrasactionItemID = this.framesMultipleSelection[0].id;

        // localStorage.setItem("editDuplicate", event.data);
        // this.rowData =  event.data;


        // this.frameService.FrameListItemId.next(event.data.id);
    }
    onRowUnselect(event) {
        // localStorage.removeItem('editframeid');
        // localStorage.removeItem('editDuplicateId');
        this.frameService.editframeid = '';
        this.frameService.editDuplicateId = '';
    }
    /**
     * Adds frames for open the popup
     */
    addFrames() {
        this.imageItemId = undefined;
        this.locationFrameId = undefined;
        // localStorage.removeItem('FortrasactionItemID');
        // localStorage.setItem('FortrasactionItemID', '');
        // if (this.framesMultipleSelection.length === 1) {
        //     this.locationFrameId = this.framesMultipleSelection[0].id;
        // } else {
        //     this.locationFrameId = undefined;
        // }
        this.framesMultipleSelection = [];
        this.dt.selection = true;
        this.dt.selection = false;
        this.addFramesSidebar = true;
        this.frameService.editframeid = '';
        this.frameService.editframeidDoubleClick = '';
        this._InventoryCommonService.FortrasactionItemID = '';
        // localStorage.removeItem('editframeid');
        // localStorage.removeItem('editframeidDoubleClick');
        // localStorage.removeItem('editDuplicateId');
        // this._router.navigate(['add-modify-frames']);
    }

    /**
     * Update frame items
     */
    updateframes() {
        this.frameService.errorHandle.msgs = [];
        // localStorage.removeItem('FortrasactionItemID');
        // localStorage.setItem('FortrasactionItemID', this.framesMultipleSelection[0].id);
        this._InventoryCommonService.FortrasactionItemID = this.framesMultipleSelection[0].id;
        if (this.framesMultipleSelection.length === 1) {
            this.locationFrameId = this.framesMultipleSelection[0].id;
            this.imageItemId = this.framesMultipleSelection[0].id;
        } else {
            this.locationFrameId = undefined;
            this.imageItemId = undefined;
        }
        // localStorage.removeItem('editDuplicateId');
        // localStorage.removeItem('editframeidItemTrasaction');
        if (this.framesMultipleSelection.length === 1) {
            // localStorage.setItem('editframeid', this.framesMultipleSelection[0].UPC);
            this.frameService.editDuplicateId = '';
            this.frameService.editframeid = this.framesMultipleSelection[0].id;
            // localStorage.setItem('editframeidItemTrasaction', this.framesMultipleSelection[0].id);
            // this.dt.selection = true;
            const selectedFrameid = this.frameService.editframeid;
            // localStorage.getItem('editframeid');
            if (!selectedFrameid) {
                // this.dt.selection = false;
                this.error.displayError({ severity: 'error', summary: 'Error Message', detail: 'Please Select Frame.' });

                // this.msgs.push({ severity: 'error', summary: 'Error Message', detail: 'Please Select Frame.' });

            } else {
                this.addFramesSidebar = true;
                //  this.framesMultipleSelection = [];
                // this._router.navigate(['add-modify-frames']);
            }
        } else {
            this.error.displayError({ severity: 'error', summary: 'Error Message', detail: 'Please Select one Item.' });

            // this.msgs.push({ severity: 'error', summary: 'Error Message', detail: 'Please Select one Item.' });

        }
    }
    /**
     * Duplicates frame items
     */
    Duplicate() {
        this.frameService.errorHandle.msgs = [];
        // let duplicateData =
        // localStorage.setItem("editDuplicateData",localStorage.getItem("editDuplicate"));
        // localStorage.removeItem('editframeid');
        // localStorage.removeItem('FortrasactionItemID');
        // localStorage.removeItem('editframeidDoubleClick');
        // localStorage.removeItem('editDuplicateId');
        if (this.framesMultipleSelection.length === 1) {
            this.frameService.editframeid = '';
            this.frameService.editframeidDoubleClick = '';
            // localStorage.setItem('editDuplicateId', this.framesMultipleSelection[0].UPC);
            this.frameService.editDuplicateId = this.framesMultipleSelection[0].id;
            this.imageItemId = this.framesMultipleSelection[0].id;
            this.addFramesSidebar = true;
        } else {
            this.error.displayError({ severity: 'error', summary: 'Error Message', detail: 'Please Select One Item.' });

            // this.msgs.push({ severity: 'error', summary: 'Error Message', detail: 'Please Select one Item.' });

        }
        this.framesMultipleSelection = [];
        // this.frameService.editDuplicate.next(this.rowData);



    }
    /**
     * Details frame
     * @returns FrameDetails page
     */
    DetailsFrame() {
        if (this.framesMultipleSelection.length === 0) {
            this.frameDetailsSidebar = false;
            this.error.displayError({ severity: 'error', summary: 'Error Message', detail: 'Please Select Atleast One Item.' });

        } else {
            this.frameDetailsSidebar = true;
        }
    }
    /**
     * Doubles clickframes
     * @param rowData for getting clicked row data
     */
    DoubleClickframes(rowData) {
        // localStorage.removeItem('editDuplicateId');
        // localStorage.removeItem('FortrasactionItemID');
        // localStorage.setItem('FortrasactionItemID', rowData.id);
        this.frameService.editDuplicateId = '';
        this._InventoryCommonService.FortrasactionItemID = rowData.id;
        // this.dt.selection = true;
        // this.dt.reset();
        //  this.addFramesSidebar = true;rowData
        //  if (this.framesMultipleSelection.length == 0) {
        this.framesMultipleSelection = [];
        // localStorage.setItem('editframeidDoubleClick', rowData.UPC);
        this.frameService.editframeidDoubleClick = rowData.id;
        this.framesMultipleSelection.push(rowData);
        this.addFramesSidebar = true;
        this.locationFrameId = rowData.id;
        this.imageItemId = rowData.id;
        // if (this.framesMultipleSelection.length <= 1){

        //     this.addFramesSidebar = true;
        // }else{
        //     this.msgs.push({ severity: 'error', summary: 'Error Message', detail: 'Please Select one Item.' });

        // }

        // this.frameService.FrameListItemDoubleClickId.next(rowData.UPC);

    }
    /**
     * Objs changed
     * @param event for getting changed event values
     */
    objChanged(event: any) {

        if (event.target.name === 'id') {
            this.searchValue = event.target.value;
        }
        if (event.target.name === 'manufacturer') {
            this.maufacturerid = '';
            this.maufacturerid = this.searchManufa;

            if (this.maufacturerid == null) {
                this.maufacturerid = '';
            } else {

                this.maufacturerid = this.searchManufa;
            }



        }


        if (event.target.name === 'del_status') {

            this.delstatusID = '';
            this.delstatusID = event.target.selectedIndex;

            if (this.delstatusID === 0) {
                this.delstatusID = '';
            } else if (this.delstatusID === 1) {
                this.delstatusID = '0';

            } else if (this.delstatusID === 2) {
                this.delstatusID = 1;
            }
        }


        if (event.target.name === 'framebrand') {

            this.barndid = '';
            this.barndid = this.searchbrand;

            if (this.barndid == null) {
                this.barndid = '';
            } else {

                this.barndid = this.searchbrand;
            }

        }


        if (event.target.name === 'framename') {

            this.Framename = '';
            this.Framename = event.target.value;

            if (this.Framename === '') {
                this.Framename = '';
            } else {

                this.Framename = event.target.value;
            }

        }

        if (event.target.name === 'framecolor') {
            this.colorid = '';
            this.colorid = this.searchcolour;

            if (this.colorid == null) {
                this.colorid = '';
            } else {
                this.colorid = this.searchcolour;
            }

        }

        if (event.target.name === 'upc') {
            this.upc = event.target.value;
        }

        if (event.target.name === 'colorcode') {
            this.colorcode = event.target.value;
        }
        if (event.target.name === 'qty_on_hand') {
            this.searchQtyOnHand = event.target.value;
        }
        this.dt.reset();
        this.paginationValue = 0;
        this.paginationRows = 100;
        this.GetFramesListBySorting();

    }
    /**
     * Gets frames list by sorting
     */
    GetFramesListBySorting() {
        this.framelist = [];
        this.payloadFilter();
        if (this.sortEnableMode === false) {
            this.payloadFilter();
            this.sortbyValue = 'id';
            this.thenbyValue = [];
            this.frameService.getFramesSearchData(this.payLoad).subscribe(
                (data: Frame) => {
                    this.framelist = [];
                    this.totalRecords = data['total'];
                    for (let i = 0; i <= data['data'].length - 1; i++) {
                        // if (data['data'][i]['del_status'] == 0) {
                        data['data'][i]['del_status'] = data['data'][i]['del_status'] === 0 ? 'Active' : 'Discontinue';
                        this.framelist.push(data['data'][i]);
                        // }
                    }
                });
        } else {
            this.payloadFilter();
            this._InventoryCommonService.getSortByAll(this.payLoad).subscribe((data: Frame) => {
                this.framelist = [];
                this.totalRecords = data['total'];
                for (let i = 0; i <= data['data'].length - 1; i++) {
                    // if (data['data'][i]['del_status'] == 0) {
                    data['data'][i]['del_status'] = data['data'][i]['del_status'] === 0 ? 'Active' : 'Discontinue';
                    this.framelist.push(data['data'][i]);
                    // }
                }
            });
        }

    }
    /**
     * Resets frame component
     * @param table getting the table status
     */
    reset(table) {

        this.dt.selection = false;
        table.reset();
        this.searchValue = '';
        this.searchName = '';
        this.searchQtyOnHand = '';
        this.searchUPC = '';
        this.searchColor = '';
        this.searchManufa = '';
        this.searchbrand = '';
        this.searchcolour = '0';
        this.upc = '';
        this.maufacturerid = '';
        this.barndid = '';
        this.colorid = '';
        this.Framename = '';
        this.colorcode = '';
        this.searchstatus = '';
        // localStorage.removeItem('editframeid');
        // localStorage.removeItem('editDuplicateId');

        this.GetFramesList();
        this.paginationValue = 0;
        this.paginationRows = 100;
        this.sortEnableMode = false;
        this.sortOrder = 'DESC';
        this.sortbyValue = 'id';
    }

    /**
     * Adds loctionclick
     */
    addLoctionclick() {
        // localStorage.removeItem('multiLocationForOtherInventory');

        this.frameService.errorHandle.msgs = [];
        this._InventoryCommonService.InventoryType = 'Frames';

        if (this.framesMultipleSelection.length === 1) {
            // this.framesMultipleSelection.forEach(element => {
            // this.frameService.FrameListItemId.next(element.id);
            // localStorage.setItem('multiLocationForOtherInventory', this.framesMultipleSelection[0].id);
            this._InventoryCommonService.FortrasactionItemID = this.framesMultipleSelection[0].id;
            // })


            this.addLoction = true;
        } else {
            this.error.displayError({
                severity: 'error',
                summary: 'Error Message', detail: 'Please Select Frame Only One Frame.'
            });

            // this.msgs.push({ severity: 'error', summary: 'Error Message', detail: 'Please Select Frame Only One Frame.' });

        }
    }

    ngOnDestroy(): void {
        //  this.subscriptionframesChange.unsubscribe();
        // this.subscriptionframesclose.unsubscribe();
        // this.subscriptionlocationclose.unsubscribe();
        // this.subscriptionAdvanceFilter.unsubscribe();
        // this.subscriptionDeactive.unsubscribe();
        // this.subscriptionCSV.unsubscribe();
    }
    advancedfilter() {
        this.advancedFilterSidebar = true;
    }
    selectFrameSidebarSet() {
        this.selectFrameSidebar = true;
    }
    addbarcodelabelsSet() {
        if (this.framesMultipleSelection.length !== 0) {
            this._InventoryCommonService.barCodelist = this.framesMultipleSelection;
            this._InventoryCommonService.barCodeLable = 'FramesBarcode';
        } else {
            this._InventoryCommonService.barCodelist = [];
            this._InventoryCommonService.barCodeLable = 'FramesBarcode';
        }
        this.addbarcodelabels = true;
    }
    /**
     * Frames details output
     * @param event  closing pop up  and refreshing the grid items
     */
    /**
     * Params frame component
     * @param event for getting data from details page
     */
    frameDetailsOutput(event) {
        if (event === 'close') {
            this.frameDetailsSidebar = false;
        }
        if (event === 'ok') {
            this.GetFramesList();
            this.frameDetailsSidebar = false;

        }
    }

    /**
     * Sortallevents frames component
     * @param event for sort the data grid
     */
    sortallevent(event) {
        this.sortEnableMode = event.sortEnable;
        this.framelist = event.gridData;
        this.sortallinventory = false;
        this.sortbyValue = event.sortValue;
        this.thenbyValue = event.thenValue;
        if (event.sortEnable === true) {
            this.sortOrder = 'ASC';
        }
        let page = 0;
        page =  (this.paginationValue / this.paginationRows) + 1;
        this.payloadFilter();
        this.sortByAllPaginator(this.payLoad, page);
    }
    /**
     * Gets sort all
     * for displaying the sort all side-bar
     */
    getSortAll() {
        this.sortallinventory = true;
        this.sortByName = 'frames';
    }
    frameDeactivevent(event) {
        if (event === 'FrameDeactive') {
            this.GetFramesList();
            this.framesMultipleSelection = [];
        }
    }
    frameLocationsEvent(event) {
        if (event === 'frame') {
            this.addLoction = false;
        } else {
            this.addLoction = false;
            // this.GetFramesList();
        }
    }

    /**
     * Adds modify output event
     * @param event for getting data from child component 
     */
    addModifyOutputEvent(event) {
        if (event.NewValue === 'close') {
            this.addFramesSidebar = false;
        }
        if (event.NewValue === 'New') {
            this.addFramesSidebar = false;
            this.GetFramesList();
        }
        if (event.Data) {
            this.addFramesSidebar = false;
            this.GetFramesListAfterSave(event.Data);

        }
    }
    /**
     * Gets frames list after save
     * @param data for updated data
     */
    GetFramesListAfterSave(data) {
        // for (let i = 0; i <= this.framelist.length - 1; i++) {
        //     if (this.framelist[i]['id'] == data.id) {
        //         if(data['del_status'] == 0){
        //             data['del_status'] =  'Active';
        //         } else{
        //             data['del_status'] =  'Discontinue';
        //         }
        //         this.framelist[i] = data;
        //     }
        // }
        this.framelist.forEach(element => {
            if (element['id'] === data.id) {
                if (data['del_status'] == 0) {
                    data['del_status'] = 'Active';
                } else {
                    data['del_status'] = 'Discontinue';
                }
                element['id'] = data.id,
                    element['upc_code'] = data.upc_code,
                    element['name'] = data.name,
                    element['manufacturer_id'] = data.ManufacturerId,
                    element['manufacturer.manufacturer_name'] = data.manufacturer.manufacturer_name,
                    element['brand_id'] = data.brand_id,
                    element['brand'] = data.brand,
                    // element['frame_type']['id'] = data.frame_type.id,
                    // element['frame_type.type_name'] = data.frame_type.type_name,
                    element['color_code'] = data.color_code,
                    element['colorid'] = data.colorid,
                    // element['color'] = data.color,
                    element['frame_shape'] = data.frame_shape,
                    element['frame_style'] = data.frame_style,
                    element['style'] = data.style,
                    element['qty_on_hand'] = data.qty_on_hand,
                    element['a'] = data.a,
                    element['b'] = data.b,
                    element['ed'] = data.ed,
                    element['dbl'] = data.dbl,
                    element['temple'] = data.temple,
                    element['bridge'] = data.bridge,
                    element['fpd'] = data.fpd,
                    element['module_type_id'] = data.module_type_id,
                    element['vendor_id'] = data.vendor_id,
                    element['rimtypeid'] = data.rimtypeid,
                    element['sourceid'] = data.sourceid,
                    element['frameusageid'] = data.frameusageid,
                    element['material_id'] = data.material_id,
                    element['upctypeid'] = data.upctypeid,
                    element['procedure_code'] = data.procedure_code,
                    element['gender'] = data.gender,
                    element['inventory'] = data.inventory,
                    element['eyesize'] = data.eyesize,
                    element['groupcost'] = data.groupcost,
                    element['frame_lens_range_id'] = data.frame_lens_range_id,
                    element['sku'] = data.sku,
                    element['msrp'] = data.msrp,
                    element['notes'] = data.notes,
                    element['min_qty'] = data.min_qty,
                    element['max_qty'] = data.max_qty,
                    element['lenscolor'] = data.lenscolor,
                    element['structure_id'] = data.structure_id,
                    element['retail_price'] = data.retail_price,
                    element['amount'] = data.amount,
                    element['trace_file'] = data.trace_file,
                    element['status'] = data.status
                element['spiff'] = data.spiff,
                    element['commission_type_id'] = data.commission_type_id,
                    element['gross_percentage'] = data.gross_percentage,
                    element['ida_id'] = data.ida_id,
                    element['wholesale_cost'] = data.wholesale_cost,
                    element['del_status'] = data.status == 0 ? 'Active' : 'Discontinue',
                    element['manufacturer']['id'] = data.manufacturer.id,
                    element['manufacturer']['manufacturer_name'] = data.manufacturer.manufacturer_name
            }
        });
    }

    /**
     * Payloads filter for searching and pagination of the frames list
     */
    payloadFilter() {
        let filterBody = {
            filter: [
                {
                    field: 'module_type_id',
                    operator: '=',
                    value: '1'
                },
                {
                    field: 'name',
                    operator: 'LIKE',
                    value: '%' + this.Framename + '%'
                },
                {
                    field: 'id',
                    operator: 'LIKE',
                    value: '%' + this.searchValue + '%'
                },
                {
                    field: 'colorid',
                    operator: '=',
                    value:   this.colorid
                },
                {
                    field: 'upc_code',
                    operator: 'LIKE',
                    value: '%' + this.upc + '%'
                },
                {
                    field: 'manufacturer_id',
                    operator: '=',
                    value: this.maufacturerid
                },

                {
                    field: 'color_code',
                    operator: 'LIKE',
                    value: '%' + this.colorcode + '%'
                },

                {
                    field: 'status',
                    operator: '=',
                    value: this.delstatusID
                },
                {
                    field: 'brand_id',
                    operator: '=',
                    value: this.searchbrand
                },
                {
                    field: 'qty_on_hand',
                    operator: 'LIKE',
                    value: '%' + this.searchQtyOnHand + '%'
                }
            ]
        };
        const resultData = filterBody.filter.filter(p => p.value !== '' && p.value !== null
        && p.value !== undefined && p.value !== '%undefined%'&& p.value !== '%  %' && p.value !== '%%');
        this.payLoad = {
            filter: resultData,
            sort: [
                {
                    field: this.sortbyValue,
                    order: this.sortOrder
                }
            ]
        };
        if (this.thenbyValue.length) {
            for (let i = 0; i <= this.thenbyValue.length - 1; i++) {
                this.payLoad.sort.push({
                    field: this.thenbyValue[i],
                    order: 'ASC'
                });
            }
        }
    }
    /**
     * Resets pagination
     * @returns refreshing the pagination
     */
    resetPagination() {
        this.dt.reset();
    }

    /**
     * Sorts by all paginator
     * @param payload for request payload
     * @param page for pagination number
     */
    sortByAllPaginator(payload: object, page: number) {
        this._InventoryCommonService.getSortByAllpaginator(payload, page).subscribe((values: GettingFramesDetails) => {
            this.framelist = [];
            this.totalRecords = values.total;
            for (let i = 0; i <= values.data.length - 1; i++) {
                values.data[i].del_status = values.data[i].del_status === 0 ? 'Active' : 'Discontinue';
                this.framelist.push(values.data[i]);
            }
        });
    }
}
