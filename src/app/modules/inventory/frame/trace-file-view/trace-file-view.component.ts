import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-trace-file-view',
  templateUrl: 'trace-file-view.component.html'
})
export class TraceFileViewComponent implements OnInit {
  traceFileView = '';
  @Input() TraceFileText: string;
  ngOnInit() {
    if (this.TraceFileText !== '') {
      this.traceFileView = this.TraceFileText;
    }
  }
}
