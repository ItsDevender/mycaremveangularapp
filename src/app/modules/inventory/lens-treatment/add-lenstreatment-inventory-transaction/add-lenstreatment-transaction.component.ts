import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { FramesService } from '@services/inventory/frames.service';
import { ErrorService } from '@app/core/services/error.service';
import { InventoryService } from '@app/core/services/inventory/inventory.service';
import { UtilityService } from '@app/shared/services/utility.service';
import { DatePipe } from '@angular/common';
import { DropdownService } from '@app/shared/services/dropdown.service';
import { AppService } from '@app/core';
import { FrameSupplier, LocationsMaster, TransactionTypeMaster, PaymentTermsMaster } from '@app/core/models/masterDropDown.model';

@Component({
    selector: 'app-add-lenstreatment-inv-transaction',
    templateUrl: 'add-lenstreatment-transaction.component.html'
})

export class AddLensTreatmentInvTransactionComponent implements OnInit {
    locationlist: any;
    locations: Array<LocationsMaster>;
    /**
     * Suppliers  of add lens treatment inv transaction component
     */
    suppliers: Array<object>;
    public supplierslist: any[];
    /**
     * Type of transaction data of add lens treatment inv transaction component
     */
    typeOfTransactionData: Array<object>;
    paymentTermsData: any = [];
    LensTreatmentTransactionForm: FormGroup;
    LensTreatmentTrasactionItemId: string;
    trasactionLensTreamentMsg: Object;
    @Output() addTrasactionOutput = new EventEmitter<any>();
    trasactionFramesMsg: Object;
    /**
     * Creates an instance of add lens treatment inv transaction component.
     * @param frameService provide frameservice
     * @param _fb  frame builder
     * @param _InventoryCommonService provide inventory common service
     * @param error provide error service
     * @param utility  provide utility service
     */
    constructor(
        private frameService: FramesService,
        private _fb: FormBuilder,
        private _InventoryCommonService: InventoryService,
        private error: ErrorService,
        private utility: UtilityService,
        private datePipe: DatePipe,
        public app: AppService,
        private dropdowns: DropdownService) {

    }

    ngOnInit(): void {
        // this.locations = [];
        // this.locations = this._DropdownsPipe.locationsDropData();

        this.typeOfInventoryTrasaction();
        this.getFramesPaymentTerms();
        // this.getFramesStockDetails();
        this.Loadformdata();
        this.GetSupplierDetails();
        this.LoadLocations();
    }

   
    Loadformdata() {
        // this.Accesstoken = localStorage.getItem('tokenKey');
        this.LensTreatmentTrasactionItemId = this._InventoryCommonService.FortrasactionItemID;
        this.LensTreatmentTransactionForm = this._fb.group({          
            item_id: this.LensTreatmentTrasactionItemId,            
            loc_id: ['',[Validators.required]],           
            lot_no: ['',[Validators.required]],
            stock: ['',[Validators.required]],
            trans_type: ['',[Validators.required]],                     
            cost: ['',[Validators.required]],
            invoiceno: ['',[Validators.required]],
            expirationdate: ['',[Validators.required]],
            supplierid: ['',[Validators.required]],
            notes: ['',[Validators.required]],
            received_date: ['',[Validators.required]],
            paymentterms: ['',[Validators.required]],
            returnDate: ['',[Validators.required]],
        });
    }

    typeOfInventoryTrasaction() {

        this.typeOfTransactionData = this.dropdowns.transactionType;
        if (this.utility.getObjectLength(this.typeOfTransactionData) === 0) {
            this.dropdowns.getTypeTrasactionframeData().subscribe(
                (values: TransactionTypeMaster) => {
                    try {
                        const dropData = values.data;
                        this.typeOfTransactionData.push({ id: '', transactiontype: 'Select Type' });
                        for (let i = 0; i <= dropData.length - 1; i++) {
                            dropData[i].id = dropData[i].id.toString();
                            this.typeOfTransactionData.push(dropData[i]);
                        }
                        this.dropdowns.transactionType = this.typeOfTransactionData;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }

                });
        }
    }
    // LoadLocations() {
    //     this.locations = [];
    //     this._ContactlensService.getLocations().subscribe(
    //         data => {
    //             this.locationlist = data['result']['Optical']['Locations'];

    //             for (let i = 0; i <= this.locationlist.length - 1; i++) {

    //                 if (!this.locations.find(a => a.Name === this.locationlist[i]['Name'])) {
    //                     this.locations.push({ Id: this.locationlist[i]['Id'], Name: this.locationlist[i]['Name'] });
    //                 }
    //             }

    //         });
    // }


    LoadLocations() {
        this.locations = this.dropdowns.locationsData;
        if (this.utility.getObjectLength(this.locations) === 0) {
            this.dropdowns.getLocationsDropData().subscribe(
                (data: LocationsMaster[]) => {
                    try {
                        this.locations.push({ id: '', name: 'Select Location' });
                        for (let i = 0; i <= data.length - 1; i++) {
                            this.locations.push(data[i]);
                        }
                        this.dropdowns.locationsData = this.locations;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }
                }
            );
        }
    }

    /**
     * Gets supplier details
     * @returns supplier details data
     */
    GetSupplierDetails() {
        this.suppliers = this.dropdowns.frameSupplier;
        if (this.utility.getObjectLength(this.suppliers) === 0) {
            this.dropdowns.getSupplierDropData().subscribe(
                (values: FrameSupplier) => {
                    try {
                        const dropData  = values.data;
                        this.suppliers.push({ Id: '', Name: 'Select Supplier' });
                        for (let i = 0; i <= dropData.length - 1; i++) {
                                this.suppliers.push({ Id: dropData[i].id, Name: dropData[i].vendor_name });
                        }
                        this.dropdowns.frameSupplier = this.suppliers;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }

                });
        }
    }
    getFramesPaymentTerms() {
        this.paymentTermsData = this.dropdowns.paymentTerms;
        if (this.utility.getObjectLength(this.paymentTermsData) === 0) {
            this.dropdowns.getPaymentTerms().subscribe(
                (values: PaymentTermsMaster) => {
                    try {
                        const dropData  = values.data;
                        this.paymentTermsData.push({ id: '', paymentterm: 'Select Payment Terms' });
                        for (let i = 0; i <= dropData.length - 1; i++) {
                            this.paymentTermsData.push(dropData[i]);
                        }
                        this.dropdowns.paymentTerms = this.paymentTermsData;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }
                });
        }
    }
    saveLensTreatmentTransaction() {
        this.frameService.errorHandle.msgs = [];

        if (!this.LensTreatmentTransactionForm.valid) {
            return;
        }
        const transactionData = {
            inventory:
            {
                locationid: this.LensTreatmentTransactionForm.controls['loc_id'].value,
                receive_date: this.datePipe.
                    transform(this.LensTreatmentTransactionForm.controls['received_date'].value, 'yyyy-MM-dd hh:mm:ss'),
                invoice: this.LensTreatmentTransactionForm.controls['invoiceno'].value,
                notes: this.LensTreatmentTransactionForm.controls['notes'].value,
                supplierid: this.LensTreatmentTransactionForm.controls['supplierid'].value,
                module_type_id: '5'

            }
            ,
            stock:
            {
                trans_type: this.LensTreatmentTransactionForm.controls['trans_type'].value,
                item_id: this.LensTreatmentTrasactionItemId,
                paymenttermsid: this.LensTreatmentTransactionForm.controls['paymentterms'].value,
                returndt: this.datePipe.transform(this.LensTreatmentTransactionForm.controls['returnDate'].value, 'yyyy-MM-dd hh:mm:ss'),
                cost: parseInt(this.LensTreatmentTransactionForm.controls['cost'].value),
                stock: parseInt(this.LensTreatmentTransactionForm.controls['stock'].value),
                expirationdate: this.datePipe.
                    transform(this.LensTreatmentTransactionForm.controls['expirationdate'].value, 'yyyy-MM-dd hh:mm:ss'),
                lot_no: this.LensTreatmentTransactionForm.controls['lot_no'].value
            }

        };

        // console.log(this.FarmesTransactionForm.value)
        this.frameService.saveFrameTransactionDetails(transactionData).subscribe(data => {
            // for (let i = 0; i <= data['record'].length - 1; i++) {
            //     data['record'][i]['id'] = data['record'][i]['id'].toString();
            //     // data["result"]["Optical"]["Frames"][i]["del_status"] =
            //     // data["result"]["Optical"]["Frames"][i]["del_status"] == 0 ? "Active" : "Discontinue";

            // }
            this.trasactionLensTreamentMsg = data;
            // alert(this.trasactionLensTreamentMsg['status']);
            if (this.trasactionLensTreamentMsg['status'] === 'Inventory Transaction added successfully') {
                //
                // this.SuccessShow();
                this.error.displayError({
                    severity: 'success',
                    summary: 'Success Message', detail: 'Frame Transaction Saved Successfully'
                });
                // this.msgs.push({ severity: 'success', summary: 'Success Message', detail: 'Frame Transaction Saved Successfully' });
                this.addTrasactionOutput.emit('');
                // this._LensTreatmentService.LenstransactionClose.next('');
            } else {
                this.error.displayError({
                    severity: 'error',
                    summary: 'Error Message', detail: 'Frame Transaction Failed'
                });
                // this.ErrorShow();

            }
        });
    }
    cancleBtn() {
        this.addTrasactionOutput.emit('cancle');
    }
}
