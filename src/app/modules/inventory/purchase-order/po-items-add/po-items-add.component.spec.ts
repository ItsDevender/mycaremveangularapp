import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PoItemsAddComponent } from './po-items-add.component';

describe('PoItemsAddComponent', () => {
  let component: PoItemsAddComponent;
  let fixture: ComponentFixture<PoItemsAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PoItemsAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PoItemsAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
