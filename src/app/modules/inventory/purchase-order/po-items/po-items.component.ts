import { Component, OnInit, OnDestroy } from '@angular/core';
import { PurchaseOrderService } from '@services/inventory/purchase-order.service';
import { Subscription } from 'rxjs';


@Component({
    selector: 'app-po-items',
    templateUrl: 'po-items.component.html'
})

export class PoItemsComponent implements OnInit, OnDestroy {
    ngOnDestroy(): void {
        // this.updateidSubscription.unsubscribe();
    }

    checked = false;
    sortallinventory: boolean;
    /**
     * Items grid data of po items component
     */
    itemsGridData: any[] = [];
    /**
     * Upc  of po items component
     */
    UPC: string = '';
    /**
     * Open frame sidebar of po items component
     */
    openFrameSidebar: boolean;
    /**
     * Module typevalue of po items component
     */
    moduleTypevalue: string = '';
    /**
     * Demo  of po items component
     */
    demo: any = [];
    /**
     * Updateid subscription of po items component
     */
    updateidSubscription: Subscription
    /**
     * Creates an instance of po items component.
     * @param purchaseOrderService for getting POservice methods
     */
    constructor(private purchaseOrderService: PurchaseOrderService) {
        // this.updateidSubscription =  purchaseOrderService.POitemsUpdate$.subscribe(PoOrderdata => {
        //     console.log('alert');
        // })
    }
    /**
     * on init for initialize methods
     */
    ngOnInit() {
        // this.locations = [
        //     { name: 'MVE: My Vision', retail: '', source: '', Purchased: '', Sold: '', onhand: '', onorder: '', committed: '' },
        //     { name: 'MVE: My Vision', retail: '', source: '', Purchased: '', Sold: '', onhand: '', onorder: '', committed: '' },
        // ];
        if (this.purchaseOrderService.savingItemsList.length != 0) {
            this.itemsGridData = this.purchaseOrderService.savingItemsList
        }
        if (this.purchaseOrderService.UpdateId != '') {
            this.itemsGridData = [];
            // this.purchaseOrderService.getPOitems(this.purchaseOrderService.UpdateId).subscribe(value => {
            //     let itemIds: any = [];
            //     for (let i = 0; i <= value['data'].length - 1; i++) {
            //         itemIds.push(value['data'][i].itemid)
            //     }
            //     if (itemIds.length != 0) {
            //         let filterReqBody = {
            //             "filter": [
            //                 {
            //                     "field": "id",
            //                     "operator": "IN",
            //                     "value": itemIds
            //                 }
    
            //             ],
            //             "sort": [
            //                 {
            //                     "field": "id",
            //                     "order": "ASC"
            //                 }
            //             ]
            //         }
            //         this.purchaseOrderService.getitemByUpc(filterReqBody).subscribe(data => {
            //           this.itemsGridData = data['data'];
            //           this.purchaseOrderService.savingItemsList = this.itemsGridData;
            //             // this.mergingArrays(data['data'])
            //             console.log(data);
            //         })
            //     }
            // })

            this.purchaseOrderService.getPOitems(this.purchaseOrderService.UpdateId).subscribe(data => {
                for(let i = 0 ; i<= data['data'].length-1 ; i++){
                    let itemObject = {
                        "id": data['data'][i].id,
                        "type_id": data['data'][i].module_type_id,
                        "type_desc": data['data'][i].item_description,
                        "upc_code": data['data'][i].upc,
                        "sourceid": data['data'][i].source,
                        "quantity": data['data'][i].quantity,
                        "wholesale_cost": data['data'][i].cost,
                        "notes": data['data'][i].notes
                    }
                    this.itemsGridData.push(itemObject);
                }
               this.purchaseOrderService.savingItemsList = this.itemsGridData;
            })


        }
    }

    /**
     * Numbers only
     * @param event  for getting kepress vlue
     * @returns true if only  
     */
    numberOnly(event): boolean {
        const charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }
    /**
     * Upcs add for getting items list by upc search
     */
    upcAdd() {
        if (this.purchaseOrderService.POsupplier != '' && this.purchaseOrderService.POlocation != '') {
            let reqBody = {
                "filter": [
                    {
                        "field": "upc_code",
                        "operator": "=",
                        "value": this.UPC
                    },
                    {
                        "field": "vendor_id",
                        "operator": "=",
                        "value": this.purchaseOrderService.POsupplier
                    },
                    {
                        "field": "loc_id",
                        "operator": "=",
                        "value": this.purchaseOrderService.POlocation
                    },

                ],
                "sort": [
                    {
                        "field": "name",
                        "order": "ASC"
                    }
                ]
            }
            // this.locations = [];
            this.purchaseOrderService.getitemByUpc(reqBody).subscribe(data => {
                this.mergingArrays(data['data']);
                // for (let i = 0; i <= data['data'].length - 1; i++) {
                //     this.locations.push(data['data'][i]);
                // }
            })
        }
    }
    /**
     * Checks box click
     * @param event for status of check box
     * @param i index of item
     */
    checkBoxClick(event, i) {
        console.log(event);
        console.log(i);
    }
    /**
     * Clicks frame sidebar
     * @param id for getting items 
     */
    clickFrameSidebar(id) {
        this.moduleTypevalue = id;
        this.openFrameSidebar = true;
    }
    /**
     * Items emitevent
     * @param event for sending selected items
     */
    ItemsEmitevent(event) {
        this.openFrameSidebar = false;
        this.mergingArrays(event);

    }
    /**
     * Merging arrays
     * @param event for getting selected array
     */
    mergingArrays(event) {
        let item_to_load: any = []; // items that need to be loaded in grid from selected items from screen//

        // Storing Selected data in temporary variables.
        const tempDataCopy = JSON.parse(JSON.stringify(event));
        const tempDataLength: number = tempDataCopy.length;

        // If items are already available in grid table.
        if (this.itemsGridData.length > 0) {
            // Matching each item that is available in grid table
            this.itemsGridData.forEach(function (item, key) {
                // Emptying the data to load 
                item_to_load = [];
                // Matching each data available on existing grid with all selected items.
                for (let i = 0; i < event.length; i++) {
                    // Checkin item is available in grid with selected item
                    if (tempDataCopy[i].id === item.id) {
                        delete event[i]; // Remove value and set it null
                        item_to_load = tempDataCopy[i];
                    }
                }
            })
            const unmatch_data = event.filter(p => !null)
            if (unmatch_data.length > 0) {
                for (let i = 0; i <= unmatch_data.length - 1; i++) {
                    this.itemsGridData.push(unmatch_data[i]);
                }
            }
        } else {
            for (let i = 0; i <= event.length - 1; i++) {
                event[i].quantity = '';
                this.itemsGridData.push(event[i]);
            }
        }
        this.purchaseOrderService.savingItemsList = this.itemsGridData;
    }
}
