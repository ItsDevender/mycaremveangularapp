import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-inventory-add-services',
    templateUrl: 'add-services.component.html'
})
export class InventoryAddServicesComponent implements OnInit {

    addServiceInvTransaction = false;
    transactionsBtnHide = false;
    // LensTreatmentForm: FormGroup;
    orderTypes: any[];
    constructor() {

    }


    ngOnInit() {

    }

    addServiceTransaction() {
        this.addServiceInvTransaction = true;
    }

    onServiceTabsClick(event) {
        if (event.index === 0) {
            this.transactionsBtnHide = false;
        }
        if (event.index === 1) {
            this.transactionsBtnHide = false;
        }
        if (event.index === 2) {
            this.transactionsBtnHide = true;
        }
    }

}

