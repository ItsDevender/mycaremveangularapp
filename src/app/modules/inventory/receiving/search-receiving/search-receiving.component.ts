import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { FormBuilder } from '@angular/forms';
import { FramesService } from '@services/inventory/frames.service';
import { InventoryService } from '@app/core/services/inventory/inventory.service';
import { ReceivingService } from '@app/core/services/inventory/receiving.service';
import { UtilityService } from '@app/shared/services/utility.service';
import { ErrorService, AppService } from '@app/core';
import { DropdownService } from '@app/shared/services/dropdown.service';
import { PaymentTermsMaster } from '@app/core/models/masterDropDown.model';

@Component({
    selector: 'app-search-receiving',
    templateUrl: 'search-receiving.component.html'
})

export class SearchReceivingComponent implements OnInit {
    SearchfilterData: any;
    SearchRecievingInventoryForm: FormGroup;
    orderTypes: any[];
    groupBy: any[];
    supplierslist: any;
    /**
     * Suppliers  of search receiving component
     */
    suppliers: Array<object>;
    locationlist: any;
    locations: any[];
    paymentTermsData: any;
    /**
     * Posearchdata  of search receiving component
     */
    POsearchdata: any;
    @Output() recevingOutput = new EventEmitter<any>();
    /**
     * Creates an instance of search receiving component.
     * @param frameService  provide frameService
     * @param _fb formbuilder
     * @param receiving provide receivingservice
     * @param _InventoryCommonService provide inventoryCommonService
     * @param utility provide utility service
     * @param error provide error service
     */
    constructor(
        private frameService: FramesService,
        private _fb: FormBuilder,
        private receiving: ReceivingService,
        private _InventoryCommonService: InventoryService,
        private utility: UtilityService,
        private error: ErrorService,
        private dropdown: DropdownService,
        public app: AppService
    ) {

    }
    ngOnInit() {
        // this.locations = [];
        // this.locations = this._DropdownsPipe.locationsDropData();
        this.GetSupplierDetails();
        this.getFramesPaymentTerms();
        this.Loadformdata();
    }
    /**
     * Loadformdatas search receiving component
     * for loading formgroup
     */
    Loadformdata() {
        this.SearchRecievingInventoryForm = this._fb.group({
            // trans_type: '',
            inventory_recieve_id: '',
            supplierid: ['', [Validators.required]],
            locationid: null,
            terms: '',
            // employee_id: '1',
            notes: '',
            // shipping_amount: 0,
            // tax_amount: 0,
            // other_amount: 0,
            // total_amount: 0,
            invoice: '',
            employee_name: '',
            receive_date: '',
            receive_status: 1,
            receive_type: ''

        });
    }
    searchClose() {
        this.recevingOutput.emit('close');
    }
    /**
     * Gets supplier details
     * @returns suppliers data
     */
    GetSupplierDetails() {
        this.suppliers = [];
        let labsVenderData: any = [];
        this.suppliers = this.dropdown.supplierlist;
        if (this.utility.getObjectLength(this.suppliers) === 0) {
            // this.supplierlist.push({ id: '', vendor_name: 'Select Lab' });
            this.dropdown.getLabsVender().subscribe(
                data => {
                    try {
                        this.suppliers.push({ id: '', vendor_name: 'Select Supplier' });
                        labsVenderData = data;
                        for (let i = 0; i <= labsVenderData.data.length - 1; i++) {
                            this.suppliers.push(labsVenderData.data[i]);
                        }
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }
                    this.dropdown.lablist = this.suppliers;
                });
        }


        // this.supliers = [];
        // this.supliers = this._InventoryCommonService.frameSupplier;
        // if (this.utility.getObjectLength(this.supliers) === 0) {
        //     this._InventoryCommonService.GetSuppliersDetails(includeInactive, maxRecord, offset, id).subscribe(
        //         data => {
        //             try {
        //                 this.supplierslist = data['result']['Optical']['Vendors'];

        //                 for (let i = 0; i <= this.supplierslist.length - 1; i++) {

        //                     if (!this.supliers.find(a => a.Name === this.supplierslist[i]['Name'])) {
        //                         this.supliers.push({ Id: this.supplierslist[i]['Id'], Name: this.supplierslist[i]['Name'] });
        //                     }
        //                 }
        //             } catch (error) {
        //                 this.error.syntaxErrors(error);
        //             }

        //         });
        // }
    }
    getFramesPaymentTerms() {
        this.paymentTermsData = this.dropdown.paymentTerms;
        if (this.utility.getObjectLength(this.paymentTermsData) === 0) {
            this.dropdown.getPaymentTerms().subscribe(
                (values: PaymentTermsMaster) => {
                    try {
                        const dropData  = values.data;
                        this.paymentTermsData.push({ id: '', paymentterm: 'Select Payment Terms' });
                        for (let i = 0; i <= dropData.length - 1; i++) {
                            this.paymentTermsData.push(dropData[i]);
                        }
                        this.dropdown.paymentTerms = this.paymentTermsData;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }
                });
        }
    }
    // LoadLocations() {
    //     this.locations = [];
    //     this._ContactlensService.getLocations().subscribe(
    //         data => {
    //             this.locationlist = data['result']['Optical']['Locations'];

    //             for (let i = 0; i <= this.locationlist.length - 1; i++) {

    //                 if (!this.locations.find(a => a.Name === this.locationlist[i]['Name'])) {
    //                     this.locations.push({ Id: this.locationlist[i]['Id'], Name: this.locationlist[i]['Name'] });
    //                 }
    //             }

    //         });
    // }
    /**
     * Filters receiving inventory
     */
    filterReceivingInventory() {
        const serchdata = {
            filter: [
                {
                    field: 'id',
                    operator: '=',
                    value: this.SearchRecievingInventoryForm.controls['inventory_recieve_id'].value
                },
                {
                    field: "supplierid",
                    operator: "=",
                    value: this.SearchRecievingInventoryForm.controls['supplierid'].value
                },
                {
                    field: "locationid",
                    operator: "=",
                    value: this.SearchRecievingInventoryForm.controls['locationid'].value
                }
            ]
        };
        const resultData = serchdata.filter.filter(p => p.value !== "" && p.value !== null);
        let filterdata = {
            filter:resultData
        }
        this.receiving.filterReceivingInventoryData(filterdata).subscribe(data => {
            this.SearchfilterData = data['data'];
        });
    }

    /**
     * Determines whether row select on
     * @param event  for clicked data
     */
    onRowSelect(event) {
        this.POsearchdata = event.data;
    }
    /**
     * Determines whether row unselect on
     * @param event for clicked data
     */
    onRowUnselect(event) {
        this.POsearchdata = '';
    }
    /**
     * Doubles click po
     * @param event for clicked data
     */
    DoubleClickPO(event) {
        this.recevingOutput.emit(event);
        // this.purchaseOrderService.POSelectedData.next(event);
        // this.purchaseOrderService.UpdateId = event.id;
        // this.purchaseOrderService.searchsupplieerData = event;
    }
    /**
     * Poselectbtns po search component
     */
    Poselectbtn() {
        if (this.POsearchdata != '' || this.POsearchdata != undefined) {
            // this.POSelectedData.emit(this.POsearchdata);
            this.recevingOutput.emit(this.POsearchdata);
            // this.purchaseOrderService.POSelectedData.next(this.POsearchdata);
            // this.purchaseOrderService.UpdateId = this.POsearchdata.id;
            // this.purchaseOrderService.searchsupplieerData = this.POsearchdata;
        }
    }
}



