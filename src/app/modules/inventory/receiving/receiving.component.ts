import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { FormGroup } from '@angular/forms';
import { OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { ContactlensService } from '@services/inventory/contactlens.service';
import { FramesService } from '@services/inventory/frames.service';
import { ReceivingService } from '@app/core/services/inventory/receiving.service';
import { InventoryService } from '@app/core/services/inventory/inventory.service';
import { UtilityService } from '@app/shared/services/utility.service';
import { ErrorService, AppService } from '@app/core';
import { DropdownService } from '@app/shared/services/dropdown.service';
import { PurchaseOrderService } from '@app/core/services/inventory/purchase-order.service';
import { DatePipe } from '@angular/common';
import { TransactionTypeMaster, PaymentTermsMaster } from '@app/core/models/masterDropDown.model';


@Component({
    selector: 'app-receiving',
    templateUrl: 'receiving.component.html'
})
export class ReceivingInventoryComponent implements OnInit, OnDestroy {
    incBtn: boolean;
    decBtn: boolean;
    dataset: number[];
    receivingIdData: any;
    btnDisable = true;
    PdfInputData: string;
    pdfviewer = false;
    disablebutton = true;
    sample: number;
    setflag: boolean;
    i: number;
    j: number;
    convertedDate: string;
    uploadedTraceFiles: any[] = [];
    receiveData: any = [];
    className: string;
    upccodeEntered: any;
    rowselectedDataIndex: any;
    head_pop: any = 'MycareMVE';
    message_pop = '';
    button_pop: any = 'Ok';
    rowselectedData: any;
    listOftheInventories: any = [];
    subscriptionframesRecievingInventory: Subscription;
    RecievingInventoryForm: FormGroup;
    datafromRecieving: any;
    framelist: any[];
    paymentTermsData: any;
    /**
     * Type of transaction data of receiving inventory component
     */
    typeOfTransactionData: Array<object>;
    supplierslist: any;
    suppliers: any[];
    locationlist: any;
    cities1: any[];
    selectedCity1: any[];
    minDateValue: string;
    maxDateValue: string;
    selectedValues: any[];
    locations: any[];
    dateValue: any[];
    searchShipmentSidebar = false;
    searchReceivingSidebar = false;
    framesReceivingSidebar = false;
    contactlensReceivingSidebar = false;
    lensReceivingSidebar = false;
    otherReceivingSidebar = false;
    poReceivingSidebar = false;
    showDialog = false;
    // For Handling Temporary data
    temp_data: any = [];
    /**
     * Duplication  of receiving inventory component
     */
    duplication: any = [];
    /**
     * Select po disable of receiving inventory component
     */
    selectPoDis: boolean = false;
    /**
     * Inventory disable of receiving inventory component
     */
    inventoryDis: boolean = false;
    /**
     * Add disable of receiving inventory component
     */
    addDisable: boolean = false
    ngOnInit() {
        this.Loadformdata();
        // this.locations = [];
        // this.locations = this._DropdownsPipe.locationsDropData();

        this.GetSupplierDetails();
        this.typeOfInventoryTrasaction();
        this.getFramesPaymentTerms();
        this.LoadLocations();
        this.listOftheInventories = [];
        // this.dataset = [1, 2, 3, 4,6,7,8,7,8,1,120];
        // if (this.dataset.length <= 1) {
        //     this.incBtn = false
        //     this.decBtn = false
        // }


    }


    /**
     * Creates an instance of receiving inventory component.
     * @param contactlensService  provide  contactlensService
     * @param frameService provide frameService
     * @param _fb formbuilder
     * @param receiving provide receiving
     * @param inventoryprovide inventory service
     * @param utility provide utility service
     * @param error provide errorservice
     */
    constructor(
        private contactlensService: ContactlensService,
        private frameService: FramesService,
        private _fb: FormBuilder,
        private receiving: ReceivingService,
        private inventory: InventoryService,
        private utility: UtilityService,
        private error: ErrorService,
        private dropdown: DropdownService,
        public app: AppService,
        private purchaseOrderService: PurchaseOrderService,
        private datePipe: DatePipe) {


    }
    ngOnDestroy(): void {
        // this.subscriptionframesRecievingInventory.unsubscribe();
    }

    /**
     * Loadformdatas receiving inventory component for formgroup
     */
    Loadformdata() {
        this.RecievingInventoryForm = this._fb.group({
            // trans_type: '',
            id: '',
            supplierid: ['', [Validators.required]],
            locationid: [null, [Validators.required]],
            terms: '',
            employee_id: '1',
            notes: '',
            shipping_amount: 0,
            tax_amount: 0,
            other_amount: 0,
            total_amount: 0,
            invoice: '',
            employee_name: '',
            receive_date: ['', [Validators.required]],
            receive_status: '1',
            receive_type: ['', [Validators.required]],

        });
    }


    /**
     * Postofthes recieve inventory data
     */
    postoftheRecieveInventoryData() {
        if (this.listOftheInventories.length !== 0) {
            this.RecievingInventoryForm.controls['receive_type'].patchValue('2');
            // alert(this.RecievingInventoryForm.controls['id'].value)
            if (this.RecievingInventoryForm.controls['id'].value !== '') {

                this.receiveInventoryUpdate();
            } else {
                this.receiveInventorySaving();
            }
        } else {
            this.error.displayError({ severity: 'info', summary: 'Info Message', detail: 'Please select Items' });
        }
    }

    /**
     * Receives inventory saving
     * For saving receive invetorys and items
     */
    receiveInventorySaving() {
        const datereceived = this.RecievingInventoryForm.controls['receive_date'].value;
        // this.convertedDate = "";
        this.convert(datereceived);
        // alert(this.convertedDate)
        this.RecievingInventoryForm.controls['receive_date'].patchValue(this.convertedDate);
        this.receiving.postingOftheInventoryReceiveDataBulk(this.RecievingInventoryForm.value).subscribe(data => {

             
            this.receivingIdData = data['id'];

            let receiveItems: any = [];
            for (let i = 0; i <= this.listOftheInventories.length - 1; i++) {
                if (this.listOftheInventories[i].ordersource == null) {
                    this.listOftheInventories[i].ordersource = '';
                }
                var savingObj = {
                    "item_id": this.listOftheInventories[i].itemid,
                    "loc_id": this.RecievingInventoryForm.controls['locationid'].value,
                    "stock": this.listOftheInventories[i].onhand,
                    "trans_type": this.RecievingInventoryForm.controls['receive_type'].value,
                    "reason": "1",
                    "source": this.listOftheInventories[i].ordersource.toString(),
                    "lot_no": "1",
                    "order_id": this.listOftheInventories[i].orderid,
                    "order_detail_id": "1",
                    "po_no": this.listOftheInventories[i].po,
                    "inventory_receive_id": data['id'],
                    "purchase_order_detail_id": "1"
                }
                for (const key in savingObj) {
                    if (savingObj[key] === null || savingObj[key] === '') {
                        delete savingObj[key];
                    }
                }
                receiveItems.push(savingObj)
            }
            var saveReceiveitem = {
                "StockDetail": receiveItems
            }
            this.receiving.savingStockDetails(saveReceiveitem).subscribe(data => {
                this.error.displayError({ severity: 'success', summary: 'Success Message', detail: 'Receive Saved Successfully' });
                
                this.receiving.gettheInventoryReceiveData(this.receivingIdData).subscribe(data => {
                    this.RecievingInventoryForm.controls['id'].patchValue(data['id']);
                    this.RecievingInventoryForm.controls['employee_name'].patchValue(data['employee_name']);
                    this.RecievingInventoryForm.controls['invoice'].patchValue(data['invoice']);
                    this.RecievingInventoryForm.controls['notes'].patchValue(data['notes']);
                    this.RecievingInventoryForm.controls['other_amount'].patchValue(data['other_amount']);
                    this.RecievingInventoryForm.controls['shipping_amount'].patchValue(data['shipping_amount']);
                    this.RecievingInventoryForm.controls['tax_amount'].patchValue(data['tax_amount']);
                    this.RecievingInventoryForm.controls['total_amount'].patchValue(data['total_amount']);
                    this.RecievingInventoryForm.controls['locationid'].patchValue(data['locationid'].toString());
                    this.RecievingInventoryForm.controls['receive_type'].patchValue(data['receive_type']);
                    this.RecievingInventoryForm.controls['receive_date'].patchValue(data['receive_date']);
                    this.RecievingInventoryForm.controls['supplierid'].patchValue(data['supplierid'].toString());

                    this.listOftheInventories = [];
                    for (let i = 0; i <= data['stock_detail_recieve'].length - 1; i++) {
                        const row = {
                            type: data['stock_detail_recieve'][i].module_type_id,
                            upc: data['stock_detail_recieve'][i].upc,
                            itemid: data['stock_detail_recieve'][i].item_id,
                            itemdesc: data['stock_detail_recieve'][i].type_desc,
                            ordersource: data['stock_detail_recieve'][i].sourceid,
                            po: data['stock_detail_recieve'][i].po_no,
                            podate: '',
                            orderid: data['stock_detail_recieve'][i].order_id,
                            pickticket: '',
                            shipment: '',
                            onhand: data['stock_detail_recieve'][i].stock,
                            ordered: '',
                            received: '',
                            retail: '',
                            cost: data['stock_detail_recieve'][i].cost,
                            canceled: data['stock_detail_recieve'][i].cancel_quantity,
                            cancelreason: '',
                            missed: '',
                            missedreason: '',
                            remaining: '',
                            backordered: '',
                            directship: '',
                            notes: data['stock_detail_recieve'][i].notes,
                            id: data['stock_detail_recieve'][i].id
                        }
                        this.listOftheInventories.push(row)
                        if (data['stock_detail_recieve'][0].po_no !== null) {
                            this.selectPoDis = true;
                            this.inventoryDis = true;
                            this.addDisable = true;
                        } else {
                            this.selectPoDis = true;
                            this.inventoryDis = false;
                            this.addDisable = false;
                        }
                    }
                })
            })
            if (this.uploadedTraceFiles.length !== 0) {
                this.SaveInvoicePDF(data['id']);
            }
            this.receiveData = data;

        });
    }

    /**
     * Receives inventory update
     * For updating receive invetorys and items
     */
    receiveInventoryUpdate() {
        const datereceived = this.RecievingInventoryForm.controls['receive_date'].value;
        // this.convertedDate = "";
        this.convert(datereceived);
        // alert(this.convertedDate)
        this.RecievingInventoryForm.controls['receive_date'].patchValue(this.convertedDate);
        this.receiving.updatreOftheInventoryReceiveDataBulk(this.RecievingInventoryForm.value).subscribe(data => {
            
            this.receivingIdData = data[0]['id'];

            let receiveItems: any = [];
            let receiveupdateItem: any = [];
            for (let i = 0; i <= this.listOftheInventories.length - 1; i++) {
                if (this.listOftheInventories[i].id != '' || this.listOftheInventories[i].id != '') {
                    if (this.listOftheInventories[i].ordersource == null) {
                        this.listOftheInventories[i].ordersource = '';
                    }
                    var savingObj = {
                        "id": this.listOftheInventories[i].id,
                        "item_id": this.listOftheInventories[i].itemid,
                        "loc_id": this.RecievingInventoryForm.controls['locationid'].value,
                        "stock": this.listOftheInventories[i].onhand,
                        "trans_type": this.RecievingInventoryForm.controls['receive_type'].value,
                        "reason": "1",
                        "source": this.listOftheInventories[i].ordersource.toString(),
                        "lot_no": "1",
                        "order_id": this.listOftheInventories[i].orderid,
                        "order_detail_id": "1",
                        "po_no": this.listOftheInventories[i].po,
                        "inventory_receive_id": data['id'],
                        "purchase_order_detail_id": "1"
                    }
                    for (const key in savingObj) {
                        if (savingObj[key] === null || savingObj[key] === '') {
                            delete savingObj[key];
                        }
                    }
                    receiveItems.push(savingObj)
                } else {
                    if (this.listOftheInventories[i].ordersource == null) {
                        this.listOftheInventories[i].ordersource = '';
                    }
                    var savingUpdatObj = {
                        "item_id": this.listOftheInventories[i].itemid,
                        "loc_id": this.RecievingInventoryForm.controls['locationid'].value,
                        "stock": this.listOftheInventories[i].onhand,
                        "trans_type": this.RecievingInventoryForm.controls['receive_type'].value,
                        "reason": "1",
                        "source": this.listOftheInventories[i].ordersource.toString(),
                        "lot_no": "1",
                        "order_id": this.listOftheInventories[i].orderid,
                        "order_detail_id": "1",
                        "po_no": this.listOftheInventories[i].po,
                        "inventory_receive_id": this.RecievingInventoryForm.controls['id'].value,
                        "purchase_order_detail_id": "1"
                    }
                    for (const key in savingUpdatObj) {
                        if (savingUpdatObj[key] === null || savingUpdatObj[key] === '') {
                            delete savingUpdatObj[key];
                        }
                    }
                    receiveupdateItem.push(savingUpdatObj)
                }
            }
            var UpdateReceiveitem = {
                "StockDetail": receiveItems
            }
            this.receiving.UpdatingStockDetails(UpdateReceiveitem).subscribe(data => {
                if (receiveupdateItem.length != 0) {
                    var UpdateSaveReceiveitem = {
                        "StockDetail": receiveupdateItem
                    }
                    this.receiving.savingStockDetails(UpdateSaveReceiveitem).subscribe(event => {
                        
                    })
                }
                
                this.error.displayError({ severity: 'success', summary: 'Success Message', detail: 'Receive Updated Successfully' });
                this.receiving.gettheInventoryReceiveData(this.receivingIdData).subscribe(data => {
                    this.RecievingInventoryForm.controls['id'].patchValue(data['id']);
                    this.RecievingInventoryForm.controls['employee_name'].patchValue(data['employee_name']);
                    this.RecievingInventoryForm.controls['invoice'].patchValue(data['invoice']);
                    this.RecievingInventoryForm.controls['notes'].patchValue(data['notes']);
                    this.RecievingInventoryForm.controls['other_amount'].patchValue(data['other_amount']);
                    this.RecievingInventoryForm.controls['shipping_amount'].patchValue(data['shipping_amount']);
                    this.RecievingInventoryForm.controls['tax_amount'].patchValue(data['tax_amount']);
                    this.RecievingInventoryForm.controls['total_amount'].patchValue(data['total_amount']);
                    this.RecievingInventoryForm.controls['locationid'].patchValue(data['locationid'].toString());
                    this.RecievingInventoryForm.controls['receive_type'].patchValue(data['receive_type']);
                    this.RecievingInventoryForm.controls['receive_date'].patchValue(data['receive_date']);
                    this.RecievingInventoryForm.controls['supplierid'].patchValue(data['supplierid'].toString());
                    this.listOftheInventories = [];
                    for (let i = 0; i <= data['stock_detail_recieve'].length - 1; i++) {
                        const row = {
                            type: data['stock_detail_recieve'][i].module_type_id,
                            upc: data['stock_detail_recieve'][i].upc,
                            itemid: data['stock_detail_recieve'][i].item_id,
                            itemdesc: data['stock_detail_recieve'][i].type_desc,
                            ordersource: data['stock_detail_recieve'][i].sourceid,
                            po: data['stock_detail_recieve'][i].po_no,
                            podate: '',
                            orderid: data['stock_detail_recieve'][i].order_id,
                            pickticket: '',
                            shipment: '',
                            onhand: data['stock_detail_recieve'][i].stock,
                            ordered: '',
                            received: '',
                            retail: '',
                            cost: data['stock_detail_recieve'][i].cost,
                            canceled: data['stock_detail_recieve'][i].cancel_quantity,
                            cancelreason: '',
                            missed: '',
                            missedreason: '',
                            remaining: '',
                            backordered: '',
                            directship: '',
                            notes: data['stock_detail_recieve'][i].notes,
                            id: data['stock_detail_recieve'][i].id
                        }
                        this.listOftheInventories.push(row)
                        if (data['stock_detail_recieve'][0].po_no !== null) {
                            this.selectPoDis = true;
                            this.inventoryDis = true;
                            this.addDisable = true;
                        } else {
                            this.selectPoDis = true;
                            this.inventoryDis = false;
                            this.addDisable = false;
                        }
                    }
                })
            })
            if (this.uploadedTraceFiles.length !== 0) {
                this.SaveInvoicePDF(data[0]['id']);
            }

            this.receiveData = data;


            // this.RecievingInventoryForm.controls['id'].patchValue(data[0]['id']);
            // this.RecievingInventoryForm.controls['employee_name'].patchValue(data[0]['employee_name']);
            // this.RecievingInventoryForm.controls['invoice'].patchValue(data[0]['invoice']);
            // this.RecievingInventoryForm.controls['notes'].patchValue(data[0]['notes']);
            // this.RecievingInventoryForm.controls['other_amount'].patchValue(data[0]['other_amount']);
            // this.RecievingInventoryForm.controls['tax_amount'].patchValue(data[0]['tax_amount']);
            // this.RecievingInventoryForm.controls['total_amount'].patchValue(data[0]['total_amount']);
            // this.RecievingInventoryForm.controls['locationid'].patchValue(data[0]['locationid']);
            // this.RecievingInventoryForm.controls['receive_type'].patchValue(data[0]['receive_type']);
            // this.RecievingInventoryForm.controls['receive_date'].patchValue(data[0]['receive_date']);
            // this.RecievingInventoryForm.controls['supplierid'].patchValue(data[0]['supplierid']);
            // this.RecievingInventoryForm.controls['shipping_amount'].patchValue(data[0]['shipping_amount']);


        });
    }
    /**
     * Converts receiving inventory component
     * @param {string} str for getting date formate
     */
    convert(str) {
        const date = new Date(str),
            mnth = ('0' + (date.getMonth() + 1)).slice(-2),
            day = ('0' + date.getDate()).slice(-2),
            hours = ('0' + date.getHours()).slice(-2),
            minutes = ('0' + date.getMinutes()).slice(-2),
            seconds = ('0' + date.getSeconds()).slice(-2);
        this.convertedDate = [date.getFullYear(), mnth, day].join('-') + ' ' + [hours, minutes, seconds].join(':');
    }
    /**
     * Loads locations
     * loading location details
     */
    LoadLocations() {
        // this.locations = [];
        // this.contactlensService.getLocations().subscribe(
        //     data => {
        //         this.locationlist = data['result']['Optical']['Locations'];

        //         for (let i = 0; i <= this.locationlist.length - 1; i++) {

        //             if (!this.locations.find(a => a.Name === this.locationlist[i]['Name'])) {
        //                 this.locations.push({ Id: this.locationlist[i]['Id'], Name: this.locationlist[i]['Name'] });
        //             }
        //         }

        //     });
        this.locations = [];
        this.dropdown.getInventtoryLocations().subscribe(
            data => {
                this.temp_data = data['result']['Optical']['Locations'];
                // this.locations.push({ Id: '', Name: 'Select Location' });
                data['result']['Optical']['Locations'].forEach(item => {
                    if (!this.locations.find(a => a.Name === item.Name)) {
                        this.locations.push({ Id: item.Id, Name: item.Name });
                    }
                });
            }
        );
    }
    /**
     * Gets supplier details
     * @returns supplier details data
     */
    GetSupplierDetails() {
        this.suppliers = [];
        let labsVenderData: any = [];
        this.suppliers = this.dropdown.supplierlist;
        if (this.utility.getObjectLength(this.suppliers) === 0) {
            // this.supplierlist.push({ id: '', vendor_name: 'Select Lab' });
            this.dropdown.getLabsVender().subscribe(
                data => {
                    try {
                        this.suppliers.push({ id: '', vendor_name: 'Select Supplier' });
                        labsVenderData = data;
                        for (let i = 0; i <= labsVenderData.data.length - 1; i++) {
                            if (!this.suppliers.find(a => a.Name === labsVenderData.data[i]['Name'])) {
                                this.suppliers.push({ Id: labsVenderData.data[i]['Id'], Name: labsVenderData.data[i]['Name'] });
                            }
                            // this.suppliers.push(labsVenderData.data[i]);
                        }


                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }
                    this.dropdown.lablist = this.suppliers;
                });
        }



        // this.supliers = [];
        // this.supliers = this.inventory.frameSupplier;
        // if (this.utility.getObjectLength(this.supliers) === 0) {
        //     this.inventory.GetSuppliersDetails(includeInactive, maxRecord, offset, id).subscribe(
        //         data => {
        //             try {
        //                 this.supplierslist = data['result']['Optical']['Vendors'];

        //                 for (let i = 0; i <= this.supplierslist.length - 1; i++) {

        //                     if (!this.supliers.find(a => a.Name === this.supplierslist[i]['Name'])) {
        //                         this.supliers.push({ Id: this.supplierslist[i]['Id'], Name: this.supplierslist[i]['Name'] });
        //                     }
        //                 }
        //             } catch (error) {
        //                 this.error.syntaxErrors(error);
        //             }

        //         });
        // }
    }
    typeOfInventoryTrasaction() {

        this.typeOfTransactionData = this.dropdown.transactionType;
        if (this.utility.getObjectLength(this.typeOfTransactionData) === 0) {
            this.dropdown.getTypeTrasactionframeData().subscribe(
                (values: TransactionTypeMaster) => {
                    try {
                        const dropData = values.data;
                        this.typeOfTransactionData.push({ id: '', transactiontype: 'Select Type' });
                        for (let i = 0; i <= dropData.length - 1; i++) {
                            dropData[i].id = dropData[i].id.toString();
                            this.typeOfTransactionData.push(dropData[i]);
                        }
                        this.dropdown.transactionType = this.typeOfTransactionData;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }

                });
        }
    }
    getFramesPaymentTerms() {
        this.paymentTermsData = this.dropdown.paymentTerms;
        if (this.utility.getObjectLength(this.paymentTermsData) === 0) {
            this.dropdown.getPaymentTerms().subscribe(
                (values: PaymentTermsMaster) => {
                    try {
                        const dropData  = values.data;
                        this.paymentTermsData.push({ id: '', paymentterm: 'Select Payment Terms' });
                        for (let i = 0; i <= dropData.length - 1; i++) {
                            this.paymentTermsData.push(dropData[i]);
                        }
                        this.dropdown.paymentTerms = this.paymentTermsData;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }
                });
        }
    }

    /**
     * Frames receiving sidebar click
     */
    framesReceivingSidebarClick() {
        
        if (this.RecievingInventoryForm.controls['locationid'].value !== ''
            && this.RecievingInventoryForm.controls['locationid'].value !== null
            && this.RecievingInventoryForm.controls['supplierid'].value !== '' &&
            this.RecievingInventoryForm.controls['supplierid'].value !== null) {
            this.datafromRecieving = this.RecievingInventoryForm.value;
            this.framesReceivingSidebar = true;
        } else {
            this.error.displayError({ severity: 'info', summary: 'info Message', detail: 'Please select location , supplier' });
            // this.showDialog = true;
            // this.className = 'dialog card';
            // this.message_pop = 'please select the location and supplier';
            // this.button_pop = 'Continue';
            //    alert("Not enter");
            //    this. confirm();
        }
    }
    /**
     * Contactlens receiving sidebar click
     */
    contactlensReceivingSidebarClick() {
        if (this.RecievingInventoryForm.controls['locationid'].value !== ''
            && this.RecievingInventoryForm.controls['locationid'].value !== null
            && this.RecievingInventoryForm.controls['supplierid'].value !== '' &&
            this.RecievingInventoryForm.controls['supplierid'].value !== null) {
            this.contactlensReceivingSidebar = true;
            this.datafromRecieving = this.RecievingInventoryForm.value;
        } else {
            this.error.displayError({ severity: 'info', summary: 'info Message', detail: 'Please select location , supplier' });
            // this.showDialog = true;
            // this.className = 'dialog card';
            // this.message_pop = 'please select the location and supplier';
            // this.button_pop = 'Continue';
            //    alert("Not enter");
            //    this. confirm();
        }

    }
    /**
     * Lens receiving sidebar click
     */
    lensReceivingSidebarClick() {
        if (this.RecievingInventoryForm.controls['locationid'].value !== ''
            && this.RecievingInventoryForm.controls['locationid'].value !== null
            && this.RecievingInventoryForm.controls['supplierid'].value !== '' &&
            this.RecievingInventoryForm.controls['supplierid'].value !== null) {
            this.lensReceivingSidebar = true;
            this.datafromRecieving = this.RecievingInventoryForm.value;
        } else {
            this.error.displayError({ severity: 'info', summary: 'info Message', detail: 'Please select location , supplier' });
            // this.showDialog = true;
            // this.className = 'dialog card';
            // this.message_pop = 'please select the location and supplier';
            // this.button_pop = 'Continue';
            //    alert("Not enter");
            //    this. confirm();
        }

    }
    /**
     * Others receiving sidebar click
     */
    otherReceivingSidebarClick() {
        if (this.RecievingInventoryForm.controls['locationid'].value !== ''
            && this.RecievingInventoryForm.controls['locationid'].value !== null
            && this.RecievingInventoryForm.controls['supplierid'].value !== '' &&
            this.RecievingInventoryForm.controls['supplierid'].value !== null) {
            this.otherReceivingSidebar = true;
            this.datafromRecieving = this.RecievingInventoryForm.value;
        } else {
            this.error.displayError({ severity: 'info', summary: 'info Message', detail: 'Please select location , supplier' });
            // this.showDialog = true;
            // this.className = 'dialog card';
            // this.message_pop = 'please select the location and supplier';
            // this.button_pop = 'Continue';
            //    alert("Not enter");
            //    this. confirm();
        }
    }

    /**
     * Receving output event
     * @param {any} data for getting search values
     */
    recevingOutputEvent(data) {
        if (data === 'close') {
            this.searchReceivingSidebar = false;
        } else {
            
            this.RecievingInventoryForm.controls['id'].patchValue(data['id']);
            this.RecievingInventoryForm.controls['employee_name'].patchValue(data['employee_name']);
            this.RecievingInventoryForm.controls['invoice'].patchValue(data['invoice']);
            this.RecievingInventoryForm.controls['notes'].patchValue(data['notes']);
            this.RecievingInventoryForm.controls['other_amount'].patchValue(data['other_amount']);
            this.RecievingInventoryForm.controls['shipping_amount'].patchValue(data['shipping_amount']);
            this.RecievingInventoryForm.controls['tax_amount'].patchValue(data['tax_amount']);
            this.RecievingInventoryForm.controls['total_amount'].patchValue(data['total_amount']);
            this.RecievingInventoryForm.controls['locationid'].patchValue(data['locationid'].toString());
            this.RecievingInventoryForm.controls['receive_type'].patchValue(data['receive_type']);
            this.RecievingInventoryForm.controls['receive_date'].patchValue(data['receive_date']);
            this.RecievingInventoryForm.controls['supplierid'].patchValue(data['supplierid'].toString());

            this.listOftheInventories = [];
            for (let i = 0; i <= data['stock_detail_recieve'].length - 1; i++) {
                const row = {
                    type: data['stock_detail_recieve'][i].module_type_id,
                    upc: data['stock_detail_recieve'][i].upc,
                    itemid: data['stock_detail_recieve'][i].item_id,
                    itemdesc: data['stock_detail_recieve'][i].type_desc,
                    ordersource: data['stock_detail_recieve'][i].sourceid,
                    po: data['stock_detail_recieve'][i].po_no,
                    podate: '',
                    orderid: data['stock_detail_recieve'][i].order_id,
                    pickticket: '',
                    shipment: '',
                    onhand: data['stock_detail_recieve'][i].stock,
                    ordered: '',
                    received: '',
                    retail: '',
                    cost: data['stock_detail_recieve'][i].cost,
                    canceled: data['stock_detail_recieve'][i].cancel_quantity,
                    cancelreason: '',
                    missed: '',
                    missedreason: '',
                    remaining: '',
                    backordered: '',
                    directship: '',
                    notes: data['stock_detail_recieve'][i].notes,
                    id: data['stock_detail_recieve'][i].id
                }
                this.listOftheInventories.push(row)
                if (data['stock_detail_recieve'][0].po_no !== null) {
                    this.selectPoDis = true;
                    this.inventoryDis = true;
                    this.addDisable = true;
                } else {
                    this.selectPoDis = true;
                    this.inventoryDis = false;
                    this.addDisable = false;
                }
            }
            this.searchReceivingSidebar = false;
        }
    }

    /**
     * Removes item from item list
     */
    removeItemfromlist() {
        for (let i = 0; i <= this.listOftheInventories.length - 1; i++) {
            if (this.listOftheInventories[i] === '') {
                if (this.listOftheInventories[i].id === this.rowselectedDataIndex.data.id) {
                    this.listOftheInventories.splice(i, 1);
                }
                this.listOftheInventories = this.listOftheInventories;
                
            }
        }
    }
    onRowSelect(event) {
        this.rowselectedDataIndex = event;

    }
    AddBtnClick() {
        // this.listOftheInventories = [];
        const data = {
            filter: [
                {
                    field: 'upc_code',
                    operator: '=',
                    value: this.upccodeEntered
                }],
        };
        // tslint:disable-next-line:no-shadowed-variable
        this.inventory.filterInventories(data).subscribe(data => {
            this.selectPoDis = true;
            this.inventoryDis = false;
            this.addDisable = false;
            
            this.itemsmergingForInventory(event);
            // for (let i = 0; i <= data['data'].length - 1; i++) {
            //     this.listOftheInventories.push(data['data'][i])
            // }
            // this.listOftheInventories = data['data'];
            // alert(this.framelist);
            
        });
    }
    inputofUPC(event) {
        this.upccodeEntered = event.target.value;
    }

    /**
     * Frameevents receiving inventory component
     * @param {any} event for getting frame items
     */
    frameevent(event) {
        
        if (event === 'close') {
            this.framesReceivingSidebar = false;
        } else {
            this.selectPoDis = true;
            this.inventoryDis = false;
            this.addDisable = false;
            this.itemsmergingForInventory(event);
            // this.listOftheInventories = [];
            // for (let i = 0; i <= event.length - 1; i++) {
            //     this.listOftheInventories.push(event[i])
            // }
            // this.listOftheInventories = event;
            this.framesReceivingSidebar = false;
            this.contactlensReceivingSidebar = false;
            this.otherReceivingSidebar = false;
            this.lensReceivingSidebar = false;
        }
    }
    shipmentEvent(event) {
        if (event === 'close') {
            this.searchShipmentSidebar = false;
        }
    }
    /**
     * Contactlensevents receiving inventory component
     * @param {any} event for getting Contact items
     */
    contactlensevent(event) {
        if (event === 'close') {
            this.contactlensReceivingSidebar = false;
        } else {
            this.selectPoDis = true;
            this.inventoryDis = false;
            this.addDisable = false;
            this.itemsmergingForInventory(event);
            // for (let i = 0; i <= event.length - 1; i++) {
            //     this.listOftheInventories.push(event[i])
            // }
            // this.listOftheInventories = [];
            // this.listOftheInventories = event;
            this.framesReceivingSidebar = false;
            this.contactlensReceivingSidebar = false;
            this.otherReceivingSidebar = false;
            this.lensReceivingSidebar = false;
        }
    }
    /**
     * Lens receivingevent
     * @param {any} event for getting lens items
     */
    lensReceivingevent(event) {
        if (event === 'close') {
            this.lensReceivingSidebar = false;
        } else {
            this.selectPoDis = true;
            this.inventoryDis = false;
            this.addDisable = false;
            this.itemsmergingForInventory(event);
            // for (let i = 0; i <= event.length - 1; i++) {
            //     this.listOftheInventories.push(event[i])
            // }
            // this.listOftheInventories = [];
            // this.listOftheInventories = event;
            this.framesReceivingSidebar = false;
            this.contactlensReceivingSidebar = false;
            this.otherReceivingSidebar = false;
            this.lensReceivingSidebar = false;
        }
    }
    /**
     * Otherevents receiving inventory component
     * @param {any} event for getting others items 
     */
    otherevent(event) {
        if (event === 'close') {
            this.otherReceivingSidebar = false;
        } else {
            this.selectPoDis = true;
            this.inventoryDis = false;
            this.addDisable = false;
            this.itemsmergingForInventory(event);
            // for (let i = 0; i <= event.length - 1; i++) {
            //     this.listOftheInventories.push(event[i])
            // }
            // this.listOftheInventories = [];
            // this.listOftheInventories = event;
            this.framesReceivingSidebar = false;
            this.contactlensReceivingSidebar = false;
            this.otherReceivingSidebar = false;
            this.lensReceivingSidebar = false;
        }
    }
    poreceiveevent(event) {
        if (event === 'close') {
            this.poReceivingSidebar = false;
        }
    }
    btnAct(data) {
        this.showDialog = false;
    }
    uploadInvoice(data) {
        this.uploadedTraceFiles = [];
        for (const file of data.target.files) {
            this.uploadedTraceFiles.push(file);
            
            // this.FarmesForm.controls['traceFile'].patchValue(this.uploadedTraceFiles[0].name);
        }
    }
    SaveInvoicePDF(receiveid) {
        const formData: FormData = new FormData();
        formData.append('file', this.uploadedTraceFiles[0]);
        this.receiving.uploadInvoice(formData, receiveid).subscribe(data => {
            const massage = data;
            if (massage['status'] === 'File upload successfully') {
                this.btnDisable = false;
            }
        });
        this.uploadedTraceFiles = [];
    }
    deletePdf() {
        const receiveId = this.receivingIdData;
        this.receiving.DeleteInvoice(receiveId).subscribe(data => {
            const Delmassage = data['status'];
            // alert(Delmassage);
        });

    }
    Viewpdf() {
        const receiveId = this.receivingIdData;
        this.receiving.ViewInvoice(receiveId).subscribe(data => {
            const Viewmassage = data;
            this.showPdf(Viewmassage);
            
            // alert(Viewmassage);
        });
    }
    showPdf(Base64Data) {
        const linkSource = 'data:application/pdf;base64,' + Base64Data.inoviceData;
        const downloadLink = document.createElement('a');
        const fileName = 'sample.pdf';

        downloadLink.href = linkSource;
        this.PdfInputData = linkSource;
        this.pdfviewer = true;
        // downloadLink.download = fileName;
        // downloadLink.click();
    }

    // incrementClick() {
    //     if (this.setflag != true) {
    //         this.i = this.sample = 2;
    //     }

    //     if(this.i == this.dataset.length - 2){
    //         this.incBtn = true
    //     }else{
    //     this.decBtn = false
    //     this.i++;
    //     //this.j = this.i;
    //     
    //     this.setflag = true
    //     }
    // }
    // DecrementClick() {
    //     
    //     if (this.setflag != true) {
    //         this.i = this.sample =2 ;
    //     }

    //     if (this.i == 1) {
    //         this.decBtn = true;
    //         this.incBtn = false;
    //     }
    //     // if (this.i == this.sample + 1) {
    //     //     
    //     //     this.disablebutton = true
    //     //     this.setflag = false
    //     // } else


    //         this.i--;
    //         
    //         this.setflag = true

    // }
    // ViewPDFClick() {
    //     this.pdfviewer = true;
    // }
    totaladd() {
        this.RecievingInventoryForm.controls['total_amount'].patchValue(
            parseInt(this.RecievingInventoryForm.controls['other_amount'].value, 10) +
            parseInt(this.RecievingInventoryForm.controls['shipping_amount'].value, 10) +
            parseInt(this.RecievingInventoryForm.controls['tax_amount'].value, 10));
    }
    searchReceivingSidebarClick() {
        this.searchReceivingSidebar = true;
    }
    /**
     * Add button click
     */
    addClick() {
        // this.RecievingInventoryForm.reset();
        this.Loadformdata();
        this.listOftheInventories = [];
        this.selectPoDis = false;
        this.inventoryDis = false;
        this.addDisable = false;
    }
    /**
     * Poselected data event
     * @param event  for getting PO items
     */
    POSelectedDataEvent(event) {
        this.poReceivingSidebar = false;
        
        this.purchaseOrderService.getPOitems(event.id).subscribe(value => {
        this.RecievingInventoryForm.controls['locationid'].patchValue(event['locationid'].toString());
        this.RecievingInventoryForm.controls['supplierid'].patchValue(event['supplierid'].toString());
            this.listOftheInventories = [];
            let itemIds: any = [];
            this.duplication = [];
            for (let i = 0; i <= value['data'].length - 1; i++) {
                itemIds.push(value['data'][i].itemid)
            }
            if (itemIds.length != 0) {
                let filterReqBody = {
                    "filter": [
                        {
                            "field": "id",
                            "operator": "IN",
                            "value": itemIds
                        }

                    ],
                    "sort": [
                        {
                            "field": "id",
                            "order": "ASC"
                        }
                    ]
                }
                this.purchaseOrderService.getitemByUpc(filterReqBody).subscribe(data => {
                    value['data'] // podata
                    data['data']  // filter data
                    for (let i = 0; i <= value['data'].length - 1; i++) {
                        const row = {
                            type: value['data'][i].module_type_id,
                            upc: value['data'][i].upc,
                            itemid: value['data'][i].itemid,
                            itemdesc: value['data'][i].item_description,
                            ordersource: value['data'][i].source,
                            po: value['data'][i].po_header_id,
                            podate: '',
                            orderid: value['data'][i].orderid,
                            pickticket: '',
                            shipment: value['data'][i].ship_to_info,
                            onhand: value['data'][i].quantity,
                            ordered: '',
                            received: '',
                            retail: '',
                            cost: data['data'][i].wholesale_cost,
                            canceled: data['data'][i].retail_price,
                            cancelreason: '',
                            missed: '',
                            missedreason: '',
                            remaining: '',
                            backordered: '',
                            directship: '',
                            notes: data['data'][i].notes,
                            id: ''
                        }
                        this.duplication.push(row);
                        // this.seriallizeItems(this.listOftheInventories.length, row);
                    }
                    // for (let i = 0; i <= data['data'].length - 1; i++) {
                    //     data['data'][i].po_id = event.id;
                    //     data['data'][i].po_date = this.datePipe.transform(event.po_date, 'yyyy-MM-dd');
                    // }
                    this.selectPoDis = false;
                    this.inventoryDis = true;
                    this.addDisable = true;
                    this.mergingArrays(this.duplication);
                    
                })

            }
        })
    }
    // seriallizeItems(index, row) {
    //     this.listOftheInventories[index] = row;
    // }
    mergingArrays(event) {
        let item_to_load: any = []; // items that need to be loaded in grid from selected items from screen//

        // Storing Selected data in temporary variables.
        const tempDataCopy = JSON.parse(JSON.stringify(event));
        const tempDataLength: number = tempDataCopy.length;

        // If items are already available in grid table.
        if (this.listOftheInventories.length > 0) {
            // Matching each item that is available in grid table
            this.listOftheInventories.forEach(function (item, key) {
                // Emptying the data to load 
                item_to_load = [];
                // Matching each data available on existing grid with all selected items.
                for (let i = 0; i < event.length; i++) {
                    // Checkin item is available in grid with selected item
                    if (tempDataCopy[i].itemid === item.itemid) {
                        delete event[i]; // Remove value and set it null
                        item_to_load = tempDataCopy[i];
                    }
                }
            })
            const unmatch_data = event.filter(p => !null)
            if (unmatch_data.length > 0) {
                for (let i = 0; i <= unmatch_data.length - 1; i++) {
                    this.listOftheInventories.push(unmatch_data[i]);
                }
            }
        } else {
            for (let i = 0; i <= event.length - 1; i++) {
                event[i].quantity = '';
                this.listOftheInventories.push(event[i]);
            }
        }
        // this.purchaseOrderService.savingItemsList = this.listOftheInventories;
    }

    /**
     * Itemsmerging for inventory
     * @param event  for getting inventory items
     */
    itemsmergingForInventory(event) {
        this.duplication = [];
        for (let i = 0; i <= event.length - 1; i++) {
            const row = {
                type: event[i].module_type_id,
                upc: event[i].upc_code,
                itemid: event[i].id,
                itemdesc: event[i].type_desc,
                ordersource: event[i].sourceid,
                po: '',
                podate: '',
                orderid: '',
                pickticket: '',
                shipment: '',
                onhand: event[i].qty_on_hand,
                ordered: '',
                received: '',
                retail: '',
                cost: event[i].wholesale_cost,
                canceled: event[i].retail_price,
                cancelreason: '',
                missed: '',
                missedreason: '',
                remaining: '',
                backordered: '',
                directship: '',
                notes: event[i].notes,
                id: ''
            }
            this.duplication.push(row)
        }
        this.mergingArrays(this.duplication);
    }
}
