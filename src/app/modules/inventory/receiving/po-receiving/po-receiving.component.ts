import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DropdownService } from '@app/shared/services/dropdown.service';
import { ErrorService, AppService } from '@app/core';
import { UtilityService } from '@app/shared/services/utility.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { PurchaseOrderService } from '@services/inventory/purchase-order.service';
import { LocationsMaster } from '@app/core/models/masterDropDown.model';

@Component({
    selector: 'app-po-receiving',
    templateUrl: 'po-receiving.component.html'
})

export class PoReceivingComponent implements OnInit {
    orderTypes: any[];
    groupBy: any[];
    supplierlist: any = [];
     /**
     * Output  of po search component
     */
    @Output() POSelectedData: EventEmitter<any> = new EventEmitter();
    /**
     * Purchaselocations  of po search component
     */
    purchaselocations: any = [];
    /**
     * Purchase search form of po search component
     */
    PurchaseSearchForm: FormGroup;
    /**
     * Podata  of po search component
     */
    POdata: any = [];
    /**
     * Posearchdata  of po search component
     */
    POsearchdata: any;
    /**
     * Labstatusdata  of po search component
     */
    labstatusdata: any = [];
    /**
     * Labstatuslist  of po search component
     */
    labstatuslist: any = [];
    /**
     * Creates an instance of po search component.
     * @param dropdownService for common services
     * @param errorService for error handle services
     * @param utility for get length method
     * @param _fb for form group
     * @param purchaseOrderService for po service methods 
     */
    constructor(private dropdownService: DropdownService,
        private errorService: ErrorService,
        private utility: UtilityService, 
        private _fb: FormBuilder,
        public app: AppService,
        private purchaseOrderService: PurchaseOrderService) {

    }
    @Output() poreceiveoutput = new EventEmitter<any>();
    /**
     * on init for initilizing methods
     */
    ngOnInit() { 
        this.supplierDropData();
        this.LoadLocations();
        this.Loadlabstatus();
        this.PurchaseSearchForm = this._fb.group({
            vendor_id: ['', [Validators.required]],
            location_id: '',
            po_status:''
        });
    }
    /**
     * Loadlabstatus po receiving component
     * loading status dropdown
     */
    Loadlabstatus() {
        this.labstatusdata = [];
        this.labstatusdata = this.purchaseOrderService.labstatus;
        if (this.utility.getObjectLength(this.labstatusdata) === 0) {
        this.labstatusdata.push({ Id: '', Name: 'Select Status' });
        this.purchaseOrderService.Getstatusdropdata().subscribe(
            data => {
                try {
                    this.labstatuslist = data;
                    for (let i = 0; i <= this.labstatuslist.length - 1; i++) {
                        if (!this.labstatusdata.find(a => a.Name === this.labstatuslist[i].description)) {
                            this.labstatusdata.push({ Id: this.labstatuslist[i].id.toString(), Name: this.labstatuslist[i].description });
                        }
                    }
                } catch (error) {
                    this.errorService.syntaxErrors(error);
                }
                this.purchaseOrderService.labstatus = this.labstatusdata;
            });
        }
    }
    /**
      * supplier drop data
      * @param {Array} supplierlist for binding dropdown data
      * @returns {object}  for dropdown data binding
      */
    supplierDropData() {
        this.supplierlist = [];
        let labsVenderData: any = [];
        this.supplierlist = this.dropdownService.supplierlist;
        if (this.utility.getObjectLength(this.supplierlist) === 0) {
            // this.supplierlist.push({ id: '', vendor_name: 'Select Lab' });
            this.dropdownService.getLabsVender().subscribe(
                data => {
                    try {
                        this.supplierlist.push({ id: '', vendor_name: 'Select Supplier' });
                        labsVenderData = data;
                        for (let i = 0; i <= labsVenderData.data.length - 1; i++) {
                            this.supplierlist.push(labsVenderData.data[i]);
                        }
                    } catch (error) {
                        this.errorService.syntaxErrors(error);
                    }
                    this.dropdownService.lablist = this.supplierlist;
                });
        }

    }
    /**
     * Loads locations
     *  load locations drop down
     */
    LoadLocations() {
        this.purchaselocations = this.dropdownService.locationsData;
        if (this.utility.getObjectLength(this.purchaselocations) === 0) {
            this.dropdownService.getLocationsDropData().subscribe(
                (data: LocationsMaster[]) => {
                    try {
                        this.purchaselocations.push({ id: '', name: 'Select Location' });
                        for (let i = 0; i <= data.length - 1; i++) {
                            this.purchaselocations.push(data[i]);
                        }
                        this.dropdownService.locationsData = this.purchaselocations;
                    } catch (error) {
                        this.errorService.syntaxErrors(error);
                    }
                }
            );
        }

    }
     /**
     * Search po items
     */
    searchPO() {
        if (this.PurchaseSearchForm.controls.vendor_id.value != '') {
            let reqbody = {
                "filter": [
                    {
                        "field": "supplierid",
                        "operator": "=",
                        "value": this.PurchaseSearchForm.controls.vendor_id.value,
                    }
                ],
                "sort": [
                    {
                        "field": "id",
                        "order": "ASC"
                    }
                ]
            }
            this.purchaseOrderService.getPOdata(reqbody).subscribe(data => {
                this.POdata = data['data'];
            })
        }
    }
     /**
     * Determines whether row select on
     * @param event for clicked data 
     */
    onRowSelect(event) {
        this.POsearchdata = event.data;
    }
    /**
     * Determines whether row unselect on
     * @param event for clicked data
     */
    onRowUnselect(event) {
        this.POsearchdata = '';
    }
    /**
     * Doubles click po
     * @param event for clicked data
     */
    DoubleClickPO(event) {
        this.POSelectedData.emit(event);
        // this.purchaseOrderService.POSelectedData.next(event);
        this.purchaseOrderService.UpdateId = event.id;
        this.purchaseOrderService.searchsupplieerData = event;
    }
    /**
     * Poselectbtns po search component
     */
    Poselectbtn() {
        if (this.POsearchdata != '' || this.POsearchdata != undefined) {
            this.POSelectedData.emit(this.POsearchdata);
            // this.purchaseOrderService.POSelectedData.next(this.POsearchdata);
            this.purchaseOrderService.UpdateId = this.POsearchdata.id;
            this.purchaseOrderService.searchsupplieerData = this.POsearchdata;
        }
    }
    /**
     * Determines whether close po on
     * Closing the side nav
     */
    onClosePO() {
        this.poreceiveoutput.emit('close');
    }
}
