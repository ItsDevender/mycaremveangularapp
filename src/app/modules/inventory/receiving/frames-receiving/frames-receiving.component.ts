import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FramesService } from '@services/inventory/frames.service';
import { InventoryService } from '@app/core/services/inventory/inventory.service';
// import { FrameService } from 'src/app/ConnectorEngine/services/frame.service';

@Component({
    selector: 'app-frames-receiving',
    templateUrl: 'frames-receiving.component.html'
})


export class FramesReceivingComponent implements OnInit {
    framelist: any[];
    orderTypes: any[];
    groupBy: any[];
    RecievingInventorySelection = [];
    @Input() dataFromRecievingComponent: any;
    @Output() frameoutput = new EventEmitter<any>();
    constructor(private frameService: FramesService,
        private inventory: InventoryService) {

    }
    ngOnInit() {
        this.GetList();
    }
    GetList() {
        this.framelist = [];
        console.log(this.dataFromRecievingComponent);
        console.log(this.dataFromRecievingComponent.loc_id);
        const vendorId = this.dataFromRecievingComponent.supplierid == null ? '' : this.dataFromRecievingComponent.supplierid;
        const locationId = this.dataFromRecievingComponent.loc_id == null ? '' : this.dataFromRecievingComponent.loc_id;
        // alert(locationId);
        const dataFilter = {
            filter: [
                {
                    field: 'vendor_id',
                    operator: '=',
                    value: vendorId
                },
                // {
                //     field: 'loc_id',
                //     operator: '=',
                //     value: locationId
                // },
                {
                    field: 'module_type_id',
                    operator: '=',
                    value: '1'
                }],
        };
        this.inventory.filterInventories(dataFilter).subscribe(
            data => {
                console.log(data);
                this.framelist = data['data'];
                // alert(this.framelist);
                console.log(this.framelist);
            });
    }
    onRowSelect(event) {
        console.log(event);

    }
    onRowUnselect(event) {
        console.log(event);
    }
    addProduct() {
        console.log(this.RecievingInventorySelection);
        this.frameoutput.emit(this.RecievingInventorySelection);
        // this.frameService.selectedRecievingValue.next(this.RecievingInventorySelection);
    }
    frameClose() {
        this.frameoutput.emit('close');
    }

}
