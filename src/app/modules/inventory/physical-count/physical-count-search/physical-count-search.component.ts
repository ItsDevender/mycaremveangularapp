import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup} from '@angular/forms';
// import { PhysicalCountService } from '../../../ConnectorEngine/services/physical-count.service';
import { PhysicalcountService } from '@services/inventory/physicalcount.service';
import { ErrorService } from '@app/core/services/error.service';
import { UtilityService } from '@shared/services/utility.service';
import { AppService } from '@app/core';
@Component({
    selector: 'app-physical-count-search',
    templateUrl: 'physical-count-search.component.html'
})

export class PhysicalSearchComponent implements OnInit {

    // Manufacturers Drop Down
    @Input() filteredManufacturers;

    // Locations Drop Down
    @Input() Locations;

    // Brands Drop Down
    @Input() brands;

    // Load Parent Selected Manufacture
    @Input() manufacturer_selected;

    // On Type Search Manufacturer
    @Output() manufacturer_selectedChange  = new EventEmitter<any>();

    // Items Dropdown
    @Input() items;

    // Send Manufacturer to parent screen
    @Output() SuggestManufacturer: EventEmitter<any> = new EventEmitter();

    // Send data to parent screen
    @Output() searchoutput = new EventEmitter<any>();

    // Globally Assign Location to application
    @Output() changeLocation: EventEmitter<any> = new EventEmitter();

    // Send Selected brand to parent screen
    @Output() changeBrand: EventEmitter<any> = new EventEmitter();

    cols: any[];

    batches: any = [];
    batchFormGroup: FormGroup;
    temp_data: any = [];
    batchesList: any = [];
    selectedBatches: any = [];

    constructor(
      private _fb: FormBuilder, 
      private service: PhysicalcountService, 
      private error: ErrorService, 
      private utilityService:UtilityService, 
      public app: AppService) {
      this.batchFormGroup  = this._fb.group(this.service.data['batchform']);
      // Form Changes
      this.batchFormGroup.valueChanges.subscribe(data => {
         this.service.data['batchform'] = data;
      });
    }

    ngOnInit() {
      this.service.errorHandle.msgs = [];
      this.cols = [
            { field: 'id', header: 'ID' },
            { field: 'date', header: 'Date' },
            { field: 'location', header: 'Location' },
            { field: 'status', header: 'Status' },
            { field: 'itemtype', header: 'Item Type'},
            { field: 'notes', header: 'Notes'},
            { field: 'employee_name', header: 'Employee Name'}
      ];

      // Load with default params;
      this.searchBatch();
    }

    // On Closing
    onClickClose() {
        this.searchoutput.emit('close');
    }

    // Parent Emits
    emitParent(event, type: string = '') {
       if (type === 'manufacturer') {
          this.SuggestManufacturer.emit(event);
       } else if (type === 'location') {
          this.changeLocation.emit(event);
       } else if (type === 'changeBrand') {
          this.changeBrand.emit(event);
          this.manufacturer_selectedChange.emit(this.manufacturer_selected);
       }
    }

    // loadBatches
    loadBatch() {
      console.log(this.selectedBatches);
        if ('id' in this.selectedBatches) {
          // Assigning data to Batch Form
          this.service.data['batchform'] = [];
          this.service.data['batchform'] = {
            location: ('locationid' in this.selectedBatches) ? this.selectedBatches.locationid : '',
            batchID: this.selectedBatches.id,
            batchDate: ('receive_date' in this.selectedBatches) ? this.selectedBatches.receive_date : '',
            batchStatus: ('receive_status' in this.selectedBatches) ? this.selectedBatches.receive_status : '',
            batchNotes:  ('notes' in this.selectedBatches) ?  this.selectedBatches.notes : '',
            item: ('module_type_id' in this.selectedBatches) ? parseInt(this.selectedBatches.module_type_id, 10) : ''
          };
          this.batchFormGroup.setValue(this.service.data['batchform']);

          // Send Signal to Parent
          this.searchoutput.emit('loadBatch');
        }
    }


    // Search Batch
    searchBatch() {
      // GET BATCH DATA
      const batchData = this.batchFormGroup.value;

      const reqBody = { 'filter' : [] , 'sort' : [ { 'field' : 'id', 'order': 'ASC' } ]  };


      const id = batchData.batchID;
      const receive_status = batchData.batchStatus;
      const locationid = batchData.location;
      const receive_date = this.utilityService.transformDate(batchData.batchDate);
      const module_type_id = batchData.item;

      // For ID
      if (id !== '' && id !== null) {
        reqBody.filter.push({'field' : 'id', 'operator': '=', 'value': id});
      }

      // For Status
      if (receive_status === '0' || receive_status === '1') {
        reqBody.filter.push({'field' : 'receive_status', 'operator': '=', 'value': receive_status});
      }

      // For Status
      if (locationid !== '' && locationid !== null) {
        reqBody.filter.push({'field' : 'locationid', 'operator': '=', 'value': locationid});
      }

      // For Receive Date
      if (receive_date !== '' && receive_date !== null) {
        reqBody.filter.push({'field' : 'receive_date', 'operator': '=', 'value': receive_date});
      }

      // For Receive Type
      if (module_type_id !== '' && module_type_id !== null) {
        reqBody.filter.push({'field' : 'module_type_id', 'operator': '=', 'value': module_type_id});
      }

      this.service.getBatches(reqBody).subscribe(
        data => {
          this.temp_data = data;
          this.temp_data = this.temp_data.data;
          if (this.temp_data.length > 0) {
            this.error.displayError(
              { severity: 'success', summary: 'Batch Search Status', detail: this.temp_data.length + ' Results Loaded' });
            this.batchesList = this.temp_data;
          } else {
            this.error.displayError(
              { severity: 'error', summary: 'No Batch found for current location', detail: this.temp_data.length + ' Results Loaded' });
          }
        },
        error => {
          this.temp_data = error;
          if ('error' in this.temp_data) {
            Object.keys(this.temp_data.error).forEach (key => {
               this.error.displayError( { severity: 'error', summary: 'Batch Status', detail: this.temp_data.error[key] });
            });
          }
        }
      );
    }

}
