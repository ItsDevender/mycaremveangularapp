import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-contact-config',
    templateUrl: 'contact-config.component.html'
})

export class ContactConfigComponent implements OnInit {
    contactConfigData: any[];

    ngOnInit(): void {
        this.contactConfigData = [
            {
                upc: '400000220612', material: '', color: '', basecurve: '', diameter: '', sphere: '',
                cylinder: '', axis: '', addpower: '', multifocal: '', cost: '', onorderqnty: '', committed: '', maxinv: ''
            },
            {
                upc: '400000220123', material: '', color: 'Brown', basecurve: '1.00', diameter: '', sphere: '',
                cylinder: '', axis: '005', addpower: '', multifocal: '', cost: '', onorderqnty: '', committed: '', maxinv: ''
            },
            {
                upc: '400000220612', material: '', color: '', basecurve: '', diameter: '', sphere: '',
                cylinder: '', axis: '', addpower: '', multifocal: '', cost: '', onorderqnty: '', committed: '', maxinv: ''
            },
            {
                upc: '400000220123', material: '', color: 'Brown', basecurve: '5.00', diameter: '', sphere: '',
                cylinder: '', axis: '005', addpower: '', multifocal: '', cost: '', onorderqnty: '', committed: '', maxinv: ''
            },
            {
                upc: '400000220612', material: '', color: '', basecurve: '', diameter: '', sphere: '',
                cylinder: '', axis: '', addpower: '', multifocal: '', cost: '', onorderqnty: '', committed: '', maxinv: ''
            },
            {
                upc: '400000220123', material: '', color: 'Brown', basecurve: '3.00', diameter: '', sphere: '',
                cylinder: '', axis: '002', addpower: '', multifocal: '', cost: '', onorderqnty: '', committed: '', maxinv: ''
            }
        ];
    }
}
