import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
// import { LODService } from 'src/app/ConnectorEngine/services/lod.service';
import { Router } from '@angular/router';



@Component({
    selector: 'app-lens-on-demand',
    templateUrl: 'lens-on-demand.component.html'
})
export class LensOnDemandComponent implements OnInit {
    headers: any[];
    private supplierlist: any[];
    /**
     * Suppliers  of lens on demand component
     */
    suppliers: Array<object>;
    gridlists: any[];
    reActivateItems: boolean;
    LODForm: FormGroup;

    constructor(
        // private _LODService: LODService, 
        private _router: Router) {

    }


    ngOnInit() {
        this.headers = [
            { field: 'manufacturer', header: 'Manufacturer' },
            { field: 'lens', header: 'Lens Name' },
            { field: 'configs', header: 'Configs' },
            { field: 'trial', header: 'Trial' },
            { field: 'active', header: 'Active' },
            { field: 'location', header: 'Location' }
        ];
        this.gridlists = [
            { 'location': 'MVE: My Vision', 'manufacturer': 'A&A Opticals' },
            { 'location': 'MVE: My Vision', 'manufacturer': 'A&A Opticals' },
            { 'location': 'MVE: My Vision', 'manufacturer': 'A&A Opticals' },
            { 'location': 'MVE: My Vision', 'manufacturer': 'A&A Opticals' },
            { 'location': 'MVE: My Vision', 'manufacturer': 'A&A Opticals' },
        ];

        //    this.LoadSuppliers();
    }



}

