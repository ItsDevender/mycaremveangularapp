
import { Component, OnInit, OnDestroy, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { Subscription } from 'rxjs';
import { FramesService } from '@services/inventory/frames.service';
import { ContactlensService } from '@services/inventory/contactlens.service';
import { ErrorService } from '@app/core/services/error.service';
import { DropdownService } from '@app/shared/services/dropdown.service';
import { LenstreatmentService } from '@app/core/services/inventory/lenstreatment.service';
import { InventoryService } from '@app/core/services/inventory/inventory.service';
import { UtilityService } from '@app/shared/services/utility.service';

import { GetProcedureCode, InventoryResultDetails } from '@app/core/models/inventory/inventoryItems.model';
import { ContactLensFormData, } from '@app/core/models/inventory/contactlens.model';
import {
    ManufacturerDropData,
    CommissionStructure,
    CommissionType,
    FrameSupplier,
    ContactReplacement,
    ContactPackage,
    FrameColorData, Data, LocationsMaster
} from '@app/core/models/masterDropDown.model';

@Component({
    selector: 'app-add-inventory',
    templateUrl: 'add-inventory.component.html'
})
export class AddInventoryComponent implements OnInit, OnDestroy {
    packageData: any[];
    refreshdetails: Subscription;
    contactLensSubscription: Subscription;
    contactlensForm: FormGroup;
    Accesstoken = '';
    Manufacturers: any[];
    /**
     * Colors  of add inventory component
     * holding list of contact lens colorId,colorName
     */
    public colors: Array<object>;
    public supplierslist: any[];
    /**
     * Suppliers  of add inventory component
     */
    suppliers: Array<object>;
    public locationlist: any[];
    public replacementlist: any;
    replacements: any[];
    public traillenslist: any;
    traillens: any[];
    locations: Array<LocationsMaster>;
    // colorid = '';
    postframes: any;
    orderTypes: any;
    rowData: any;
    upcdisable = false;
    buttonsHide = false;
    LODLink = false;
    AddConfiguration = false;
    parameters: any[];
    Contactlensconfigcolumns: any[];
    public contactlenslistconfigurationslist: any[];
    visible = true;
    ConfigHide = false;
    configurationID: '';
    addContactLensInvTransaction = false;
    transactionsBtnHide = false;
    transacctionStockDetails: any = [];
    StockDetails: any = [];
    UpdateStock: any;
    parameterstab = false;
    parameterssavedata: any;
    uploadedFiles: any[] = [];

    locationNameData: any[] = [];
    locationGridData: any[] = [];
    @Input() locationItem: any;
    @Input() imageItem: any;
    filteredprocedurescodes: any[];
    procedurecoderesults: any[];
    brands: string[] = ['test'];
    brand: string;

    /**
     * Filtered procedures of add inventory component
     *  Holding procedure code data id, Name
     */
    filteredProcedures: Array<object>;
    listImages: any[] = [];
    getlistImage: any[] = [];

    @ViewChild('dtconfig') public dtconfig: any;
    @Output() contactAddItem = new EventEmitter<any>();
    @Input() showconfigtab: any;
    subscriptionStockDetailsUpdateContactLens: Subscription;
    FilteredTrasaction: any = [];
    TypeDropData = [];
    StructureDropData = [];
    configurationItemId: any = '';
    locationsList: any;

    /**
     * Selectedlensid  of add inventory component
     */
    selectedlensid: any;
    ContactlensItemId: any;
    /**
     * Saving pay load of add inventory component
     * stroes form controller values
     */
    savingPayLoad: ContactLensFormData;

    /**
     * Creates an instance of add inventory component.
     * @param _fb form builder
     * @param contactLensService provide contactlens service
     * @param _FrameService provide frame service
     * @param _InventoryCommonService provide inventoryCommonService
     * @param error provide  error service
     * @param dropdownService  provide  dropdownService
     * @param inventoryService provide inventoryService
     * @param utility  provide utility service
     */
    constructor(
        private _fb: FormBuilder,
        private contactLensService: ContactlensService,
        private frameService: FramesService,
        // private _StateInstanceService: StateInstanceService,
        private inventoryCommonService: InventoryService,
        // private _DropdownsPipe: DropdownsPipe,
        private error: ErrorService,
        private dropdownService: DropdownService,
        private inventoryService: InventoryService,
        private utility: UtilityService
    ) {



    }


    ngOnDestroy(): void {


    }

    /**
     * Filters procedures
     * @returns filtering procedure codes details based on hiting alphabets
     */
    filterProcedures(event) {
        this.procedurecoderesults = [];
        this.filteredprocedurescodes = [];
        // let query = string.query;
        const values = {

            filter: [{
                field: 'cpt_prac_code',
                operator: 'LIKE',
                value: '%' + event.query.toLowerCase() + '%'
            }]
        };


        this.frameService.getProcedureCode(values).subscribe((data: GetProcedureCode) => {
            this.procedurecoderesults = data.data;
            this.filteredprocedurescodes = this.filteredprocedurescodes;
            this.filteredProcedures = [];
            // tslint:disable-next-line:prefer-for-of
            for (let i = 0; i < this.procedurecoderesults.length; i++) {
                const procedure = this.procedurecoderesults[i].cpt_prac_code;
                const procedureId = this.procedurecoderesults[i].cpt_fee_id;
                if (procedure.toLowerCase().indexOf(event.query.toLowerCase()) === 0) {
                    this.filteredProcedures.push({ Id: procedureId, Name: procedure });
                }
            }
            if (this.filteredProcedures.length === 0) {
                this.error.displayError({ severity: 'error', summary: 'Error Message', detail: 'No Data Found.' });


            }
        });
    }

    ngOnInit() {
        if (this.showconfigtab === 'open') {
            this.ConfigHide = true;
        }
        this.LoadLocations();
        this.Loadmasters();
        this.loadContactlensconfigurations();
        this.Loadformdata();
        this.Getcontacttraillens();
        this.getLocationsItems();


    }

    /**
     * Gets locations items 
     * Here we are getting location details
     */
    getLocationsItems() {
        if (this.locationItem !== undefined) {
            this.locationNameData = this.dropdownService.inventoryLocation;
            if (this.utility.getObjectLength(this.locationNameData) === 0) {
                this.dropdownService.getInventtoryLocations().subscribe(data => {
                    this.locationNameData = data['result']['Optical']['Locations'];
                    this.dropdownService.inventoryLocation = data['result']['Optical']['Locations'];
                    this.inventoryCommonService.getLocationByFilter(this.locationItem).subscribe((data: any[]) => {
                        this.locationGridData = data['data'];
                        this.locationGridData.forEach(element => {
                            for (let i = 0; i <= this.locationNameData.length - 1; i++) {
                                if (element.loc_id === this.locationNameData[i].Id) {
                                    element.loc_id = this.locationNameData[i].Name;
                                }
                            }
                        });
                    });
                })
            } else {
                this.inventoryCommonService.getLocationByFilter(this.locationItem).subscribe((data: any[]) => {
                    this.locationGridData = data['data'];
                    this.locationGridData.forEach(element => {
                        for (let i = 0; i <= this.locationNameData.length - 1; i++) {
                            if (element.loc_id === this.locationNameData[i].Id) {
                                element.loc_id = this.locationNameData[i].Name;
                            }
                        }
                    });
                });
            }
        }
    }

    /**
     * Loadformdatas add inventory component loading date to the form
     */
    Loadformdata() {
        this.ContactlensItemId = this.inventoryCommonService.FortrasactionItemID;
        this.ConfigHide = false;
        this.upcdisable = false;
        this.Accesstoken = ''
        this.contactlensForm = this._fb.group({
            name: ['', [Validators.required]],
            manufacturer_id: ['', [Validators.required]],
            vendor_id: '',
            brand_id: [12],
            cl_packaging: '',
            type_id: [''],
            material_id: [''],
            wearing_schedule_id: [''],
            replacementId: [null],
            hard_contact: false,
            inventory: false,
            supply_id: [''],
            colorid: '',
            retail_price: '',
            upc_code: ['', [Validators.required]],
            // supplierId: '',
            locationid: '',
            configlocationid: '',
            max_qty: '',
            min_qty: '',
            notes: '',
            traillensId: '',
            units: '',
            cl_replacement: '',
            wholesale_cost: '',
            procedure_code: '',
            profit: '',
            six_months_price: '',
            six_months_qty: '',
            twelve_months_price: '',
            twelve_months_qty: '',
            sixmonthsprofit: '',
            twelvemonthsprofit: '',
            parameters: '',
            commission_type_id: '',
            structure_id: '',
            amount: '',
            gross_percentage: '',
            module_type_id: 3,
            trial_lens_id: '',
            imagesList: this._fb.array([])

        });

        this.selectedlensid = this.contactLensService.editlensid || this.contactLensService.editlensidDoubleClick;

        if (this.selectedlensid) {
            this.DataBindingToMOdify();
        }

    }

    /**
     * Datas binding to modify screen
     * binding the selected item values
     */
    DataBindingToMOdify() {
        this.ConfigHide = true;
        this.upcdisable = true;
        let filterdata = {
            filter:
                [{
                    field: 'id',
                    operator: '=',
                    value: this.selectedlensid
                },
                {
                    field: 'module_type_id',
                    operator: '=',
                    value: '3'
                }],
            sort: [
                {
                    field: 'module_type_id',
                    order: 'DESC'
                }
            ]
        }
        let contactLensData: any = []
        this.contactLensService.getcontactlensNew(filterdata).subscribe(
            data => {
                contactLensData = data
                const contactLensFormData = contactLensData.data[0];
                this.contactlensForm = this._fb.group({
                    upc_code: [contactLensFormData.upc_code, [Validators.required]],
                    name: [contactLensFormData.name, [Validators.required]],
                    manufacturer_id: [contactLensFormData.manufacturer_id, [Validators.required]],
                    vendor_id: contactLensFormData.vendor_id === 0 ? '' : contactLensFormData.vendor_id,
                    brand_id: [contactLensFormData.brand_id, [Validators.required]],
                    cl_packaging: contactLensFormData.cl_packaging === 0 ? '' : contactLensFormData.cl_packaging,
                    type_id: contactLensFormData.type_id,
                    material_id: contactLensFormData.material_id,
                    wearing_schedule_id: contactLensFormData.wearing_schedule_id,
                    supply_id: contactLensFormData.supply_id,
                    colorid: contactLensFormData.colorid === null  ? '' : contactLensFormData.colorid,
                    retail_price: contactLensFormData.retail_price,
                    trial_chk: contactLensFormData.trial_chk === 0 ? '0' : contactLensFormData.trial_chk,
                    // supplierId: '',
                    locationid: '',
                    configlocationid: '',
                    min_qty: contactLensFormData.min_qty,
                    max_qty: contactLensFormData.max_qty,
                    notes: contactLensFormData.notes,
                    traillensId: '',
                    units: contactLensFormData.units,
                    cl_replacement: contactLensFormData.cl_replacement,

                    hard_contact: contactLensFormData.hard_contact,

                    inventory: contactLensFormData.inventory,
                    wholesale_cost: contactLensFormData.wholesale_cost,

                    procedure_code: contactLensFormData.procedure_code === '' ? null : contactLensFormData.procedure_code,

                    profit: '',
                    six_months_price: contactLensFormData.six_months_price,

                    six_months_qty: contactLensFormData.six_months_qty,

                    twelve_months_price: contactLensFormData.twelve_months_price,

                    twelve_months_qty: contactLensFormData.twelve_months_qty,

                    sixmonthsprofit: '',

                    twelvemonthsprofit: '',
                    parameters: '',
                    structure_id: contactLensFormData.structure_id === null ? '' : contactLensFormData.structure_id,

                    commission_type_id: contactLensFormData.commission_type_id === 0 ? '' : contactLensFormData.commission_type_id,
                    amount: contactLensFormData.amount,
                    gross_percentage: contactLensFormData.gross_percentage,
                    module_type_id: 3,

                    imagesList: this._fb.array([])
                });
                this.filterProcedureCodeDetails(contactLensFormData.procedure_code);

                this.listAllImages(this.selectedlensid);
                this.duplicateCheck();

            });

    }

    /**
     * Duplicates check checks wheather duplicate or not
     */
    duplicateCheck() {
        if (this.contactLensService.modifyOrDuplicate == 'Duplicate') {
            this.contactlensForm.controls['upc_code'].patchValue('')
            this.upcdisable = false;
        }
    }

    /**
     * Lists all images
     * @param {number} selectedlensid 
     */
    listAllImages(selectedlensid) {
        this.listImages = [];
        this.getlistImage = [];
        this.contactLensService.contactlensListImages(this.imageItem).subscribe(data => {
            if (data['result']['Optical'] !== 'No record found') {
                this.listImages = data['result']['Optical']['GetImages'];
                for (let i = 0; i <= this.listImages.length - 1; i++) {
                    this.contactLensService.ContactlensGetImages(this.listImages[i].Id, this.listImages[i].ItemId).subscribe(Value => {
                        this.getlistImage.push(Value['result']['Optical']['GetImage'][0]);
                        const control = <FormArray>this.contactlensForm.controls['imagesList'];
                        control.push(
                            this._fb.group({
                                Id: Value['result']['Optical']['GetImage'][0].Id,
                                Image: 'data:image/JPEG;base64,' + Value['result']['Optical']['GetImage'][0].Image
                            })
                        );

                    });
                }

            }
        });


        this.profitcalculate();
        this.getparametersbyconfiqID(selectedlensid);
        this.parameterstab = this.contactlensForm.controls['hard_contact'].value;
    }


    /**
     * Adds add inventory component uploading image
     * @param {number} event 
     */
    add(event) {
        for (const file of event.target.files) {
            this.uploadedFiles.push(file);
            const reader = new FileReader();
            reader.readAsDataURL(file);
            const control = <FormArray>this.contactlensForm.controls['imagesList'];
            reader.onload = (value) =>
                control.push(
                    this._fb.group({
                        Image: value.target['result']
                    })
                );
        }
    }



    /**
     * Deletes image selected image
     * @param {number} i 
     */
    deleteImage(i) {

        if (this.imageItem === undefined) {
            const control = <FormArray>this.contactlensForm.controls['imagesList'];
            control.removeAt(i);
            this.uploadedFiles.splice(i, 1);
        } else {
            if (this.contactlensForm.controls['imagesList']['controls'][i].value.Id) {
                const formData: FormData = new FormData();
                formData.append('accessToken', ''

                );
                formData.append('Id', this.contactlensForm.controls['imagesList']['controls'][i].value.Id);
                formData.append('itemId', this.imageItem);
                this.contactLensService.ContactImageDeleting(formData).subscribe(data => {
                    const control = <FormArray>this.contactlensForm.controls['imagesList'];
                    control.removeAt(i);
                });
            } else {
                const control = <FormArray>this.contactlensForm.controls['imagesList'];
                control.removeAt(i);
                this.uploadedFiles.splice(i, 1);
            }


        }

    }

    /**
     * Sets parameters
     */
    SetParameters() {
        this.parameterstab = this.contactlensForm.controls['hard_contact'].value;
    }

    /**
     * Loadmasters add inventory component masters
     */
    Loadmasters() {
        this.LoadColors();
        this.GetSupplierDetails();
        this.Getcontactlensreplacements();
        this.getPackagingData();
        this.getparameters();
        this.loadCommisions();
        this.loadStructure();
        this.LoadManufacturers();
    }

    /**
     * Loads manufacturers for masters
     */
    LoadManufacturers() {
        this.Manufacturers = this.dropdownService.Manufactures;
        if (this.utility.getObjectLength(this.Manufacturers) === 0) {
            const reqbody = {
                filter: [
                    {
                        field: 'manufacturer_name',
                        operator: 'LIKE',
                        value: '%%'
                    }
                ]
            };
            this.dropdownService.manufacturesDropDown(reqbody).subscribe(
                (values: ManufacturerDropData) => {
                    try {
                        const dropData = values.data;
                        this.Manufacturers.push({ Id: '', Name: 'Select Manufacturer' });
                        for (let i = 0; i <= dropData.length - 1; i++) {
                            this.Manufacturers.push({ Id: dropData[i].id, Name: dropData[i].manufacturer_name });
                        }
                        this.dropdownService.Manufactures = this.Manufacturers;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }
                });
        }
    }
    /**
     * Loads 
     * @returns commision data for masters
     */
    loadCommisions() {
        this.TypeDropData = this.inventoryService.commissionType;
        if (this.utility.getObjectLength(this.TypeDropData) === 0) {
            this.inventoryService.getCommissionTypedropData().subscribe((values: CommissionType) => {
                try {
                    this.TypeDropData.push({ Id: '', Name: 'Select Type' });
                    const dropData  = values.data;
                    for (let i = 0; i <= dropData.length - 1; i++) {
                        this.TypeDropData.push({ Id: dropData[i].id, Name: dropData[i].commissiontype });
                    }
                    this.inventoryService.commissionType = this.TypeDropData;
                } catch (error) {
                    this.error.syntaxErrors(error);
                }
            });
        }

    }
    /**
     * Loads structure
     * @returns display structure data for masters
     */
    loadStructure() {
        this.StructureDropData = this.inventoryService.structureData;
        if (this.utility.getObjectLength(this.StructureDropData) === 0) {

            this.inventoryService.getStructuredropData().subscribe((values: CommissionStructure) => {
                try {
                    this.StructureDropData.push({ Id: '', Name: 'Select Structure' });
                    const dropData  = values.data;
                    for (let i = 0; i <= dropData.length - 1; i++) {
                        this.StructureDropData.push({ Id: dropData[i].id, Name: dropData[i].structure_name });
                    }
                    this.inventoryService.structureData = this.StructureDropData;
                } catch (error) {
                    this.error.syntaxErrors(error);
                }
            });
        }
    }





    /**
     * Getparameters add inventory component for masters
     */
    getparameters() {
        if (this.contactLensService.parametersData.length === 0) {
            this.contactLensService.getcontactParametersDetails().subscribe(data => {
                this.parameters = data['data'];
                for (let i = 0; i <= this.parameters.length - 1; i++) {
                    this.parameters[i].Availablebool = false;
                    this.parameters[i].requiredbool = false;
                }
                this.contactLensService.parametersData = this.parameters;
            });
        } else {
            this.parameters = this.contactLensService.parametersData;
        }
    }

    /**
     * Getparametersbyconfiqs id getting the configurations
     * @param {number} confiqID 
     */
    getparametersbyconfiqID(confiqID) {
        this.contactLensService.getcontactParametersByConfiqID(confiqID).subscribe(data => {
            const selectparams = data['data'];
            selectparams.forEach(element => {
                const a = this.parameters.findIndex(x => x.measurementname === element.measurementname);
                if (a !== -1) {
                    if (this.parameters[a].measurementname === element.measurementname) {
                        this.parameters[a].checkedvalues = true;
                        this.parameters[a].Availablebool = element.available === 1 ? true : false;
                        this.parameters[a].requiredbool = element.required === 1 ? true : false;
                    }
                }
            });
        });
    }


    /**
     * Checkunchekevents add inventory component
     * @param {string} data 
     * @param {object} add 
     * @param {number} index 
     * @param {string} detailsofvalue 
     */
    checkunchekevent(data, add, index, detailsofvalue) {
        if (this.parameters !== undefined) {
            const a = this.parameters.findIndex(x => x.measurementname === data.measurementname);
            if (a !== -1) {
                if (detailsofvalue === 'available') {
                    this.parameters[a].measurementname = data.measurementname;
                    this.parameters[a].checkedvalues = true;
                    this.parameters[a].Availablebool = add;
                }
                if (detailsofvalue === 'required') {
                    this.parameters[a].measurementname = data.measurementname;
                    this.parameters[a].checkedvalues = true;
                    this.parameters[a].requiredbool = add;
                }
                this.parameters = this.parameters;
            }
        }
    }



    /**
     * Saveparametersbys confiq id
     * @param  {number} configId 
     * @param savevalues 
     */
    saveparametersbyConfiqID(configId, savevalues) {
        this.parameterssavedata = [];
        this.parameterssavedata.parameters = [];

        const details = this.parameters.filter(x => x.checkedvalues === true);

        details.forEach(element => {
            // element.Availablebool=false;

            const availablebool = element.Availablebool;
            const requiredbool = element.requiredbool;
            let available = availablebool === true ? 1 : 0;
            let required = requiredbool === true ? 1 : 0;
            if (element.requiredbool === true) {
                available = 1;
                required = 1;
            }
            const values = {
                'measurementname': element.measurementname,
                'available': available,
                'required': required
            };

            this.parameterssavedata.parameters.push(values);
        });

        const savevaluesdetails = {
            'parameters': this.parameterssavedata.parameters
        };

        this.contactLensService.SaveContactLensParametersByConfiqID(configId, savevaluesdetails).subscribe(data => {


        },
            error => {

            }

        );

    }


    /**
     * Checkminmaxvalues add inventory component validating the max should not greater than min
     * @returns true if checkminmaxvalue 
     */
    checkminmaxvalue(): boolean {
        const minvalue = parseInt(this.contactlensForm.controls['min_qty'].value, 10);
        const maxvalue = parseInt(this.contactlensForm.controls['max_qty'].value, 10);
        if (maxvalue != null && maxvalue !== 0 && minvalue != null && minvalue !== 0 && minvalue > maxvalue) {
            this.error.displayError({
                severity: 'warn', summary: 'Warning Message',
                detail: 'Min qty should be less then Max qty/Max qty should be Grater then Min qty.'
            });

            // this.WarningShow('Min qty should be less then Max qty/Max qty should be Grater then Min qty.');

            return false;
        }
        return true;
    }


    /**
     * Profitcalculates add inventory component
     */
    profitcalculate() {
        if (this.contactlensForm.controls['wholesale_cost'].value !== '' ||
            this.contactlensForm.controls['wholesale_cost'].value != null || this.contactlensForm.controls['retail_price'].value !== '' ||
            this.contactlensForm.controls['retail_price'].value != null) {
            const diffvalue = this.contactlensForm.controls['retail_price'].value - this.contactlensForm.controls['wholesale_cost'].value;
            this.contactlensForm.controls['profit'].patchValue(diffvalue.toFixed(2));
        }
        if (this.contactlensForm.controls['wholesale_cost'].value === '' && this.contactlensForm.controls['retail_price'].value === '') {
            this.contactlensForm.controls['profit'].patchValue('');
        }

        if (this.contactlensForm.controls['wholesale_cost'].value !== '' || this.contactlensForm.controls['wholesale_cost'].value != null ||
            this.contactlensForm.controls['six_months_price'].value !== '' ||
            this.contactlensForm.controls['six_months_price'].value != null) {
            const diffvalue = this.contactlensForm.controls['six_months_price'].value - this.contactlensForm.controls['wholesale_cost'].value;
            this.contactlensForm.controls['sixmonthsprofit'].patchValue(diffvalue.toFixed(2));
        }
        if (this.contactlensForm.controls['six_months_price'].value === '') {
            this.contactlensForm.controls['sixmonthsprofit'].patchValue('');
        }

        if (this.contactlensForm.controls['wholesale_cost'].value !== '' || this.contactlensForm.controls['wholesale_cost'].value != null ||
            this.contactlensForm.controls['twelve_months_price'].value !== '' ||
            this.contactlensForm.controls['twelve_months_price'].value != null) {
            const diffvalue = this.contactlensForm.controls['twelve_months_price'].value - this.contactlensForm.controls['wholesale_cost'].value;
            this.contactlensForm.controls['twelvemonthsprofit'].patchValue(diffvalue.toFixed(2));
        }
        if (this.contactlensForm.controls['twelve_months_price'].value === '') {
            this.contactlensForm.controls['twelvemonthsprofit'].patchValue('');
        }
    }



    /**
     * Numbers only
     * @param {object} event 
     * @returns true if only 
     */
    numberOnly(event): boolean {
        const charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    }

    /**
     * Gets supplier details
     * Here we are getting lab suppliers data
     */
    GetSupplierDetails() {
        this.suppliers = this.dropdownService.frameSupplier;
        if (this.utility.getObjectLength(this.suppliers) === 0) {
            this.dropdownService.getSupplierDropData().subscribe(
                (values: FrameSupplier) => {
                    try {
                        const dropData  = values.data;
                        this.suppliers.push({ Id: '', Name: 'Select Supplier' });
                        for (let i = 0; i <= dropData.length - 1; i++) {
                                this.suppliers.push({ Id: dropData[i].id, Name: dropData[i].vendor_name });
                        }
                        this.dropdownService.frameSupplier = this.suppliers;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }

                });
        }
    }


    /**
     * Loads locations masters
     */
    LoadLocations() {
        this.locations = this.dropdownService.locationsData;
        if (this.utility.getObjectLength(this.locations) === 0) {
            this.dropdownService.getLocationsDropData().subscribe(
                (data: LocationsMaster[]) => {
                    try {
                        this.locations.push({ id: '', name: 'Select Location' });
                        for (let i = 0; i <= data.length - 1; i++) {
                            this.locations.push(data[i]);
                        }
                        this.dropdownService.locationsData = this.locations;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }
                }
            );
        }
        // Load Locations
       
    }





    // new micro API

    /**
     * Loads colors masters
     */
    LoadColors() {
        this.colors = this.contactLensService.colorsData;
        if (this.utility.getObjectLength(this.colors) === 0) {
            this.contactLensService.getContactlensColors('').subscribe(
                (values: FrameColorData) => {
                    try {
                        const dropData = values.data;
                        this.colors.push({ Id: '', Name: 'Select Color' });
                        for (let i = 0; i <= dropData.length - 1; i++) {

                            this.colors.push({ Id: dropData[i].id, Name: dropData[i].color_name });
                        }
                        this.contactLensService.colorsData = this.colors;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }
                });
        }

    }


    /**
     * Determines whether row configuration select on
     * @param {object} rowData 
     */
    onRowConfigurationSelect(rowData) {
        this.configurationID = rowData.data.id;

    }

    /**
     * Adds details configuration
     */
    AddDetailsConfiguration() {

        this.AddConfiguration = true;
        this.configurationItemId = '';

    }
    onRowUnselectconfig() {
        this.configurationID = '';

    }

    UpdateDetailsConfiguration() {
        if (this.configurationID !== '' && this.configurationID !== undefined) {
            this.configurationItemId = this.configurationID;

            this.AddConfiguration = true;
        } else {
            this.error.displayError({
                severity: 'error', summary: 'Error Message',
                detail: 'Please Select ContactLens Config.'
            });


            this.AddConfiguration = false;
        }
    }

    /**
     * Cancles contact lense for closing
     */
    cancleContactLense() {
        const item = {
            "status": 'contactclose',
            "data": ''
        }
        this.contactAddItem.emit(item);
        // this.contactAddItem.emit('contactclose');

    }

    /**
     * Savecontactlens add inventory component
     * @returns  {object} for saving contact lens data
     */
    savecontactlens() {
        const maxqtyrcheck = this.checkminmaxvalue();
        this.itemBelongsTo(maxqtyrcheck);
        if (this.contactLensService.modifyOrDuplicate === 'Duplicate') {
            this.selectedlensid = '';
        }
        this.structureformcontrols();
        let savePayload = this.contactlensForm.value;
        this.savingReqPayLoad();
        delete savePayload.imagesList;
        if (this.selectedlensid > 0) {
            this.contactLensService.Updatecontactlens(this.utility.formatWithOutControls(this.savingPayLoad), this.selectedlensid)
                .subscribe((data: InventoryResultDetails) => {
                    this.contactLensImageProcess(data, 'Updated');
                },
                    error => {
                        this.error.displayError({
                            severity: 'error', summary: 'Error Message',
                            detail: 'Save Failed'
                        });

                        // this.ErrorShow();

                    }

                );
        } else {
            this.contactLensService.Savecontactlens(this.utility.formatWithOutControls(this.savingPayLoad))
                .subscribe((data: InventoryResultDetails) => {
                    this.contactLensImageProcess(data, 'saved');
                },
                    error => {
                        this.error.displayError({
                            severity: 'error', summary: 'Error Message',
                            detail: 'Save Failed'
                        });
                    }

                );
        }

    }


    /**
     * Items belongs to
     * @param maxqtyrcheck  checking the max qty
     * @returns  if fails
     */
    itemBelongsTo(maxqtyrcheck) {
        if (maxqtyrcheck === false) {
            return;
        }
        if (!this.contactlensForm.valid) {
            return;
        }

        this.contactlensForm.value['replacementId'] = '[' + this.contactlensForm.value['replacementId'] + ']';

        if (this.contactlensForm.controls.inventory.value === true) {
            this.contactlensForm.controls.inventory.patchValue('1');
        } else {
            this.contactlensForm.controls.inventory.patchValue('0');
        }

        if (this.contactlensForm.value['hard_contact'] === true) {
            this.contactlensForm.value['hard_contact'] = true;
        } else {
            this.contactlensForm.value['hard_contact'] = false;
        }
    }

    /**
     * Contacts lens image process
     * @param {object} data 
     * @param {string} massage 
     */
    contactLensImageProcess(data, massage) {
        if (this.imageItem === undefined) {
            if (data.id) {
                for (let i = 0; i <= this.uploadedFiles.length - 1; i++) {
                    const formData: FormData = new FormData();
                    formData.append('itemId', data.id);
                    formData.append('item_pic', this.uploadedFiles[i]);
                    this.contactLensService.ContactlensImageAdding(formData).subscribe(value => {

                    });
                }

            }
        } else {
            for (let i = 0; i <= this.uploadedFiles.length - 1; i++) {
                const formData: FormData = new FormData();
                formData.append('itemId', this.imageItem);
                formData.append('item_pic', this.uploadedFiles[i]);
                this.contactLensService.ContactlensImageAdding(formData).subscribe(value => {
                });
            }
        }


        this.postframes = data;
        this.saveparametersbyConfiqID(this.postframes.item_Id, this.parameters);
        this.error.displayError({
            severity: 'success', summary: 'Success Message',
            detail: 'Contactlens ' + massage + ' Successfully'
        });
        if (massage == 'Updated') {
            const item = {
                "status": 'Updated',
                "data": this.postframes.item_Id
            }
            this.contactAddItem.emit(item);
            this.contactAddItem.emit(this.postframes);
        } else if (massage == 'saved') {
            const item = {
                "status": 'saved',
                "data": this.postframes.item_Id
            }
            this.contactAddItem.emit(item);

        }

    }



    /**
     * Determines whether tabs click on
     * @param {object} event 
     */
    onTabsClick(event) {
        if (event.index === 0) {
            this.buttonsHide = false;
            this.transactionsBtnHide = false;
        }
        if (event.index === 1) {
            this.getLocationsItems();
            this.buttonsHide = false;
            this.transactionsBtnHide = false;
        }
        if (event.index === 2) {
            this.buttonsHide = false;
            this.transactionsBtnHide = true;

            this.filterTransactionOnLocation();
        }
        if (event.index === 3) {
            this.buttonsHide = true;
            this.Getcontactlensconfigurations();
            this.transactionsBtnHide = false;
        }
    }


    /**
     * Loads contactlensconfigurations
     */
    loadContactlensconfigurations() {
        this.Contactlensconfigcolumns = [
            { field: 'id', header: 'ID', visible: this.visible },
            { field: 'item_id', header: 'itemID', visible: false },
            { field: 'upc', header: 'UPC', visible: this.visible },
            { field: 'basecurve', header: 'BaseCurve', visible: this.visible },
            { field: 'diameter', header: 'Diameter', visible: this.visible },
            { field: 'sphere', header: 'Sphere', visible: this.visible },
            { field: 'cylinder', header: 'Cylinder', visible: this.visible },
            { field: 'axis', header: 'Axis', visible: this.visible },
            { field: 'addpower', header: 'Add Power', visible: this.visible },
            { field: 'multifocal', header: 'Multifocal', visible: this.visible }

        ];
    }

    /**
     * Getcontactlensconfigurations add inventory component
     */
    Getcontactlensconfigurations() {

        const contactlensid = this.contactLensService.editlensid;
        if (contactlensid != null) {
            this.contactLensService.getContactconfigurationdetails(contactlensid).subscribe(
                data => {
                    this.contactlenslistconfigurationslist = data['data'];
                    this.dtconfig.selection = false;
                });
        }



    }

    /**
     * Getcontactlensreplacements add inventory component masters
     */
    Getcontactlensreplacements() {
        this.replacements = this.contactLensService.contactReplacement;
        if (this.utility.getObjectLength(this.replacements) === 0) {
        this.contactLensService.getcontactreplacements().subscribe(
            (values: ContactReplacement) => {
                try {
                    const dropData = values.data;
                    this.replacements.push({ Id: '', Name: 'Select Schedule' });
                    for (let i = 0; i <= dropData.length - 1; i++) {
                        this.replacements.push({ Id: dropData[i].id.toString(), Name: dropData[i].opt_val });
                    }
                    this.contactLensService.contactReplacement = this.replacements;
                } catch (error) {
                    this.error.syntaxErrors(error);
                }
            });
        }
    }

    /**
     * Getcontacttraillens add inventory component 
     */
    Getcontacttraillens() {
        if (this.contactLensService.traillensData.length === 0) {
            this.traillens = [];
            this.traillenslist = [];
            this.traillens.push({ Id: '0', Name: 'Select Lens' });
            this.contactLensService.getcontacttraillens().subscribe(
                data => {
                    this.traillenslist = data['result']['Optical']['ContactLenses'];
                    this.traillenslist.forEach(item => {
                        if (!this.traillens.find(a => a.Name === item.Name)) {
                            this.traillens.push({ Id: item.ItemId, Name: item.Name });
                        }
                    });
                });
            this.contactLensService.traillensData = this.traillens;
        } else {
            this.traillens = this.contactLensService.traillensData;
        }
    }

    /**
     * Gets packaging data 
     */
    getPackagingData() {
        this.packageData = this.contactLensService.contactPackage;
        if (this.utility.getObjectLength(this.packageData) === 0) {
        this.contactLensService.getcontactreplacements().subscribe(
            (values: ContactPackage) => {
                try {
                    const dropData = values.data;
                    this.packageData.push({ id: '', opt_val: 'Select Package' });
                    for (let i = 0; i <= dropData.length - 1; i++) {
                        this.packageData.push(dropData[i]);
                    }
                    this.contactLensService.contactPackage = this.packageData;
                } catch (error) {
                    this.error.syntaxErrors(error);
                }
            });
        }

    }

    /**
     * Adds contact lens transaction button click to add new
     */
    addContactLensTransaction() {
        if (this.ContactlensItemId !== '') {
            this.addContactLensInvTransaction = true;
        }
        else {
            this.error.displayError({
                severity: 'error',
                summary: 'Error Message', detail: 'Select any item or save new item for saving transactions'
            });
            this.addContactLensInvTransaction = false;

        }


    }





    /**
     * Gets contact lenstock details avalible
     * @returns Getting contact lens stock deatails
     */
    getContactLenstockDetails() {
        this.frameService.getstockDetail().subscribe(data => {
            for (let i = 0; i <= data['data'].length - 1; i++) {
                const loca = this.locationlist.filter(x => x.Id === data['data'][i]['loc_id']);
                data['data'][i]['loc_id'] = loca[0]['Name'];
                data['data'][i]['trans_type'] = data['data'][i]['transaction_type']['transactiontype'];
            }
            this.StockDetails = data['data'];
            this.transacctionStockDetails = this.StockDetails;
        });

    }

    /**
     * Sales api sales made on the items
     */
    salesApi() {
        this.transacctionStockDetails = [];
        for (let i = 0; i <= this.StockDetails.length - 1; i++) {
            if (this.StockDetails[i]['order_id'] > 0) {
                this.transacctionStockDetails.push(this.StockDetails[i]);
            }
        }

    }

    /**
     * Trasactions api sales not  made on the items
     */
    trasactionApi() {
        this.transacctionStockDetails = [];
        for (let i = 0; i <= this.StockDetails.length - 1; i++) {
            if (this.StockDetails[i]['order_id'] >= 0) {
                this.transacctionStockDetails.push(this.StockDetails[i]);
                // alert(this.transacctionStockDetails.push(this.StockDetails))
            }
        }

    }

    /**
     * Filters transaction based on location with particular items
     */
    filterTransactionOnLocation() {
        this.transacctionStockDetails = [];
        if (this.inventoryCommonService.FortrasactionItemID === '') {
            this.transacctionStockDetails = [];
        } else {
            const dataFilter = {
                filter: [
                    {
                        field: 'item_id',
                        operator: '=',
                        value: this.inventoryCommonService.FortrasactionItemID,
                    },
                ]
            };
            this.frameService.filterTrasactionData(dataFilter).subscribe(data => {
                for (let i = 0; i <= data['data'].length - 1; i++) {
                    data['data'][i]['trans_type'] = data['data'][i]['transaction_type']['transactiontype'];

                }
                this.StockDetails = data['data'];
                this.transacctionStockDetails = this.StockDetails;
            });
        }

    }
    /**
     * Filters transaction on location based items based on locations
     * Filtering transaction data based on  location
     */
    filterTransactionOnLocationBased() {
        this.transacctionStockDetails = [];
        if (this.inventoryCommonService.FortrasactionItemID !== '') {
            if (this.contactlensForm.controls['locationid'].value === '') {
                this.filterTransactionOnLocation();
            } else {
                const dataFilterBased = {
                    filter: [
                        {
                            field: 'item_id',
                            operator: '=',
                            value: this.inventoryCommonService.FortrasactionItemID,
                        },
                        {
                            field: 'loc_id',
                            operator: '=',
                            value: this.contactlensForm.controls['locationid'].value,
                        }
                    ]
                };
                this.frameService.filterTrasactionData(dataFilterBased).subscribe(data => {
                    this.FilteredTrasaction = data['data'];
                    this.StockDetails = this.FilteredTrasaction;
                    for (let i = 0; i <= this.StockDetails.length - 1; i++) {
                        this.StockDetails[i]['trans_type'] = this.StockDetails[i]['transaction_type']['transactiontype'];
                    }
                    this.transacctionStockDetails = this.StockDetails;
                });
            }
        } else {
            this.transacctionStockDetails = [];
        }
    }

    /**
     * Keys press restrict the alphabets
     * @param { object} event 
     */
    keyPress(event: any) {
        const pattern = /[0-9]/;

        const inputChar = String.fromCharCode(event.charCode);
        if (event.keyCode !== 8 && !pattern.test(inputChar)) {
            event.preventDefault();
        }
    }


    /**
     * Contacts lens transevent
     * @param {object} data 
     */
    ContactLensTransevent(data) {
        if (data === 'cancle') {
            this.addContactLensInvTransaction = false;
        } else {
            this.UpdateStock = data;
            this.addContactLensInvTransaction = false;
            this.filterTransactionOnLocation();
        }

    }

    /**
     * Cancles cont config event
     * @param {object} event 
     */
    cancleContConfigEvent(event) {
        if (event === 'cancle') {
            this.AddConfiguration = false;
        } else {
            this.AddConfiguration = false;
            this.loadContactlensconfigurations();
            this.Getcontactlensconfigurations();
            this.configurationID = '';
            this.AddConfiguration = false;
        }
    }
    /**
     * Structureformcontrols add inventory component
     * clearing the from controller data
     */
    structureformcontrols() {
        this.contactlensForm.removeControl('brand_id');
        this.contactlensForm.removeControl('material_id');
        this.contactlensForm.removeControl('profit');
        this.contactlensForm.removeControl('sixmonthsprofit');
        this.contactlensForm.removeControl('twelvemonthsprofit');
        this.contactlensForm.removeControl('replacementId');
        this.contactlensForm.removeControl('status');
        this.contactlensForm.removeControl('trial_lens_id');
        this.contactlensForm.removeControl('traillensId');
        this.contactlensForm.removeControl('units');
    }



    /**
     * Saving req pay load
     * Holding form contoller values
     */
    savingReqPayLoad() {
        if (this.contactlensForm.controls.inventory.value === true) {
            this.contactlensForm.controls.inventory.patchValue('1');
        } else if (this.contactlensForm.controls.hard_contact.value === true) {
            this.contactlensForm.controls.hard_contact.patchValue('1');
        } else {
            this.contactlensForm.controls.inventory.patchValue('0');
            this.contactlensForm.controls.hard_contact.patchValue('0');
        }
        this.savingPayLoad = {
            module_type_id: '3',
            upc_code: this.contactlensForm.controls.upc_code.value,
            manufacturer_id: this.contactlensForm.controls.manufacturer_id.value,
            name: this.contactlensForm.controls.name.value,
            cl_replacement: this.contactlensForm.controls.cl_replacement.value,
            colorid: this.contactlensForm.controls.colorid.value,
            hard_contact: this.contactlensForm.controls.hard_contact.value,
            inventory: this.contactlensForm.controls.inventory.value,
            min_qty: this.contactlensForm.controls.min_qty.value,
            max_qty: this.contactlensForm.controls.max_qty.value,
            vendor_id: this.contactlensForm.controls.vendor_id.value === "0" ? '' :
             this.contactlensForm.controls.vendor_id.value,
            cl_packaging: this.contactlensForm.controls.cl_packaging.value,
            retail_price: this.contactlensForm.controls.retail_price.value,
            wholesale_cost: this.contactlensForm.controls.wholesale_cost.value,
            six_months_price: this.contactlensForm.controls.six_months_price.value,
            six_months_qty: this.contactlensForm.controls.six_months_qty.value,
            twelve_months_price: this.contactlensForm.controls.twelve_months_price.value,
            twelve_months_qty: this.contactlensForm.controls.twelve_months_qty.value,
            procedure_code: this.contactlensForm.controls.procedure_code.value === '' ? null :
                this.contactlensForm.controls.procedure_code.value.Id.toString(),
            structure_id: this.contactlensForm.controls.structure_id.value,
            commission_type_id: this.contactlensForm.controls.commission_type_id.value,
            amount: this.contactlensForm.controls.amount.value,
            gross_percentage: this.contactlensForm.controls.gross_percentage.value,
            notes: this.contactlensForm.controls.notes.value,
        };
    }
    /**
     * Filters procedure code details
     * getting selected id procedure code details
     */
    filterProcedureCodeDetails(procedureValue) {
        this.procedurecoderesults = [];
        if (procedureValue !== null) {
            const values = {

                filter: [{
                    field: 'cpt_fee_id',
                    operator: '=',
                    value: procedureValue
                }]
            };
            this.frameService.getProcedureCode(values).subscribe((data: GetProcedureCode) => {
                this.procedurecoderesults = data.data;
                this.contactlensForm.controls.procedure_code.patchValue({
                    Id: this.procedurecoderesults[0].cpt_fee_id, Name: this.procedurecoderesults[0].cpt_prac_code
                });
            });
        }
    }
    /**
     * Selects pocedure data
     * @param event getting click event values
     */
    selectPocedureData(event: any) {
        if (event.target.value !== undefined) {
            event.target.select();
        }
    }
}
