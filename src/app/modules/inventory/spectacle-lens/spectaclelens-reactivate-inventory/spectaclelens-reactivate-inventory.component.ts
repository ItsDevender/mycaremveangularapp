import { Component, OnInit, ViewChild, OnDestroy, Output, EventEmitter } from '@angular/core';
import { InventoryService } from '@services/inventory/inventory.service';
import { SpectaclelensService } from '@services/inventory/spectaclelens.service';
import { ErrorService } from '@app/core/services/error.service';
import { DropdownService } from '@app/shared/services/dropdown.service';
import { UtilityService } from '@app/shared/services/utility.service';
import { SpectacleLensMaterial, ManufacturerDropData, LocationsMaster  } from '@app/core/models/masterDropDown.model';


@Component({
    selector: 'app-spectaclelens-reactivate-inventory',
    templateUrl: 'spectaclelens-reactivate-inventory.component.html'
})
export class SpectaclelensReactivateInventoryComponent implements OnInit, OnDestroy {
    reactivespectaclerefreshdetails: any;
    Spectaclelist: any[];
    cols: any[];
    gridlists: any[];
    spectaclesactivateMultipleSelection = [];
    activatedReqbody: any;
    /**
     * Manufacturers  of spectaclelens reactivate inventory component for manufacturers drop-down data
     */
    public Manufacturers: any[];
    /**
     * Material list drop down of spectaclelens reactivate inventory component for Material list drop-down
     */
    MaterialListDropDown: any[];
    /**
     * Search material of spectaclelens reactivate inventory component for the search material
     */
    searchMaterial: '';
    /**
     * Search manufa of spectaclelens reactivate inventory component for the search manufacturers
     */
    searchManufa: '';
    locations: Array<LocationsMaster>;
    /**
     * Locations list of spectaclelens reactivate inventory component for location drop-down list
     */
    locationsList: any;
    @ViewChild('dtspectaclereactive') public dtspectaclereactive: any;
    @Output() spectReactivate = new EventEmitter<any>();
    ngOnInit() {
        this.cols = [
            { field: 'id', header: 'ID' },
            { field: 'location', header: 'Location' },
            { field: 'manufacturer', header: 'Manufacturer' },
            { field: 'spectacle_material', header: 'Material' },
            { field: 'name', header: 'Name' },
            { field: 'upc_code', header: 'UPC' },
            { field: 'retail_price', header: 'Default Retail' },
        ];
        this.spectaclesactivateMultipleSelection = [];
        this.spectacleLensFilter();
        this.LoadManufacturers();
        this.loadMaterialList();
        this.LoadLocations();
    }
    constructor(private spectcalelensService: SpectaclelensService,
        private _InventoryService: InventoryService,
        private dropdownService: DropdownService,
        private error: ErrorService,
        public utility: UtilityService) {

    }
    /**
     * Spectacles lens filter
     * @returns for stroge the filter details
     */
    spectacleLensFilter() {
        this.Spectaclelist = [];
        const spectaclelens = {
            filter: [
                {
                    field: "module_type_id",
                    operator: "=",
                    value: "2"
                },
                {
                    field: "material_id",
                    operator: "=",
                    value: this.searchMaterial
                },
                {
                    field: "manufacturer_id",
                    operator: "=",
                    value: this.searchManufa
                },
            ]
        };

        const resultData = spectaclelens.filter.filter(p => p.value !== "" && p.value !== null && p.value !== undefined && p.value !== '%undefined%'
            && p.value !== "%  %" && p.value !== "%%");
        let filterdata = {
            filter: resultData,
            sort: [
                {
                    field: "id",
                    order: "DESC"
                }
            ]
        }
        this.spectcalelensService.getSpectaclens(filterdata).subscribe(
            data => {
                for (let i = 0; i <= data['data'].length - 1; i++) {
                    if (data['data'][i]['del_status'] == 1) {
                        this.Spectaclelist.push(data['data'][i])
                    };
                }
            });
    }

    /**
     * Cancels spectaclelens reactivate inventory component
     * @returns close the reactive side-bar
     */
    Cancel() {
        this.spectaclesactivateMultipleSelection = [];
        this.spectReactivate.emit('specClose');
    }
    /**
     * Activates spectaclelens reactivate inventory component
     * @returns  for the activate the items
     */
    Activate() {
        this.spectcalelensService.errorHandle.msgs = [];
        if (this.spectaclesactivateMultipleSelection.length === 0) {
            this.error.displayError({
                severity: 'warn',
                summary: 'Warning Message', detail: 'Please Select Item To Re-Activate'
            });
            return;
        }

        this.activatedReqbody = [];
        this.spectaclesactivateMultipleSelection.forEach(element => {
            this.activatedReqbody.push(element.id);
        });
        const activatedata = {
            'itemId': this.activatedReqbody
        };

        this._InventoryService.ActivateInventory(activatedata).subscribe(data => {
            this.Spectaclelist = [];
            this.spectaclesactivateMultipleSelection = [];
            this.error.displayError({
                severity: 'success',
                summary: 'Success Message', detail: 'Re-Activated Successfully'
            });
            this.spectReactivate.emit('');
        },
            error => {
                this.error.displayError({
                    severity: 'error',
                    summary: 'Error Message', detail: 'Unable to Re-Activate the selected values'
                });
            }
        );

    }
    /**
     * Loads manufacturers list
     * loading the manufacturers drop-down data
     */
    LoadManufacturers() {
        this.Manufacturers = this.dropdownService.Manufactures;
        if (this.utility.getObjectLength(this.Manufacturers) === 0) {
            const reqbody = {
                filter: [
                    {
                        field: 'manufacturer_name',
                        operator: 'LIKE',
                        value: '%%'
                    }
                ]
            };
            this.dropdownService.manufacturesDropDown(reqbody).subscribe(
                (values: ManufacturerDropData) => {
                    try {
                        const dropData = values.data;
                        this.Manufacturers.push({ Id: '', Name: 'Select Manufacturer' });
                        for (let i = 0; i <= dropData.length - 1; i++) {
                            this.Manufacturers.push({ Id: dropData[i].id, Name: dropData[i].manufacturer_name });
                        }
                        this.dropdownService.Manufactures = this.Manufacturers;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }
                });
        }
    }

    /**
     * Loads material list
     * loading the material drop-down data
     */
    loadMaterialList() {
        this.MaterialListDropDown = this.spectcalelensService.MaterialListDropDownData;
        if (this.utility.getObjectLength(this.MaterialListDropDown) === 0) {
            this.spectcalelensService.getSpectaclesMasterList().subscribe((values: SpectacleLensMaterial) => {
                try {
                    const dropData = values.data;
                    this.MaterialListDropDown.push({ Id: '', Name: 'Select Material' });
                    for (let i = 0; i <= dropData.length - 1; i++) {
                        this.MaterialListDropDown.push({ Id: dropData[i].id, Name: dropData[i].material_name });
                    }
                    this.spectcalelensService.MaterialListDropDownData = this.MaterialListDropDown;
                } catch (error) {
                    this.error.syntaxErrors(error);
                }
            });
        }
    }
    /**
     * Loads location list
     * loading the location drop-down data
     */
    LoadLocations() {
        this.locations = this.dropdownService.locationsData;
        if (this.utility.getObjectLength(this.locations) === 0) {
            this.dropdownService.getLocationsDropData().subscribe(
                (data: LocationsMaster[]) => {
                    try {
                        this.locations.push({ id: '', name: 'Select Location' });
                        for (let i = 0; i <= data.length - 1; i++) {
                            this.locations.push(data[i]);
                        }
                        this.dropdownService.locationsData = this.locations;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }
                }
            );
        }
    }

    ngOnDestroy(): void {
        // this.reactivespectaclerefreshdetails.unsubscribe();
    }

}

