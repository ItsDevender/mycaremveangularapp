import { Component, OnInit, EventEmitter, Output, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { SpectaclelensService } from '@services/inventory/spectaclelens.service';
import { DropdownService } from '@shared/services/dropdown.service';
import { Manufacturer } from '@models/inventory/Manufacturer.model';
import { UtilityService } from '@shared/services/utility.service';
import { ErrorService } from '@services/error.service';
import { LenstreatmentService } from '@services/inventory/lenstreatment.service';
import { InventoryService } from '@app/core/services/inventory/inventory.service';
import {
    CommissionStructure,
    CommissionType,
    SpectacleLensMaterial,
    LensStyleMaster,
    ManufacturerDropData
} from '@app/core/models/masterDropDown.model';


@Component({
    selector: 'app-spectaclelens-details',
    templateUrl: 'spectaclelens-details.component.html'
})
export class SpectaclelensDetailsComponent implements OnInit {

    /**
     * Details form of spectaclelens details component  for formgroup
     */
    detailsForm: FormGroup;

    /**
     * Material list drop down of spectaclelens details component for material dropdown data 
     */
    MaterialListDropDown: any = [];
    /**
     * Manufacturers  of spectaclelens details component for Manufacturers dropdown data 
     */
    Manufacturers: any = [];
    /**
     * Manufacturerlist  of spectaclelens details component for Manufacturers dropdown data 
     */
    public manufacturerlist: Manufacturer[];
    /**
     * Styles  of spectaclelens details component for styles dropdown data 
     */
    styles = [];
    /**
     * Lensstylecopy  of spectaclelens details component for lensstyle dropdown data 
     */
    lensstylecopy: any = [];
    /**
     * Structure drop data of spectaclelens details component for structure dropdown data 
     */
    StructureDropData: any = [];
    /**
     * Type drop data of spectaclelens details component for type dropdown data 
     */
    TypeDropData: any = [];
    /**
     * Specdetails list of spectaclelens details component for storing spectacle lens selected item id's 
     */
    specdetailsList: any = [];
    /**
     * Output  of spectaclelens details component for sending events to spectacle lens main page
     */
    @Output() detailsOutput = new EventEmitter<any>();
    /**
     * Input  of spectaclelens details component for getting data from spectacle lens main page
     */
    @Input() batchUpdateList: any;
    /**
     * Measurmentsslist  of spectaclelens details component loading measurement grid data
     */
    measurmentsslist: any = [];
    /**
     * Measurmentsslistsavedata  of spectaclelens details component storing the selected measurement data
     */
    measurmentsslistsavedata: any = [];
    /**
     * Treatmentslist  of spectaclelens details component loading treatment grid data
     */
    treatmentslist: any = [];
    /**
     * Treatmentssavevalue  of spectaclelens details component storing the selected treatment data
     */
    treatmentssavevalue: any[];
    /**
     * Indexvalue  of spectaclelens details component getting index from procedure code
     */
    indexvalue: number;
    /**
     * Add procedure code sidebar of spectaclelens details component for opentnig procedure code side-nav
     */
    addProcedureCodeSidebar = false;
    /**
     * Cpt from data of spectaclelens details component for storing tha event result from procedure code side-nav
     */
    cptFromData: any;
    obj: string;
    /**
     * on init
     * for initialize the dropdown and grid data 
     */
    ngOnInit() {
        this.loadMaterialList();
        this.LoadManufacturers();
        this.loadFormdata();
        this.LoadLensStyle();
        this.loadStructures();
        this.getmeasurmenstmasster();
        this.gettreatments();
    }

    /**
     * Creates an instance of spectaclelens details component.
     * @param fb for formbuilder
     * @param spectcalelensService for spectaclelens methods 
     * @param dropdownService for common dropdown methods
     * @param utility for calling getObjectLength method
     * @param error  for syntaxerrors
     * @param lensTreatmentService  for lenstreatent methods
     */
    constructor(private fb: FormBuilder, private spectcalelensService: SpectaclelensService, private dropdownService: DropdownService,
        private utility: UtilityService, private error: ErrorService, private lensTreatmentService: LenstreatmentService,
        private InventoryCommonService: InventoryService) {

    }

    /**
     * Loads formdata
     * load React FormBuilder
     */
    loadFormdata() {
        this.detailsForm = this.fb.group({
            itemIds: [],
            module_type_id: 2,
            manufacturer_id: '',
            type_id: '',
            material_id: '',
            retail_price: '',
            wholesale_cost: '',
            color_code: '',
            lab_id: '',
            returnable_flag: false,
            max_add_size: '',
            addd: '',
            diameter: '',
            oversize_diameter_charge: '',
            sphere: '',
            cylinder: '',
            bc: '',
            min_ht: '',
            structure_id: '',
            commission_type_id: '',
            amount: '',
            gross_percentage: '',
            spiff: '',
            profit:'',
            eye_type:'',
            procedureCodes: this.fb.array([
                this.initlanguage(),
            ])
        });
    }


    
    /**
     * Loads material list
     * @param {array} MaterialListDropDown for dropdown binding
     * @returns {object} for loading Material List drop-down
     */
    loadMaterialList() {
        this.MaterialListDropDown = this.spectcalelensService.MaterialListDropDownData;
        if (this.utility.getObjectLength(this.MaterialListDropDown) === 0) {
            this.spectcalelensService.getSpectaclesMasterList().subscribe((values: SpectacleLensMaterial) => {
                try {
                    const dropData = values.data;
                    this.MaterialListDropDown.push({ Id: '', Name: 'Select Material' });
                    for (let i = 0; i <= dropData.length - 1; i++) {
                        this.MaterialListDropDown.push({ Id: dropData[i].id, Name: dropData[i].material_name });
                    }
                    this.spectcalelensService.MaterialListDropDownData = this.MaterialListDropDown;
                } catch (error) {
                    this.error.syntaxErrors(error);
                }
            });
        }
    }

    
    /**
     * Checkunchekevents spectaclelens details component
     * @param {string} data for selected grid item
     * @param {string} add for checkbox check event 
     * @param {string} index for checkbox index event
     * @param {string} detailsofvalue for getting avalable , required values
     */
    checkunchekevent(data, add, index, detailsofvalue) {
        if (this.measurmentsslist !== undefined) {
            const a = this.measurmentsslist.findIndex(x => x.id === data.id);
            if (a !== -1) {
                if (detailsofvalue === 'available') {
                    this.measurmentsslist[a].id = data.id;
                    this.measurmentsslist[a].checkedvalues = true;
                    this.measurmentsslist[a].Availablebool = add;
                }

                if (detailsofvalue === 'required') {
                    this.measurmentsslist[a].id = data.id;
                    this.measurmentsslist[a].checkedvalues = true;
                    this.measurmentsslist[a].requiredbool = add;
                }
                this.measurmentsslist = this.measurmentsslist;
            }
        }
    }
    
    /**
     * Checkunchecktreatmentsevents spectaclelens details component
     * @param {string} data for selected grid item
     * @param {string} add for checkbox check event 
     * @param {string} index for checkbox index event
     * @param {string} detailsofvalue for getting avalable , required values
     */
    checkunchecktreatmentsevent(data, add, index, detailsofvalue) {
        if (this.treatmentslist !== undefined) {
            const a = this.treatmentslist.findIndex(x => x.itemId === data.itemId);
            if (a !== -1) {
                if (detailsofvalue === 'available') {
                    this.treatmentslist[a].itemId = data.itemId;
                    this.treatmentslist[a].checkedvalues = true;
                    this.treatmentslist[a].Availablebool = add;
                }
                if (detailsofvalue === 'include') {
                    this.treatmentslist[a].itemId = data.itemId;
                    this.treatmentslist[a].checkedvalues = true;
                    this.treatmentslist[a].requiredbool = add;
                }
                this.treatmentslist = this.treatmentslist;
            }
        }
    }

   
    /**
     * Loads manufacturers
     * @param {array} Manufacturers for dropdown binding
     * @returns {object} for loading Manufacturers List drop-down
     */
    LoadManufacturers() {
        this.Manufacturers = this.dropdownService.Manufactures;
        if (this.utility.getObjectLength(this.Manufacturers) === 0) {
            const reqbody = {
                filter: [
                    {
                        field: 'manufacturer_name',
                        operator: 'LIKE',
                        value: '%%'
                    }
                ]
            };
            this.dropdownService.manufacturesDropDown(reqbody).subscribe(
                (values: ManufacturerDropData) => {
                    try {
                        const dropData = values.data;
                        this.Manufacturers.push({ Id: '', Name: 'Select Manufacturer' });
                        for (let i = 0; i <= dropData.length - 1; i++) {
                            this.Manufacturers.push({ Id: dropData[i].id, Name: dropData[i].manufacturer_name });
                        }
                        this.dropdownService.Manufactures = this.Manufacturers;
                    } catch (error) {
                        this.error.syntaxErrors(error);
                    }
                });
        }
    }

   
    /**
     * Loads lens style
     *  @param {array} styles for dropdown binding
     * @returns {object} for loading styles List drop-down
     */
    LoadLensStyle() {
        this.styles = this.dropdownService.styles;
        if (this.utility.getObjectLength(this.styles) === 0) {
            this.dropdownService.getSpectacleLensStyle().subscribe((values: LensStyleMaster) => {
                try {
                    const dropData = values.data;
                    this.styles.push({ name: 'Select Style', Id: '' });
                    for (let i = 0; i <= dropData.length - 1; i++) {
                        this.styles.push({ name: dropData[i].type_name, Id: dropData[i].id });
                    }
                    // this.lensstylecopy = this.styles;
                    this.dropdownService.styles = this.styles;
                } catch (error) {
                    this.error.syntaxErrors(error);
                }
            });
        }
    }

    /**
     * Loads structures
     * @param {array} StructureDropData for dropdown binding
     * @returns {object} for loading Structure List drop-down
     * @param {array} TypeDropData for dropdown binding
     * @returns {object} for loading CommissionType List drop-down
     */
    loadStructures() {
        this.StructureDropData = this.InventoryCommonService.structureData;
        if (this.utility.getObjectLength(this.StructureDropData) === 0) {

            this.InventoryCommonService.getStructuredropData().subscribe((values: CommissionStructure) => {
                try {
                    this.StructureDropData.push({ Id: '', Name: 'Select Structure' });
                    const dropData  = values.data;
                    for (let i = 0; i <= dropData.length - 1; i++) {
                        this.StructureDropData.push({ Id: dropData[i].id.toString(), Name: dropData[i].structure_name });
                    }
                    this.InventoryCommonService.structureData = this.StructureDropData;
                } catch (error) {
                    this.error.syntaxErrors(error);
                }
            });
        }

        this.TypeDropData = this.InventoryCommonService.commissionType;
        if (this.utility.getObjectLength(this.TypeDropData) === 0) {
            this.InventoryCommonService.getCommissionTypedropData().subscribe((values: CommissionType) => {
                try {
                    this.TypeDropData.push({ Id: '', Name: 'Select Type' });
                    const dropData  = values.data;
                    for (let i = 0; i <= dropData.length - 1; i++) {
                        this.TypeDropData.push({ Id: dropData[i].id.toString(), Name: dropData[i].commissiontype });
                    }
                    this.InventoryCommonService.commissionType = this.TypeDropData;
                } catch (error) {
                    this.error.syntaxErrors(error);
                }
            });
        }

    }

   
    /**
     * Getmeasurmenstmassters spectaclelens details component
     * @param {array} measurmentsslist for grid binding
     * @returns {object} for loading measurement List
     */
    getmeasurmenstmasster() {
        this.spectcalelensService.getmeasurmentsmaster().subscribe(
            data => {
                this.measurmentsslist = data['data'];
                for (let i = 0; i <= this.measurmentsslist.length - 1; i++) {
                    this.measurmentsslist[i].checkedvalues = false;
                    this.measurmentsslist[i].Availablebool = false;
                    this.measurmentsslist[i].requiredbool = false;
                }
            });
    }

    
    /**
     * Gettreatments spectaclelens details component
     *  @param {array} treatmentslist for grid binding
     * @returns {object} for loading treatment List
     */
    gettreatments() {

        this.lensTreatmentService.getLensTreatmentList().subscribe(
            data => {
                this.treatmentslist = data['result']['Optical']['LensTreatment'].length;
                for (let i = 0; i <= this.treatmentslist.length - 1; i++) {
                    this.treatmentslist[i]['itemId'] = parseInt(data['result']['Optical']['LensTreatment'][i]['itemId'], 10);
                    this.treatmentslist[i].includebool = false;
                    this.treatmentslist[i].Availablebool = false;
                }
                this.treatmentslist = data['result']['Optical']['LensTreatment'];
            });
    }

   
    /**
     * Determines whether save details on
     *  @returns {object} batchDetailsSave for saving bulk spectaclelens details
     *  @returns {object} saveBulkMeasurement for saving bulk measurements
     *  @returns {object} saveBulkLensTreatment for saving bulk treatment
     *  @returns {object} saveBulkSpectacleLensProcedure for saving bulk procedures
     */
    onSaveDetails() {
        if (this.batchUpdateList.length !== 0) {
            this.specdetailsList = [];
            for (let i = 0; i <= this.batchUpdateList.length - 1; i++) {
                this.specdetailsList.push(this.batchUpdateList[i].id);
            }
            this.detailsForm.controls.itemIds.patchValue(this.specdetailsList);
            this.detailsForm.controls.type_id.patchValue("" + this.detailsForm.controls.type_id.value +"");
            let spectacleDetailsData = this.utility.format(this.detailsForm);
            
            delete spectacleDetailsData.procedureCodes;
            delete spectacleDetailsData.profit;

           
            this.spectcalelensService.batchDetailsSave(spectacleDetailsData).subscribe(data => {
                // if (data['status'] === 'Items Detail updated successfully') {
                this.detailsOutput.emit('save');
                this.error.displayError({
                    severity: 'success', summary: 'Success Message',
                    detail: 'Items Detail updated successfully'
                });
                // }
            });
        }

        this.saveMeasurmentsList();
        this.saveTreatmentsList()
        this.saveProcedureCode();

    }

    /**
     * Gets measurments list
     * @param {array} measurmentsslistsavedata for grid binding
     * @returns {object} for loading Measurments List
     */
    saveMeasurmentsList() {
        if (this.measurmentsslist.length !== 0) {
            this.measurmentsslistsavedata = [];
            const details = this.measurmentsslist.filter(x => x.checkedvalues === true);
            if (details.length !== 0) {
                if (details !== null && details !== undefined) {
                    details.forEach(element => {
                        const availablebool = element.Availablebool;
                        const requiredbool = element.requiredbool;
                        let available = availablebool === true ? 1 : 0;
                        let required = requiredbool === true ? 1 : 0;
                        if (element.requiredbool === true) {
                            available = 1;
                            required = 1;
                        }
                        const values = {
                            'measure_id': element.id,
                            'available': available,
                            'required': required
                        };
                        this.measurmentsslistsavedata.push(values);
                    });
                    let reqBody = {
                        "itemIds": this.specdetailsList,
                        "module_type_id": 2,
                        "measurements": this.measurmentsslistsavedata
                    }
                    this.spectcalelensService.saveBulkMeasurement(reqBody).subscribe(data => {
                        // if (data['status'] === 'Spectacle Lens Measurement Detail updated successfully') {
                        this.detailsOutput.emit('save');
                        this.error.displayError({
                            severity: 'success', summary: 'Success Message',
                            detail: data['status']
                        });
                        // }
                    });
                }
            }
        }
    }
    /**
     * Gets treatments list
     * @param {array} treatmentssavevalue for grid binding
     * @returns {object} for loading treatmentslist List
     */
    saveTreatmentsList() {
        if (this.treatmentslist.length !== 0) {
            this.treatmentssavevalue = [];
            const details = this.treatmentslist.filter(x => x.checkedvalues === true);
            if (details.length !== 0) {
                if (details != null && details !== undefined) {
                    details.forEach(element => {
                        const availablebool = element.Availablebool;
                        const requiredbool = element.requiredbool;
                        let available = availablebool === true ? 1 : 0;
                        let required = requiredbool === true ? 1 : 0;
                        if (element.requiredbool === true) {
                            available = 1;
                            required = 1;
                        }
                        // include is required
                        // includeinorder available
                        const values = {
                            'treatment_id': element.itemId,
                            'include': required,
                            'includeinorder': available
                        };

                        this.treatmentssavevalue.push(values);
                    });
                    const reqBody = {
                        "itemIds": this.specdetailsList,
                        "module_type_id": 2,
                        "treatments": this.treatmentssavevalue
                    };
                    this.spectcalelensService.saveBulkLensTreatment(reqBody).subscribe(data => {
                        // if (data['status'] === 'Spectacle Lens Treatment Detail updated successfully') {
                        this.detailsOutput.emit('save');
                        this.error.displayError({
                            severity: 'success', summary: 'Success Message',
                            detail: data['status']
                        });
                        // }
                    });
                }
            }
        }
    }

    /**
     * Gets procedure code
     * @param {array} proceduredata for grid binding
     * @returns {object} for loading procude code List
     */
    saveProcedureCode() {
        if (this.detailsForm.controls['procedureCodes']['controls'].length !== 0) {
            if (this.detailsForm.controls['procedureCodes']['controls'][0].value.id !== null) {
                const proceduredata = [];
                for (let j = 0; j < this.detailsForm.controls['procedureCodes']['controls'].length; j++) {
                    if (this.detailsForm.controls['procedureCodes']['controls'][j].controls.procedure_id.value != null) {
                        const data = {
                            // "id":this.detailsForm.controls['procedureCodes']['controls'][j].controls.id.value,
                            'procedure_id': this.detailsForm.controls['procedureCodes']['controls'][j].controls.procedure_id.value,
                            'procedure_code': this.detailsForm.controls['procedureCodes']['controls'][j].controls.procedure_code.value,
                            'retailprice': this.detailsForm.controls['procedureCodes']['controls'][j].controls.retailprice.value
                        };
                        proceduredata.push(data);
                    }
                }
                const procedurReqBody = {
                    "itemIds": this.specdetailsList,
                    "module_type_id": 2,
                    "procedures": proceduredata
                }
                this.spectcalelensService.saveBulkSpectacleLensProcedure(procedurReqBody).subscribe(data => {
                    this.error.displayError({
                        severity: 'success', summary: 'Success Message',
                        detail: data['status']
                    });
                })
            }
        }
    }


    /**
     * Initlanguages spectaclelens details component
     * @returns  {object} for initialize procedure code form group 
     */
    initlanguage() {
        return this.fb.group({
            id: [],
            procedure_id: [],
            retailprice: [],
            procedure_code: []
        });
    }

    /**
     * Adds spectaclelens details component
     * adding fields to table grid
     */
    add() {
        const control = <FormArray>this.detailsForm.controls['procedureCodes'];
        control.push(this.initlanguage());
        // this.detailsForm.controls['words2'].value.push({value: 'gsre'});
    }

    /**
     * Removes language
     * @param {number} i for getting index
     * for removing procedure code from table grid 
     */
    removeLanguage(i: number) {
        const id = this.detailsForm.controls['procedureCodes']['controls'][i].controls.id.value;
        const control = <FormArray>this.detailsForm.controls['procedureCodes'];
        control.removeAt(i);

    }


    /**
     * Procedures code click
     * @param {string} index  for getting index
     * for displaying the procedure code side-nav
     */
    procedureCodeClick(index) {
        this.indexvalue = index;
        const control = <FormArray>this.detailsForm.controls['procedureCodes']['controls'][0];
        // control.push(this.initlanguage());
        this.addProcedureCodeSidebar = true;
    }
    /**
     * Procedurs result
     * @param {string} event for getting values from Output event 
     */
    procedurResult(event) {
        if (event !== 'cancle') {
            this.cptFromData = event;
            if (this.cptFromData !== '') {
                const id = this.detailsForm.controls['procedureCodes']['controls'][this.indexvalue]['controls'].id.value;
                if (id === '' || id == null) {
                    this.detailsForm.controls
                    ['procedureCodes']['controls'][this.indexvalue]['controls'].id.patchValue('');
                    this.detailsForm.controls
                    ['procedureCodes']['controls'][this.indexvalue]['controls'].procedure_id.patchValue(this.cptFromData.cpt_fee_id);
                    this.detailsForm.controls
                    ['procedureCodes']['controls'][this.indexvalue]['controls'].procedure_code.patchValue(this.cptFromData.cpt_prac_code);
                    this.detailsForm.controls
                    ['procedureCodes']['controls'][this.indexvalue]['controls'].retailprice.patchValue('0.00');
                }
                this.addProcedureCodeSidebar = false;
            } else {
                this.addProcedureCodeSidebar = false;
            }
        } else {
            this.addProcedureCodeSidebar = false;
        }
    }

    /**
     * Profitcalculates spectaclelens details component
     * @returns {object} for profit value
     */
    profitcalculate() {
        const value = this.detailsForm.controls['retail_price'].value === 0 ? '' : this.detailsForm.controls['retail_price'].value;
        this.detailsForm.controls['retail_price'].patchValue(value);
        const cost = this.detailsForm.controls['wholesale_cost'].value === 0 ? '' : this.detailsForm.controls['wholesale_cost'].value;
        this.detailsForm.controls['wholesale_cost'].patchValue(cost);
        if (this.detailsForm.controls['wholesale_cost'].value !== '' ||
            this.detailsForm.controls['wholesale_cost'].value != null || this.detailsForm.controls['retail_price'].value !== '' ||
            this.detailsForm.controls['retail_price'].value != null) {
            const diffvalue = this.detailsForm.controls['retail_price'].value - this.detailsForm.controls['wholesale_cost'].value;
            const calculatedvalue = diffvalue === 0 ? '' : diffvalue;
            this.detailsForm.controls['profit'].patchValue(diffvalue.toFixed(2));
        }
        if (this.detailsForm.controls['wholesale_cost'].value === '' && this.detailsForm.controls['retail_price'].value === '') {
            this.detailsForm.controls['profit'].patchValue('');
        }
    }


    /**
     * Numbers only 
     * @returns true if only 
     */
    numberOnly(event): boolean {
        const charCode = (event.which) ? event.which : event.keyCode;
        if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
    }
    /**
     * Determines whether close click on
     * close the popup
     */
    onCloseClick() {
        this.detailsOutput.emit('close');
    }

}

