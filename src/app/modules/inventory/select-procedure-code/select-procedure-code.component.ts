import { Component, OnInit,  Output, EventEmitter } from '@angular/core';

// import { LensTreatmentService } from '../../ConnectorEngine/services/lenstreatments.service';
import { LenstreatmentService } from '@services/inventory/lenstreatment.service';


@Component({
    selector: 'app-select-procedure-code',
    templateUrl: 'select-procedure-code.component.html'
})
export class SelectProcedureCodeComponent implements OnInit {

    procedureCodeGrid: any[];
    selectedProcedure: any;
    @Output() procedureData = new EventEmitter<any>();
    constructor(private lensTreatmentService: LenstreatmentService) {

    }
    ngOnInit() {
        this.procedureCodeGrid = [];
        this.lensTreatmentService.getLensTreatmentProcedures().subscribe((data: any[]) => {
            this.procedureCodeGrid = data['data'];
            console.log(this.procedureCodeGrid);
        });
    }
    onRowSelect(event) {
        this.selectedProcedure = event.data;
    }
    onRowUnselect(event) {
        this.selectedProcedure = '';
    }
    DoubleClickCpt(event) {
        this.procedureData.emit(event);
    }
    onOkClick() {
            this.procedureData.emit(this.selectedProcedure);
    }
    onCancleClick() {
        this.procedureData.emit('cancle');
    }

}

