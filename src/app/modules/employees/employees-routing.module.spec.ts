import { EmployeesRoutingModule } from './employees-routing.module';

describe('EmployeesRoutingModule', () => {
  let employeesRoutingModule: EmployeesRoutingModule;

  beforeEach(() => {
    employeesRoutingModule = new EmployeesRoutingModule();
  });

  it('should create an instance', () => {
    expect(employeesRoutingModule).toBeTruthy();
  });
});
