// Modules
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SettingsRoutingModule } from './settings-routing.module';
import { SharedModule } from '@app/shared/shared.module';

// Components
import {
  CalendarSetupComponent,
  CommissionStructuresComponent,
  DocumentsComponent,
  FeeSchedulesComponent,
  InsuranceCompanyComponent,
  InsuranceGroupsComponent,
  InsuranceSetupComponent,
  MarketingSetupComponent,
  AddMarketingDetailsComponent,
  AddSupplierLabsComponent,
  SupplierAdvancedFilterComponent,
  SupplierComponent,
  CompanyComponent,
  SystemComponent,
  CommissionsComponent,
  TaxComponent,
  ErxAdminComponent
} from './';

import { SupplierService } from '@services/inventory/supplier.service';
import { CommissionStructureService } from '@app/core/services/settings/commission-structure.service';
import { AddCommissionStructureComponent } from './commission-structures/add-commission-structure/add-commission-structure.component';


@NgModule({
  imports: [
    CommonModule,
    SettingsRoutingModule,
    SharedModule
  ],
  declarations: [
    AddSupplierLabsComponent,
    SupplierComponent,
    CalendarSetupComponent,
    DocumentsComponent,
    CommissionStructuresComponent,
    InsuranceSetupComponent,
    InsuranceCompanyComponent,
    InsuranceGroupsComponent,
    FeeSchedulesComponent,
    SupplierAdvancedFilterComponent,
    MarketingSetupComponent,
    AddMarketingDetailsComponent,
    CompanyComponent,
    SystemComponent,
    CommissionsComponent,
    TaxComponent,
    ErxAdminComponent,
    AddCommissionStructureComponent
  ],
  providers: [SupplierService, CommissionStructureService]
})
export class SettingsModule { }
