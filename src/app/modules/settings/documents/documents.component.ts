// Core Modules
import { Component, OnInit } from '@angular/core';
import { IframesService } from '@app/shared/services/iframes.service';



@Component({
  selector: 'app-documents',
  templateUrl: './documents.component.html'
})
export class DocumentsComponent implements OnInit {

  CommonUrl: Object;


  constructor(private service: IframesService) {
      this.CommonUrl = this.service.routeUrl('DocumentsUrl', true, '?showpage=prescriptions');
  }

  ngOnInit() {
  }

  onLoad() {
    this.service.resizeIframe(true);
    $('.loader-iframe').fadeOut();
  }

  changeFrameUrl(frameKey, completeFrame: boolean = false) {
     if (completeFrame) {
        this.CommonUrl = this.service.routeUrl(frameKey, true);
     } else {
       this.CommonUrl = this.service.routeUrl('DocumentsUrl', true, '?showpage=' + frameKey);
     }
  }

}
