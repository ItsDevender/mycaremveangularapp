import {
    CommissionStructure,
    GettingCommissionStructure,
    Data,
    SaveCommissionStructure,
    Emitter
} from '@app/core/models/settings/commission-structure.model';
import { CommissionStructureService } from '@app/core/services/settings/commission-structure.service';
import { Component, OnInit } from '@angular/core';
import { ErrorService } from '@services/error.service';
import { UtilityService } from '@app/shared/services/utility.service';




@Component({
    selector: 'app-commission-stuctures',
    templateUrl: './commission-structures.component.html'
})
export class CommissionStructuresComponent implements OnInit {

    /**
     * Advanced filter sidebar of commission structures component
     * display popup messages
     */
    advancedFilterSidebar: boolean;
    /**
     * Commisssioncolumns  of commission structures component
     * display header feilds
     */
    commisssionColumns: object;
    /**
     * Commission list of commission structures component
     * holds commision structure details
     */
    commissionList: Array<Data>;
    /**
     * Multiple selection of commission structures component
     * stores selected items id's
     */
    multipleSelection: Array<Data>;
    /**
     * Save update values of commission structures component
     * it stores action status value
     */
    saveUpdateValues: object;
    /**
     * Pay load of commission structures component
     * stores formControllers details
     */
    payLoad: Emitter;
    /**
     * Create an instance of commission structures component.
     * @param commissionStrutureService provide commission struture service data
     * @param utility provide utility service data
     * @param error provide error service data
     */
    constructor(
        private commissionStrutureService: CommissionStructureService,
        public utility: UtilityService,
        private error: ErrorService) {
            this.commissionStrutureService.editCommistionId = null;

    }
    ngOnInit() {
        this.advancedFilterSidebar = false;
        this.loadCommissionHeaders();
        this.gettingData();
        this.advancedFilterSidebar = false;
    }

    /**
     * Load commissionheaders
     * @returns header details
     */
    loadCommissionHeaders() {
        this.commisssionColumns = [
            { field: 'structure_name', header: 'Structure Name' },
            { field: 'commissiontype_id', header: 'Commission Type' },
            { field: 'gross_percentage', header: 'Gross Persentage' },
            { field: 'margin_percentage_before', header: 'Margin Percentage Before' },
            { field: 'margin_percentage_after', header: 'Margin Percentage After' },
            { field: 'margin_percentage_amount', header: 'Margin Percentage Amount' },
            { field: 'amount', header: 'Amount' },
            { field: 'spiff', header: 'Spiff' }
        ];
    }

    /**
     * Getting data
     * Getting CommissionStructure details from api
     */
    gettingData() {
        this.commissionList = [];
        this.commissionStrutureService.gettingCommissionStructures().subscribe(
            (response: GettingCommissionStructure) => {
                this.commissionList = response.data;
            });
    }
    /**
     * Determine whether row select on
     * Holding selected item id
     */
    onRowSelect(event) {
        this.commissionStrutureService.editCommistionId = event.data.id;
        this.saveUpdateValues = { status: 'Update', value: event.data };
    }
    /**
     * Determine whether row unselect on
     * Cleared  unselected item id
     */
    onRowUnSelect(event) {
        this.commissionStrutureService.editCommistionId = null;
    }
    /**
     * Double click
     * Getting selected item id from api
     */
    doubleClick(rowData) {
        this.commissionStrutureService.editCommistionId = rowData.id;
        this.saveUpdateValues = { status: 'Update', value:  rowData };
        this.advancedFilterSidebar = true;
        this.multipleSelection = [];
        this.multipleSelection.push(rowData);
    }
    /**
     * Add modify
     * Display advanced filter sidebar
     */
    addModify() {
        this.multipleSelection = [];
        this.saveUpdateValues = { status: 'New', value: null };
        this.advancedFilterSidebar = true;
    }
    /**
     * Modify click
     * Store selected item details
     */
    modifyClick() {
        if (this.multipleSelection.length === 1) {
            this.advancedFilterSidebar = true;
        } else {
            this.error.displayError({
                severity: 'error',
                summary: 'error Message', detail: 'please select only one item'
            });

        }
    }
    /**
     * Delete commission details
     * Delete the selected item details
     */
    deleteCommissionDetails() {
        if (this.multipleSelection.length > 0) {
            this.commissionStrutureService.multiSelectionId = this.multipleSelection[0].id;
            const idList = [];
            for (let i = 0; i <= this.multipleSelection.length - 1; i++) {
                idList.push(this.multipleSelection[i].id);
            }
            const reqBody: CommissionStructure = {
                deleteIds: idList
            };
            this.commissionStrutureService.deleteCommissionData(reqBody).subscribe(
                data => {
                    this.error.displayError({
                        severity: 'success',
                        summary: 'success Message', detail: ' deleted Successfully'
                    });
                    this.multipleSelection = [];
                    this.gettingData();
                });
        }
    }
    /**
     * Save update emit event
     * Displaying commission structure details
     */
    saveUpdateEmitEvent(event: Emitter) {
        this.payLoad = event;
        switch (event.cancel) {
            case 'cancel':
            this.advancedFilterSidebar = false;
            break;
            case null:
                    if ( this.commissionStrutureService.editCommistionId === null) {
                        this.saveDetails();
                        this.advancedFilterSidebar = false;
                    } else if ( this.commissionStrutureService.editCommistionId !== null) {
                        this.updateDetails();
                    }
                    break;
        }

    }
    /**
     * Save details
     * save commission structure details
     */
    saveDetails() {
        const saveData = this.utility.formatWithOutControls(this.payLoad.value);
        this.commissionStrutureService.saveCommissionStructures(saveData).subscribe(
            (data: SaveCommissionStructure) => {
                try {
                    this.error.displayError({
                        severity: 'success',
                        summary: 'success Message', detail: ' Saved Successfully'
                    });
                    this.advancedFilterSidebar = false;
                    this.gettingData();
                } catch (error) {
                    this.error.syntaxErrors(error);
                }
            },
        );
    }
    /**
     * Update details
     * updating the commission struture details
     */
    updateDetails() {
        const saveData = this.utility.formatWithOutControls(this.payLoad.value);
        this.commissionStrutureService.updateCommissionDetails( this.commissionStrutureService.editCommistionId, saveData).
        subscribe(data => {
            try {
                this.error.displayError({
                    severity: 'success',
                    summary: 'success Message', detail: ' Updated Successfully'
                });
                this.commissionStrutureService.gettingCommissionStructures().subscribe(
                    (value: GettingCommissionStructure) => {
                        const req = value.data;
                        for (let i = 0; i <= this.commissionList.length - 1; i++) {
                            if ( this.commissionStrutureService.editCommistionId === req[i].id) {
                                this.commissionList[i].structure_name = req[i].structure_name;
                                this.commissionList[i].commissiontype_id = req[i].commissiontype_id;
                                this.commissionList[i].gross_percentage = req[i].gross_percentage;
                                this.commissionList[i].gross_percentage = req[i].gross_percentage;
                                this.commissionList[i].margin_percentage_after = req[i].margin_percentage_after;
                                this.commissionList[i].margin_percentage_amount = req[i].margin_percentage_amount;
                                this.commissionList[i].amount = req[i].amount;
                                this.commissionList[i].spiff = req[i].spiff;
                                this.commissionStrutureService.editCommistionId = null;
                            }
                        }
                    });
                this.advancedFilterSidebar = false;
            } catch (error) {
                this.error.syntaxErrors(error);
            }
        },
        );
    }
}
