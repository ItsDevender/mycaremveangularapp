import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppService } from '@app/core';


@Component({
    selector: 'app-system-component',
    templateUrl: 'system.component.html'
})
export class SystemComponent implements OnInit {
    othersetting: any[];

    ngOnInit() {
        {
            this.othersetting =
                [
                    {
                        'label': 'Calendar',
                        'data': 'Calendar Folder',
                        'expandedIcon': 'fa fa-folder-open',
                        'collapsedIcon': 'fa fa-folder',
                        'children': [{
                            'label': 'Calendar',
                            'data': 'Calendar Folder',
                            'expandedIcon': 'fa fa-folder-open',
                            'collapsedIcon': 'fa fa-folder',
                            'children': [{ 'label': 'Expenses.doc', 'icon': 'fa fa-file-word-o', 'data': 'Expenses Document' },
                             { 'label': 'Resume.doc', 'icon': 'fa fa-file-word-o', 'data': 'Resume Document' }]
                        },
                        {
                            'label': 'Patient',
                            'data': 'Patient Folder',
                            'expandedIcon': 'fa fa-folder-open',
                            'collapsedIcon': 'fa fa-folder',
                            'children': [{ 'label': 'Invoices.txt', 'icon': 'fa fa-file-word-o', 'data': 'Invoices for this month' }]
                        }]
                    },
                    {
                        'label': 'Communications',
                        'data': 'Pictures Folder',
                        'expandedIcon': 'fa fa-folder-open',
                        'collapsedIcon': 'fa fa-folder',
                        'children': [
                            { 'label': 'barcelona.jpg', 'icon': 'fa fa-file-image-o', 'data': 'Barcelona Photo' },
                            { 'label': 'logo.jpg', 'icon': 'fa fa-file-image-o', 'data': 'PrimeFaces Logo' },
                            { 'label': 'primeui.png', 'icon': 'fa fa-file-image-o', 'data': 'PrimeUI Logo' }]
                    },
                    {
                        'label': 'Daily Closing',
                        'data': 'Daily Closing Folder',
                        'expandedIcon': 'fa fa-folder-open',
                        'collapsedIcon': 'fa fa-folder',
                        'children': [{
                            'label': 'Al Pacino',
                            'data': 'Pacino Movies',
                            'children': [{ 'label': 'Scarface', 'icon': 'fa fa-file-video-o', 'data': 'Scarface Movie' },
                             { 'label': 'Serpico', 'icon': 'fa fa-file-video-o', 'data': 'Serpico Movie' }]
                        },
                        {
                            'label': 'Robert De Niro',
                            'data': 'De Niro Movies',
                            'children': [{ 'label': 'Goodfellas', 'icon': 'fa fa-file-video-o', 'data': 'Goodfellas Movie' },
                             { 'label': 'Untouchables', 'icon': 'fa fa-file-video-o', 'data': 'Untouchables Movie' }]
                        }]
                    }
                ];
        }
    }
    constructor(
        private _router: Router,
        public app: AppService) {

    }
}

