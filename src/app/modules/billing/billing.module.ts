import { BillingRoutingModule } from './billing-routing.module';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import {
  CPTComponent,
  FeeTableComponent,
  DXCodesComponent,
  ICD10Component,
  WriteOffCodesComponent,
  ModifiersComponent,
  ManagePOSComponent,
  PoliciesComponent,
  ElectronicComponent
} from './billing.component';


@NgModule({
  imports: [
    CommonModule,
    BillingRoutingModule
  ],
  declarations: [
    CPTComponent,
    FeeTableComponent,
    DXCodesComponent,
    ICD10Component,
    WriteOffCodesComponent,
    ModifiersComponent,
    ManagePOSComponent,
    PoliciesComponent,
    ElectronicComponent
  ]
})
export class BillingModule { }
