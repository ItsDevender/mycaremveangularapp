// Core Modules
import { Component, OnInit, AfterViewInit } from '@angular/core';

// Services
import { UserService } from '@services/user.service';
import { DropdownService } from '@shared/services/dropdown.service';
import { UtilityService } from '@shared/services/utility.service';
import { JwtService } from '@services/jwt.service';
import { ErrorService } from '@services/error.service';
import { AppService } from '@services/app.service';
import { Router } from '@angular/router';

// Forms
import { FormControl, FormGroup, Validators } from '@angular/forms';

// Models
import { MicroServiceLogin, GatewayServiceLogin } from '@models/login.model';
import {User } from '@models/user.model';
import {practice } from '@models/practice.model';
import { JwtHelperService } from '@auth0/angular-jwt';
import { LocationsMaster } from '@app/core/models/masterDropDown.model';

// Setting Up Component
@Component({
  selector: 'app-login-form',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})


export class LoginComponent implements OnInit {

  // Date
  objDate: any;

  // Login Form
  loginForm: FormGroup;
  location: FormControl;
  username: FormControl;
  password: FormControl;


  // Locations
  locations: Array<LocationsMaster>;


  // Selected Location
  selectedValue: any = 1;

  /**
   * Get date time of login component
   */
  getDateTime: object;

  /**
   * Micro login params of login component
   * Use for creating payload based on model.
   */
  microLoginParams: MicroServiceLogin;

  /**
   * Gateway login params of login component
   * Use for creating payload based on model.
   */
  gatewayLoginParams: GatewayServiceLogin;

   constructor(
    private service: UserService,
    private dropdowns: DropdownService,
    private utility: UtilityService,
    private error: ErrorService,
    private app: AppService,
    private jwt: JwtService,
    private router: Router) {
    // Date Now
    this.objDate = Date.now();
    this.microLoginParams = new MicroServiceLogin();
    this.gatewayLoginParams = new GatewayServiceLogin();   
  } 

  ngOnInit() {
    // Load Locations
    this.LoadLocations();

    // Intialize Form
    this.intializeForm();

    // Disabling Patient Banner and Patient Id
    this.app.isPatientSigned = false;
    this.app.patientId = 0;
  }

  intializeForm() {
    this.location = new FormControl('', Validators.required);
    this.username = new FormControl('', Validators.required);
    this.password = new FormControl('', Validators.required);

    this.loginForm = new FormGroup({
      location: this.location,
      username: this.username,
      password: this.password
    });
    // Setting Default Location
    this.loginForm.controls.location.setValue(this.app.locationId);
  }


  /**
   * Validate User
   * Check for required and empty parameters
   */
  validateUser() {
    const credentials = this.loginForm.value;
    const lt = credentials.location;
    const un = credentials.username;
    const pd = credentials.password;



    if (this.loginForm.invalid) {
      this.error.displayFormErrors(this.loginForm.controls);
      return false;
    }

    // Used for Microservices
    this.microLoginParams = {
      username: un,
      password: pd,
      facility: lt
    };

    // GateWay Payload
    this.gatewayLoginParams = {
      key: pd,
      user: un
    };

    // Authenticate User
    this.authenticateCredentials();
  }

  /**
   * Stage 1  - Authenticates credentials
   * Send API request to authenticate for microservice
   * Trigger My Care EMR Authentication
   */
  authenticateCredentials() {
    // Micro Token Login
    this.service.authenticateUser(this.microLoginParams).subscribe(
      (returnData: any) => {
        try {
          // Setting Time
          this.app.sessionTime = returnData.expires_in;

          // Saving Token
          this.jwt.saveToken(returnData.access_token);

          // Authenticate IMW
          this.authenticateIMW();


        } catch (error) {
          this.error.syntaxErrors(error);
        }
      }
    );
  }


  /**
   * Stage 2 - Authenticate IMW
   * Send API Request to authenticate for my care emr
   * Trigger Second API Gateway authentication
   */
  authenticateIMW() {
    // PHP Login
    const mainpart = 'IMWAPI/getToken';
    let inputpart = '&userId=' + this.microLoginParams.username;
    inputpart += '&password=' + this.microLoginParams.password;
    inputpart += '&facility=' + this.microLoginParams.facility;
    inputpart += '&userType=1&window_height=' + window.screen.availHeight;
    this.service.phpTokenLogin(mainpart, inputpart).subscribe(
      (returnData: any) => {
        try {
          this.jwt.savePHPToken(returnData.result.Token);

          // Authenticate API Gateway
          this.authenticateGateway();
        } catch (error) {
          this.error.syntaxErrors(error);
        }
      }
    );
  }


  /**
   * Stage 3 - Authenticates gateway
   * Send API Request to authenticate api gateway
   * @returns Set user active and allow permissions to access application
   */
  authenticateGateway() {
    // Micro Token Login
    this.service.authenticateUserGateway(this.gatewayLoginParams).subscribe(
      (returnData: any) => {
        try {
          // Setting Time
          this.app.sessionGatewayTime = returnData.expiration;

          // Saving Token
          this.jwt.saveGatewayToken(returnData.token);

          // Setting Time
          this.app.sessionGatewayTime = returnData.expiration;

          // Set Logged In
          this.app.userLoggedIn = true;

          this.locationTimeZone();

          // Setting Username
          this.app.userName = this.gatewayLoginParams.user;

          this.locationTimeZone();

          // setting EMR tab enable/Disanled
          const helper = new JwtHelperService();
          const decodedToken = helper.decodeToken(this.jwt.getToken());
          this.app.emrDisabled = decodedToken.emrTabs;
          this.app.loginUserID = decodedToken.sub;

          //Checking the portal Registsered status login user 
          this.setuserPortalstatus(this.app.loginUserID);

          //Setting practice level Portal SignlesignOn url 
          this.setPracticeSSOUrl();

          // Showing Messages
          this.error.displayError({
            severity: 'success',
            summary: 'Authentication',
            detail: 'Logged in Successfully'
          });


        } catch (error) {
          this.error.syntaxErrors(error);
        }
      }
    );
  }

  /**
   * Loads locations
   * For Loading Locations from API
   * @param {string} locations binding dropdown data
   * @result {object} for location dropdown
   */
  LoadLocations() {
    this.locations = this.dropdowns.locationsData;
    if (this.utility.getObjectLength(this.locations) === 0) {
        this.dropdowns.getLocationsDropData().subscribe(
            (data: LocationsMaster[]) => {
                try {
                    this.locations.push({ id: '', name: 'Select Location' });
                    for (let i = 0; i <= data.length - 1; i++) {
                        this.locations.push(data[i]);
                    }
                    this.dropdowns.locationsData = this.locations;
                } catch (error) {
                    this.error.syntaxErrors(error);
                }
            }
        );
    }
    // Load Locations
    // this.locations = this.dropdowns.locations;
    // if (this.utility.getObjectLength(this.locations) === 0) {
    //   this.dropdowns.getLocations().subscribe(
    //     data => {
    //       try {
    //         this.locations = data['result'].Optical.Locations;
    //         this.dropdowns.locations = this.locations;
    //       } catch (error) {
    //         this.error.syntaxErrors(error);
    //       }
    //     }
    //   );
    // }
  }

  locationChange(event: any) {
    const selectedLoc = event.target.value;
    if (selectedLoc !== null && selectedLoc !== 0) {
      this.app.locationId = selectedLoc;
    }
  }

  /**
   * Locations time zone
   * Display the date and time based on locations
   */
  locationTimeZone() {
    const TimeZone = {
      'filter': [
        {
          field: 'id',
          operator: '=',
          value: this.loginForm.controls.location.value
        }
      ],
      "sort": [
        {
          field: "id",
          order: "ASC"
        }
      ]
    };
    this.service.getTimeZone(TimeZone).subscribe(data => {
      this.getDateTime = data;
      if (data['data'][0].timezone_details.length != 0) {
        this.app.timeZone = data['data'][0].timezone_details.timezonedesc.slice(4, 10);
      }
      this.router.navigate(['dashboard']);
    });
  }


  /**
     *load the Practice wich is registered to portal 
     * Set the SSO url to the variables need to be used for laiunching the Portal SSO
     */
  setPracticeSSOUrl() {    
    this.app.portalSSOUrl = "";
    this.app.getPortalRegisteredPractice().subscribe(
      (practicedata: practice)   => {
        try {        
        
          if(practicedata !=null)
          {
            if(practicedata[0].portal_sso_url !== null) {
              this.app.portalSSOUrl =practicedata[0].portal_sso_url;
            }
          }
          
        } catch (err) {
          this.error.syntaxErrors(err);

        }
      }
    );
  }

  /**
     *load the User info for the logged in user 
     * Set the protal status of the user logged in to launch portal SSO
     */
  setuserPortalstatus(userId: number) {
    this.app.isUserRegisteredtoPortal = false;    
    this.app.getUserPortalStatus(userId).subscribe(
      (userdata: User) => {
        try {          
          if (userdata.portal_registered_id !== null) {
            this.app.isUserRegisteredtoPortal = true;
          }
        } catch (err) {
          this.error.syntaxErrors(err);
        }
      }
    );
  }




}
