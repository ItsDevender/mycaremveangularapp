import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { OrderService } from '@app/core/services/order/order.service';
// import { UserService } from '@app/core/services/user.service';
import { FramesService } from '@app/core/services/inventory/frames.service';
import { AppService, ErrorService } from '@app/core';
// import { ApiAlertPipe } from 'src/app/shared/pipes/api-alert.pipe';
// import { FrameService } from '../../ConnectorEngine/services/frame.service';
// import { OrderSearchService } from '../../ConnectorEngine/services/order.service';
// import { StateInstanceService } from 'src/app/shared/state-instance.service';
// import { UserService } from '../../ConnectorEngine/services/user.service';


@Component({
    selector: 'app-order-frame-lab',
    templateUrl: './tab.component.html'
})
export class OrderFrameMainComponent implements OnInit, OnDestroy {
    addPatientInfoSidebar;
    paymentDetails;
    orderTypes: any[];
    table2: any[];
    ShippingEnableStatus = false;
    subscribeShippingStatus: Subscription;
    subscribeFramePrice: Subscription;
    shippingScreenVisible = false;
    frameScreenVisible = true;
    otherScreenVisible = false;
    framesPrice = '';
    otherDetailsPrice = '';
    frameLable = false;
    otherLable = false;
    subscribelensOsPrice: Subscription;
    subscribeAlertPopUp: Subscription;
    orderlensalertpopup: Subscription;
    constructor(private orderService: OrderService,  public app: AppService, private errorService: ErrorService) {
        this.subscribeFramePrice = orderService.framesPrice$.subscribe(data => {
            this.frameLable = true;
            this.framesPrice = ('Frames:$' + parseFloat(data).toFixed(2));
            if (data === '') {
                this.frameLable = false;
            }
            if (this.orderService.orderSaveUpdateStatus === false) {
                this.orderService.orderFormChangeStatus = false;
            }
        });
        this.subscribelensOsPrice = orderService.orderDetails$.subscribe(data => {
            this.otherLable = true;
            this.otherDetailsPrice = ('Order total:$' + parseFloat(data).toFixed(2));
            if (data === '') {
                this.otherLable = false;
            }
            if (this.orderService.orderSaveUpdateStatus === false) {
                this.orderService.orderFormChangeStatus = false;
            }
        });
        this.orderService.orderScreenSavePath = 'Frames';
        this.subscribeShippingStatus = orderService.ShippingDetalesEnable$.subscribe(data => {
            if (data === true) {
                this.ShippingEnableStatus = true;
                this.shippingScreenVisible = true;
                this.frameScreenVisible = false;
                this.otherScreenVisible = false;
            } else {
                this.ShippingEnableStatus = false;
                this.shippingScreenVisible = false;
                this.frameScreenVisible = true;
                this.otherScreenVisible = false;
            }

        });
        this.errorService.msgs = [];
        this.subscribeAlertPopUp = orderService.OrderAlertPopup.subscribe(data => {
            if (data === 'save') {
                this.errorService.displayError({
                    severity: 'success',
                    summary: 'Success Message', detail: 'Order Saved Successfully'
                });
                // this.msgs.push({ severity: 'success', summary: 'Success Message', detail: 'Order Saved Successfully' });

            }
            if (data === 'update') {
                this.errorService.displayError({
                    severity: 'success',
                    summary: 'Success Message', detail: 'Order Updated Successfully'
                });
                // this.msgs.push({ severity: 'success', summary: 'Success Message', detail: 'Order Updated Successfully' });

            }
        });

        this.orderlensalertpopup = this.orderService.orderPopupAlertPopup$.subscribe(data => {
            if (data.messagetype === 'required') {
                this.errorService.displayError({ severity: 'warn', summary: 'Warning Message', detail: data.message });
                // this.msgs.push({ severity: 'warn', summary: 'Warning Message', detail: data.message });

                return false;
            }
        });
    }

    ngOnDestroy() {
        this.subscribeShippingStatus.unsubscribe();
        this.subscribeFramePrice.unsubscribe();
        this.subscribelensOsPrice.unsubscribe();
        this.subscribeAlertPopUp.unsubscribe();
        this.orderlensalertpopup.unsubscribe();
    }
    ngOnInit(): void {
        
        // console.log('orderframes');
        // localStorage.removeItem('Order_id');
        this.orderService.showCategory = false;
        this.orderService.orderScreenMultipleSavePath = 'Frames Only';
        this.orderService.orderId = 'New Order';
        this.orderService.orderSelectTypeobj.next(1);
        this.orderService.showorderdiv.next(true);
        this.orderTypes = [
            { name: '92002 - New Patient - intermediate Exam', value: '1', price: '0.00' },
            { name: '92012 - Exsting Patient - intermediate Exam', value: '1', price: '345.00' },
            { name: '32014 - Exsting Patient - Comprehensive Exam', value: '1', price: '30.00' },
            { name: '92015 - Determination of Refractive state', value: '1', price: '300.00' },
            { name: '92083 - Visual Field Examination', value: '1', price: '500.00' },
        ];
        this.table2 = [
            {
                changes: 'Bausch & Lomb Biotrue Oneday For Pres', Eye: 'OD', patient: '6.00', total: '6.00',
                insurance: '0.00', itemtotal: '6.00', retail: '3.00', unitprice: '6.00'
            },
            {
                changes: 'Bausch & Lomb Biotrue Oneday For Pres', Eye: 'OS', patient: '6.00', total: '3.00',
                insurance: '6.00', itemtotal: '6.00', retail: '6.00', unitprice: '1.00'
            },
            {
                changes: 'Bausch & Lomb Biotrue Oneday For Pres', Eye: 'OD', patient: '6.00', total: '6.00',
                insurance: '10.00', itemtotal: '12.00', retail: '3.00', unitprice: '0.00'
            },
            {
                changes: 'Bausch & Lomb Biotrue Oneday For Pres', Eye: 'OS', patient: '', total: '6.00',
                insurance: '', itemtotal: '6.00', retail: '3.00', unitprice: '12.00'
            },
        ];
        this.orderService.orderLocation.next(this.app.locationId);
        this.orderService.disableSavePayBtn = false;
        // this.orderService.orderStatusGlobal = '';
        this.orderService.orderLabValid = false;
    }
    PageClick(event) {
        const data = event.index;
        if (data === 0) {
            this.orderService.orderScreenSavePath = 'Frames';
            this.otherScreenVisible = false;
            this.frameScreenVisible = true;
        }
        if (data === 1) {
            this.orderService.orderScreenSavePath = 'Other Details';
            this.frameScreenVisible = false;
            this.otherScreenVisible = true;
        }
        if (data === 2) {
            this.orderService.orderScreenSavePath = 'Shipping Details';
            this.frameScreenVisible = false;
            this.otherScreenVisible = false;
        }
        // console.log(event.target.childnode[0].data)
    }
}
