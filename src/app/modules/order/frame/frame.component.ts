import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Frame } from '@models/inventory/frames.model';
import { FrameBrand } from '@models/inventory/Brands.model';
import { FrameColor, LenColorData, LensData, FrameColorData, Data } from '@models/inventory/Colors.model';
import { FramesShapes } from '@models/inventory/frameshapes.model';
import { Manufacturer } from '@models/inventory/Manufacturer.model';

import { SelectItem } from 'primeng/primeng';
import { Subscription } from 'rxjs/internal/Subscription';

// Settings

import { OrderService } from '@services/order/order.service';
import { AppService } from '@services/app.service';
import { DropdownService } from '@shared/services/dropdown.service';
import { IframesService } from '@shared/services/iframes.service';
import { ErrorService } from '@app/core';
import { UtilityService } from '@app/shared/services/utility.service';
import { InventoryModel } from '@app/core/models/inventory/inventoryItems.model';
import {
    MasterDropDown,
    ManufacturerDropData,
    LensColorMaster,
    FramesMaterial,
    Material,
    RimType,
    FrameUsage,
    FrameBrands
} from '@app/core/models/masterDropDown.model';
@Component({
    selector: 'app-order-frame',
    templateUrl: './frame.component.html'
})
export class OrderFrameComponent implements OnInit, OnDestroy {
    Ordersearchsubscription: Subscription;
    OrderForm: FormGroup;
    Accesstoken = '';
    public manufacturerlist: Manufacturer[];
    public Manufacturers: any = [];
    public Brandlist: FrameBrand[];
    Brands: any = [];
    cars: any[];
    /**
     * Frame color of order frame component
     * holdind list of frame Colorid, colorName
     */
    frameColor: Array<object>;
    public framedatalist: Frame[];
    framename: any = [];
    manufacId = '';
    brandId = '';
    UPCId = '';
    selecteddmanufactureId = '';
    selectedbrandid = '';
    selectedupc = '';
    selectedframeid = '';
    public frameShapeslist: FramesShapes[];
    frameShapes: any[];
    public frameTypeslist: any[];
    frameTypes: any[];
    public materiallist: any[];
    materials: any[];
    public copynames: any[];
    selectednameid = '';
    selectedFrameColorId = '';
    selectedColid = '';
    selectedlenscolorId = '';
    selectedframetypeid = '';
    public selectedShapeId = '0';
    subscriptionSave: Subscription;
    subscriptionLocationId: Subscription;
    subscriptionOrderId: Subscription;
    subscriptionSaveNew: Subscription;
    subscriptionorderDate: Subscription;
    subscriptionFrameBrows: Subscription;
    subscriptionOrderType: Subscription;
    subscriptionOrderCancle: Subscription;
    module = [{ Id: '', Name: 'select Module' }, { Id: '1', Name: 'Frame' }];
    LocationId = '';
    orderId = null;
    orderformOrderId = '';
    orderformItemId = '';
    orderDateSaveObj = Date;
    public copycolor: any[];
    public source: any[];
    public DataSet: any;
    upcreadonly = false;
    UPCDataArray = [];
    selectInventorySidebar = false;
    public orderType: any;
    public orderField: string;
    public ordersearchvalue: any;
    genderDropDown = [{ Id: 'Male', Name: 'Male' }, { Id: 'Female', Name: 'Female' }, { Id: 'Unisex', Name: 'Unisex' }];
    framesUsage: any = [];
    eyedetails: SelectItem[];
    framesRimType: any = [];
    orderSelectedItems: any;
    /**
     * Upcfilter  of order frame component for upc filter items
     */
    upcfilter: object;
    /**
     * Name filter data of order frame component for item name list
     */
    nameFilterData: object;

    lenscolor: Array<object>;
    /**
     * Lens color of order frame component
     * Holding lens color details
     */
    lensColor: Array<object>;
    /**
     * Creates an instance of order frame component.
     * @param fb  for form group
     * @param orderService calling order service methods
     * @param appSettings for app setting methods
     * @param errorService for calling errorhandling methods
     * @param app for calling appservice methods
     * @param dropdownService for calling common dropdown methods
     * @param utility for calling utility service methods
     */
    constructor(
        private fb: FormBuilder,
        private orderService: OrderService,
        public appSettings: IframesService,
        private errorService: ErrorService,
        public app: AppService,
        private dropdownService: DropdownService,
        public utility: UtilityService) {
        this.orderService.disablePaymentButton = true;
        this.subscriptionSaveNew = orderService.orderSaveNew$.subscribe(data => {
            this.OrderForm.controls.id.patchValue('');
            console.log('orderframe');
        });

        this.subscriptionOrderType = orderService.orderSelectTypeobj$.subscribe(data => {
            this.orderType = data;
        });

        this.Ordersearchsubscription = this.orderService.OrderSearchValue$.subscribe(data => {

            this.ordersearchvalue = [];
            this.ordersearchvalue = data;
            this.orderService.lensSelectionUPCStatus = 'lens';
        });

        this.subscriptionSave = orderService.orderFrameSave$.subscribe(data => {
            this.orderSaving(data);
        })

        this.subscriptionLocationId = orderService.orderLocationId$.subscribe(data => {
            if (data !== '') {
                this.LocationId = data;
                // this.orderService.orderFrameSave.next(false)
            } else {
                this.LocationId = data;
                // this.orderService.orderFrameSave.next(false)
            }
        });

        this.subscriptionorderDate = orderService.orderDateobj$.subscribe(data => {
            this.orderDateSaveObj = data;
        });

        this.subscriptionOrderId = orderService.orderselectionId$.subscribe(value => {
            // alert("framesonly")
            this.orderformOrderId = '';
            this.orderformItemId = '';
            if (value !== 'New Order') {
                let OrderItemsData: any = [];
                this.orderService.getOrderItemDetails(value).subscribe(data => {
                    // console.log(data)
                    OrderItemsData = data;
                    //  if(data['result']['Optical'] != "No record found"){
                    for (let i = 0; i <= OrderItemsData.length - 1; i++) {
                        if (OrderItemsData[i].module_type_id === 1 || OrderItemsData[i].module_type_id === '1') {
                            this.orderSelectedItems = OrderItemsData[i];
                        }
                    }
                    const orderFormData = this.orderSelectedItems;
                    this.orderformOrderId = orderFormData.order_id;
                    this.orderformItemId = orderFormData.id;
                    this.LoadformdataByOrderId(orderFormData);
                    this.upcreadonly = true;
                    //  }else{
                    //  }
                });
            } else {
                this.orderformOrderId = '';
                this.orderformItemId = '';
                this.Loadformdata();
                this.upcreadonly = false;
                // localStorage.removeItem('Order_id');
            }
        });

        this.subscriptionOrderCancle = orderService.OrderCancleValue$.subscribe(data => {
            this.orderformOrderId = '';
            this.orderformItemId = '';
            this.Loadformdata();
            this.upcreadonly = false;
            // localStorage.removeItem('Order_id');
        });

        // this.subscriptionFrameBrows = orderService.browsOrderFrameobj$.subscribe(element => {
        //     this.appSettings.enableSidebar = false;
        //     this.OrderForm.controls['manufacturer_id'].patchValue(element.ManufacturerId);
        //     this.OrderForm.controls['color_id'].patchValue(element.ColorId);
        //     this.OrderForm.controls['brand_id'].patchValue(element.BrandId);
        //     this.OrderForm.controls['shape_id'].patchValue(element.StyleId);
        //     this.OrderForm.controls['a'].patchValue(element.a);
        //     this.OrderForm.controls['b'].patchValue(element.b);
        //     this.OrderForm.controls['ed'].patchValue(element.ed);
        //     this.OrderForm.controls['dbl'].patchValue(element.dbl);
        //     this.OrderForm.controls['temple'].patchValue(element.temple);
        //     this.OrderForm.controls['bridge'].patchValue(element.bridge);
        //     this.OrderForm.controls['fpd'].patchValue(element.fpd);
        //     this.OrderForm.controls['upc_code'].patchValue(element.UPC);
        //     this.OrderForm.controls['item_id_temp'].setValue(element.itemId);
        //     //this.OrderForm.controls['item_id'].patchValue(element.itemId);
        //     this.framedatalist.forEach(data => {
        //         if (element.itemId == data.itemId) {
        //             this.framename.forEach(value => {
        //                 if (data.Name == value.Name) {
        //                     this.OrderForm.controls['item_id'].patchValue(value.itemId);
        //                 }
        //             });

        //         }
        //     });

        // });
    }

    /**
     * Orders frame saving
     * @param {string} data for getting order type 
     * @returns  {object} for saving complete orders
     */
    orderSaving(data) {
        this.orderField = data;
        if (data === 'Frames Only' || data === 'Spectacle Lens') {
            this.framename.forEach(value => {
                const itemId = this.OrderForm.controls.item_id.value;
                if (itemId === value.itemId) {
                    this.OrderForm.controls.item_name.patchValue(value.Name);
                }
            });
            const itemidtemp = this.OrderForm.controls.item_id_temp.value;
            this.OrderForm.controls.item_id.patchValue(itemidtemp);
            const tempControls = this.OrderForm;
            // alert(tempControls);
            console.log(tempControls);
            // ISSUE IN BUILD
            $(this.orderService.otherItemRows).each(function (i, obj: any) {
                if ((obj.item_id === itemidtemp && obj.module_type_id === 1) || (obj.item_id === itemidtemp && obj.module_type_id === '1')) {
                    tempControls.controls.qty.patchValue(obj.qty);
                }
            });
            this.OrderForm = tempControls;
            const messagedetails = {
                messagetype: 'required',
                message: 'Please Enter the required fields'
            };
            const patientid = this.app.patientId;
            if (patientid == 0 || patientid === undefined) {
                this.errorService.displayError({ severity: 'warn', summary: 'Warning Message', detail: 'Please Select a Patient' });
                // messagedetails.message = 'Please Select a Patient';
                // this.orderService.orderPopupAlertPopup.next(messagedetails);
                return false;
            }

            if (this.app.patientId != null) {
                if (this.LocationId !== '') {
                    if (this.OrderForm.controls.module_type_id.value !== '') {
                        this.OrderForm.controls.order_status_id.patchValue(this.orderService.orderStatusGlobal);
                        if (data === 'Frames Only') {
                            if (this.OrderForm.controls.id.value === '') {
                                this.framesSaving();
                            } else {
                                this.framesUpdating();
                            }
                        }
                        if (data === 'Spectacle Lens') {
                            let save_validation: boolean = true;
                            for (var items in this.orderService.mandatoryFields_Validation) {
                                let validation = this.orderService.mandatoryFields_Validation[items];
                                if (validation.required == false) {
                                    save_validation = false;
                                    this.errorService.displayError({ severity: 'warn', summary: 'Warning Message', detail: validation.label + ' is required.' });
                                }
                            }
                            if (this.OrderForm.controls.id.value === '') {
                                if (save_validation == true) {
                                    this.framesSaving();
                                } else {
                                    return false;
                                }
                            } else {
                                if (save_validation == true) {
                                    this.framesUpdating();
                                } else {
                                    return false;
                                }
                            }
                        }
                    } else {
                        this.errorService.displayError({
                            severity: 'warn',
                            summary: 'Warning Message', detail: 'Please select module'
                        });
                        // this.msgs.push({ severity: 'warn', summary: 'warning Message', detail: 'Please select module' });

                    }
                } else {
                    this.errorService.displayError({
                        severity: 'warn',
                        summary: 'Warning Message', detail: 'Please select location'
                    });
                    // this.msgs.push({ severity: 'warn', summary: 'warning Message', detail: 'Please select location' });

                }
            } else {
                this.errorService.displayError({
                    severity: 'warn',
                    summary: 'Warning Message', detail: 'Please select patient'
                });
                // this.msgs.push({ severity: 'warn', summary: 'warning Message', detail: 'Please select patient' });

            }
            // this.orderService.orderLensSelectionSave.next("2");
        }

    }

    /**
     * Frames saving 
     * @returns  {object} for saving  orders
     */
    framesSaving() {
        const messagedetails = {
            messagetype: 'required',
            message: 'Please Enter the required fields'
        };
        this.OrderForm.patchValue({
            loc_id: this.LocationId
        });
        if (this.OrderForm.invalid) {
            this.errorService.displayFormErrors(this.OrderForm.controls);
            return false;
        }
        if (!this.OrderForm.valid) {
            this.errorService.displayError({ severity: 'warn', summary: 'Warning Message', detail: 'Please Enter the required fields' });
            // messagedetails.message = 'Please Enter the required fields';
            // this.orderService.orderPopupAlertPopup.next(messagedetails);
            return false;
        }

        let framessavedata: any = [];
        // alert(this.orderService.disablePaymentButton);
        // alert(this.orderService.orderStatusGlobal);
        if (this.app.insuranceValue == '' || this.app.insuranceValue == "null") {
            this.OrderForm.removeControl('insuranceid');
        } else {
            this.OrderForm.controls.insuranceid.patchValue(this.app.insuranceValue)
        }
        this.orderService.saveOrderFrames(this.utility.formatWithOutControls(this.OrderForm.value)).subscribe(saveData => {
            framessavedata = saveData;

            // localStorage.removeItem('Order_id');
            // if (framessavedata.status === 'New Order Crated') {
            // this.orderService.Order_id = data['order_id'];
            this.orderService.orderId = framessavedata.order_id;
            this.orderService.disablePaymentButton = false;
            this.orderService.disabledModify = ''
            // this.orderService.getOrderStatusData(data['order_id']).subscribe(data=>{
            //     console.log(data['order_enc_id'])
            //     this.orderService.orderEncounterId.next( data['order_enc_id']);
            //    //console.log(this.locationId)
            // })
            if (this.orderField === 'Spectacle Lens') {
                this.orderService.orderLensSelectionSave.next('2');

            }
            if (this.orderField === 'Frames Only') {
                this.orderService.orderspectacleLensSave.next('3');

            }
            this.orderService.OrderAlertPopup.next('save');
            // this.msgs.push({ severity: 'success', summary: 'Success Message',
            // detail: 'Order Saved Successfully' });

            //  this.Loadformdata();
            this.orderService.orderFormUpdate.next('success');
            this.orderService.orderSaveUpdateStatus = false;
            // }
            // this.orderService.getOrderSearchMaster(
            // "", this._StateInstanceService.PatientId, "", "", "", "", "", 1000, "", "", "").subscribe(
            // DataSet => {
            // })
        }, error => {

        });
    }

    /**
     * Frames updating
     * @returns  {object} for updating  orders
     */
    framesUpdating() {
        this.OrderForm.patchValue({
            loc_id: this.LocationId
        });

        this.framename.forEach(value => {
            const itemId = this.OrderForm.controls.item_id.value;
            if (itemId === value.itemId) {
                this.OrderForm.controls.item_name.patchValue(value.Name);
            }
        });

        const itemidtemp = this.OrderForm.controls.item_id_temp.value;
        // const item_id_temp = this.OrderForm.controls['item_id_temp'].value;
        this.OrderForm.controls.item_id.patchValue(itemidtemp);

        const tempControls = this.OrderForm;
        $(this.orderService.otherItemRows).each(function (i, obj: any) {

            if (obj.item_id === itemidtemp && obj.module_type_id === 1) {
                tempControls.controls.qty.patchValue(obj.qty);
            }
        });

        this.OrderForm = tempControls;
        let updateorderframedata: any = [];
        // if (this.app.insuranceValue == '' || this.app.insuranceValue == "null") {
        //     this.OrderForm.removeControl('insuranceid');
        // } else {
        //     this.OrderForm.controls.insuranceid.patchValue(this.app.insuranceValue)
        // }
        this.orderService.updateOrderFrames(this.OrderForm.value, this.orderformOrderId,
            this.orderformItemId).subscribe(updateData => {
                updateorderframedata = updateData;
                // localStorage.removeItem('Order_id');
                // if (updateorderframedata.status === 'Order item updated successfully.') {
                this.orderService.orderId = updateorderframedata.order_id;
                // this.orderService.getOrderStatusData(data['order_id']).subscribe(data=>{
                //     console.log(data['order_enc_id'])
                //     this.orderService.orderEncounterId.next( data['order_enc_id']);
                //    //console.log(this.locationId)
                // })
                if (this.orderField === 'Spectacle Lens') {
                    this.orderService.orderLensSelectionSave.next('2');
                }
                if (this.orderField === 'Frames Only') {
                    this.orderService.orderspectacleLensSave.next('3');
                }

                let getorderdata: any = [];
                this.orderService.getOrderStatusData(updateorderframedata.order_id).subscribe(OrderData => {
                    getorderdata = OrderData;
                    this.orderService.orderEncounterId.next(getorderdata.order_enc_id);
                    // console.log(this.locationId)
                });
                this.orderService.OrderAlertPopup.next('update');
                //  this.msgs.push(

                // this.Loadformdata();

                // this.orderService.orderFormUpdate.next("success");
                // }
            });

    }
    FrameUpcData(element) {
        this.selectInventorySidebar = false;
        this.orderService.frameItemId = element.id;
        this.OrderForm.controls.item_id.patchValue(element.id);
        this.OrderForm.controls.item_name.patchValue(element.name);
        this.OrderForm.controls.manufacturer_id.patchValue(element.manufacturer === null ? null : element.manufacturer.id);
        this.OrderForm.controls.color_id.patchValue(element.colorid);
        this.OrderForm.controls.color_code.patchValue(element.color_code);
        this.OrderForm.controls.brand_id.patchValue(element.brand_id);
        this.OrderForm.controls.shape_id.patchValue(element.supply_id);
        this.OrderForm.controls.type_id.patchValue(element.frame_type === null ? null : element.frame_type.id);
        this.OrderForm.controls.gendertype.patchValue(element.gender);
        this.OrderForm.controls.shape_id.patchValue(element.frame_shape);
        this.OrderForm.controls.eyesize.patchValue(element.eyesize);
        this.OrderForm.controls.a.patchValue(element.a);
        this.OrderForm.controls.b.patchValue(element.b);
        this.OrderForm.controls.ed.patchValue(element.ed);
        this.OrderForm.controls.dbl.patchValue(element.dbl);
        this.OrderForm.controls.temple.patchValue(element.temple);
        this.OrderForm.controls.bridge.patchValue(element.bridge);
        this.OrderForm.controls.fpd.patchValue(element.fpd);
        this.OrderForm.controls.upc_code.patchValue(element.upc_code);
        this.OrderForm.controls.item_id_temp.setValue(element.id);
        this.OrderForm.controls.source_id.setValue(element.sourceid);
        this.OrderForm.controls.price_retail.setValue(element.retail_price);
        this.OrderForm.controls.rimtypeid.patchValue(element.rimtypeid);
        this.OrderForm.controls.material_id.patchValue(element.frame_material === null ? null : element.frame_material.id);
        this.OrderForm.controls.lens_color.patchValue(element.lenscolor);
        this.OrderForm.controls.frameusageid.patchValue(element.frameusageid);
        this.OrderForm.controls.item_id.patchValue(element.id);
        // this.OrderForm.controls['item_id'].patchValue(element.itemId);
        // this.framedatalist.forEach(data => {
        //     if (element.id === data.itemId) {
        //         this.framename.forEach(value => {
        //             if (data.Name === value.Name) {
        //                 this.OrderForm.controls.item_id.patchValue(value.itemId);
        //             }
        //         });
        //     }
        // });
        this.OrderForm.controls.traceFile.patchValue('');
        // this.frameService.framesTraceFileGet(this.app.accessTokenGlobal, element.itemId).subscribe(data => {
        //     if (data['result']['Optical']['name']) {
        //         this.OrderForm.controls['traceFile'].patchValue(data['result']['Optical']['name']);
        //     }
        // });


    }
    /**
     * on init for initilizing methods
     */
    ngOnInit(): void {
        this.orderService.orderStatusGlobal = '9';
        // this.frameService.errorHandle.msgs = [];
        // localStorage.removeItem('Order_id');
        // if (this.app.patientId === 0) {
        //     this.errorService.displayError({
        //         severity: 'warn',
        //         summary: 'Warning Message', detail: 'Please select patient'
        //     });

        //     // this.msgs.push({ severity: 'error', summary: 'warning Message', detail: 'Please select Patient' });

        // }
        this.LoadManufacturers();
        this.Loadformdata();
        this.Loadmasters();
        this.loadRimType();
        this.LoadFrameShapes();
        this.LoadFrameType();
        this.selectedupc = '';
        this.selectedFrameColorId = 'null';
        this.selectedColid = 'null';
        this.selectedlenscolorId = 'null';
        this.selectedframetypeid = 'null';
        this.selectednameid = 'null';
        this.selectedShapeId = '0';
        this.OrderForm.controls.module_type_id.patchValue('1');
        this.OrderForm.controls.color_id.patchValue('');
        if (this.app.ordersearchvalueService !== undefined) {
            if (this.app.ordersearchvalueService.length !== 0) {
                this.orderService.OrderSearchFilterValue.next((
                    {
                        Id: this.app.ordersearchvalueService[0].Id,
                        OrderType: this.app.ordersearchvalueService[0].OrderType
                    })
                );
                this.app.ordersearchvalueService = [];
            }
        }
        // this.Manufacturers = [];
        // this.Manufacturers = this.dropdownService.Manufactures;
        // // .getManufactures().subscribe(data=>{

        // });
        // this.Brands = [];
        // this.Brands = this.dropdownService.getBrands();
        // this.materials = [];
        // this.materials = this._DropdownsPipe.specticalMaterialDropDown();
        // this.framename = [];
        // this.framename = this.dropdownService.getframesdetails();
        // this.copynames = this.dropdownService.framedatalist;
        // this.framedatalist = this.dropdownService.framedatalist;
        // this.framesUsage = [];
        // this.framesUsage = this.dropdownService.getUsage();
        this.LocationId = this.app.locationId;
    }
    ngOnDestroy(): void {
        this.subscriptionSave.unsubscribe();
        this.subscriptionLocationId.unsubscribe();
        this.subscriptionOrderId.unsubscribe();
        this.subscriptionSaveNew.unsubscribe();
        this.subscriptionorderDate.unsubscribe();
        this.subscriptionorderDate.unsubscribe();
        this.subscriptionOrderType.unsubscribe();
        this.Ordersearchsubscription.unsubscribe();
        this.subscriptionOrderCancle.unsubscribe();
    }
    LoadformdataByOrderId(orderFormData) {
        //const tempItemName = orderFormData.item_id;
        this.OrderForm = this.fb.group({
            // accessToken: this._StateInstanceService.accessTokenGlobal,
            patient_id: this.app.patientId,
            id: orderFormData.id,
            comment: [''],
            main_default_ins_case: [''],
            loc_id: [orderFormData.loc_id === '' || orderFormData.loc_id === '0' ?
                orderFormData.loc_id = '' : orderFormData.loc_id, [Validators.required]],
            item_id: orderFormData.item_id,
            item_prac_code: [orderFormData.item_prac_code],
            item_prac_code_default: [orderFormData.item_prac_code_default],
            upc_code: [orderFormData.upc_code, [Validators.required]],
            item_name: orderFormData.item_name,
            module_type_id: [orderFormData.module_type_id, [Validators.required]],
            manufacturer_id: orderFormData.manufacturer_id === '' ||
                orderFormData.manufacturer_id === '0' ? orderFormData.manufacturer_id = '' : orderFormData.manufacturer_id,
            brand_id: orderFormData.brand_id,
            style_id: orderFormData.style_id,
            shape_id: orderFormData.shape_id,
            color_id: orderFormData.color_id,
            color_code: orderFormData.color_code,
            a: orderFormData.a,
            b: orderFormData.b,
            ed: orderFormData.ed,
            dbl: orderFormData.dbl,
            temple: orderFormData.temple,
            bridge: orderFormData.bridge,
            fpd: orderFormData.fpd,
            use_on_hand_chk: [''],
            order_chk: [''],
            safety_glass: [''],
            job_type: [''],
            item_id_temp: orderFormData.item_id,
            qty: orderFormData.qty,
            source_id: orderFormData.source_id,
            ordertypeid: this.orderType,
            frameusageid: orderFormData.frameusageid,
            eyesize: orderFormData.eyesize,
            material_id: orderFormData.material_id,
            type_id: orderFormData.type_id === 0 ? null : orderFormData.type_id,
            gendertype: orderFormData.gendertype,
            Labinstructions: orderFormData.Labinstructions,
            traceFile: '',
            rimtypeid: orderFormData.rimtypeid,
            lens_color: orderFormData.lens_color_id,
            order_status_id: this.orderService.orderStatusGlobal,
            price_retail: orderFormData.price_retail
        });
        this.orderService.frameItemId = orderFormData.id;
        this.upcreadonly = true;
    }
    Loadformdata() {
        // alert(this.orderService.orderStatus);
        // alert(this.orderService.orderStatusGlobal);
        this.OrderForm = this.fb.group({
            // accessToken: this._StateInstanceService.accessTokenGlobal,
            patient_id: this.app.patientId,
            id: '',
            comment: [''],
            main_default_ins_case: [''],
            loc_id: ['', [Validators.required]],
            item_id: ['', [Validators.required]],
            item_prac_code: [''],
            item_prac_code_default: [''],
            upc_code: ['', [Validators.required]],
            item_name: [''],
            module_type_id: ['1', [Validators.required]],
            manufacturer_id: [''],
            brand_id: [''],
            style_id: [''],
            shape_id: [''],
            color_id: [''],
            color_code: [''],
            a: [''],
            b: [''],
            ed: [''],
            dbl: [''],
            temple: [''],
            bridge: [''],
            fpd: [''],
            use_on_hand_chk: [''],
            order_chk: [''],
            safety_glass: [''],
            job_type: [''],
            item_id_temp: '',
            qty: '',
            source_id: '',
            ordertypeid: this.orderType,
            gender: null,
            frameusageid: '',
            eyesize: null,
            material_id: [''],
            type_id: null,
            gendertype: null,
            Labinstructions: null,
            traceFile: '',
            rimtypeid: [''],
            lens_color: [''],
            price_retail: '',
            order_status_id: this.orderService.orderStatusGlobal,
            insuranceid: ''
        });
        this.upcreadonly = false;
    }
    /**
     * Upcs enter event
     * @param event for getting keypress values
     */
    upcEnterEvent(event) {
        if (event.keyCode === 13) {
            this.onchange(event);
        }
    }

    selectInventoryBar() {
        //this.app.enableSidebar = true;
        this.selectInventorySidebar = true;
    }
    Loadmasters() {
        this.eyedetails = [
            { label: 'Select Eye', value: 'Select Eye' },
            { label: 'OU', value: 'OU' },
            { label: 'OD', value: 'OD' },
            { label: 'OS', value: 'OS' }
        ];
        this.LoadColors();
        this.LoadLensColor();
        this.getItemName();
        this.LoadMaterialType();
        this.LoadSource();
        this.LoadBrands();
        this.loadUsage();
        this.LoadLensColor();

    }

    /**
     * Loads usage
     * @param {Array} framesUsage for binding dropdown data
     * @returns {object}  for dropdown data binding
     */
    loadUsage() {
        this.framesUsage = this.dropdownService.framesUsage;
        if (this.utility.getObjectLength(this.framesUsage) === 0) {
            this.orderService.getUsage().subscribe((values: FrameUsage) => {
                try {
                    this.framesUsage.push({ Id: '', Name: 'Select Usage' });
                    for (let i = 0; i <= values.data.length - 1; i++) {
                        this.framesUsage.push({ Id: values.data[i].id, Name: values.data[i].usagetype });
                    }
                    this.dropdownService.framesUsage = this.framesUsage;
                } catch (error) {
                    this.errorService.syntaxErrors(error);
                }


            });
        }
    }

    /**
     * Loads source
     * @param {Array} source for binding dropdown data
     * @returns {object}  for dropdown data binding
     */
    LoadSource() {
        let getsourcedata: any = [];
        this.source = [];
        this.DataSet = [];
        this.source = this.orderService.ordersource;
        if (this.utility.getObjectLength(this.source) === 0) {
            this.source.push({ Id: '', Name: 'Select Source' });
            this.orderService.getSourceMaster('').subscribe(
                data => {
                    try {
                        getsourcedata = data;
                        this.DataSet = getsourcedata.data;
                        this.DataSet.forEach(item => {
                            if (!this.source.find(a => a.Name === item.sourcename)) {
                                this.source.push({ Id: item.id, Name: item.sourcename });
                            }
                        });
                        this.orderService.ordersource = this.source;
                    } catch (error) {
                        this.errorService.syntaxErrors(error);
                    }


                });
        }
    }
    /**
     * Filters pay load for name item list
     */
    filterPayLoad() {
        this.nameFilterData = {
            filter: [
                {
                    field: 'module_type_id',
                    operator: '=',
                    value: '1'
                },
            ],
            sort: [
                {
                    field: 'id',
                    order: 'DESC'
                }
            ]
        };
    }

    /**
     * Gets item namedata for the selection of the item to order
     */
    getItemName() {
        this.filterPayLoad();
        this.orderService.getUpcItemsGridData(this.nameFilterData).subscribe((resp: InventoryModel) => {
            try {
                this.framename = [];
                this.framename.push({id: '', name: 'Select Frame Name'});
                for (let i = 0; i <= resp.data.length - 1; i++) {
                    this.framename.push(resp.data[i]);
                }
            } catch (error) {
                this.errorService.syntaxErrors(error);
            }
        });
    }
    onframecolorchangeevent(event) {
        const colid = event.target.value;
        console.log(colid);
        const values = this.copycolor.filter(d => d.Id === colid);
        if (values.length > 0 || colid !== '') {
            const colorid = values[0].colorCode;
            this.OrderForm.controls.color_code.patchValue(colorid);
        } else {
            this.OrderForm.controls.color_code.patchValue('');
        }
    }
    /**
     * Loads colors
     * @returns frame color details
     */
    LoadColors() {
        this.frameColor = this.dropdownService.colors;
        if (this.utility.getObjectLength(this.frameColor) === 0) {
            this.dropdownService.getColors().subscribe(
                (values: FrameColorData) => {
                    try {
                        const dropData = values.data;
                        this.frameColor.push({ Id: '', Name: 'Select Color' });
                        for (let i = 0; i <= dropData.length - 1; i++) {

                            this.frameColor.push({ Id: dropData[i].id, Name: dropData[i].color_name });
                        }
                        this.dropdownService.colors = this.frameColor;
                    } catch (error) {
                        this.errorService.syntaxErrors(error);
                    }
                });
        }
        this.OrderForm.controls.color_id.patchValue('');
        this.OrderForm.controls.color_code.patchValue('');

        // this.selectedFrameColorId = "null";
        // this.selectedColid = "null";
        // this.selectedlenscolorId = "null";
    }


    /**
     * Loads frame shapes
     * @param {Array} frameshapes for binding dropdown data 
     * @returns {object}  for dropdown data binding
     */
    LoadFrameShapes() {
        let frameshapesdata: any = [];
        this.frameShapes = [];
        this.frameShapes = this.dropdownService.frameShapes;
        if (this.utility.getObjectLength(this.frameShapes) === 0) {
            this.dropdownService.getFrameShapes().subscribe(
                data => {
                    try {
                        frameshapesdata = data;
                        this.frameShapeslist = frameshapesdata.result.Optical.FrameShapes;
                        this.frameShapes.push({ Id: '', Name: 'Select Shape' });
                        for (let i = 0; i <= this.frameShapeslist.length - 1; i++) {

                            if (!this.frameShapes.find(a => a.Name === this.frameShapeslist[i].Name)) {
                                this.frameShapes.push({ Id: this.frameShapeslist[i].Id, Name: this.frameShapeslist[i].Name });
                            }
                        }
                        this.dropdownService.frameShapes = this.frameShapes;
                    } catch (error) {
                        this.errorService.syntaxErrors(error);
                    }
                });
        }

    }

    /**
     * Loads frame type
     * @param {Array} frameType for binding dropdown data
     * @returns {object}  for dropdown data binding
     */
    LoadFrameType() {
        let frametypedata: any = [];
        this.frameTypes = [];
        this.frameTypes = this.dropdownService.frameTypes;
        if (this.utility.getObjectLength(this.frameTypes) === 0) {
            this.dropdownService.getFrameTypes().subscribe(
                data => {
                    try {
                        frametypedata = data;
                        this.frameTypeslist = frametypedata.result.Optical.FrameTypes;
                        for (let i = 0; i <= this.frameTypeslist.length - 1; i++) {
                            if (!this.frameTypes.find(a => a.Name === this.frameTypeslist[i].Name)) {
                                this.frameTypes.push({ Id: this.frameTypeslist[i].Id, Name: this.frameTypeslist[i].Name });
                            }
                        }
                        this.dropdownService.frameTypes = this.frameTypes;
                    } catch (error) {
                        this.errorService.syntaxErrors(error);
                    }
                });
        }
    }


    /**
     * Loads material type
     *  @param {Array} materials for binding dropdown data
     * @returns {object}  for dropdown data binding
     */
    LoadMaterialType() {
        this.materials = this.dropdownService.framesmaterials;
        if (this.utility.getObjectLength(this.materials) === 0) {
            this.dropdownService.getFrameMaterial().subscribe((values: Material[]) => {
                try {
                    const dropData  = values;
                    this.materials.push({id: '', materialname: 'Select Material'});
                    for (let i = 0; i <= dropData.length - 1; i++) {
                        dropData[i].id = dropData[i].id.toString();
                        this.materials.push(dropData[i]);
                    }
                    this.dropdownService.framesmaterials = this.materials;
                } catch (error) {
                    this.errorService.syntaxErrors(error);
                }
            });
        }

    }
    /**
     * Loads rim type
     * @param {Array} framesRimType for binding dropdown data
     * @returns {object}  for dropdown data binding
     */
    loadRimType() {
        this.framesRimType = this.dropdownService.framesRimType;
        if (this.utility.getObjectLength(this.framesRimType) === 0) {
            this.dropdownService.getRimType().subscribe((values: RimType[]) => {
                try {
                    this.framesRimType.push({ Id: '', Name: 'Select Rim Type', ColorCode: '' });
                    for (let i = 0; i <= values.length - 1; i++) {
                        this.framesRimType.push({ Id: values[i].id, Name: values[i].rimtype, ColorCode: values[i].colorcode });
                    }
                    this.dropdownService.rimType = this.framesRimType;
                } catch (error) {
                    this.errorService.syntaxErrors(error);
                }
            });
        }
    }




    // On the Name selection dropdown the framecolors,shape and frametype need to be selected
    onNameSelected(event) {
        this.selectednameid = '';
        const nameid = event.target.value;
        console.log(nameid);

        // this.orderService.FrameNAMEID.next(nameid)


        this.selectednameid = nameid;
        const values = this.copynames.filter(d => d.itemId === nameid);

        if (values.length > 0 || this.selectednameid !== '') {
            const data = {
                UPC: values[0].UPC,
                Name: values[0].Name,
                ModuleTypeId: 1,
                uniqueid: '1'
            };
            // on change upc and the itemid
            this.OrderForm.controls.upc_code.patchValue(values[0].UPC);
            this.OrderForm.controls.item_id_temp.patchValue(nameid);
            // this.orderService.FrameNAME.next(values[0].Name);
            this.orderService.FrameNAMEID.next(data);
            this.orderService.FrameNAME.next(nameid);


            let colorid = values[0].ColorId;
            const frameid = values[0].FrameTypeId;
            let shapeiddetails = values[0].ShapeId;

            // this.selectedframetypeid = frameid == "" ? frameid = "null" : frameid;
            //  this.selectedShapeId = shapeiddetails == "" ? shapeiddetails = "0" : shapeiddetails;
            this.OrderForm.controls.shape_id.patchValue(shapeiddetails === '' ? shapeiddetails = '' : shapeiddetails);
            // this.selectedFrameColorId = colorid == "" ? colorid = "null" : colorid;
            this.OrderForm.controls.color_id.patchValue(colorid === '' ? colorid = '' : colorid);
            // this.selectedColid = colorid == "" ? colorid = "null" : colorid;
            // this.OrderForm.controls['color_code'].patchValue(colorid == "" ? colorid = "" : colorid)
            // this.selectedlenscolorId = colorid == "" ? colorid = "null" : colorid;

        } else {
            this.CleardropdownValues();
        }
    }


    onchange(event) {


        let framesdata: any = [];
        // if (event.target.name == "manufacturer") {
        //     this.selecteddmanufactureId = "";
        //     this.selecteddmanufactureId = event.target.value;
        // }
        // if (this.selecteddmanufactureId == "0" || this.selecteddmanufactureId == "null") {
        //     this.selecteddmanufactureId = "";
        // }

        // if (event.target.name == "brand") {
        //     this.selectedbrandid = ""
        //     this.selectedbrandid = event.target.value;

        // }
        // if (this.selectedbrandid == "0" || this.selectedbrandid == "null") {
        //     this.selectedbrandid = "";
        // }
        if (event.target.name === 'upc') {
            this.selectedupc = event.target.value;
            this.orderService.getUpcItemsGridData(this.upcFilterPayLoad(this.selectedupc)).subscribe(
                (data: InventoryModel) => {
                    framesdata = data.data;
                    let condCheck = false;
                    framesdata.forEach(element => {
                        if (this.OrderForm.controls.upc_code.value === element.upc_code) {
                            condCheck = true;
                            this.FrameUpcData(element);
                        }
                        if (condCheck === false) {
                            this.OrderForm.controls.manufacturer_id.patchValue('');
                            this.OrderForm.controls.color_id.patchValue('');
                            this.OrderForm.controls.brand_id.patchValue('');
                            this.OrderForm.controls.shape_id.patchValue('');
                            this.OrderForm.controls.a.patchValue('');
                            this.OrderForm.controls.b.patchValue('');
                            this.OrderForm.controls.ed.patchValue('');
                            this.OrderForm.controls.dbl.patchValue('');
                            this.OrderForm.controls.temple.patchValue('');
                            this.OrderForm.controls.bridge.patchValue('');
                            this.OrderForm.controls.fpd.patchValue('');
                            this.OrderForm.controls.item_id.patchValue('');
                            this.orderService.FrameNAMEID.next('');
                            this.orderService.FrameNAME.next('');
                        }
                    });
                    const dataSet = {
                        UPC: this.OrderForm.controls.upc_code.value,
                        Name: this.OrderForm.controls.item_name.value,
                        ModuleTypeId: 1,
                        uniqueid: '1'
                    };
                    this.orderService.FrameNAMEID.next(dataSet);
                    this.orderService.FrameNAME.next(this.OrderForm.controls.item_id.value);
                });
        }
        // this.LoadFrameName(this.selectedupc, "", "", "", "200000");
        //  this.OrderForm.controls['item_id'].patchValue("")
        this.CleardropdownValues();

    }

    CleardropdownValues() {
        this.OrderForm.controls.color_id.patchValue('');
        this.OrderForm.controls.color_code.patchValue('');
        this.OrderForm.controls.shape_id.patchValue('');

        // this.selectedFrameColorId = "null";
        // this.selectedColid = "null";
        // this.selectedlenscolorId = "null";
        // this.selectedframetypeid = "null";
        // this.selectedShapeId = "0";
    }
    OrderframeChange() {
        if (this.orderService.orderSaveUpdateStatus === false) {
            this.orderService.orderFormChangeStatus = false;
        }
        if (this.LocationId != "" && this.OrderForm.controls['module_type_id'].value != "" &&
            this.app.patientId != null) {
            this.orderService.orderFrameSave.next(false)
        } else {
            this.orderService.orderFrameSave.next(true)
        }
    }
    LoadManufacturers() {
        this.Manufacturers = this.dropdownService.Manufactures;
        if (this.utility.getObjectLength(this.Manufacturers) === 0) {
            const reqbody = {
                filter: [
                    {
                        field: 'manufacturer_name',
                        operator: 'LIKE',
                        value: '%%'
                    }
                ]
            };
            this.dropdownService.manufacturesDropDown(reqbody).subscribe(
                (values: ManufacturerDropData) => {
                    try {
                        const dropData = values.data;
                        this.Manufacturers.push({ Id: '', Name: 'Select Manufacturer' });
                        for (let i = 0; i <= dropData.length - 1; i++) {
                            this.Manufacturers.push({ Id: dropData[i].id, Name: dropData[i].manufacturer_name });
                        }
                        this.dropdownService.Manufactures = this.Manufacturers;
                    } catch (error) {
                        this.errorService.syntaxErrors(error);
                    }
                });
        }
    }
    LoadBrands() {
        this.Brands = this.dropdownService.brands;
        if (this.utility.getObjectLength(this.Brands) === 0) {
            this.dropdownService.getBrands().subscribe(
                (values: FrameBrands) => {
                    try {
                        const dropData = values.data;
                        this.Brands.push({ Id: '', Name: 'Select Brand' });
                        for (let i = 0; i <= dropData.length - 1; i++) {
                            this.Brands.push({ Id: dropData[i].id, Name: dropData[i].frame_source });
                        }
                        this.dropdownService.brands = this.Brands;
                    } catch (error) {
                        this.errorService.syntaxErrors(error);
                    }
                });
        }
    }
    /**
     * Upcs filter pay load
     * @returns for based on the upc code getting the order item details
     */
    upcFilterPayLoad(upc) {
        this.upcfilter = {
            filter: [
                {
                    field: 'module_type_id',
                    operator: '=',
                    value: '1'
                },
                {
                    field: 'upc_code',
                    operator: '=',
                    value: upc,
                }
            ],
        };
        return this.upcfilter;
    }
       /**
        * Loads lens color
        * @returns lenscolors data
        */
    LoadLensColor() {
        this.lenscolor = this.dropdownService.lensColors;
        if (this.utility.getObjectLength(this.lenscolor) === 0) {
            try {
                this.dropdownService.getFrameLenscolor().subscribe((values: LensColorMaster) => {
                    const dropData = values.data;
                    this.lenscolor.push({ Id: '', Name: 'Select Lens Color' });
                    for (let i = 0; i <= dropData.length - 1; i++) {
                        {
                            this.lenscolor.push({ Id: dropData[i].id, Name: dropData[i].colorname });
                        }
                        this.dropdownService.lensColors = this.lenscolor;
                    };
                });
            } catch (error) {
                this.errorService.syntaxErrors(error);
            }

        }
    }
}
