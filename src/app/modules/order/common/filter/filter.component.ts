import { Component, OnInit, OnDestroy } from '@angular/core';
// import { DropdownsPipe } from 'src/app/shared/pipes/dropdowns.pipe';
import { FormBuilder } from '@angular/forms';
// import { OrderSearchService } from '../../ConnectorEngine/services/order.service';
import { Router } from '@angular/router';
import { SelectItem } from 'primeng/primeng';
import { Subscription } from 'rxjs';
import { AppService, ErrorService } from '@app/core';
import { OrderService } from '@app/core/services/order/order.service';
import { DropdownService } from '@app/shared/services/dropdown.service';
import { RxService } from '@services/rx/rx.service';
import { UtilityService } from '@app/shared/services/utility.service';
import { OrderHistory, OrderFilter, HistoryData, History, Event, Ordertype, SearchFilter } from '@app/core/models/order/order.model';
import { ContactlensService } from '@app/core/services/order/contactlens.service';
import { LocationsMaster } from '@app/core/models/masterDropDown.model';
// import { StateInstanceService } from 'src/app/shared/state-instance.service';


@Component({
    selector: 'app-order-filter',
    templateUrl: 'filter.component.html'
})
export class OrderFilterComponent implements OnInit, OnDestroy {
    OrderSearchFilterValue: Subscription;
    Ordersearchsubscription: Subscription;
    isEnabledOption = true;
    orderEncounterId: Subscription;
    encounter_id: any;
    public physicianData: any = '';
    public locations: any = [];
    public historydata: any[];
    public patientid = '';
    LocationWarning = false;

    /**
     * Location model of order filter component for location dropdown model
     */
    LocationModel: any = this.app.locationId.toString();

    /**
     * Order model of this for the assign in the orderid and if new order assigned New Order order filter component
     */
    orderModel: string;
    orderId = null;
    orderformUpdateSubscription: Subscription;
    subscriptionSaveNew: Subscription;
    subscriptionType: Subscription;
    subscriptionOrderCancle: Subscription;
    /**
     * Order date of order filter component
     * for displaying order date by selected order
     */
    orderDate: any;
    SelectTypeModel = null;
    // TypeDropdown = [{ id: '1', type: 'Frame Only' }, { id: '2', type: 'Spectacle Lens' },
    // { id: '3', type: 'Contact Lens' }, { id: '4', type: 'Other' }];
    public orderType: object;
    public physicians: SelectItem[];
    public selectedphyiscianId: any;
    public physicianslist: any;
    Rxdata: any[];
    public lastOrder: number;
    public physicianId: any;
    public locationId: any;
    public types: any;
    Ordertype: string;
    public historycopy: any;

    /**
     * Order  of order filter component
     */
    order: string;
    /**
     * Filtered order of order filter component
     */
    // filteredOrder: any;
    /**
     * Temp data of order filter component
     * for storing responce data from orderHistory data
     */
    temp_Data: Array<History>;
    /**
     * Historys  of order filter component
     * for storing the history dropdown data
     */
    orderHistorys: Array<HistoryData>;
    /**
     * Filtered history of order filter component
     * for storing the history dropdown data
     */
    filteredHistory: Array<HistoryData>;
    /**
     * History  of order filter component
     * ngmodel for order history dropdown
     */
    history: HistoryData;

    /**
     * History value of order filter component
     * for storing the saved and selected order id
     */
    history_Value: HistoryData;
    /**
     * Creates an instance of order filter component.
     * @param app global service for instanceses
     * @param router for nvigation
     * @param rxService for rx methods
     * @param _fb  for formbuilder
     * @param spectcalelensServicee for spectacle lens methods
     * @param orderService  common service for complete order
     * @param dropdownService for common dropdowns
     * @param utility for getObjectLength
     * @param error for syntaxerrors
     */
    constructor(
        private _fb: FormBuilder,
        public app: AppService,
        private router: Router,
        private orderService: OrderService,
        private dropdownService: DropdownService,
        private errorService: ErrorService,
        private rxService: RxService,
        private utility: UtilityService,
        private contactlensService: ContactlensService) {
        this.subscriptionOrderCancle = orderService.OrderCancleValue$.subscribe((data: string) => {
            this.history = { Id: 'New Order', Name: 'New Order', ordertypeID: 0 };
            this.orderId = '';
            this.orderDate = new Date();
            this.LocationModel = parseInt(this.app.locationId, 10);
        });
        this.subscriptionSaveNew = orderService.orderSaveNew$.subscribe((data: string) => {
            this.history = { Id: 'New Order', Name: 'New Order', ordertypeID: 0 };
            this.orderId = '';
            this.orderService.orderSaveUpdateStatus = true;
        });
        this.orderformUpdateSubscription = orderService.orderFormUpdate$.subscribe((data: string) => {
            const Id = this.orderService.orderId;
            this.orderModel = Id;
            this.orderId = Id;
            this.gettingSavedOrderValues(this.orderModel);

        });
        this.subscriptionType = orderService.orderSelectTypeobj$.subscribe(data => {
            this.SelectTypeModel = data;
        });

        this.OrderSearchFilterValue = this.orderService.OrderSearchFilterValue$.subscribe((data: SearchFilter) => {
            if (data.OrderType === 4) {
                this.contactlensService.Disablehardcontactcomponent.next(true);
            }
            this.orderModel = data.Id;
            this.SelectTypeModel = data.OrderType;
            this.isEnabledOption = false;
            this.gettingSavedOrderValues(data.Id);
            // this.orderSelect(this.OrderModel);
        });
    }
    /**
     * on init for initiliazing the methods
     */
    ngOnInit(): void {
        this.history = { Id: 'New Order', Name: 'New Order', ordertypeID: 0 };
        const locationdetails = this.app.locationId;
        this.LocationModel = locationdetails;
        this.patientid = this.app.patientId;
        this.LoadPhysicians();
        this.orderDate = new Date();
        this.orderService.orderDateobj.next(this.orderDate);
        this.errorService.msgs = [];
        if (this.app.patientId === 0) {
            this.errorService.displayError({
                severity: 'warn',
                summary: 'Warning Message', detail: 'Please select patient'
            });

            // this.msgs.push({ severity: 'error', summary: 'warning Message', detail: 'Please select Patient' });


        }
        if (this.SelectTypeModel == null) {
            this.isEnabledOption = true;
        } else {
            this.isEnabledOption = false;
        }
        this.LoadLocations();
        this.LoadOrderTypeDetails();
        this.LocationModel = parseInt(this.app.locationId, 10);
        //  alert(this._StateInstanceService.UsernameLogged);
    }
    LoadPhysicians() {
        this.physicians = [];
        this.physicianslist = [];

        const filtervalue = {
            filter: [{
                field: 'user_type', operator: '=', value: '1'
            }], sort: [{ field: 'id', order: 'ASC' }]
        };

        this.physicians.push({ value: '', label: 'Select Physician' });
        this.orderService.getPhysicianFilter('', filtervalue).subscribe(


            (data: any) => {
                this.physicianslist = data.data;
                this.physicianslist.forEach(item => {
                    if (!this.physicians.find(a => a.label === item.username)) {
                        this.physicians.push({ value: item.id, label: item.username });
                    }

                });
                const phys = this.app.userName;
                // localStorage.getItem('username');
                const phyid = this.physicians.filter(x => x.label === phys);
                if (phyid.length !== 0) {
                    this.selectedphyiscianId = phyid[0].value;
                    this.orderService.physicianID = phyid[0].value;

                }
            });
    }

    ngOnDestroy(): void {
        this.orderformUpdateSubscription.unsubscribe();
        this.subscriptionType.unsubscribe();
        this.subscriptionSaveNew.unsubscribe();
        // this.Ordersearchsubscription.unsubscribe();
        this.OrderSearchFilterValue.unsubscribe();
        this.subscriptionOrderCancle.unsubscribe();
    }

    /**
     * Loads locations
     *  @param {Array} locations for binding dropdown data
     * @returns {object}  for dropdown data binding
     */
    LoadLocations() {
        this.locations = this.dropdownService.locationsData;
        if (this.utility.getObjectLength(this.locations) === 0) {
            this.dropdownService.getLocationsDropData().subscribe(
                (data: LocationsMaster[]) => {
                    try {
                        this.locations.push({ id: '', name: 'Select Location' });
                        for (let i = 0; i <= data.length - 1; i++) {
                            this.locations.push(data[i]);
                        }
                        this.dropdownService.locationsData = this.locations;
                    } catch (error) {
                        this.errorService.syntaxErrors(error);
                    }
                }
            );
        }

    }
    /**
     * Searchs order history
     * @param event getting enterd events on history field
     */
    searchOrderHistory(event: Event) {
        if (this.app.patientId != 0) {
            let query = event.query;
            let mdata = [];
            this.order = '';
            // this.filteredOrder = [];
            if (query === 'New Order') {
                query = '';
            }
            const reqbody = {
                filter: [
                    { field: 'patient_id', operator: '=', value: this.app.patientId },
                    { field: 'id', operator: 'LIKE', value: '%' + query + '%' },
                    { field: 'ordertypeid', operator: '=', value: this.SelectTypeModel }
                ]
            };
            const resultData = reqbody.filter.filter(p => p.value !== '%%' && p.value !== '% %' && p.value !== null);
            const payLoad: OrderFilter = {
                filter: resultData,
                sort: [{ field: 'id', order: 'DESC' }]
            }
            this.app.getOrderBySearchFilter(payLoad).subscribe(
                (res: OrderHistory) => {
                    mdata = res.data;
                    this.temp_Data = mdata;
                    this.orderHistorys = [];
                    this.temp_Data.forEach(item => {
                        this.historycopy = item;
                        this.switchCaeData(item);
                        this.orderHistorys.push({
                            Id: item.id.toString(), Name: item.entered_date + ' ' + '#' + item.id + '-' +
                                this.Ordertype, ordertypeID: item.ordertypeid
                        });
                    });
                    this.filteredHistory = this.filteredOrdersHistoryLoad(query, this.orderHistorys);
                    this.filteredHistory.unshift({ Id: 'New Order', Name: 'New Order', ordertypeID: 0 });
                    if (this.history_Value) {
                        let condForSearch = true;
                        this.filteredHistory.forEach(value => {
                            if (value.Id === this.history_Value.Id) {
                                condForSearch = false;
                            }
                        })
                        if (condForSearch === true) {
                            this.filteredHistory.push(this.history_Value);
                        }
                    }
                },
                error => {
                    this.filteredHistory = [];
                }
            );
        } else {
            this.filteredHistory = [];
        }


    }

    // Filtered Order history data
    filteredOrdersHistoryLoad(query, data_m: any[]): any[] {
        const filtered: any[] = [];
        if (data_m.length > 0) {
            for (let i = 0; i < data_m.length; i++) {
                const data = data_m[i];
                filtered.push(data);
            }
        }
        return filtered;
    }



    /**
     * Types dropdown change
     * @param data for getting dropdown change event values.
     */
    TypeDropdownChange(data: string) {
        this.app.bannerOrderId = null;
        if (this.SelectTypeModel === 1 || this.SelectTypeModel === 2) {
            this.router.navigateByUrl('/order/frame');
        }
        if (this.SelectTypeModel === 7 || this.SelectTypeModel === 3) {
            this.router.navigateByUrl('/order/spectacle-lens');
        }
        if (this.SelectTypeModel === 4 || this.SelectTypeModel === 5) {
            this.router.navigateByUrl('/order/contact-lens');
        }
        if (this.SelectTypeModel === 6) {
            this.router.navigateByUrl('/order/other-details');
        }
    }

    locationChange() {
        console.log(this.LocationModel);
        if (this.LocationModel !== '') {
            this.LocationWarning = false;
            this.orderService.orderLocation.next(this.LocationModel);
        } else {
            this.LocationWarning = true;
            this.orderService.orderLocation.next(this.LocationModel);
        }
    }


    onSelectType(event) {
        if (event) {
            this.selectedphyiscianId = event.value;
            this.orderService.physicianID = this.selectedphyiscianId;
            this.orderService.lensPhysicianId.next(event.value);
            this.orderDate = new Date();
            this.orderService.orderDateobj.next(this.orderDate);
        }

    }

    onDateChange(event) {
        if (event != null || event !== undefined) {
            const date = new Date(event),
                mnth = ('0' + (date.getMonth() + 1)).slice(-2),
                day = ('0' + date.getDate()).slice(-2);
            const dateanew = [mnth, day, date.getFullYear()].join('/').toString();
            this.orderDate = event;
            this.orderService.orderDateobj.next(dateanew);
        }
        // if (event.target.value != null || event.target.value != undefined) {
        //     this.orderDate = event.target.value;
        //     this.orderService.orderDateobj.next(event.target.value);
        // }

    }


    // orderSelect(data) {
    //     this.orderService.orderselection.next(this.OrderModel);
    //     this.orderId = data.target.value;
    //     //this.orderId =data.target.value == "null"  ? data.target.value="":data.target.value
    //     this.orderDate = new Date;

    // onSelectType() {
    //     this.orderService.lensPhysicianId.next(this.selectedphyiscianId);
    // }

    /**
     * Orders select
     * @param {string} orderData for getting history dropdown change functionality
     * @returns  {object} for getting saved orders data
     */
    orderSelect(orderData: any) {
        if (orderData !== 'New Order') {
            this.app.bannerOrderId = orderData;
            if (this.temp_Data.length > 0) {
                this.temp_Data.forEach(value => {
                    if (value.id.toString() === orderData.toString()) {
                        const dateOrder = value.entered_date.split('-');
                        this.orderDate = dateOrder[1] + '/' + dateOrder[2] + '/' + dateOrder[0];
                    }
                });
            }
        } else {
            this.orderDate = new Date();
            this.app.bannerOrderId = null;
        }
        this.orderService.orderId = orderData;
        this.orderService.discountObject = '';
        this.orderService.discountChargeList = [];
        this.orderService.orderId = orderData;
        if (this.SelectTypeModel === 5 || this.SelectTypeModel === 4) {
            this.orderService.disablePaymentButton = false;
            this.orderId = orderData;
            // this.selectedphyiscianId = this.orderService.physicianID;
            if (this.SelectTypeModel === 5) {
                this.orderService.orderSoftContactLens.next(this.history.Id);
            }
            if (this.SelectTypeModel === 4) {
                if (this.history.Id !== 'New Order') {
                    this.contactlensService.HardContactbyorderID.next(this.history.Id);
                    this.contactlensService.savecontactlensupdate = 'Hard Contact Lens';
                } else {
                    // this.contactlensService.Disablehardcontactcomponent.next(true);
                    this.contactlensService.HardContactbyorderID.next(this.history.Id);
                }
            }
            this.orderService.getOrderStatusData(this.orderId).subscribe(data => {
                this.locationId = data;
                this.LocationModel = parseInt(this.locationId.loc_id, 10);
                this.encounter_id = this.locationId.order_enc_id;
                this.orderService.orderEncounterId.next(this.encounter_id);
                this.orderService.orderLocation.next(this.LocationModel);
                this.orderService.lensPhysicianId.next('');

                const filter = {
                    filter: [{
                        field: 'order_id',
                        operator: '=',
                        value: this.orderService.orderId
                    }], sort: [
                        {
                            field: 'id',
                            order: 'ASC'
                        }]
                };

                this.orderService.getPatientrxContactLens(filter, this.app.patientId).subscribe((data2: any) => {
                    this.physicianData = data2.data[0];
                    this.lastOrder = this.physicianData.length - 1;
                    this.orderService.physicianID = this.physicianData.physician_id;
                    this.selectedphyiscianId = this.orderService.physicianID; // this.physicianData[this.lastOrder].physician_id;
                    this.orderService.lensPhysicianId.next(this.selectedphyiscianId);
                });
            });
            if (orderData === 'New Order') {
                // this.orderService.disabledDiscount = ''
                this.orderService.disabledModify = 'disabled';
                // this.orderId = '';
                this.LocationModel = parseInt(this.app.locationId, 10);
                // this.selectedphyiscianId = '';
                this.orderService.disablePaymentButton = true;
            }
            if (orderData !== 'New Order') {
                // this.orderService.disabledDiscount = 'disabled'
                this.orderService.disabledModify = '';
                this.orderService.disablePaymentButton = false;
                this.orderService.GetotherlabDetails(orderData).subscribe((labdetails: any) => {
                    console.log(orderData);
                    // alert(labdetails['order_status_id']);
                    this.orderService.orderStatusGlobal = labdetails.order_status_id;
                    // alert(this.orderService.orderStatusGlobal);
                    if (labdetails.order_status_id === '') {
                        this.orderService.disableSavePayBtn = false;
                    }
                    if (labdetails.order_status_id !== '9') {
                        this.orderService.disableSavePayBtn = true;
                    } else {
                        this.orderService.disableSavePayBtn = false;
                    }
                });
            } else {
                // this.orderService.disabledDiscount = ''
                this.orderService.disabledModify = 'disabled';
                this.orderService.disableSavePayBtn = false;
                // this.orderService.orderStatusGlobal = '';
            }
            return;
        }
        this.orderService.lensPopupOrderCondi = true;

        if (orderData !== 'New Order') {
            // this.orderService.disabledDiscount = 'disabled'
            this.orderService.disabledModify = '';
            this.orderService.disablePaymentButton = false;
            this.orderService.GetotherlabDetails(orderData).subscribe((labdetails: any) => {
                console.log(orderData);
                this.orderService.orderStatusGlobal = labdetails.order_status_id;
                if (labdetails.order_status_id === '') {
                    this.orderService.disableSavePayBtn = false;
                }
                if (labdetails.order_status_id !== '9') {
                    this.orderService.disableSavePayBtn = true;
                } else {
                    this.orderService.disableSavePayBtn = false;
                }
                this.orderService.orderselection.next(this.history.Id);
            });
        } else {

            this.orderService.disableSavePayBtn = false;
            // this.orderService.disabledDiscount = ''
            this.orderService.disabledModify = 'disabled';
            // this.orderService.orderStatusGlobal = '';
            this.orderService.orderselection.next(this.history.Id);
            // this.history_Value = '';
        }


        if (orderData === 'New Order') {
            this.orderId = '';
            this.LocationModel = parseInt(this.app.locationId, 10);
            this.orderService.disablePaymentButton = true;
        } else {
            this.orderId = orderData;
        }
        if (this.orderId === '') {
            this.orderService.orderSaveUpdateStatus = true;
        } else {
            this.orderService.orderSaveUpdateStatus = false;
        }
        const patientId = this.app.patientId;
        this.rxService.getLensSelection(patientId).subscribe(data => {
            this.physicianData = data['data'];
            // this.lastOrder = this.physicianData.length - 1;
            this.selectedphyiscianId = this.physicianData[0].physician_id;
            this.orderService.lensPhysicianId.next(this.selectedphyiscianId);
        });
        if (this.orderId !== '' || this.orderId !== 'New Order') {
            this.orderService.getOrderStatusData(this.orderId).subscribe(data => {
                this.locationId = data;
                this.LocationModel = parseInt(this.locationId.loc_id, 10);
                this.encounter_id = this.locationId.order_enc_id;
                this.orderService.orderEncounterId.next(this.encounter_id);
                this.orderService.orderLocation.next(this.LocationModel);
                this.orderService.disablePaymentButton = false;
            });
        } else {
            this.orderService.orderEncounterId.next('null');
        }


    }
    /**
     * Loads order type details
     */
    LoadOrderTypeDetails() {
        this.orderType = this.dropdownService.orderType;
        if (this.utility.getObjectLength(this.orderType) === 0) {
            this.dropdownService.loadOrderType().subscribe(
                data => {
                    try {
                        this.orderType = data;
                        this.dropdownService.orderType = this.orderType;

                    } catch (error) {
                        this.errorService.syntaxErrors(error);
                    }
                }
            );
        }
    }



    /**
     * Getting saved order values
     * @param event for getting new order id or selected order from order search
     */
    gettingSavedOrderValues(event: string) {
        // let query = event.query;
        let mdata = [];
        this.order = '';
        // this.filteredOrder = [];
        if (event === 'New Order') {
            event = '';
        }
        const reqbody: OrderFilter = {
            filter: [
                { field: 'patient_id', operator: '=', value: this.app.patientId },
                { field: 'id', operator: '=', value: event },
                { field: 'ordertypeid', operator: '=', value: this.SelectTypeModel }
            ], sort: [
                { field: 'id', order: 'DESC' }
            ]
        };
        this.app.getOrderBySearchFilter(reqbody).subscribe(
            (res: OrderHistory) => {
                mdata = res.data;
                this.temp_Data = mdata;
                this.orderHistorys = [];
                this.temp_Data.forEach(item => {
                    this.historycopy = item;
                    this.switchCaeData(item);
                    this.filteredHistory = [{
                        Id: item.id.toString(), Name: item.entered_date + ' ' + '#' + item.id + '-' +
                            this.Ordertype, ordertypeID: item.ordertypeid
                    }];
                    this.history_Value = ({
                        Id: item.id.toString(), Name: item.entered_date + ' ' + '#' + item.id + '-' +
                            this.Ordertype, ordertypeID: item.ordertypeid
                    });
                });
                this.history = { Id: this.history_Value.Id, Name: this.history_Value.Name, ordertypeID: this.history_Value.ordertypeID };
                this.orderSelect(this.orderModel);
            },
            error => {
                this.filteredHistory = [];
            }
        );
    }


    /**
     * Switchs cae data
     * @param item for getting the list of the orders and assigning names
     */
    switchCaeData(item: Ordertype) {
        switch (item.ordertypeid) {
            case 1: {
                this.Ordertype = 'Frame';
                break;
            }
            case 7: {
                this.Ordertype = 'Spectacle Lens';
                break;
            }
            case 4: {
                this.Ordertype = 'Hard Contact Lens';
                break;
            }
            case 5: {
                this.Ordertype = 'Soft Contact Lens';
                break;
            }
            case 6: {
                this.Ordertype = 'Other Details';
                break;
            }
        }
    }


    /**
     * Selects all data
     * @param event for getting click event values
     */
    selectAllData(event: any) {
        if (event.target.value !== undefined) {
            event.target.select();
        }
    }
}
