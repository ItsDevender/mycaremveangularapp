import { ActivatedRoute, Router } from '@angular/router';
// App Configuration Module
import { IframesService } from '@app/shared/services/iframes.service';
// Core Modules
import { Component, OnInit } from '@angular/core';



// Setting Up Component
@Component({
  selector: 'app-order-payments',
  templateUrl: './payments.component.html',
  styleUrls: ['./payments.component.css']
})
export class OrderPaymentsComponent implements OnInit {
  OrderPaymentUrl: any;
  EncounterId: number;
  params: any;
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private iframeService: IframesService) {
    // this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  }

  // Get Parameters on Intialize
  ngOnInit() {
    this.params = this.route
      .queryParams
      .subscribe(params => {
        // Defaults to 0 if no query param provided.
        this.EncounterId = +params['encounter_id'] || 0;
        this.OrderPaymentUrl = this.iframeService.routeUrl('OrderPaymentUrl', true, '?eId=' +
          this.EncounterId + '&del_charge_list_id=0&tabvalue=Enter_Charges&show_load=yes');
      });

  }

  // Unsubcribe Service
  // tslint:disable-next-line:use-life-cycle-interface
  ngOnDestroy() {
    this.params.unsubscribe();
  }

  // Loading
  onLoad() {
    this.iframeService.resizeIframe();
    $('.loader-iframe').fadeOut();
  }

}
