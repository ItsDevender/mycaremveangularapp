import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { OrderService } from '@services/order/order.service';
import { ErrorService } from '@app/core';


@Component({
    selector: 'app-add-discount',
    templateUrl: 'add-discount.component.html'
})
export class AddOtherDiscountComponent implements OnInit {

    /**
     * Discount reason for the post request
     */
    discountReason: any = '';

    /**
     * Discount reason list 
     */
    discountReasonList: any;
    discountID: any;
    disableDelete: boolean = true;

    /**
     * Creates an instance of add other discount component.
     * @param orderService 
     * @param error error handling
     */
    constructor(
        private orderService: OrderService,
        private error: ErrorService) {

    }

    ngOnInit() {

        this.loadreasons();
    }

    /**
     * Loadreasons avalible
     */
    loadreasons() {
        this.orderService.getDiscountReasons().subscribe(data => {
            this.discountReasonList = data['data'];
        });
    }

    /**
     * Adds new reason add of the reason
     */
    addNewReason() {
        const discount = {
            discount_reason: this.discountReason
        };
        this.orderService.postDiscountReason(discount).subscribe(resp => {
            try {
                const data = resp;
                this.error.displayError({
                    severity: 'success',
                    summary: 'Discount Reason',
                    detail: 'New Discount Reason added successfully'
                });
                this.discountReason = ''
                this.loadreasons();
            } catch (error) {
                this.error.syntaxErrors(error);
            }

        });

    }

    /**
     * Determines whether row select on seleted row
     * @param {object} data 
     */
    onRowSelect(data) {
        this.discountID = data.data.id;
        this.disableDelete = false;
    }

    /**
     * Deletes reason deleting the reasons
     */
    deleteReason() {
        this.orderService.deletediscountreason(this.discountID).subscribe(resp => {
            try {
                const data = resp;
                this.error.displayError({
                    severity: 'success',
                    summary: 'Discount Reason',
                    detail: 'Discount Reason deleted successfully'
                });
                this.disableDelete = true;
                this.loadreasons();
            } catch (error) {
                this.error.syntaxErrors(error);
            }
        });
    }

    onDiscountClose() {
        
    }

}




