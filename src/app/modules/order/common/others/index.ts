// parent component
export * from './other-details-tab.component';
export * from './other-details.component';
export * from './add-details/add-details.component';
export * from './lab-history/lab-history.component';
export * from './discount/discount.component';
export * from './modify/frames.component';
export * from './modify/lens-treatment.component';
export * from './modify/soft-contact.component';
export * from './modify/spectacle-lens.component';
export * from './promise-date-history/promise-date-history.component';
export * from './status-history/status-history.component';
export * from './tray-label/tray-label.component';
export * from './shipping/shipping.component';
export * from './return-reason/return-reason.component';
export * from './add-discount/add-discount.component'
