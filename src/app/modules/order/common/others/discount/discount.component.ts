import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { OrderService } from '@services/order/order.service';
import { ErrorService } from '@app/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DiscountReason } from '@app/core/models/masterDropDown.model';
import { UtilityService } from '@app/shared/services/utility.service';


@Component({
    selector: 'app-discount',
    templateUrl: 'discount.component.html'
})
export class OrderOtherDiscountComponent implements OnInit {
    // addDiscountSidebar = false;
    @Input() discountItems: any;
    @Output() discountoutput = new EventEmitter<any>();
    // @Output() discountoutputData = new EventEmitter<any>();

    /**
     * Discount reason list 
     */
    discountReasonList: Array<object>;

    /**
     * Discount type list of order other discount component
     */
    discountTypeList: any = [];

    /**
     * Show material of order other discount component
     */
    showMaterial = false;
    /**
     * Show services of order other discount component
     */
    showServices = false;

    /**
     * Show other of order other discount component
     */
    showOther = false;

    /**
     * Discount type of order other discount component
     */
    discountType: any;

    /**
     * Discount reason item of order other discount component
     */
    discountReasonItem: any;

    /**
     * Discount number of order other discount component
     */
    discountNumber: any;

    /**
     * Selected values of order other discount component
     */
    selectedValues: any = [];

    /**
     * Checkservice  of order other discount component
     */
    checkservice = false;

    /**
     * Checkother  of order other discount component
     */
    checkother = false;

    /**
     * Checkmaterial  of order other discount component
     */
    checkmaterial = false;

    /**
     * Discount form of order other discount component
     */
    discountForm: FormGroup;

    /**
     * Discount type listing of order other discount component
     */
    discountTypeListing: any = []

    /**
     * Error msg check box of order other discount component
     */
    ErrorMsgCheckBox = false;


    /**
     * Creates an instance of order other discount component.
     * @param orderService for order items
     * @param fb is formbuilder
     */
    constructor(
        private orderService: OrderService,
        private fb: FormBuilder,
        private error: ErrorService,
        private utility: UtilityService
    ) {

    }
    ngOnInit() {
        this.discountTypeListing = [{ id: '', type: 'Select Discount Type' },
        { id: 'Amount', type: 'Amount' },
        { id: 'Percentage', type: 'Percentage' }];
        for (let i = 0; i <= this.discountItems.length - 1; i++) {
            this.discountItems[i]['afterDiscountAmount'] = ''
            this.discountItems[i]['discountAmount'] = '';
            if (this.discountItems[i].module_type_id === 1 || this.discountItems[i].module_type_id === "1"
                || this.discountItems[i].module_type_id === 2 || this.discountItems[i].module_type_id === "2") {
                this.checkmaterial = true;
                this.showMaterial = true;
                // this.selectedValues.push('Material');
                if (this.selectedValues.includes('Material') === false) {
                    this.selectedValues.push('Material');
                }
            }
            if (this.discountItems[i].module_type_id === 8 || this.discountItems[i].module_type_id === "8") {
                this.showServices = true;
                this.checkservice = true;
                if (this.selectedValues.includes('Services') === false) {
                    this.selectedValues.push('Services');
                }

            }
            if (this.discountItems[i].module_type_id === 5 || this.discountItems[i].module_type_id === "5") {
                this.showOther = true;
                this.checkother = true;
                if (this.selectedValues.includes('Other') === false) {
                    this.selectedValues.push('Other');
                }

            }
        }
        this.loadreasonsdropDown();
        this.getdiscountType();
        // alert(this.discountItems);
        this.Loadformdata();
    }

    /**
     * Loadformdatas order other discount component for builder
     */
    Loadformdata() {

        this.discountForm = this.fb.group({
            discountreason: ['', [Validators.required]],
            discountTypeitem: ['', [Validators.required]],
            discountamount: ['', [Validators.required]]
        });
    }


    onDiscountClose() {
        this.discountoutput.emit('close');
    }

    /**
     * Loadreasonsdrops down is for the loading of the reasons dropdown
     */
    loadreasonsdropDown() {
        this.discountReasonList = this.orderService.discountReason;
        if (this.utility.getObjectLength(this.discountReasonList) === 0) {
            this.orderService.getDiscountReasons().subscribe((values: DiscountReason) => {
                try {
                    const dropData = values.data;
                    this.discountReasonList.push({ cas_id: '', cas_desc: 'Select Reason' });
                    for (let i = 0; i <= dropData.length - 1; i++) {
                        this.discountReasonList.push(values.data[i]);
                    }
                    this.orderService.discountReason = this.discountReasonList;
                } catch (error) {
                    this.error.syntaxErrors(error);
                }
            });
        }
    }



    /**
     * Getdiscounts type is for getting discount type
     */
    getdiscountType() {
        this.orderService.getListDiscounttype().subscribe(data => {
            this.discountTypeList = data['data'];
        });
    }

    /**
     * Discounts button click  to apply the discounts
     */
    discountButtonClick() {
        const discountObj = {
            reason: this.discountForm.controls['discountreason'].value,
            discountType: this.discountForm.controls['discountTypeitem'].value,
            discountNumber: this.discountForm.controls['discountamount'].value,
            discountOnItems: this.selectedValues

        };
        if (this.selectedValues.length !== 0) {
            this.discountoutput.emit(discountObj);
        } else {
            this.ErrorMsgCheckBox = true;
        }

    }

    /**
     * Types of items for discount
     * @param {object} event
     */
    typeOfItemsForDisc(event) {
        if (event.target.checked === true) {
            this.selectedValues.push(event.target.value);
            this.ErrorMsgCheckBox = false;
        } else if (event.target.checked === false) {
            if (this.selectedValues.length >= 1) {
                const index = this.selectedValues.indexOf(event.target.value);            // }
                this.selectedValues.splice(index, 1);
                if (this.selectedValues.length === 0) {
                    // alert("should select any of the items")
                    this.ErrorMsgCheckBox = true;
                }
            }

        }
    }

}
