import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClaimordersComponent } from './claimorders.component';

describe('ClaimordersComponent', () => {
  let component: ClaimordersComponent;
  let fixture: ComponentFixture<ClaimordersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClaimordersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClaimordersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
