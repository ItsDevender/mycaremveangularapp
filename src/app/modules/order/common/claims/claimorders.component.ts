import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { IframesService } from '@app/shared/services/iframes.service';

@Component({
  selector: 'app-claimorders',
  templateUrl: './claimorders.component.html',
  styleUrls: ['./claimorders.component.css']
})
export class ClaimordersComponent implements OnInit {

  /**
   * Encounter id of the items proceesedof claimorders component
   */
  EncounterId: number;

  /**
   * Params  of the activated route data  of claimorders component
   */
  params: Subscription;

  /**
   * Order claim url for navigation  of claimorders component
   */
  OrderClaimUrl: object;

  /**
   * Creates an instance of claimorders component.
   * @param route is activated route
   * @param iframeService for loading the iframe URL
   */
  constructor(
    private route: ActivatedRoute,
    private iframeService: IframesService
  ) { }

  ngOnInit() {
    this.params = this.route
      .queryParams
      .subscribe(params => {
        // Defaults to 0 if no query param provided.
        this.EncounterId = +params['encounter_id'] || 0;
        this.OrderClaimUrl = this.iframeService.routeUrl('ClaimWithEncounterId', true, '?encounter_id=' +
          this.EncounterId + '&del_charge_list_id=0&tabvalue=Enter_Charges&show_load=yes');
      });
  }

  /**
   * on destroy params which subscribed
   */
  ngOnDestroy() {
    this.params.unsubscribe();
  }

  // Loading
  onLoad() {
    this.iframeService.resizeIframe();
    $('.loader-iframe').fadeOut();
  }

}
