import { Component, ViewChild } from '@angular/core';
import { DatePipe } from '@angular/common';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AppService, ErrorService } from '@app/core';
import { OrderService } from '@app/core/services/order/order.service';
import { PatientSearchService } from '@app/core/services/patient/patient-search.service';
import { DropdownService } from '@app/shared/services/dropdown.service';
import { UtilityService } from '@app/shared/services/utility.service';


@Component({
  selector: 'app-order-search',
  templateUrl: './search.component.html'
})


export class OrderSearchComponent {
  ordersearchForm: FormGroup;
  orderTypes: any[];
  Ordercolumns: any[];
  orderTypesCopy: any[];
  searchDate = '';
  searchPatientId = '';
  searchOrderId = '';
  searchOrderDate = '';
  searchOrderType = '';
  searchtotalqty = '';
  searchstatus = '';
  data: any = null;
  /**
   * Insurance drop down of order search component
   */
  public insuranceDropDown = [];
  /**
   * Insurancemodel  of order search component
   */
  public insurancemodel: any = [];
  @ViewChild('dt') public dt: any;
  status: any;
  orderTypeData: object;

  constructor(
    private fb: FormBuilder,
    private orderService: OrderService,
    private datePipe: DatePipe,
    private router: Router,
    private patient: PatientSearchService,
    public app: AppService,
    private error: ErrorService,
    private dropDown: DropdownService,
    private utility: UtilityService,

  ) { }

  // tslint:disable-next-line:use-life-cycle-interface
  ngOnInit(): void {
    this.app.bannerOrderId = null;
    this.LoadFormData();
    this.getPatientInsurance();
    this.orderStatus();
    this.orderTypeDetails();
    this.Ordercolumns = [
      { field: 'patient_id', header: 'Patient#', datetime: false },
      { field: 'id', header: 'Order ID', datetime: false },
      { field: 'entered_date', header: 'Order Date', datetime: true },
      { field: 'ordertypeid', header: 'Order Type', datetime: false },
      { field: 'total_qty', header: 'Total Quantity', datetime: false },
      { field: 'order_status_id', header: 'Order Status', datetime: false }
    ];

  }


  // To load the orders dropdown
  LoadOrdersData(patientid = '', orrdernumber = '', orderDate = '') {
    this.orderTypes = [];
    this.orderTypesCopy = [];
    this.orderService.getOrderSearchMaster(orrdernumber, patientid, '', '', orderDate, orderDate, '', 1000, '', '', '').subscribe(
      (DataSet: any) => {
        this.orderTypes = DataSet.result.Optical.Orders;
        // if (this.orderTypes != null && this.orderTypes !== [] && this.orderTypes !== undefined) {
        //   for (let i = 0; i <= this.orderTypes.length - 1; i++) {
        //     this.orderTypes[i].ordertypeid = this.orderTypes[i].ordertypeid === '1' ?
        //     'Frames' : this.orderTypes[i].ordertypeid = this.orderTypes[i].ordertypeid === '2' ?
        //     'Spectacle Lens' :  this.orderTypes[i].ordertypeid = this.orderTypes[i].ordertypeid === '3' ?'Contact lens': 'Other';;
        //   }

        const map = new Map();
        for (const item of this.orderTypes) {
          if (!map.has(item.Id)) {
            map.set(item.Id, true);    // set any value to Map
            this.orderTypesCopy.push({
              Id: item.Id,
              PatientId: item.PatientId,
              EnteredDate: item.EnteredDate,
              EnteredTime: item.EnteredTime,
              ItemName: item.ItemName,
              ItemType: item.ItemType,
              LocationId: item.LocationId,
              OperatorId: item.OperatorId,
              OrderStatus: item.OrderStatus,
              TotalPrice: item.TotalPrice,
              TotalQuantity: item.TotalQuantity,
              UpcCode: item.UpcCode,
              ordertypeid: item.ordertypeid
            });
          }
        }
        this.orderTypes = [];
        this.orderTypes = this.orderTypesCopy;
      },
      error => {
      }
    );
  }

  /**
   * Gets patient insurance drop down data
   */
  getPatientInsurance() {

    // if (this.app.patientId !== 0) {
    const id = this.app.patientId;
    this.insuranceDropDown = [];
    this.dropDown.getInsuranceMaster().subscribe(data => {
      this.insuranceDropDown = data['data'];
      // this.insurancemodel.forEach(element => {
      //   this.insuranceDropDown.push({ Id: element.id, Name: element.type + '-' + element.plan_name });
      // });
    });
    // }
  }
  onBlurMethod(e) {
    if (e.value === null || e.value === undefined) {
      this.orderTypes = this.orderTypesCopy;
      return;
    }
  }

  getorderbyDate(datedetails) {
    if (datedetails != null || datedetails !== undefined) {
      const datedetail = this.datePipe.transform(datedetails, 'yyyy-MM-dd');
      const value = this.orderTypes.filter(d => d.EnteredDate === datedetail);
      this.orderTypes = value;
    } else {
      this.orderTypes = this.orderTypesCopy;
    }
  }


  /**
   * Loads form data initilize form group
   */
  LoadFormData() {
    this.ordersearchForm = this.fb.group({
      patientId: '',
      orderId: '',
      orderDate: '',
      orderType: '',

      lname: '',
      fname: '',
      phoneHome: '',
      phoneWork: '',
      providerID: '',
      patient_id: '',
      id: '',
      entered_date: '',
      order_status_id: '',
      loc_id: '',
      insuranceid: null

    });
  }

  clearvalues() {
    this.searchDate = '';
    this.searchPatientId = '';
    this.searchOrderId = '';
    this.searchOrderDate = '';
    this.searchOrderType = '';
    this.searchtotalqty = '';
    this.searchstatus = '';
    this.dt.reset();
  }


  /**
   * Determines whether row select on on particular row select 
   * @param {string} event 
   */
  onRowSelect(event) {
    const PatientId = event;
    const data = {
      accessToken: '',
      patientId: PatientId
    };
    this.patient.loadPatient(data).subscribe(
      data => {
        try {
          this.data = data;
          this.app.patientId = 0;
          this.app.patientId = PatientId;
          this.app.isPatientSigned = true;
        }
        catch (error) {
          this.error.syntaxErrors(error);
        }
      });
  }


  /**
   * Doubles clickframesto 
   * @param {any} rowData  to get selected row data
   */
  DoubleClickframes(rowData) {

    this.onRowSelect(rowData.patient_id);
    this.app.patientId = rowData.patient_id;

    switch (rowData.ordertypeid) {
      case 7: {
        this.navigateToOrders(rowData, 'spectacle-lens', 7);
        break;
      }
      case 1: {
        this.navigateToOrders(rowData, 'frame', 1);
        break;
      }
      case 5: {
        this.navigateToOrders(rowData, 'contact-lens', 5);
        break;
      }
      case 4: {
        this.navigateToOrders(rowData, 'contact-lens', 4);
        break;
      }
      case 6: {
        this.navigateToOrders(rowData, 'other-details', 6);
        break;
      }
    }
  }


  /**
   * Navigates to orders
   * @param {string} rowData for getting selected row data
   * @param {string} routevalue for getting route  data
   * @param {string} ordertype for getting order type data
   */
  navigateToOrders(rowData, routevalue, ordertype) {
    this.router.navigateByUrl('/order/' + routevalue).then(success => {

      this.app.ordersearchvalueService = [];
      const data = {
        Id: rowData.id,
        OrderType: ordertype
      };
      this.app.ordersearchvalueService.push(data);
    });
  }

  /*  Funtion for the loading of the data of ordersearch here
  modified the logic for the not loaded when
   user is not not selected any data */
  ordersearch() {
    let dateValue;
    if (this.ordersearchForm.controls.entered_date.value == "") {
      dateValue = "";
    } else {
      dateValue = this.datePipe.transform(this.ordersearchForm.controls.entered_date.value, 'yyyy-MM-dd');
    }
    let filterPayLoad = {

      "filter": [

        {
          "field": "entered_date",
          "operator": "=",
          "value": dateValue
        },

        {
          "field": "patient_id",
          "operator": "LIKE",
          "value": "%" + this.ordersearchForm.controls.patient_id.value + "%"
        },
        {
          "field": "id",
          "operator": "LIKE",
          "value": "%" + this.ordersearchForm.controls.id.value + "%"
        },
        {
          "field": "insuranceid",
          "operator": "=",
          "value": this.ordersearchForm.controls.insuranceid.value
        }


      ], "sort": [
        {
          "field": "id",
          "order": "DESC"
        }
      ]
    }
    const resultData = filterPayLoad.filter.filter(p => p.value !== "" && p.value !== null && p.value !== undefined && p.value !== '%undefined%'
      && p.value !== "%  %" && p.value !== "%%" && p.value !== "null");
    let payLoad = {
      "filter": resultData,
      "sort": [
        {
          "field": "id",
          "order": "DESC"
        }
      ]
    }
    this.orderTypes = []
    this.app.getOrderBySearchFilter(payLoad).subscribe(data => {

      this.orderTypes = data['data']
      // for (let i = 0; i <= this.orderTypes.length - 1; i++) {
      //   this.orderTypes[i].ordertypeid = this.orderTypes[i].ordertypeid === 1 ?
      //   'Frames' : this.orderTypes[i].ordertypeid = this.orderTypes[i].ordertypeid === 2 ?
      //   'Spectacle Lens' :  this.orderTypes[i].ordertypeid = this.orderTypes[i].ordertypeid === 3 ?'Contact lens': 'Other';
      // }

    })
    // this.clearvalues();
    // this.orderTypes = [];
    // this.orderTypesCopy = [];
    // const fromdate = this.ordersearchForm.controls.orderDate.value;
    // let datedetail = '';
    // if (fromdate !== '' ||
    //   this.ordersearchForm.controls.patientId.value !== '' ||
    //   this.ordersearchForm.controls.orderId.value !== '') {
    //   // this.ordersearchForm.controls['orderDate'].patchValue("");
    //   // datedetail = '';
    //   if (fromdate === null || fromdate === undefined || fromdate === '') {
    //     datedetail = '';
    //     this.LoadOrdersData(this.ordersearchForm.controls.patientId.value,
    //       this.ordersearchForm.controls.orderId.value, datedetail);
    //   } else {
    //     datedetail = this.datePipe.transform(fromdate, 'yyyy-MM-dd');
    //     this.LoadOrdersData(this.ordersearchForm.controls.patientId.value,
    //       this.ordersearchForm.controls.orderId.value, datedetail);
    //   }
    // }
  }
  /**
   * Orders status
   * @returns order status data
   */
  orderStatus() {
    this.status = this.dropDown.status;
    if (this.utility.getObjectLength(this.status) === 0) {
      this.dropDown.loadOrderstatus().subscribe(
        data => {
          try {
            this.status = data;

          } catch (error) {
            this.error.syntaxErrors(error);
          }

        }
      );
    }
  }
  /**
   * Orders type details
   * @returns order type details
   */
  orderTypeDetails() {
    this.orderTypeData = this.dropDown.orderType;
    if (this.utility.getObjectLength(this.orderTypeData) === 0) {
      this.dropDown.loadOrderType().subscribe(
        data => {
          try {
            this.orderTypeData = data;

          } catch (error) {
            this.error.syntaxErrors(error);
          }
        }
      );
    }
  }

}
