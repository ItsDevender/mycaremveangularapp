// parent component
export * from './tab.component';
// child components

// Hard contact-lens components
export * from './hard-contact-lens/hard-contact-lens-main.component';
export * from './hard-contact-lens/hard-contact-lens.component';
// select contact-lens
export * from './select-contactlens-od/select-contactlens-od.component';
// soft contact-lens components
export * from './soft-contact-lens/soft-contact-lens-main.component';
export * from './soft-contact-lens/soft-contact-lens.component';
