import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { ErrorService } from '@app/core';
import { OrderService } from '@app/core/services/order/order.service';

@Component({
    selector: 'app-order-soft-contact-lens-main',
    templateUrl: 'soft-contact-lens-main.component.html'
})
export class OrderSoftContactLensMainComponent implements OnInit, OnDestroy {

    subscribeAlertPopUp: Subscription;
    constructor(private errorService: ErrorService, private orderService: OrderService) {
        this.errorService.msgs = [];
        this.subscribeAlertPopUp = orderService.OrderAlertPopup.subscribe(data => {
            if (data === 'save') {
                this.errorService.msgs.push({
                    severity: 'success',
                    summary: 'success Message', detail: 'Order Saved Successfully'
                });

            }
            if (data === 'update') {
                this.errorService.msgs.push({
                    severity: 'success',
                    summary: 'success Message', detail: 'Order Updated Successfully'
                });

            }
            if (data === 'required') {
                this.errorService.msgs.push({
                    severity: 'warn',
                    summary: 'Please enter the required fields', detail: 'Order Updated Successfully'
                });

            }
        });
    }
    ngOnInit(): void {

    }
    ngOnDestroy() {
        this.subscribeAlertPopUp.unsubscribe();
    }


}
