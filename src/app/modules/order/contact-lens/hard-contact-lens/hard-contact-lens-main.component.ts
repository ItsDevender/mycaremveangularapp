import { Component, OnInit } from '@angular/core';


@Component({
    selector: 'app-order-hard-contact-lens-main',
    templateUrl: 'hard-contact-lens-main.component.html'
})
export class OrderHardContactLensMainComponent implements OnInit {
    addPatientInfoSidebar;
    orderTypes: any[];
    table2: any[];
    selectContactlensSidebar: boolean;
    ngOnInit(): void {
        this.orderTypes = [
            { name: '92002 - New Patient - intermediate Exam', value: '1', price: '0.00' },
            { name: '92012 - Exsting Patient - intermediate Exam', value: '1', price: '345.00' },
            { name: '32014 - Exsting Patient - Comprehensive Exam', value: '1', price: '30.00' },
            { name: '92015 - Determination of Refractive state', value: '1', price: '300.00' },
            { name: '92083 - Visual Field Examination', value: '1', price: '500.00' },
        ];
        this.table2 = [
            {
                changes: 'Bausch & Lomb Biotrue Oneday For Pres', Eye: 'OD', patient: '6.00', total: '6.00',
                insurance: '0.00', itemtotal: '6.00', retail: '3.00', unitprice: '6.00'
            },
            {
                changes: 'Bausch & Lomb Biotrue Oneday For Pres', Eye: 'OS', patient: '6.00', total: '3.00',
                insurance: '6.00', itemtotal: '6.00', retail: '6.00', unitprice: '1.00'
            },
            {
                changes: 'Bausch & Lomb Biotrue Oneday For Pres', Eye: 'OD', patient: '6.00', total: '6.00',
                insurance: '10.00', itemtotal: '12.00', retail: '3.00', unitprice: '0.00'
            },
            {
                changes: 'Bausch & Lomb Biotrue Oneday For Pres', Eye: 'OS', patient: '', total: '6.00',
                insurance: '', itemtotal: '6.00', retail: '3.00', unitprice: '12.00'
            },
        ];
    }

}
