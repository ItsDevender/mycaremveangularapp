
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ContactlensService } from '@services/order/contactlens.service';
import { OrderService } from '@app/core/services/order/order.service';
import { Subscription } from 'rxjs';
import { ErrorService } from '@app/core';

@Component({
    selector: 'app-order-contact-lens-main',
    templateUrl: './tab.component.html'
})
export class OrderContactLensMainComponent implements OnInit, OnDestroy {
    contatclensalertpopup: Subscription;
    subscribeAlertPopUp: Subscription;
    hardcontactlensdetails: any;
    addPatientInfoSidebar;
    paymentDetails;
    orderTypes: any[];
    table2: any[];
    ShippingEnableStatus = false;
    SoftScreenVisible = true;
    HardScreenVisible = false;
    OtherScreenVisible = false;
    ShippingScreenVisible = false;
    hardcontactlens = false;
    subscribeShippingStatus: Subscription;
    constructor(private orderService: OrderService, private contactlensService: ContactlensService,
                private errorService: ErrorService) {
        this.subscribeShippingStatus = orderService.ShippingDetalesEnable$.subscribe(data => {
            if (data === true) {
                this.ShippingEnableStatus = true;
                this.ShippingScreenVisible = true;
                this.SoftScreenVisible = false;
                this.HardScreenVisible = false;
                this.OtherScreenVisible = false;
                this.orderService.ShippingTo.next(true);
            } else {
                this.ShippingEnableStatus = false;
                this.ShippingScreenVisible = false;
                this.SoftScreenVisible = true;
                this.HardScreenVisible = false;
                this.OtherScreenVisible = false;
            }



        });
        this.hardcontactlensdetails = this.contactlensService.Disablehardcontactcomponent$.subscribe(data => {
            this.HardScreenVisible = data;
            this.hardcontactlens = data;
            this.SoftScreenVisible = !data;
            this.OtherScreenVisible = false;
        });
        // this.contactlensService.errorHandle.msgs = [];
        this.contatclensalertpopup = this.orderService.ContactLensPopupAlertPopup$.subscribe(data => {
            if (data.messagetype === 'required') {
                this.errorService.displayError({ severity: 'warn', summary: 'Warning Message', detail: data.message });
                // this.msgs.push({ severity: 'warn', summary: 'Warning Message', detail: data.message });

                return false;
            }
        });

        this.subscribeAlertPopUp = orderService.OrderAlertPopup.subscribe(data => {
            if (data === 'save') {
                this.errorService.displayError({
                    severity: 'success',
                    summary: 'success Message', detail: 'Order Saved Successfully'
                });
                // this.msgs.push({ severity: 'success', summary: 'Success Message', detail: 'Order Saved Successfully' });

            }
            if (data === 'update') {
                this.errorService.displayError({
                    severity: 'success',
                    summary: 'success Message', detail: 'Order Saved Successfully'
                });
                // this.msgs.push({ severity: 'success', summary: 'Success Message', detail: 'Order Updated Successfully' });

            }
            if (data === 'update') {
                this.errorService.displayError({
                    severity: 'success',
                    summary: 'success Message', detail: 'Order Saved Successfully'
                });
                // this.msgs.push({ severity: 'success', summary: 'Success Message', detail: 'Order Updated Successfully' });

            }
            if (data === 'required') {
                this.errorService.displayError({
                    severity: 'warn',
                    summary: 'Warning Message', detail: 'Please enter the required fields'
                });
                // this.msgs.push({ severity: 'warn', summary: 'Warning Message', detail: 'Please enter the required fields' });

                return false;
            }


        });
    }
    ngOnDestroy() {
        this.subscribeShippingStatus.unsubscribe();
        this.hardcontactlensdetails.unsubscribe();
        this.subscribeAlertPopUp.unsubscribe();
        this.contatclensalertpopup.unsubscribe();
    }
    ngOnInit(): void {
        this.orderService.orderId = 'New Order';
        this.orderService.showCategory = false;
        this.orderService.orderSelectTypeobj.next(5);
        this.orderTypes = [
            { name: '92002 - New Patient - intermediate Exam', value: '1', price: '0.00' },
            { name: '92012 - Exsting Patient - intermediate Exam', value: '1', price: '345.00' },
            { name: '32014 - Exsting Patient - Comprehensive Exam', value: '1', price: '30.00' },
            { name: '92015 - Determination of Refractive state', value: '1', price: '300.00' },
            { name: '92083 - Visual Field Examination', value: '1', price: '500.00' },
        ];
        this.table2 = [
            {
                changes: 'Bausch & Lomb Biotrue Oneday For Pres', Eye: 'OD', patient: '6.00', total: '6.00',
                insurance: '0.00', itemtotal: '6.00', retail: '3.00', unitprice: '6.00'
            },
            {
                changes: 'Bausch & Lomb Biotrue Oneday For Pres', Eye: 'OS', patient: '6.00', total: '3.00',
                insurance: '6.00', itemtotal: '6.00', retail: '6.00', unitprice: '1.00'
            },
            {
                changes: 'Bausch & Lomb Biotrue Oneday For Pres', Eye: 'OD', patient: '6.00', total: '6.00',
                insurance: '10.00', itemtotal: '12.00', retail: '3.00', unitprice: '0.00'
            },
            {
                changes: 'Bausch & Lomb Biotrue Oneday For Pres', Eye: 'OS', patient: '', total: '6.00',
                insurance: '', itemtotal: '6.00', retail: '3.00', unitprice: '12.00'
            },
        ];
        this.orderService.showorderdiv.next(true);
        this.orderService.disableSavePayBtn = false;
        // this.orderService.orderStatusGlobal = '';
    }
    PageClick(event) {
        const data = event.index;
        if (data === 0) {
            //  this.orderService.orderScreenSavePath = "Soft Contact Lens"
            // this.SoftScreenVisible = true;
            // this.HardScreenVisible = false;
            this.OtherScreenVisible = false;

        }
        if (data === 1) {
            // this.orderService.orderScreenSavePath = "Hard Contact Lens"
            // this.SoftScreenVisible = false;
            this.HardScreenVisible = true;
            this.OtherScreenVisible = false;

        }
        if (data === 2) {
            //  this.orderService.orderScreenSavePath = "Other Details"
            // this.SoftScreenVisible = false;
            // this.HardScreenVisible = false;
            this.OtherScreenVisible = true;
        }
        if (data === 3) {
            // this.orderService.orderScreenSavePath = "Shipping Details"
            this.SoftScreenVisible = false;
            this.HardScreenVisible = false;
            this.OtherScreenVisible = false;
        }
    }
}
