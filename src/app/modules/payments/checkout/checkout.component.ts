import { Component, OnInit } from '@angular/core';
import { AppService } from '@app/core';

@Component({
    selector: 'app-checkout',
    templateUrl: 'checkout.component.html'
})

export class PaymentCheckoutComponent implements OnInit {
    constructor(public app: AppService) {

    }
    searchOrderSidebar = false;
    printReceiptSidebar = false;
    paymentLayoutSidebar = false;
    searchCheckoutSidebar = false;
    checkoutPaymentSidebar = false;
    managerDiscountSidebar = false;
    paymentAdjustmentSidebar = false;
    patientSearchSidebar = false;
    orderTypes: any[];
    ngOnInit(): void {
        this.orderTypes = [
            {
                charges: 'Related Orders', eye: 'OD', copay: '0', patient: '1252.25', patienttotal: '1252.25',
                patientbalance: '1252.25', insurance: '0', itemtotal: '1252.25', retail: '1145.00', qty: '2', tax: '107.25', tax2: '0'
            },
            {
                charges: 'Related Orders', eye: 'OD', copay: '0', patient: '1252.25', patienttotal: '1252.25', patientbalance: '1252.25',
                insurance: '0', itemtotal: '1252.25', retail: '1145.00', qty: '2', tax: '107.25', tax2: '0'
            },
            {
                charges: 'Related Orders', eye: 'OD', copay: '0', patient: '1252.25', patienttotal: '1252.25', patientbalance: '1252.25',
                insurance: '0', itemtotal: '1252.25', retail: '1145.00', qty: '2', tax: '107.25', tax2: '0'
            },
            {
                charges: 'Related Orders', eye: 'OD', copay: '0', patient: '1252.25', patienttotal: '1252.25', patientbalance: '1252.25',
                insurance: '0', itemtotal: '1252.25', retail: '1145.00', qty: '2', tax: '107.25', tax2: '0'
            },
            {
                charges: 'Related Orders', eye: 'OD', copay: '0', patient: '1252.25', patienttotal: '1252.25', patientbalance: '1252.25',
                insurance: '0', itemtotal: '1252.25', retail: '1145.00', qty: '2', tax: '107.25', tax2: '0'
            },
            {
                charges: 'Related Orders', eye: 'OD', copay: '0', patient: '1252.25', patienttotal: '1252.25', patientbalance: '1252.25',
                insurance: '0', itemtotal: '1252.25', retail: '1145.00', qty: '2', tax: '107.25', tax2: '0'
            },
            {
                charges: 'Related Orders', eye: 'OD', copay: '0', patient: '1252.25', patienttotal: '1252.25', patientbalance: '1252.25',
                insurance: '0', itemtotal: '1252.25', retail: '1145.00', qty: '2', tax: '107.25', tax2: '0'
            },
            {
                charges: 'Related Orders', eye: 'OD', copay: '0', patient: '1252.25', patienttotal: '1252.25', patientbalance: '1252.25',
                insurance: '0', itemtotal: '1252.25', retail: '1145.00', qty: '2', tax: '107.25', tax2: '0'
            },
            {
                charges: 'Related Orders', eye: 'OD', copay: '0', patient: '1252.25', patienttotal: '1252.25', patientbalance: '1252.25',
                insurance: '0', itemtotal: '1252.25', retail: '1145.00', qty: '2', tax: '107.25', tax2: '0'
            },
            {
                charges: 'Related Orders', eye: 'OD', copay: '0', patient: '1252.25', patienttotal: '1252.25', patientbalance: '1252.25',
                insurance: '0', itemtotal: '1252.25', retail: '1145.00', qty: '2', tax: '107.25', tax2: '0'
            },
            {
                charges: 'Related Orders', eye: 'OD', copay: '0', patient: '1252.25', patienttotal: '1252.25', patientbalance: '1252.25',
                insurance: '0', itemtotal: '1252.25', retail: '1145.00', qty: '2', tax: '107.25', tax2: '0'
            },
            {
                charges: 'Related Orders', eye: 'OD', copay: '0', patient: '1252.25', patienttotal: '1252.25', patientbalance: '1252.25',
                insurance: '0', itemtotal: '1252.25', retail: '1145.00', qty: '2', tax: '107.25', tax2: '0'
            },
            {
                charges: 'Related Orders', eye: 'OD', copay: '0', patient: '1252.25', patienttotal: '1252.25', patientbalance: '1252.25',
                insurance: '0', itemtotal: '1252.25', retail: '1145.00', qty: '2', tax: '107.25', tax2: '0'
            },
            {
                charges: 'Related Orders', eye: 'OD', copay: '0', patient: '1252.25', patienttotal: '1252.25', patientbalance: '1252.25',
                insurance: '0', itemtotal: '1252.25', retail: '1145.00', qty: '2', tax: '107.25', tax2: '0'
            },
            {
                charges: 'Related Orders', eye: 'OD', copay: '0', patient: '1252.25', patienttotal: '1252.25', patientbalance: '1252.25',
                insurance: '0', itemtotal: '1252.25', retail: '1145.00', qty: '2', tax: '107.25', tax2: '0'
            },
            {
                charges: 'Related Orders', eye: 'OD', copay: '0', patient: '1252.25', patienttotal: '1252.25', patientbalance: '1252.25',
                insurance: '0', itemtotal: '1252.25', retail: '1145.00', qty: '2', tax: '107.25', tax2: '0'
            },
        ];

    }

    paymentSearchOrder() {
        this.searchOrderSidebar = true;
    }
    searchCheckout() {
        this.searchCheckoutSidebar = true;
    }
    managerDiscount() {
        this.managerDiscountSidebar = true;
    }
    paymentPrintReceipt() {
        this.printReceiptSidebar = true;
    }
    paymentLayout() {
        this.paymentLayoutSidebar = true;
    }
    paymentAdjustment() {
        this.paymentAdjustmentSidebar = true;
    }
    checkoutPayment() {
        this.checkoutPaymentSidebar = true;
    }
    patientSearch() {
        this.patientSearchSidebar = true;
    }
}
