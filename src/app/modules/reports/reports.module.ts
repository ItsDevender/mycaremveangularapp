import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReportsRoutingModule } from './reports-routing.module';

// Components
import {
  SchedulerComponent,
  SchedulerPatientMonitorComponent,
  SchedulerFaceSheetComponent,
  SchedulerApptInfoComponent,
  SchedulerPtDocsComponent,
  SchedulerSxPlanningComponent,
  SchedulerRecallUrlComponent,
  SchedulerConsultLetterComponent,
  SchedulerReportComponent,
  SchedulerPatientCSVExportComponent,
  ComplianceComponent,
  ComplianceQRDAComponent,
  ComplianceCQMComponent,
  PracticeAnalyticComponent,
  CcdComponent,
  ApiComponent,
  ApiCallLogComponent,
  StateComponent,
  StateTNComponent,
  FinancialsComponent,
  FinancialsPreviousHCFAComponent,
  FinancialsEIDStatusComponent,
  FinancialsEIDPaymentComponent,
  FinancialsNewStatementComponent,
  FinancialsPreviousStatementComponent,
  OpticalComponent,
  RemindersDayApptComponent,
  RemindersRecallsComponent,
  RemindersListComponent,
  ClinicalComponent,
  CommonlyUsedComponent
} from '.';

// import {
//   
// } from './financials/financials.component';
import { SharedModule } from '@app/shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    ReportsRoutingModule,
    SharedModule
  ],
  declarations: [
    SchedulerComponent,
    SchedulerPatientMonitorComponent,
    SchedulerFaceSheetComponent,
    SchedulerApptInfoComponent,
    SchedulerPtDocsComponent,
    SchedulerSxPlanningComponent,
    SchedulerRecallUrlComponent,
    SchedulerConsultLetterComponent,
    SchedulerReportComponent,
    SchedulerPatientCSVExportComponent,
    ComplianceComponent,
    ComplianceQRDAComponent,
    ComplianceCQMComponent,
    PracticeAnalyticComponent,
    CcdComponent,
    ApiComponent,
    ApiCallLogComponent,
    StateComponent,
    StateTNComponent,
    OpticalComponent,
    RemindersDayApptComponent,
    RemindersRecallsComponent,
    RemindersListComponent,
    ClinicalComponent,
    FinancialsComponent,
    FinancialsPreviousHCFAComponent,
    FinancialsEIDStatusComponent,
    FinancialsEIDPaymentComponent,
    FinancialsNewStatementComponent,
    FinancialsPreviousStatementComponent,
    CommonlyUsedComponent
  ]
})
export class ReportsModule { }
