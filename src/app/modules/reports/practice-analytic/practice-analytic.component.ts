// Core Modules
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { IframesService } from '@app/shared/services/iframes.service';

// App Configuration Module

// Setting Up Component
@Component({
  selector: 'app-practice-analytic',
  templateUrl: './practice-analytic.component.html',
  styleUrls: ['./practice-analytic.component.css']
})
export class PracticeAnalyticComponent implements OnInit, OnDestroy {

  PracticeAnalyticUrl: any = '';
  SchedulerId: number;
  params: any;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: IframesService) {
    // this.router.routeReuseStrategy.shouldReuseRoute = () => false;
  }

  // Get Parameters on Intialize
  ngOnInit() {
    this.params = this.route
      .queryParams
      .subscribe(params => {
        // Defaults to 0 if no query param provided.
        this.SchedulerId = +params['sch_temp_id'] || 0;
        this.PracticeAnalyticUrl = this.service.routeUrl('PracticeAnalyticUrl', true, '?sch_temp_id=' + this.SchedulerId);
      });

  }

  // Unsubcribe Service
  ngOnDestroy() {
    this.params.unsubscribe();
  }

  onLoad() {
    this.service.resizeIframe();
    $('.loader-iframe').fadeOut();
  }

}
