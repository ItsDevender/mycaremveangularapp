import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PracticeAnalyticComponent } from './practice-analytic.component';

describe('PracticeAnalyticComponent', () => {
  let component: PracticeAnalyticComponent;
  let fixture: ComponentFixture<PracticeAnalyticComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PracticeAnalyticComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PracticeAnalyticComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
