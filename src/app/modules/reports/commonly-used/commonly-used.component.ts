import { Component, OnInit } from '@angular/core';
import { IframesService } from '@app/shared/services/iframes.service';
import { SafeUrlPipe } from '@app/shared/pipes/safe-url.pipe';
import { ActivatedRoute } from '@angular/router';
import { environment as env } from '@env/environment';

@Component({
  selector: 'app-commonly-used',
  templateUrl: './commonly-used.component.html',
  styleUrls: ['./commonly-used.component.css']
})
export class CommonlyUsedComponent implements OnInit {

  /**
   * Common reports url of commonly used component
   */
  commonReportsUrl: any = '';
  /**
   * Params  of commonly used component
   */
  params: any;
  /**
   * reports Url of commonly used component
   */
  reportsUrl: any;
  /**
   * Creates an instance of commonly used component.
   * @param activateRoute for activating routes
   * @param service for calling iframeservice methods
   * @param _SafeUrlPipe for handling url pipe
   */
  constructor(
    private activateRoute: ActivatedRoute,
    private service: IframesService, private _SafeUrlPipe: SafeUrlPipe) {
  }

  /**
   * on init
   * Get Parameters on Intialize
   */
  ngOnInit() {
    this.params = this.activateRoute.queryParams.subscribe(params => {
      this.reportsUrl = params['reports'];
      let url = env.ssrsurl + this.reportsUrl + '&rs:Command=Render';
      this.commonReportsUrl = this._SafeUrlPipe.safeUrl(url);
    });
  }

  /**
   * Determines whether load on
   * Resize the Iframe grid
   */
  onLoad() {
    this.service.resizeIframe();
    $('.loader-iframe').fadeOut();
  }

}
