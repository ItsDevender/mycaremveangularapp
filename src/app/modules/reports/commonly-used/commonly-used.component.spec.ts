import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CommonlyUsedComponent } from './commonly-used.component';

describe('CommonlyUsedComponent', () => {
  let component: CommonlyUsedComponent;
  let fixture: ComponentFixture<CommonlyUsedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CommonlyUsedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CommonlyUsedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
