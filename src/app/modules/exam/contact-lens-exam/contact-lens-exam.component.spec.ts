import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ContactLensExamComponent } from './contact-lens-exam.component';

describe('ContactLensExamComponent', () => {
  let component: ContactLensExamComponent;
  let fixture: ComponentFixture<ContactLensExamComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ContactLensExamComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactLensExamComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
