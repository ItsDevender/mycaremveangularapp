import { CommonModule } from '@angular/common';
import { ContactLensExamComponent } from './contact-lens-exam/contact-lens-exam.component';
import { ExamRoutingModule } from './exam-routing.module';

// components
import { CompleteExamComponent } from './complete-exam/complete-exam.component';
import { NgModule } from '@angular/core';
import { PatientInstructionsComponent } from './patient-instructions/patient-instructions.component';
import { ProceduresComponent } from './procedures/procedures.component';

@NgModule({
  imports: [
    CommonModule,
    ExamRoutingModule
  ],
  declarations: [
    CompleteExamComponent,
    ContactLensExamComponent,
    PatientInstructionsComponent,
    ProceduresComponent
  ]
})
export class ExamModule { }
