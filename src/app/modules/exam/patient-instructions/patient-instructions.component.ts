// Core Modules
import { Component, OnInit } from '@angular/core';
import { IframesService } from '@app/shared/services/iframes.service';




// Setting Up Component
@Component({
  selector: 'app-patient-instructions',
  templateUrl: './patient-instructions.component.html'
})
export class PatientInstructionsComponent implements OnInit {

  PatientInstructionsUrl: Object;

  constructor(private service: IframesService) {

     this.PatientInstructionsUrl = this.service.routeUrl('PatientInstructionsUrl', true);

  }

  ngOnInit() {
  }
  onLoad() {
    this.service.resizeIframe();
    $('.loader-iframe').fadeOut();
  }

}
