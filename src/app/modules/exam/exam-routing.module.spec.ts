import { ExamRoutingModule } from './exam-routing.module';

describe('ExamRoutingModule', () => {
  let examRoutingModule: ExamRoutingModule;

  beforeEach(() => {
    examRoutingModule = new ExamRoutingModule();
  });

  it('should create an instance', () => {
    expect(examRoutingModule).toBeTruthy();
  });
});
