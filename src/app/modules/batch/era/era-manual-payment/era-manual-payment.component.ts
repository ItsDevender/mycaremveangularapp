// Core Modules
import { Component, OnInit, AfterViewInit } from '@angular/core';
import { IframesService } from '@app/shared/services/iframes.service';



// Setting Up Component
@Component({
  selector: 'app-era-manual-payment',
  templateUrl: './era-manual-payment.component.html',
  styleUrls: ['./era-manual-payment.component.css']
})
export class EraManualPaymentComponent implements OnInit, AfterViewInit {

  PostPaymentsEra: any = '';

  constructor(private service: IframesService) {

    this.PostPaymentsEra = this.service.routeUrl('PostPaymentsEra', true);

  }

  ngOnInit() { }
  ngAfterViewInit() {
    this.service.resizeIframe();
  }

}
