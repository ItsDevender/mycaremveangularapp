import { Component, OnInit } from '@angular/core';
import { AppService } from '@app/core';


@Component({
    selector: 'app-order-status',
    templateUrl: 'order-status.component.html'
})
export class BatchOrderStatusComponent implements OnInit {
    constructor(public app: AppService) {

    }
    orderTypes: any[];
    pastOrderBy: any[];

    ngOnInit() { }
}


