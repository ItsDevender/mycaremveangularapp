import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LensRXCalculatorComponent } from './lens-rx-calculator';
import { QuickbooksSyncComponent } from './quickbooks-sync/quickbooks-sync.component';
import { PaymentsLedgerComponent } from './payments-ledger/payments-ledger.component';

// Components


const routes: Routes = [
    {
        path: '',
        children: [
            // {
            //     path: 'company',
            //     component: CompanyComponent,
            //     data: { 'title': 'Company SetUp' }
            // },
            {
                path: 'payment-ledgers',
                component: PaymentsLedgerComponent,
                data: { 'title': 'Accounting  Payment Ledger' }
            },
            {
                path: 'quickbooks-sync',
                component: QuickbooksSyncComponent,
                data: { 'title': 'Accounting  Quickbooks Sync' }
            },
            {
                path: 'lens-RX-calculator',
                component: LensRXCalculatorComponent,
                data: { 'title': 'Accounting  Lens RX Calculator' }

            }
        ]
    }
];

@NgModule({
    imports: [
        RouterModule.forChild(routes)
    ],
    exports: [RouterModule]
})
export class AccountingRoutingModule { }
