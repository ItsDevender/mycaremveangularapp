import { Component, OnInit } from '@angular/core';
import { AppService } from '@app/core';

@Component({
    selector: 'quickbooks-sync',
    templateUrl: 'quickbooks-sync.component.html'
})

export class QuickbooksSyncComponent implements OnInit {
    constructor(public app: AppService) {

    }
    orderTypes: any[];
    processTypes: any[];
    ngOnInit(): void {
        this.orderTypes = [
            { id: '1', name: '', active: '', status: '' },
            { id: '2', name: '', active: '', status: '' },
            { id: '3', name: '', active: '', status: '' },
            { id: '4', name: '', active: '', status: '' },
            { id: '5', name: '', active: '', status: '' },
            { id: '6', name: '', active: '', status: '' },
            { id: '7', name: '', active: '', status: '' },
            { id: '8', name: '', active: '', status: '' },
            { id: '9', name: '', active: '', status: '' },
            { id: '10', name: '', active: '', status: '' },

        ];
        this.processTypes = [
            { details: '', status: 'Completed' },
            { details: '', status: 'Completed' },
            { details: '', status: 'Completed' },
            { details: '', status: 'Completed' },
            { details: '', status: 'Completed' },
            { details: '', status: 'Completed' },
            { details: '', status: 'Completed' },
            { details: '', status: 'Completed' },
            { details: '', status: 'Completed' },
            { details: '', status: 'Completed' },

        ];

    }

}
