import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ClaimsRoutingModule } from './claims-routing.module';
import {
  ClaimSearchComponent,
  ClaimsComponent,
  SingleClaimComponent,
  ReferingProviderComponent,
  ChargesComponent,
  InteractionsComponent,
  PaperworkComponent,
  PatientComponent,
  ProviderComponent,
  ValidationsComponent
} from '.';
import { SharedModule } from '@app/shared';

@NgModule({
  declarations: [ClaimSearchComponent,
    ClaimsComponent,
    SingleClaimComponent,
    ReferingProviderComponent,
    ChargesComponent,
    InteractionsComponent,
    PaperworkComponent,
    PatientComponent,
    ProviderComponent,
    ReferingProviderComponent,
    ValidationsComponent
  ],
  imports: [
    CommonModule,
    ClaimsRoutingModule,
    SharedModule
  ]
})
export class ClaimsModule { }
