import { AppComponent } from './app.component';
import { LayoutComponent } from './layouts/layout.component';
import { AuthGuard } from '@guards/auth.guard';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CONTENT_ROUTES } from '@app/shared';

const routes: Routes = [
  {
    path: 'login',
    loadChildren: './modules/login/login.module#LoginModule'
  },
  {
    path: '',
    canActivate: [AuthGuard],
    component: LayoutComponent,
    children: CONTENT_ROUTES
  },


  // Fallback when no prior routes is matched
  { path: '**', redirectTo: '/login', pathMatch: 'full' }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
