export const environment = {
    production: true,
    prodHideLinks : false,
    imedicUrl: 'http://10.1.12.201/mycareemr/',
    apiEndPoint: 'http://10.1.12.201/microservices/public',
    apiEndPoint2: 'http://10.1.1.135/apigateway/api/v1',
    showAllErrors: false,
    ssrsurl: 'http://10.1.1.167/ReportServer_SQL01/Pages/ReportViewer.aspx?%2fMCM%2f'
};